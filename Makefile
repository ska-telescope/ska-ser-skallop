-include PrivateRules.mak

# This Makefile uses templates defined in the .make/ folder, which is a git submodule of
# https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile.

-include .make/base.mk
-include .make/oci.mk
-include .make/python.mk
-include .make/helm.mk
-include develop.mk

PYTHON_LINT_TARGET:=src/ tests/ resources/examples/
PYTHON_LINE_LENGTH = 100
DOCS_SPHINXBUILD:= poetry run python3 -msphinx

build-base-image:
	@docker login -u $(CI_REGISTRY_USER) -p $(CI_REGISTRY_PASSWORD) $(CI_REGISTRY)
    @docker buildx -t $(CI_REGISTRY)/ska-telescope/ska-ser-skallop/base:$(TAG) docker/
    @docker push $(CI_REGISTRY)/ska-telescope/ska-ser-skallop/base:$(TAG)
.PHONY: build-base-image