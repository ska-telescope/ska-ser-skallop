import logging
from typing import Callable, List, Set

import pytest
from assertpy import assert_that

from ska_ser_skallop.mvp_control.describing.mvp_conditions import Condition, Getter, Readiness
from ska_ser_skallop.mvp_control.describing.mvp_states import ListInspection

logger = logging.getLogger(__name__)


class Options:
    correct = ["ON", "ON", "ON"]
    wrong = ["OFF", "OFF", "OFF"]
    inconsistent1 = ["OFF", "OFF", "ON"]
    inconsistent2 = ["OFF", "OFF", "FAULT"]

    @classmethod
    def items(cls) -> List:
        attrs = [i for i in cls.__dict__ if i[:1] != "_"]
        return [getattr(cls, attr) for attr in attrs if attr != "items"]


@pytest.fixture(params=Options.items(), name="values")
def fxt_values(request) -> List[str]:
    values = request.param
    return values


@pytest.fixture(name="get_function")
def fxt_get_function() -> Callable[..., ListInspection]:
    def get_items(values: List):
        return ListInspection({f"item{i}": values[i] for i in range(len(values))})

    return get_items


@pytest.fixture(name="getter")
def fxt_getter(values: List[str], get_function: Callable[..., ListInspection]) -> Getter:
    return Getter(get_function, values)


def test_generate_condition(getter: Getter, values: List[str]):
    condition = Condition(getter, "ON")
    if values == Options.correct:
        assert condition
        assert_that(condition.state.name).is_equal_to("OK")
        return
    assert not condition
    if values == Options.wrong:
        assert_that(condition.state.name).is_equal_to("WRONG")
    else:
        assert_that(condition.state.name).is_equal_to("INCONSISTENT")


def test_readiness(values: List[str], get_function: Callable[..., ListInspection]):
    system_ready = Readiness()
    system_ready.expect("ALL items").state(get_function, values).to_be("ON")
    if values == Options.correct:
        assert system_ready
    else:
        assert not system_ready


class ConditionType:
    @property
    def health_of_on_items(self):
        """
        Returns the literal string "health_of_on_items"

        :return: the literal string "health_of_on_items"
        """
        return "health_of_on_items"

    @property
    def health_of_off_items(self):
        """
        Returns the literal string "health_of_off_items"

        :return: the literal string "health_of_off_items"
        """
        return "health_of_off_items"

    @property
    def health_of_failed_items(self):
        """
        Returns the literal string "health_of_failed_items"

        :return: the literal string "health_of_failed_items"
        """
        return "health_of_failed_items"

    @classmethod
    def as_labels(cls) -> Set:
        attrs = [i for i in cls.__dict__ if i[:1] != "_"]
        return {attr for attr in attrs if attr != "as_labels"}


@pytest.fixture(name="labels")
def fxt_labels():
    return ConditionType.as_labels()


def test_readiness_multiple_conditions(get_function: Callable[..., ListInspection], labels: Set):
    cond = ConditionType()
    system_ready = Readiness(labels)
    system_ready.expect("All ON items", cond.health_of_on_items).state(
        get_function, ["ON", "ON", "ON"]
    ).to_be("ON")
    system_ready.expect("ALL OFF items", cond.health_of_off_items).state(
        get_function, ["OFF", "OFF", "OFF"]
    ).to_be("OFF")
    system_ready.expect("ALL FAULTY items", cond.health_of_failed_items).state(
        get_function, ["FAULTY", "FAULTY", "FAULTY"]
    ).to_be("FAULTY")
    assert system_ready


def test_readiness_some_conditions_inconsistent(
    get_function: Callable[..., ListInspection], labels: Set
):
    cond = ConditionType()
    system_ready = Readiness(labels)
    system_ready.expect("All ON items", cond.health_of_on_items).state(
        get_function, ["ON", "ON", "ON"]
    ).to_be("ON")
    system_ready.expect("ALL OFF items", cond.health_of_off_items).state(
        get_function, ["OFF", "ON", "OFF"]
    ).to_be("OFF")
    system_ready.expect("ALL FAULTY items", cond.health_of_failed_items).state(
        get_function, ["FAULTY", "ON", "FAULTY"]
    ).to_be("FAULTY")
    logger.info(f"\n{system_ready.describe_why_not_ready()}")
    with pytest.raises(Exception):
        assert system_ready


def test_readiness_some_conditions_wrong(get_function: Callable[..., ListInspection], labels: Set):
    cond = ConditionType()
    system_ready = Readiness(labels)
    system_ready.expect("All ON items", cond.health_of_on_items).state(
        get_function, ["ON", "ON", "ON"]
    ).to_be("ON")
    system_ready.expect("ALL OFF items", cond.health_of_off_items).state(
        get_function, ["ON", "ON", "ON"]
    ).to_be("OFF")
    system_ready.expect("ALL FAULTY items", cond.health_of_failed_items).state(
        get_function, ["OFF", "OFF", "OFF"]
    ).to_be("FAULTY")
    logger.info(f"\n{system_ready.describe_why_not_ready()}")
    with pytest.raises(Exception):
        assert system_ready


def test_readiness_some_conditions_inconsistent2(
    get_function: Callable[..., ListInspection], labels
):
    cond = ConditionType()
    system_ready = Readiness(labels)
    system_ready.expect("All ON items", cond.health_of_on_items).state(
        get_function, ["ON", "ON", "ON"]
    ).to_be("ON")
    system_ready.expect("ALL OFF items", cond.health_of_off_items).state(
        get_function, ["FAULTY", "ON", "FAULTY"]
    ).to_be("OFF")
    system_ready.expect("ALL FAULTY items", cond.health_of_failed_items).state(
        get_function, ["OFF", "ON", "ON"]
    ).to_be("FAULTY")
    logger.info(f"\n{system_ready.describe_why_not_ready()}")
    with pytest.raises(Exception):
        assert system_ready
