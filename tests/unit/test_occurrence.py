"""Testing module that tests the basic functionality of Occurrences class"""

import logging

import pytest

from ska_ser_skallop.event_handling.base import EventDataInt
from ska_ser_skallop.event_handling.occurrences import Occurrences

logger = logging.getLogger(__name__)


@pytest.fixture(name="occurrences")
def fxt_occurrences() -> Occurrences:
    occurrences = Occurrences(["transit1", "transit2", "transit3"])
    occurrences.add_occurrence(EventDataInt("device1", "att", "transit1"))
    occurrences.add_occurrence(EventDataInt("device2", "att", "transit1"))
    occurrences.add_occurrence(EventDataInt("device3", "att", "transit1"))
    occurrences.add_occurrence(EventDataInt("device1", "att", "transit2"))
    occurrences.add_occurrence(EventDataInt("device2", "att", "transit2"))
    occurrences.add_occurrence(EventDataInt("device3", "att", "transit2"))
    occurrences.add_occurrence(EventDataInt("device1", "att", "transit3"))
    occurrences.add_occurrence(EventDataInt("device2", "att", "transit3"))
    occurrences.add_occurrence(EventDataInt("device3", "att", "transit3"))
    occurrences.set_subject_device("device1")
    return occurrences


def test_device_assertions(occurrences: Occurrences):
    occurrences.assert_that("device1").is_ahead_of_all_on_transit("transit1")
    occurrences.assert_that("device1").is_ahead_of_all_on_transit("transit2")
    occurrences.assert_that("device1").is_ahead_of_all_on_transit("transit3")
    with pytest.raises(AssertionError):
        occurrences.assert_that("device1").is_behind_all_on_transit("transit1")
    with pytest.raises(AssertionError):
        occurrences.assert_that("device1").is_behind_all_on_transit("transit2")
    with pytest.raises(AssertionError):
        occurrences.assert_that("device1").is_behind_all_on_transit("transit3")
    with pytest.raises(AssertionError):
        occurrences.assert_that("device2").is_behind_all_on_transit("transit1")
    with pytest.raises(AssertionError):
        occurrences.assert_that("device2").is_behind_all_on_transit("transit2")
    with pytest.raises(AssertionError):
        occurrences.assert_that("device2").is_behind_all_on_transit("transit3")
    with pytest.raises(AssertionError):
        occurrences.assert_that("device2").is_ahead_of_all_on_transit("transit1")
    with pytest.raises(AssertionError):
        occurrences.assert_that("device2").is_ahead_of_all_on_transit("transit2")
    with pytest.raises(AssertionError):
        occurrences.assert_that("device2").is_ahead_of_all_on_transit("transit3")
    occurrences.assert_that("device3").is_behind_all_on_transit("transit1")
    occurrences.assert_that("device3").is_behind_all_on_transit("transit2")
    occurrences.assert_that("device3").is_behind_all_on_transit("transit3")
    with pytest.raises(AssertionError):
        occurrences.assert_that("device3").is_ahead_of_all_on_transit("transit1")
    with pytest.raises(AssertionError):
        occurrences.assert_that("device3").is_ahead_of_all_on_transit("transit2")
    with pytest.raises(AssertionError):
        occurrences.assert_that("device3").is_ahead_of_all_on_transit("transit3")


def test_device_assertions_print(occurrences: Occurrences):
    result = occurrences.print_outcome_for("a_device")
    assert result == "outcome for a_device:\n"
