from typing import List, NamedTuple

import mock
import pytest
from assertpy import assert_that

from ska_ser_skallop.connectors import configuration as conf
from ska_ser_skallop.connectors.mockingfactory import MockingFactory
from ska_ser_skallop.mvp_control.configuration.types import Composition
from ska_ser_skallop.mvp_control.entry_points import configuration as entry_conf
from ska_ser_skallop.mvp_control.entry_points import types as conf_types
from ska_ser_skallop.mvp_control.entry_points.base import EntryPoint
from ska_ser_skallop.mvp_control.subarray.base import SBConfig
from ska_ser_skallop.mvp_control.subarray.compose import (
    EWhileComposing,
    I_can_compose_a_subarray,
    I_can_teardown_a_subarray,
    compose_subarray,
    start_subarray_composing,
    tear_down_subarray,
)
from ska_ser_skallop.mvp_fixtures.base import ExecSettings
from ska_ser_skallop.subscribing.message_board import MessageBoard


@pytest.fixture(name="test_factory")
def fxt_test_factory():
    testing_factory = MockingFactory()
    with conf.patch_factory(testing_factory, "test fixture"):
        yield testing_factory


@pytest.fixture(name="entrypoint_mock")
def fxt_entrypoint_mock() -> EntryPoint:
    with entry_conf.patch_entry_point(synched_impl=True) as entrypoint_mock:
        yield entrypoint_mock


class SubarrayArgs(NamedTuple):
    subarray_id: int
    receptors: List[int]
    composition: Composition
    sb_config: SBConfig
    settings: ExecSettings


@pytest.fixture(name="exec_settings")
def fxt_exec_settings() -> ExecSettings:
    settings = ExecSettings()
    settings.time_out = 0.1
    return settings


@pytest.fixture(name="subarray_args")
def fxt_subarray_args(exec_settings, tmp_path):
    return SubarrayArgs(
        subarray_id=1,
        receptors=[1, 2],
        composition=conf_types.CompositionByFile(tmp_path, conf_types.CompositionType.STANDARD),
        sb_config=SBConfig(sbid="sbid1"),
        settings=exec_settings,
    )


def test_compose_wait_timeout(test_factory: MockingFactory, subarray_args):
    """
    Test that the wait times out waiting for obsState attribute on sdp, csp and tm
    subarray devices
    """
    test_factory.set_producer_spy()
    test_factory.set_device_spy()
    with pytest.raises(EWhileComposing):
        compose_subarray(*subarray_args)


@mock.patch("ska_ser_skallop.mvp_control.event_waiting.wait.wait")
def test_compose_subarray(
    mocked_wait,
    test_factory: MockingFactory,
    entrypoint_mock: EntryPoint,
    subarray_args: SubarrayArgs,
):
    """Test the compose parameters"""
    test_factory.set_producer_spy()
    test_factory.set_device_spy()
    # Check that the wait is called
    compose_subarray(*subarray_args)  # type: ignore
    assert mocked_wait.called
    wait_call = mocked_wait.call_args.call_list()[0]
    assert wait_call[1]["live_logging"] == subarray_args.settings.log_enabled
    assert wait_call[0][1] == subarray_args.settings.time_out
    assert isinstance(wait_call[0][0], MessageBoard)
    # Check compose_subarray command for oet hidden behind entrypoint.
    compose_subarray(*subarray_args)  # type: ignore
    assert entrypoint_mock.compose_subarray.called
    compose_subarray_args = entrypoint_mock.compose_subarray.call_args[0]
    assert compose_subarray_args[0] == subarray_args.subarray_id
    assert compose_subarray_args[1] == subarray_args.receptors
    assert isinstance(compose_subarray_args[2], type(subarray_args.composition))


@mock.patch("ska_ser_skallop.mvp_control.subarray.compose.Job")
@mock.patch("ska_ser_skallop.mvp_control.event_waiting.wait.wait")
def test_start_subarray_composing(
    mocked_wait,
    mocked_job,
    test_factory: MockingFactory,
    entrypoint_mock: EntryPoint,
    subarray_args: SubarrayArgs,
):
    """Test the compose parameters"""
    test_factory.set_producer_spy()
    test_factory.set_device_spy()
    # Check that the wait is called
    start_subarray_composing(*subarray_args)  # type: ignore
    assert mocked_wait.called
    wait_call = mocked_wait.call_args.call_list()[0]
    assert wait_call[1]["live_logging"] == subarray_args.settings.log_enabled
    assert wait_call[0][1] == subarray_args.settings.time_out
    assert_that(wait_call[0][0]).is_instance_of(MessageBoard)
    # Check compose_subarray command for oet hidden behind entrypoint.

    start_subarray_composing(*subarray_args)  # type: ignore
    assert mocked_job.called
    job_args = mocked_job.call_args[0]
    assert job_args[0] == entrypoint_mock.compose_subarray
    assert job_args[1] == subarray_args.subarray_id
    assert job_args[2] == subarray_args.receptors
    assert isinstance(job_args[3], type(subarray_args.composition))


@mock.patch("ska_ser_skallop.mvp_control.event_waiting.wait.wait")
def test_tear_down_subarray(
    mocked_wait,
    test_factory: MockingFactory,
    entrypoint_mock: EntryPoint,
    subarray_args: SubarrayArgs,
):
    """Test the compose parameters"""
    test_factory.set_producer_spy()
    test_factory.set_device_spy()
    # Check that the wait is called
    tear_down_subarray(subarray_args.subarray_id, subarray_args.settings)
    assert mocked_wait.called
    wait_call = mocked_wait.call_args.call_list()[0]
    assert wait_call[1]["live_logging"] == subarray_args.settings.log_enabled
    assert wait_call[0][1] == subarray_args.settings.time_out
    assert isinstance(wait_call[0][0], MessageBoard)
    # Check compose_subarray command for oet hidden behind entrypoint.
    tear_down_subarray(subarray_args.subarray_id, subarray_args.settings)  # type: ignore
    assert entrypoint_mock.tear_down_subarray.called
    compose_subarray_args = entrypoint_mock.tear_down_subarray.call_args[0]
    assert compose_subarray_args[0] == subarray_args.subarray_id


@mock.patch("ska_ser_skallop.mvp_control.subarray.compose.assert_I_can_compose_a_subarray")
def test_can_compose_a_subarray(mock_assert_can_compose_subarray, subarray_args: SubarrayArgs):
    I_can_compose_a_subarray(subarray_args.subarray_id, subarray_args.receptors)
    assert mock_assert_can_compose_subarray.called


@mock.patch("ska_ser_skallop.mvp_control.subarray.compose.assert_I_can_tear_down_a_subarray")
def test_can_teardown_a_subarray(mock_assert_can_teardown_subarray, subarray_args: SubarrayArgs):
    I_can_teardown_a_subarray(subarray_args.subarray_id, subarray_args.receptors)
    assert mock_assert_can_teardown_subarray.called
