import pytest
from assertpy import assert_that

from ska_ser_skallop.utils.combinations import combine
from ska_ser_skallop.utils.singleton import Memo, Singleton
from ska_ser_skallop.utils.string_manipulation import trunc


def test_singleton():
    class SingletonUnderTest(metaclass=Singleton):
        def __init__(self) -> None:
            self.count = 0

        def inc(self):
            self.count += 1

    single1 = SingletonUnderTest()
    single1.inc()
    single1.inc()
    single2 = SingletonUnderTest()
    assert single1.count == single2.count


def test_combine():
    options = {}
    options["arg1"] = [1, 2]
    options["arg2"] = ["a", "b"]
    options["arg3"] = ["i", "ii"]
    options["arg4"] = ["-", "--"]
    result = combine(options)
    expected_result = {}
    expected_result["argvalues"] = [
        (1, "a", "i", "-"),
        (1, "a", "i", "--"),
        (1, "a", "ii", "-"),
        (1, "a", "ii", "--"),
        (1, "b", "i", "-"),
        (1, "b", "i", "--"),
        (1, "b", "ii", "-"),
        (1, "b", "ii", "--"),
        (2, "a", "i", "-"),
        (2, "a", "i", "--"),
        (2, "a", "ii", "-"),
        (2, "a", "ii", "--"),
        (2, "b", "i", "-"),
        (2, "b", "i", "--"),
        (2, "b", "ii", "-"),
        (2, "b", "ii", "--"),
    ]
    expected_result["argnames"] = ["arg1", "arg2", "arg3", "arg4"]

    assert_that(result).is_equal_to(expected_result)


def test_memo():
    assert_that(Memo(foo="bar").get("foo")).is_equal_to("bar")
    Memo().reset()
    Memo(foo="original")
    Memo(foo="updated")
    assert_that(Memo().get("foo")).is_equal_to("updated")
    Memo().reset()
    assert_that(Memo().get("foo")).is_none()
    Memo(key1="key1_val")
    Memo(key2="key2_val")
    assert_that(Memo().get("key2")).is_equal_to("key2_val")
    assert_that(Memo().get("key1")).is_equal_to("key1_val")


@pytest.mark.parametrize(
    ["marker", "from_last"],
    [
        ["/", True],
        ["@", True],
        ["trx", True],
        ["/", True],
        ["@", True],
        ["trx", True],
    ],
)
def test_trunc(marker, from_last):
    if from_last:
        expected = f"first{marker}second{marker}third"
        input = f"{expected}{marker}last"
    else:
        expected = "first"
        input = f"{expected}{marker}second{marker}third{marker}last"
    result = trunc(input, marker, from_last)
    assert_that(result).is_equal_to(expected)
