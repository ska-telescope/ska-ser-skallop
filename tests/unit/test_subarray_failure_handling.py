from unittest import mock

import pytest

from ska_ser_skallop.mvp_control.entry_points import configuration as entry_conf
from ska_ser_skallop.mvp_control.entry_points.base import EntryPoint
from ska_ser_skallop.mvp_control.subarray.failure_handling import DirtySubarraysHandler


@pytest.fixture(name="entrypoint_mock")
def fxt_entrypoint_mock() -> EntryPoint:
    with entry_conf.patch_entry_point(synched_impl=True) as entrypoint_mock:
        yield entrypoint_mock


@pytest.mark.usefixtures("entrypoint_mock")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.create_rectifier")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.Readiness")
def test_restart_faulty_subarrays_is_called_when_obsState_fault(mock_readiness, mock_rectifier):
    mock_readiness.get_not_ok_conditions().get().are_in_state.return_value = [
        "sa01",
        "sa02",
    ]
    dd_handler = DirtySubarraysHandler(mock_readiness)
    dd_handler.clear_any_devices_in_obsstate_fault()
    mock_rectifier.return_value.restart_faulty_subarrays.assert_called()


@pytest.mark.usefixtures("entrypoint_mock")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.create_rectifier")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.Readiness")
def test_restart_faulty_subarrays_is_not_called_when_obsState_fault(mock_readiness, mock_rectifier):
    mock_readiness.get_not_ok_conditions().get().are_in_state.return_value = []
    dd_handler = DirtySubarraysHandler(mock_readiness)
    dd_handler.clear_any_devices_in_obsstate_fault()
    mock_rectifier.return_value.restart_faulty_subarrays.assert_not_called()


@pytest.mark.usefixtures("entrypoint_mock")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.create_rectifier")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.Readiness")
def test_abort_and_restart_subarrays_is_called_for_stuck_subarray_devices(
    mock_readiness, mock_rectifier
):
    mock_readiness.get_not_ok_conditions().get().are_in_state.return_value = [
        "CONFIGURING",
        "SCANNING",
    ]
    dd_handler = DirtySubarraysHandler(mock_readiness)
    dd_handler.abort_and_restart_stuck_subarray_devices()
    mock_rectifier.return_value.abort_and_restart_subarrays.assert_called()


@pytest.mark.usefixtures("entrypoint_mock")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.create_rectifier")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.Readiness")
def test_abort_and_restart_subarrays_is_not_called_for_stuck_subarray_devices(
    mock_readiness, mock_rectifier
):
    mock_readiness.get_not_ok_conditions().get().are_in_state.return_value = []
    dd_handler = DirtySubarraysHandler(mock_readiness)
    dd_handler.abort_and_restart_stuck_subarray_devices()
    mock_rectifier.return_value.abort_and_restart_subarrays.assert_not_called()


@pytest.mark.usefixtures("entrypoint_mock")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.create_rectifier")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.Readiness")
def test_abort_and_reset_stuck_subarray_devices_is_called_for_stuck_subarray_devices(
    mock_readiness, mock_rectifier
):
    mock_readiness.get_not_ok_conditions().get().are_in_state.return_value = [
        "CONFIGURING",
        "SCANNING",
    ]
    dd_handler = DirtySubarraysHandler(mock_readiness)
    dd_handler.abort_and_reset_stuck_subarray_devices()
    mock_rectifier.return_value.abort_and_reset_subarrays.assert_called()


@pytest.mark.usefixtures("entrypoint_mock")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.create_rectifier")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.Readiness")
def test_abort_and_reset_stuck_subarray_devices_isnt_called_for_stuck_subarray_devices(
    mock_readiness, mock_rectifier
):
    mock_readiness.get_not_ok_conditions().get().are_in_state.return_value = []
    dd_handler = DirtySubarraysHandler(mock_readiness)
    dd_handler.abort_and_reset_stuck_subarray_devices()
    mock_rectifier.return_value.abort_and_reset_subarrays.assert_not_called()


@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.Readiness")
def test_tear_down_subarray_is_called_for_idle_subarray(mock_readiness, entrypoint_mock):
    sub_states = mock.MagicMock()
    sub_states.are_in_state.return_value = ["IDLE"]
    mock_readiness.get_not_ok_conditions().get().items.return_value = {
        "0": sub_states,
        "1": sub_states,
    }.items()
    mock_readiness.get_not_ok_conditions().get().value = ["IDLE"]
    dd_handler = DirtySubarraysHandler(mock_readiness)
    dd_handler.take_any_idle_subarrays_to_empty()
    entrypoint_mock.tear_down_subarray.assert_called()


@pytest.mark.usefixtures("entrypoint_mock")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.create_rectifier")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.Readiness")
def test_abort_and_restart_subarrays_is_called_for_non_idle_subarray(
    mock_readiness,
    mock_rectifier,
):
    sub_states = mock.MagicMock()
    sub_states.are_in_state.return_value = [None]
    mock_readiness.get_not_ok_conditions().get().items.return_value = {
        "0": sub_states,
        "1": sub_states,
    }.items()
    mock_readiness.get_not_ok_conditions().get().value = ["IDLE"]
    dd_handler = DirtySubarraysHandler(mock_readiness)
    dd_handler.take_any_idle_subarrays_to_empty()
    mock_rectifier.return_value.abort_and_restart_subarrays.assert_called()


@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.Readiness")
def test_tear_down_subarray_and_clear_config_are_called_for_ready_subarray(
    mock_readiness, entrypoint_mock
):
    sub_states = mock.MagicMock()
    sub_states.are_in_state.return_value = ["READY"]
    mock_readiness.get_not_ok_conditions().get().items.return_value = {
        "0": sub_states,
        "1": sub_states,
    }.items()
    mock_readiness.get_not_ok_conditions().get().value = ["READY"]
    dd_handler = DirtySubarraysHandler(mock_readiness)
    dd_handler.take_any_ready_subarrays_to_empty()
    entrypoint_mock.clear_configuration.assert_called()
    entrypoint_mock.tear_down_subarray.assert_called()


@pytest.mark.usefixtures("entrypoint_mock")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.create_rectifier")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.Readiness")
def test_abort_and_restart_subarrays_is_called_for_non_ready_subarray(
    mock_readiness, mock_rectifier
):
    sub_states = mock.MagicMock()
    sub_states.are_in_state.return_value = [None]
    mock_readiness.get_not_ok_conditions().get().items.return_value = {
        "0": sub_states,
        "1": sub_states,
    }.items()
    mock_readiness.get_not_ok_conditions().get().value = ["READY"]
    dd_handler = DirtySubarraysHandler(mock_readiness)
    dd_handler.take_any_ready_subarrays_to_empty()
    mock_rectifier.return_value.abort_and_restart_subarrays.assert_called()


@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.Readiness")
def test_clear_configuration_is_called_for_ready_subarray(mock_readiness, entrypoint_mock):
    sub_states = mock.MagicMock()
    sub_states.are_in_state.return_value = ["READY"]
    mock_readiness.get_not_ok_conditions().get().items.return_value = {
        "0": sub_states,
        "1": sub_states,
    }.items()
    mock_readiness.get_not_ok_conditions().get().value = ["READY"]
    dd_handler = DirtySubarraysHandler(mock_readiness)
    dd_handler.take_any_ready_subarrays_to_idle()
    entrypoint_mock.clear_configuration.assert_called()


@pytest.mark.usefixtures("entrypoint_mock")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.create_rectifier")
@mock.patch("ska_ser_skallop.mvp_control.subarray.failure_handling.Readiness")
def test_abort_and_reset_subarrays_is_called_for_non_ready_subarray(mock_readiness, mock_rectifier):
    sub_states = mock.MagicMock()
    sub_states.are_in_state.return_value = [None]
    mock_readiness.get_not_ok_conditions().get().items.return_value = {
        "0": sub_states,
        "1": sub_states,
    }.items()
    mock_readiness.get_not_ok_conditions().get().value = ["READY"]
    dd_handler = DirtySubarraysHandler(mock_readiness)
    dd_handler.take_any_ready_subarrays_to_idle()
    mock_rectifier.return_value.abort_and_reset_subarrays.assert_called()
