from typing import Any, Dict, NamedTuple

import pytest
from assertpy import assert_that
from mock import Mock

from ska_ser_skallop.connectors import configuration as conf
from ska_ser_skallop.connectors.mockingfactory import MockingFactory
from ska_ser_skallop.mvp_control.model import mvp_model


@pytest.fixture(name="mock_calling")
def fxt_mock_calling() -> Mock:
    return Mock()


class BaseModel(NamedTuple):
    dict_model: Dict[str, Any]
    name: str
    attribute_name: str
    attribute_value: str
    init_state: str


@pytest.fixture(name="base_model")
def fxt_base_model() -> BaseModel:
    return BaseModel({"state": "ON", "att1": "att1"}, "dummy", "att1", "att1", "ON")


@pytest.fixture(name="empty_device_mock")
def fxt_empty_device_mock(base_model: BaseModel) -> mvp_model.MockDeviceProxy:
    return mvp_model.MockDeviceProxy(base_model.name, base_model.dict_model)


def test_mock_device_spied(empty_device_mock: mvp_model.MockDeviceProxy, base_model: BaseModel):
    assert_that(empty_device_mock.spy.ping()).is_equal_to(1)
    assert_that(empty_device_mock.spy.command_inout("command")).is_equal_to(("command", None))
    assert_that(empty_device_mock.spy.State()).is_equal_to(base_model.init_state)
    assert_that(empty_device_mock.spy.read_attribute(base_model.attribute_name).value).is_equal_to(
        base_model.attribute_value
    )
    empty_device_mock.spy.ping.assert_called()
    empty_device_mock.spy.command_inout.assert_called()
    empty_device_mock.spy.State.assert_called()
    empty_device_mock.spy.read_attribute.assert_called()


class SubscriptionTester:
    def __init__(self, device_mock: mvp_model.MockDeviceProxy, mock_calling: Mock) -> None:
        self.device_mock = device_mock
        self._mock_calling = mock_calling
        self._base_calls = mock_calling.call_count

    def assert_subscription_event_pushed(self):
        if self._mock_calling.call_count == self._base_calls:
            raise AssertionError("Subscription Event Not pushed")


@pytest.fixture(name="mock_subscription")
def fxt_mock_subscription(
    empty_device_mock: mvp_model.MockDeviceProxy,
    base_model: BaseModel,
    mock_calling: Mock,
):
    def handler(*args, **kwargs):
        mock_calling()

    sub_id = empty_device_mock.subscribe_event(base_model.attribute_name, "dummy", handler)
    yield SubscriptionTester(empty_device_mock, mock_calling)
    empty_device_mock.unsubscribe_event(sub_id)


def test_push_event_from_mock_device(
    mock_subscription: SubscriptionTester,
    base_model: BaseModel,
):
    mock_subscription.device_mock.set_attribute(base_model.attribute_name, "dummy value")
    mock_subscription.assert_subscription_event_pushed()


@pytest.fixture(name="factory")
def fxt_factory() -> MockingFactory:
    factory = MockingFactory()
    conf.set_factory(factory, provided_by="test_testing factory fixture")
    return factory


def test_inject_into_factory(
    factory: MockingFactory,
    empty_device_mock: mvp_model.MockDeviceProxy,
    base_model: BaseModel,
):
    empty_device_mock.inject_into_factory((factory))
    device = conf.get_device_proxy(base_model.name)
    assert_that(device.name()).is_equal_to(empty_device_mock.name())
    pass
