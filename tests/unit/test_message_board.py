"""Testing module that tests messageboard and producers"""

import datetime
from collections import deque
from queue import Queue
from typing import Tuple, Union

import mock
import pytest
from assertpy.assertpy import assert_that

from ska_ser_skallop.subscribing import base, subscription
from ska_ser_skallop.subscribing.message_board import (
    EventItem,
    EventTimedOut,
    MessageBoard,
    Subscription,
)
from ska_ser_skallop.subscribing.producers import BufferedSubscriber, Producer

LIB = "ska_ser_skallop.subscribing.message_board"
# workaround to fix unused Queue problem in linting
_ = Queue


class Spec:
    device_name = "dummy_device"
    attr = "dummy_attr"
    multiplicity = range(1, 4)
    multiple_attr_names = [f"dummy_attr{index}" for index in multiplicity]
    subscription_id = 0
    dummy_event = "dummy_event"


class MockProducer(Producer):
    def __init__(self) -> None:
        self.event_item_mock = mock.Mock(EventItem)
        self.event_item_mock.describe.return_value = (
            "dummy",
            "dummy",
            "dummy",
            "dummy",
        )
        self.event_item_mock.get_date_lodged.return_value = datetime.datetime.now()
        self.board = None
        self.buffer_board = None
        super().__init__(name="mock_producer")

    def subscribe_by_callback(self, board: "Queue[EventItem]"):
        self.board = board

    def subscribe_buffer(self):
        self.buffer_board = deque()

    def poll(self):
        return [self.buffer_board.pop()]

    def push_poll(self, event: base.EventDataInt):
        self.event_item_mock.event = event
        self.buffer_board.append(self.event_item_mock)

    def push(self, event: base.EventDataInt):
        self.event_item_mock.event = event
        self.board.put_nowait(self.event_item_mock)


class Context:
    def __init__(
        self,
        message_board: MessageBoard,
        subscription_cls: mock.Mock,
        mock_handler: mock.Mock,
        producer: MockProducer,
    ) -> None:
        self.message_board = message_board
        self.producer = producer
        self.subscription_cls = subscription_cls
        self.patch_mocked_subscription()
        self.mock_handler = mock_handler
        self.polling = False
        self.new_subscription: Union[subscription.Subscription, None] = None

    def patch_mocked_subscription(self):
        self.subscription = self.subscription_cls.return_value
        self.subscription.subscribe_by_callback.side_effect = self.producer.subscribe_by_callback
        self.subscription.subscribe_buffer.side_effect = self.producer.subscribe_buffer
        self.subscription.poll = self.producer.poll
        self.subscription.describe.return_value = (
            Spec.device_name,
            Spec.attr,
            Spec.subscription_id,
        )

    # apis
    def assert_new_subscription_called(self):
        self.subscription_cls.assert_called()

    def assert_subscribe_by_call_back_called(self):
        method: mock.Mock = self.subscription.subscribe_by_callback
        method.assert_called_with(self.message_board.board)

    def assert_subscribe_by_buffer_called(self):
        method: mock.Mock = self.subscription.subscribe_buffer
        method.assert_called_with()

    def assert_unsubscribed_called(self):
        method: mock.Mock = self.subscription.unsubscribe
        method.assert_called_with()


@pytest.fixture(name="mock_producer")
def fxt_mock_producer():
    producer = MockProducer()
    return producer


@pytest.fixture(name="handler")
def fxt_handler():
    handler_cls = mock.Mock(base.MessageHandlerBase)
    return handler_cls()


@pytest.fixture(name="message_board_context")
def fxt_message_board_context(mock_producer, handler):
    with mock.patch(f"{LIB}.Subscription") as mock_subscription_cls:
        message_board = MessageBoard()
        yield Context(message_board, mock_subscription_cls, handler, mock_producer)


@pytest.mark.parametrize("polling", [False, True])
def test_add_subscription(message_board_context, polling):
    # given a messageboard
    context: Context = message_board_context
    brd: MessageBoard = context.message_board
    prod: MockProducer = context.producer
    handler = context.mock_handler
    # when I add a new subscription
    new_subscription = brd.add_subscription(prod, Spec.attr, handler, polling)
    # then I expect a new subscription to have been created
    context.assert_new_subscription_called()
    expected_subscription = message_board_context.subscription
    assert expected_subscription in brd.subscriptions
    assert_that(expected_subscription).is_equal_to(new_subscription)
    if not polling:
        # I expect a subscribe by callback to have been done
        context.assert_subscribe_by_call_back_called()
    else:
        # I expect a subscribe to buffer
        context.assert_subscribe_by_buffer_called()


@pytest.fixture(params=["with_polling", "without_polling"], name="running_context")
def fxt_running_context(message_board_context, request) -> Context:
    context: Context = message_board_context
    prod = context.producer
    brd: MessageBoard = context.message_board
    handler = context.mock_handler
    if request.param == "with_polling":
        new_subscription = brd.add_subscription(prod, Spec.attr, handler, polling=True)
        context.polling = True
        new_subscription.polled = True
    else:
        new_subscription = brd.add_subscription(prod, Spec.attr, handler, polling=False)
        new_subscription.polled = False
    context.new_subscription = new_subscription
    return context


def test_unsubscribe(running_context):
    # given a running subscription with a messageboard
    context: Context = running_context
    brd: MessageBoard = context.message_board
    # when I unsubscribe
    assert context.new_subscription is not None
    brd.remove_subscription(context.new_subscription)
    # I expect the subscription to be removed from the device
    context.assert_unsubscribed_called()
    # I expect no more subscriptions to be on the messageboard
    assert_that(brd.subscriptions).is_length(0)


def test_remove_all_subscriptions(running_context):
    # given a running subscription with a messageboard
    context: Context = running_context
    brd: MessageBoard = context.message_board
    # when I unsubscribe
    brd.remove_all_subscriptions()
    # I expect the subscription to be removed from the device
    context.assert_unsubscribed_called()
    # I expect no more subscriptions to be on the messageboard
    assert_that(brd.subscriptions).is_length(0)


@pytest.fixture(name="dummy_event")
def fxt_dummy_event():
    return mock.Mock(name=Spec.dummy_event)


@pytest.fixture(name="dummy_event_pushed")
def fxt_dummy_event_pushed(running_context, mock_producer, dummy_event):
    producer: MockProducer = mock_producer
    context: Context = running_context
    if context.polling:
        producer.push_poll(dummy_event)
    else:
        producer.push(dummy_event)
    return dummy_event


def test_get_items(running_context, dummy_event_pushed):
    # given a running subscription with a messageboard
    context: Context = running_context
    brd: MessageBoard = context.message_board
    # given a  new message has been pushed
    dummy_event: base.EventDataInt = dummy_event_pushed
    # when I call get items on the messageboard
    item = next(brd.get_items(0))
    # I expect the item to contain the dummy event
    assert_that(item.event).is_equal_to(dummy_event)


# the rest of the tests integrates with direct dependendencies:
# subscription, EventsPusher and Handler
@pytest.fixture(name="producer")
def fxt_producer():
    return Producer(Spec.device_name)


@pytest.fixture(name="attr")
def fxt_attr():
    return Spec.attr


@pytest.mark.parametrize("polling", [False, True])
def test_integrated_subscribe(polling, producer, attr, handler):
    # given a producer that generates events on dummy attribute
    # and a handler that can handle the events
    # and a message board for managing subscriptions to that producer
    brd = MessageBoard()
    # when I subscribe to that producer on that attribute for events
    sbscr: Subscription = brd.add_subscription(producer, attr, handler, polling)
    # then I expect a new subscription to have been created
    assert sbscr in brd.subscriptions
    assert_that(sbscr.producer).is_equal_to(producer)
    assert_that(sbscr.attr).is_equal_to(attr)
    assert_that(sbscr.handler).is_equal_to(handler)
    # and I expect a subscription to have been generated on the producer
    result = producer.describe_subscription(sbscr.id)
    if polling:
        assert_that(result["attr"]).is_equal_to(attr)
        assert_that(result["subscriber"]).is_instance_of(BufferedSubscriber)
    else:
        subscriber = sbscr.eventsPusher
        assert_that(result).is_equal_to({"attr": attr, "subscriber": subscriber})


@pytest.fixture(params=["polling", "no polling"], name="running_subscription")
def fxt_running_subscription(request, producer, attr, handler):
    brd = MessageBoard()
    if request.param == "polling":
        sbscr = brd.add_subscription(producer, attr, handler, True)
        return brd, sbscr, True
    sbscr = brd.add_subscription(producer, attr, handler, False)
    return brd, sbscr, False


def test_integrated_unsubscribe(running_subscription: Tuple[MessageBoard, Subscription, bool]):
    # given a running subscription on a  message board
    brd, sbscr, _ = running_subscription

    # when  I unsubscribe that subscription
    brd.remove_subscription(sbscr)
    # I expect the subscription t to have been removed from the messageboard
    assert sbscr not in brd.subscriptions
    # I expect the subsription to have been placed in the archive
    assert sbscr in brd.archived_subscriptions


@pytest.fixture(name="unsubscribed_subscription")
def fxt_unsubscribed_subscription(running_subscription: Tuple[MessageBoard, Subscription, bool]):
    brd, sbscr, _ = running_subscription
    brd.remove_subscription(sbscr)
    return brd, sbscr, _


def test_unsubscribed_events(unsubscribed_subscription, producer, attr, dummy_event):
    # given an unsubscribed subscription
    brd, __, _ = unsubscribed_subscription
    # when I attempt to push and event
    producer.push_event(attr, dummy_event)
    # The event shall not be on the messageboard
    with pytest.raises(StopIteration):
        next(brd.get_items())


def test_get_event_from_integrated_subscription(
    running_subscription: Tuple[MessageBoard, Subscription, bool],
    producer,
    attr,
    dummy_event,
    handler,
):
    # given a running subscription on a  message board
    brd, sbscr, _ = running_subscription
    # when I push a new event on that subscription
    producer.push_event(attr, dummy_event)
    # then I expect that event to be available as part of an event item on the
    # messageboard
    event_item: EventItem = next(brd.get_items())
    assert_that(event_item.event).is_equal_to(dummy_event)
    # I expect that event item to also contain the handler and subscriptiom
    assert_that(event_item.handler).is_equal_to(handler)
    assert_that(event_item.subscription).is_equal_to(sbscr)


@pytest.fixture(
    params=[" suppress timeouts", " dont suppress timeouts"],
    name="suppress_timeouts",
)
def fxt_suppress_timeouts(handler, request):
    suppress_timeouts = bool(request.param == " suppress timeouts")
    handler.suppress_timeout.return_value = suppress_timeouts
    return suppress_timeouts


def test_time_out_from_integrated_subscription(running_subscription, suppress_timeouts):
    # given a running subscription on a  message board
    brd, __, _ = running_subscription
    # given that no events have been pushed on the producer
    # ...
    # when I try to get an event from the board within 0 timeout
    # ...
    # I expect the subscription to stop
    # if suppressed timeouts have been set on the handler for that subscription
    if suppress_timeouts:
        with pytest.raises(StopIteration):
            next(brd.get_items())
    # else I expect an timeout exception to be raised
    else:
        with pytest.raises(EventTimedOut):
            next(brd.get_items())
    # I expect an timeout exception to be raised
    # ...
    # I expect all subscriptions to be removed
    assert_that(brd.subscriptions).is_empty()


@pytest.fixture(name="attrs")
def fxt_attrs():
    return [f"{Spec.attr}{index}" for index in Spec.multiplicity]


@pytest.fixture(params=["partial", "complete"], name="multiple_running_subscriptions")
def fxt_multiple_running_subscriptions(request, producer, attrs, handler):
    brd = MessageBoard()
    subscriptions = []
    for attr in attrs:
        sbscr = brd.add_subscription(producer, attr, handler, False)
        brd.log("message logged")
        subscriptions.append(sbscr)
    if request.param == "partial":
        # for which half of them have  been unsubscribed
        def half_length(the_list: list) -> int:
            return int(len(the_list) / 2)

        for sbscr in subscriptions[: half_length(subscriptions)]:
            brd.remove_subscription(sbscr)
    return brd, subscriptions


def test_print_remaining_subscriptions(multiple_running_subscriptions):
    # given a set of subscriptions on a set of attrs
    brd, _ = multiple_running_subscriptions
    # when I print the remaining subscriptions
    result = brd.print_remaining_subscriptions()
    # I expect all subscriptions to be described
    assert_that(result).is_instance_of(str)


def test_replay_subscription(running_subscription):
    # given a running subscription on a  message board
    brd, sbscr, _ = running_subscription
    # when I replay the subscription
    result = brd.replay_subscription(sbscr)
    # I expect it to be replayed as a string
    assert_that(result).is_instance_of(str)


def test_replay_remaining_subscriptions(multiple_running_subscriptions):
    # given a set of subscriptions on a set of attrs
    brd, _ = multiple_running_subscriptions
    # when I print the remaining subscriptions
    result = brd.replay_subscriptions()
    # I expect all subscriptions to be described
    assert_that(result).is_instance_of(str)


def test_replay_self(multiple_running_subscriptions):
    # given a set of subscriptions on a set of attrs
    brd, _ = multiple_running_subscriptions
    # when I print the remaining subscriptions
    result = brd.replay_self()
    # I expect all subscriptions to be described
    assert_that(result).is_instance_of(str)


def test_play_log_book(multiple_running_subscriptions):
    # given a set of subscriptions on a set of attrs
    brd, _ = multiple_running_subscriptions
    # when I print the remaining subscriptions
    result = brd.play_log_book()
    # I expect all subscriptions to be described
    assert_that(result).is_instance_of(str)
