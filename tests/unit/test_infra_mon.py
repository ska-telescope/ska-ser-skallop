import json
from functools import partial
from typing import (
    Callable,
    Dict,
    Iterable,
    List,
    Literal,
    NamedTuple,
    Tuple,
    TypedDict,
    TypeVar,
    Union,
    cast,
)

import mock
import pytest
from assertpy import assert_that
from pipe import chain, select, where

from ska_ser_skallop.connectors.configuration import MockingFactory, patch_factory_for_testing
from ska_ser_skallop.mvp_control.base import AbstractDeviceProxy, AbstractDevicesQuery
from ska_ser_skallop.mvp_control.infra_mon import (
    base,
    configuration,
    deviceservers_crd,
    helm,
    tango_infra,
)
from ska_ser_skallop.utils import piping

ChartsDataHealth = Literal["READY", "ERROR", "PENDING", "UNKNOWN"]

T = TypeVar("T")


class TestEnv(TypedDict):
    charts_health: ChartsDataHealth
    test_chart: Union["Chartsdata", None]


class Mock_K8_API(NamedTuple):
    corev1_api: mock.Mock
    apps1_api: mock.Mock
    customv1_api: mock.Mock


class ConnectorsMock(NamedTuple):
    device_mock: AbstractDeviceProxy
    device_query_mock: AbstractDevicesQuery
    factory: MockingFactory


class BaseItem:
    def __init__(self, name: str, reference: list["BaseItem"] = []) -> None:
        self.name = name
        self._reference = reference

    def __str__(self):
        return self.name

    def items(self):
        return {
            self.name: {key: val for ref in self._reference for key, val in ref.items()}
        }.items()

    def as_dict(self):
        return dict(self.items())

    def as_json(self):
        return json.dumps(self.as_dict())


class Metadata:
    def __init__(self, name: str, labels: Dict) -> None:
        self.labels = labels
        self.name = name


class Selector:
    def __init__(self, match_labels: dict) -> None:
        self.match_labels = match_labels


class SetStatus:
    def __init__(
        self,
        spec: "SetSpec",
        phase: Literal["Running", "Pending", "Failed", "Unknown"] = "Running",
        ready_replicas: Union[int, None] = None,
    ) -> None:
        self.spec = spec
        if not ready_replicas:
            ready_replicas = spec.replicas
        self.ready_replicas = ready_replicas

    def update_status(self, expected_health: "ChartsDataHealth"):
        if expected_health == "READY":
            self.ready_replicas = self.spec.replicas
        elif expected_health == "PENDING":
            self.ready_replicas = self.spec.replicas - 1
        elif expected_health == "ERROR":
            self.ready_replicas = self.spec.replicas - 1
        else:
            self.ready_replicas = self.spec.replicas - 1


class PodStatus:
    def __init__(self) -> None:
        self.phase = "Running"

    def update_status(self, expected_health: "ChartsDataHealth"):
        if expected_health == "READY":
            self.phase = "Running"
        elif expected_health == "PENDING":
            self.phase = "Pending"
        elif expected_health == "ERROR":
            self.phase = "Failed"
        else:
            self.phase = "Unknown"


class SetSpec:
    def __init__(
        self,
        labels: dict,
        replicas: int,
    ) -> None:
        self.selector = Selector(labels)
        self.replicas = replicas


class ServiceSpec:
    def __init__(
        self,
        labels: dict,
    ) -> None:
        self.selector = Selector(labels)


class MockPodSet(mock.Mock):
    def __init__(self, name: str, labels: dict, replicas: int = 2) -> None:
        super().__init__(base.AbstractResource)
        replicas = 2 if replicas < 2 else replicas
        self.spec = SetSpec(labels, replicas)
        self.metadata = Metadata(name, labels)
        self.status = SetStatus(self.spec)
        self.name = name

    def set_corresponding_status(self, expected_health: "ChartsDataHealth"):
        self.status.update_status(expected_health)


class MockDeviceserverCRD(mock.Mock):
    def __init__(
        self,
        name: str,
        labels: dict,
        index: int,
        device_mapper: tango_infra.DeviceMapper,
        replicas: int = 2,
    ) -> None:
        super().__init__(base.AbstractResource)
        config = {
            "servers": {
                f"test-srv-{index}": {
                    "01": {
                        f"TestClass{index}": {
                            f"test-{index}/device-{index}/01": {
                                "properties": {
                                    "Property1": ["property"],
                                    "Property2": ["1000"],
                                }
                            }
                        }
                    }
                }
            }
        }
        self.crd = deviceservers_crd.DeviceServerCRD(
            apiVersion="tango.tango-controls.org/v1",
            kind="kind",
            metadata={
                "labels": labels,
                "name": name,
            },
            spec={
                "config": json.dumps(config),
            },
            status={
                "replicas": replicas,
            },
        )
        self.device: tango_infra.Device = tango_infra.parse_dsconfig(
            json.loads(self.crd.spec.config)["servers"], device_mapper
        )[0]

    def model(self) -> str:
        return self.crd.model_dump()


class MockService(mock.Mock):
    def __init__(self, name: str, labels: dict) -> None:
        super().__init__(base.AbstractResource)
        self.spec = ServiceSpec(labels)
        self.metadata = Metadata(name, labels)


class MockPod(mock.Mock):
    def __init__(
        self,
        name: str,
        labels: Dict,
        pod_set: Union[None, MockPodSet],
        device: Union[None, str] = None,
    ) -> None:
        super().__init__(base.AbstractResource)
        self.spec = mock.Mock()
        self.metadata = Metadata(name, labels)
        self.status = PodStatus()
        self.device = device
        self.pod_set = pod_set
        self.name = name

    def set_corresponding_status(self, expected_health: "ChartsDataHealth"):
        self.status.update_status(expected_health)

    @property
    def devices(self) -> List[str]:
        if self.device:
            return [self.device]
        return []


class MockConfigMap(mock.Mock):
    def __init__(self, name: str, labels: Dict, data: Dict) -> None:
        super().__init__(base.AbstractResource)
        self.metadata = Metadata(name, labels)
        self.data = data


class ClusterMocking:
    def __init__(self, api: Mock_K8_API, env: TestEnv = None) -> None:
        self.name = "ClusterMocking"
        self._api = api
        self._env = env
        self.expected_pods: Dict[str, mock.Mock] = {}
        self.mock_pods: List[mock.Mock] = []
        self.expected_deployments: Dict[str, mock.Mock] = {}
        self.mock_deployments: List[mock.Mock] = []
        self.expected_statefulsets: Dict[str, mock.Mock] = {}
        self.mock_statefulsets: List[mock.Mock] = []
        self.expected_configmaps: Dict[str, mock.Mock] = {}
        self.mock_configmaps: List[mock.Mock] = []
        self.expected_services: Dict[str, mock.Mock] = {}
        self.mock_services: List[mock.Mock] = []
        self.expected_deviceservers: Dict[str, mock.Mock] = {}
        self.mock_deviceservers_crd: List[MockDeviceserverCRD] = []

    @property
    def pods(self) -> Dict[str, base.V1Pod]:
        return self.mock_pods

    @staticmethod
    def _as_dict(iterable: Iterable[T]) -> Dict[str, T]:
        return dict(iterable | select(lambda item: (item.metadata.name, item)))

    def set_mock_pods(
        self,
        app: str,
        nr_of_pods_or_injected_ones: Union[int, Iterable[mock.Mock]] = 3,
    ):
        if isinstance(nr_of_pods_or_injected_ones, int):
            nr_of_pods = nr_of_pods_or_injected_ones
            mock_pods = [self._set_up_a_mock_pod(app, i) for i in range(nr_of_pods)]
        else:
            mock_pods = nr_of_pods_or_injected_ones
        self.mock_pods.extend(mock_pods)
        self._api.corev1_api.list_namespaced_pod.return_value.items = self.mock_pods
        self.expected_pods = self._as_dict(self.mock_pods)

    def set_mock_deployments(
        self, app: str, nr_of_deps_or_injected_ones: Union[int, Iterable[mock.Mock]] = 3
    ):
        if isinstance(nr_of_deps_or_injected_ones, int):
            nr_of_deps = nr_of_deps_or_injected_ones
            mock_deployments = [self._set_up_a_mock_pod_set(app, i) for i in range(nr_of_deps)]
        else:
            mock_deployments = nr_of_deps_or_injected_ones
        self.mock_deployments.extend(mock_deployments)
        self._api.apps1_api.list_namespaced_deployment.return_value.items = self.mock_deployments
        self.expected_deployments = self._as_dict(self.mock_deployments)

    def set_mock_statefulsets(
        self, app: str, nr_of_ss_or_injected_ones: Union[int, Iterable[mock.Mock]] = 3
    ):
        if isinstance(nr_of_ss_or_injected_ones, int):
            nr_of_ss = nr_of_ss_or_injected_ones
            mock_statefulsets = [self._set_up_a_mock_pod_set(app, i) for i in range(nr_of_ss)]
        else:
            mock_statefulsets = nr_of_ss_or_injected_ones
        self.mock_statefulsets.extend(mock_statefulsets)
        self._api.apps1_api.list_namespaced_stateful_set.return_value.items = self.mock_statefulsets
        self.expected_statefulsets = self._as_dict(self.mock_statefulsets)

    def set_mock_services(
        self, app: str, nr_of_ser_or_injected_ones: Union[int, Iterable[mock.Mock]] = 3
    ):
        if isinstance(nr_of_ser_or_injected_ones, int):
            nr_of_ser = nr_of_ser_or_injected_ones
            mock_services = [self._set_up_a_mock_service(app, i) for i in range(nr_of_ser)]
        else:
            mock_services = nr_of_ser_or_injected_ones
        self.mock_services.extend(mock_services)
        self._api.corev1_api.list_namespaced_service.return_value.items = self.mock_services
        self.expected_services = self._as_dict(self.mock_services)

    def set_mock_configmaps(
        self, app: str, nr_of_cm_or_injected_ones: Union[int, Iterable[mock.Mock]] = 3
    ):
        if isinstance(nr_of_cm_or_injected_ones, int):
            nr_of_cm = nr_of_cm_or_injected_ones
            mock_configmaps = [self._set_up_a_mock_configmap(app, i) for i in range(nr_of_cm)]
        else:
            mock_configmaps = nr_of_cm_or_injected_ones
        self.mock_configmaps.extend(mock_configmaps)
        self._api.corev1_api.list_namespaced_config_map.return_value.items = self.mock_configmaps
        self.expected_configmaps = self._as_dict(self.mock_configmaps)

    def set_mock_deviceservers_crds(
        self,
        app: str,
        nr_of_deviceservers_or_injected_ones: Union[int, Iterable[MockDeviceserverCRD]] = 3,
    ):
        if isinstance(nr_of_deviceservers_or_injected_ones, int):
            nr_of_ds = nr_of_deviceservers_or_injected_ones
            mock_deviceservers_crd: List[MockDeviceserverCRD] = [
                self._set_up_a_mock_deviceserver_crd(app, i) for i in range(nr_of_ds)
            ]
        else:
            mock_deviceservers_crd = nr_of_deviceservers_or_injected_ones

        self.mock_deviceservers_crd.extend(mock_deviceservers_crd)
        as_dicts = [m.model() for m in self.mock_deviceservers_crd]
        self._api.customv1_api.list_namespaced_custom_object.return_value = {
            "apiVersion": "tango.tango-controls.org/v1",
            "kind": "DeviceServerList",
            "items": as_dicts,
        }
        self.expected_deviceservers = {m["metadata"]["name"]: m for m in as_dicts}

    def _set_up_a_mock_pod(self, app: str, item_index: int):
        name = f"mock_pod{item_index}"
        labels = {"app": app, "item_index": f"{item_index}", "subsystem": app}
        return MockPod(name, labels, None)

    def _set_up_a_mock_pod_set(self, app: str, item_index: int):
        name = f"mock_pod{item_index}"
        labels = {"app": app, "item_index": f"{item_index}", "subsystem": app}
        return MockPodSet(name, labels)

    def _set_up_a_mock_service(self, app: str, item_index: int):
        name = f"mock_pod{item_index}"
        labels = {"app": app, "item_index": f"{item_index}", "subsystem": app}
        return MockService(name, labels)

    def _set_up_a_mock_configmap(self, app: str, item_index: int):
        name = f"mock_pod{item_index}"
        labels = {"app": app, "item_index": f"{item_index}", "subsystem": app}
        return MockPodSet(name, labels)

    def _set_up_a_mock_deviceserver_crd(self, app: str, item_index: int) -> MockDeviceserverCRD:
        name = f"mock_deviceserver{item_index}"
        labels = {"app": app, "item_index": f"{item_index}", "subsystem": app}
        return MockDeviceserverCRD(name, labels, item_index, self)


class Chartsdata:
    def __init__(
        self,
        name: str,
        version: str,
        parent: str,
        cluster_mock: ClusterMocking,
        dependencies: List["Chartsdata"] = [],
        health: ChartsDataHealth = "READY",
        has_devices=False,
    ) -> None:
        self.name = name
        self.version = version
        self.parent = parent
        self._cluster_mock = cluster_mock
        self.health: ChartsDataHealth = health
        self.has_devices = has_devices
        self.dependencies = dependencies
        self.data = ""
        self.mock_configmaps = []
        self.mock_deviceservers_crd: List[MockDeviceserverCRD] = []
        self.mock_statefulsets = []
        self.mock_deployments = []
        self.mock_pods = []
        self.mock_services = []
        self._lookup_table: Dict[str, Chartsdata] = {}

    @property
    def devices(self) -> List[str]:
        ds: List[str] = []
        if self.dependencies:
            ds = list(
                self.dependencies
                | where(lambda x: x.has_devices)
                | select(lambda x: x.devices)
                | chain
            )
        if self.mock_deviceservers_crd:
            ds.extend([v.device.name for v in self.mock_deviceservers_crd])
        ds = list(set(ds))
        ds.sort()
        return ds

    def get_device_chart(self, device_name: str) -> Union["Chartsdata", None]:
        if self._lookup_table:
            return self._lookup_table.get(device_name)
        if not self.dependencies:
            return None
        self._lookup_table: Dict[str, Chartsdata] = {}
        for dep in self.dependencies:
            if not dep.has_devices:
                continue
            m: MockDeviceserverCRD = None
            for m in dep.mock_deviceservers_crd:
                self._lookup_table[m.device.name] = dep
        return self._lookup_table.get(device_name)

    def get_device_pod(self, device_name: str) -> Union[None, MockPod]:
        if self.has_devices:
            lookup_table = dict(
                self.mock_pods
                | select(
                    lambda x: list(
                        zip(
                            cast(MockPod, x).devices,
                            [x for _ in range(len(x.devices))],
                        )
                    )
                )
                | chain
                | select(lambda x: (cast(MockPod, x[0]), x[1]))
            )
            return lookup_table.get(device_name)
        return None

    def populate_with_mock_items(
        self,
        nr_of_devices: int = 2,
    ):
        if len(self.dependencies) > 0:
            self.mock_configmaps = [self._set_up_main_chart_info_configmap()]
            self._cluster_mock.set_mock_configmaps(self.name, self.mock_configmaps)
        self.set_up_mock_configmaps(nr_of_devices)
        self.set_up_mock_deployments(nr_of_devices)
        self.set_up_mock_statefulset(nr_of_devices)
        self.set_up_mock_deviceservers(nr_of_devices)
        self.set_up_mock_pods(nr_of_devices)
        self.set_up_mock_services(nr_of_devices)
        for d in self.dependencies:
            d.populate_with_mock_items(nr_of_devices=nr_of_devices)

    def _generate_new_mock_item(
        self,
        generator: Callable[[int], mock.Mock],
        nr_of_items: int,
    ) -> List[mock.Mock]:
        return [generator(i) for i in range(nr_of_items)]

    def _set_up_main_chart_info_configmap(self):
        name = f"chartinfo-{self.name}"
        dependencies = [{"name": d.name, "version": d.version} for d in self.dependencies]
        json_data = json.dumps(
            {
                "name": self.name,
                "version": self.version,
                "release": self.name,
                "dependencies": dependencies,
            }
        )
        data = {"chartinfo.json": json_data}
        labels = {"app": self.name}
        return MockConfigMap(name, labels, data)

    def set_up_mock_configmaps(self, nr_of_items: int = 2):
        self.mock_configmaps.extend(
            self._generate_new_mock_item(self._set_up_a_mock_configmap, nr_of_items)
        )
        self._cluster_mock.set_mock_configmaps(self.name, self.mock_configmaps)

    def set_up_mock_deviceservers(self, nr_of_items: int = 2):
        self.mock_deviceservers_crd = self._generate_new_mock_item(
            self._set_up_a_mock_deviceserver_crd, nr_of_items
        )
        self._cluster_mock.set_mock_deviceservers_crds(self.name, self.mock_deviceservers_crd)

    def set_up_mock_pods(self, nr_of_items: int = 2):
        self.mock_pods = self._generate_new_mock_item(self._set_up_a_mock_pod, nr_of_items)
        self._cluster_mock.set_mock_pods(self.name, self.mock_pods)

    def set_up_mock_deployments(self, nr_of_items: int = 2):
        self.mock_deployments = self._generate_new_mock_item(self._set_up_a_deployment, nr_of_items)
        self._cluster_mock.set_mock_deployments(self.name, self.mock_deployments)

    def set_up_mock_statefulset(self, nr_of_items: int = 2):
        self.mock_statefulsets = self._generate_new_mock_item(
            self._set_up_a_statefulset, nr_of_items
        )
        self._cluster_mock.set_mock_statefulsets(self.name, self.mock_statefulsets)

    def set_up_mock_services(self, nr_of_items: int = 2):
        self.mock_services = self._generate_new_mock_item(self._set_up_a_mock_service, nr_of_items)
        self._cluster_mock.set_mock_services(self.name, self.mock_services)

    @staticmethod
    def _generate_mock_metadata(name: str, labels: dict) -> mock.Mock:
        mock_metadata = mock.Mock(base.AbstractMetadata)
        mock_metadata.name = name
        mock_metadata.labels = labels
        return mock_metadata

    @staticmethod
    def _generate_mock_resource(metadata: mock.Mock) -> mock.Mock:
        mock_resource = mock.Mock(base.AbstractResource)
        mock_resource.metadata = metadata
        return mock_resource

    def _get_labels(self, index: int) -> Dict[str, str]:
        subsystem = helm.default_chart_subsystem_mapping().get(self.name, self.name)
        return {"app": self.parent, "item_index": f"{index}", "subsystem": subsystem}

    def _set_up_a_mock_deviceserver_crd(self, index: int) -> MockDeviceserverCRD:
        name = f"mock_deviceserver_{self.name}-{index}"
        labels = self._get_labels(index)
        return MockDeviceserverCRD(name, labels, index, self._cluster_mock)

    def _set_up_a_mock_pod(self, index: int):
        name = f"mock_pod_{self.name}-{index}"
        labels = self._get_labels(index)
        device = None
        if len(self.mock_deviceservers_crd) > index:
            device = self.mock_deviceservers_crd[index].device.name
        if len(self.mock_statefulsets) > index:
            pod_set = self.mock_statefulsets[index]
        elif len(self.mock_deployments) > index:
            pod_set = self.mock_deployments[index]
        else:
            pod_set = None
        mock_pod = MockPod(name, labels, pod_set, device)
        mock_pod.set_corresponding_status(self.health)
        self.mock_pods.append(mock_pod)
        return mock_pod

    def _set_up_a_deployment(self, index: int):
        name = f"mock_deployment_{self.name}-{index}"
        labels = self._get_labels(index)
        mock_pod_set = MockPodSet(name, labels)
        mock_pod_set.set_corresponding_status(self.health)
        self.mock_deployments.append(mock_pod_set)
        return mock_pod_set

    def _set_up_a_statefulset(self, index: int):
        name = f"mock_statefulset_{self.name}-{index}"
        labels = self._get_labels(index)
        mock_pod_set = MockPodSet(name, labels)
        mock_pod_set.set_corresponding_status(self.health)
        self.mock_statefulsets.append(mock_pod_set)
        return mock_pod_set

    def _set_up_a_mock_service(self, index: int):
        name = f"mock_pod_{self.name}-{index}"
        labels = self._get_labels(index)
        service = MockService(name, labels)
        self.mock_services.append(service)
        return service

    def _set_up_a_mock_configmap(self, index: int):
        labels = self._get_labels(index)
        data = {"dummy.json": ""}
        name = f"mock_pod_{self.name}-{index}"
        configmap = MockConfigMap(name, labels, data)
        self.mock_configmaps.append(configmap)
        return configmap

    def __str__(self) -> str:
        dependency_str = ""
        if deps := self.dependencies:
            deps = "\n".join(
                [
                    f"{{ name: '{dep.name}' , version: '{dep.version}' }},"
                    for dep in self.dependencies
                ]
            )
            dependency_str = f"dependencies: [\n{deps}\n" "]\n"
        return (
            "const CHARTINFO = {\n"
            f"name: '{self.name}',\n"
            f"version: '{self.version}',\n"
            f"{dependency_str}]\n"
            "}\n"
        )

    @staticmethod
    def _set_item_per_chart(item: "Chartsdata", index: int = 1) -> Tuple[str, Dict]:
        labels = (
            {"app": item.name, "component": "configurator"}
            if item.has_devices
            else {"app": item.name}
        )
        return (
            f"mock_item_{index}_for_chart_{item.name}",
            labels,
        )

    def get_resources_for_chart(
        self,
        chart_name: str,
        resource_type: Literal["configmaps", "deployments", "services", "statefulsets", "pods"],
    ):
        if chart_name == self.name:
            resources = {
                "configmaps": self.mock_configmaps,
                "deployments": self.mock_deployments,
                "services": self.mock_services,
                "statefulsets": self.mock_statefulsets,
                "pods": self.mock_pods,
            }[resource_type]
            return dict(
                resources
                | where(lambda item: item.metadata.labels.get("subsystem") == chart_name)
                | select(lambda item: (item.metadata.name, item))
            )
        for d in self.dependencies:
            result = d.get_resources_for_chart(chart_name, resource_type)
            if result:
                return result
        return None


class TestChart(NamedTuple):
    sut_chart: helm.Chart
    input_chart: Chartsdata


def get_mock_command_inout(test_env: TestEnv):
    def mock_command_inout(
        test_env: TestEnv,
        _: str,
        device_name: str,
    ):
        allocation_str = ""
        if chart := test_env["test_chart"]:
            if device_chart := chart.get_device_chart(device_name):
                if device_pod := device_chart.get_device_pod(device_name):
                    if device_pod_set := device_pod.pod_set:
                        allocation_str = f"{device_pod.name}.{device_pod_set.name}"
        return [None, [None, None, None, None, allocation_str]]

    return partial(mock_command_inout, test_env)


def get_devices_query(test_env: TestEnv):
    def mock_devices_query(test_env: TestEnv, device_list: List[str], _: str, __=None):
        state = "ON"
        health_state = test_env["charts_health"]
        if health_state == "ERROR":
            state = "FAULT"
        elif health_state == "PENDING":
            state = "INIT"
        return dict(device_list | select(lambda x: (x, state)))

    return partial(mock_devices_query, test_env)


# fixtures
@pytest.fixture(name="mock_connectors")
def fxt_mock_connectors(test_env: TestEnv):
    with patch_factory_for_testing() as mock_factory:
        device_mock = mock_factory.set_device_spy()
        device_mock.command_inout.side_effect = get_mock_command_inout(test_env)
        device_query_mock = mock_factory.set_devices_query()
        device_query_mock.query.side_effect = get_devices_query(test_env)
        yield ConnectorsMock(device_mock, device_query_mock, mock_factory)


@pytest.fixture(name="mock_k8_api")
def fxt_mock_k8_api(mock_connectors):
    root = "ska_ser_skallop.mvp_control.infra_mon.infra.{}"
    with mock.patch(root.format("config")):
        with mock.patch(root.format("client")) as mod:
            corev1_api = mod.CoreV1Api.return_value
            apps1_api = mod.AppsV1Api.return_value
            customv1_api = mod.CustomObjectsApi.return_value
            yield Mock_K8_API(corev1_api, apps1_api, customv1_api)


@pytest.fixture(name="test_env")
def fxt_test_env() -> TestEnv:
    return TestEnv(charts_health="READY", test_chart=None)


@pytest.fixture(name="cluster_mocking")
def fxt_cluster_mocking(mock_k8_api, test_env):
    return ClusterMocking(mock_k8_api, test_env)


@pytest.fixture(name="mock_pods")
def fxt_mock_pods(cluster_mocking: ClusterMocking):
    cluster_mocking.set_mock_pods("dummy")
    return cluster_mocking.expected_pods


@pytest.fixture(name="mock_deployments")
def fxt_mock_deployments(cluster_mocking: ClusterMocking):
    cluster_mocking.set_mock_deployments("dummy")
    return cluster_mocking.expected_deployments


@pytest.fixture(name="mock_services")
def fxt_mock_services(cluster_mocking: ClusterMocking):
    cluster_mocking.set_mock_services("dummy")
    return cluster_mocking.expected_services


@pytest.fixture(name="mock_configmaps")
def fxt_mock_configmaps(cluster_mocking: ClusterMocking):
    cluster_mocking.set_mock_configmaps("dummy")
    return cluster_mocking.expected_configmaps


@pytest.fixture(name="mock_statefulsets")
def fxt_mock_statefulsets(cluster_mocking: ClusterMocking):
    cluster_mocking.set_mock_statefulsets("dummy")
    return cluster_mocking.expected_statefulsets


@pytest.fixture(name="cluster")
def fxt_cluster(cluster_mocking) -> configuration.ClusterWithDevices:
    return configuration.get_mvp_cluster()


@pytest.fixture(name="cluster_with_charts")
def fxt_cluster_with_charts() -> configuration.ClusterWithDevices:
    return configuration.get_mvp_cluster()


@pytest.fixture(name="charts_test_data")
def fxt_charts_test_data(test_env: TestEnv, cluster_mocking: ClusterMocking):
    charts_data = Chartsdata(
        "ska-low",
        "0.8.2",
        "ska-low",
        cluster_mocking,
        [
            Chartsdata(
                "ska-tango-base",
                "0.3.5",
                "ska-low",
                cluster_mocking,
                health=test_env["charts_health"],
            ),
            Chartsdata(
                "ska-log-consumer",
                "0.1.6",
                "ska-low",
                cluster_mocking,
                health=test_env["charts_health"],
            ),
            Chartsdata(
                "ska-tango-util",
                "0.3.5",
                "ska-low",
                cluster_mocking,
                health=test_env["charts_health"],
            ),
            Chartsdata(
                "ska-taranta",
                "1.0.29",
                "ska-low",
                cluster_mocking,
                health=test_env["charts_health"],
            ),
            Chartsdata(
                "ska-sdp",
                "0.8.1",
                "ska-low",
                cluster_mocking,
                has_devices=True,
                health=test_env["charts_health"],
            ),
            Chartsdata(
                "ska-low-mccs",
                "0.8.6",
                "ska-low",
                cluster_mocking,
                has_devices=True,
                health=test_env["charts_health"],
            ),
            Chartsdata(
                "ska-landingpage",
                "0.8.2",
                "ska-low",
                cluster_mocking,
                health=test_env["charts_health"],
            ),
            Chartsdata(
                "ska-csp-lmc-low",
                "0.1.4",
                "ska-low",
                cluster_mocking,
                has_devices=True,
                health=test_env["charts_health"],
            ),
            Chartsdata(
                "ska-low-cbf",
                "0.3.5",
                "ska-low",
                cluster_mocking,
                has_devices=True,
                health=test_env["charts_health"],
            ),
        ],
        test_env["charts_health"],
    )
    test_env["test_chart"] = charts_data
    return charts_data


@pytest.fixture(name="setup_chartinfo")
def fxt_setup_chartinfo(charts_test_data: Chartsdata):
    charts_test_data.populate_with_mock_items()


@pytest.fixture(
    name="setup_current_chart_health",
    params=["READY", "ERROR", "PENDING", "UNKNOWN"],
)
def fxt_setup_chart_health(request, test_env: TestEnv):
    charts_health = cast(ChartsDataHealth, request.param)
    test_env["charts_health"] = charts_health
    return charts_health


@pytest.fixture(name="current_chart_health")
def fxt_current_chart_health(setup_current_chart_health, charts_test_data):
    return setup_current_chart_health


@pytest.fixture(name="test_chart")
def fxt_test_chart(
    setup_chartinfo,
    charts_test_data: Chartsdata,
    cluster: configuration.ClusterWithDevices,
):
    sut_chart = cast(piping.Item, (cluster.sub_charts | piping.first_element)).value
    input_chart = charts_test_data
    return TestChart(sut_chart, input_chart)


# tests


@pytest.mark.parametrize("app", [None, "dummy", "non_existant_dummy"])
def test_get_pods(app, cluster: configuration.ClusterWithDevices, mock_pods: dict):
    if app:
        pods = cluster.get_pods(app=app)
    else:
        pods = cluster.get_pods()
    if app is None:
        assert_that(pods).is_equal_to(mock_pods)
    elif app == "dummy":
        assert_that(pods).is_equal_to(mock_pods)
    else:
        assert_that(pods).is_empty()


@pytest.mark.parametrize("app", [None, "dummy", "non_existant_dummy"])
def test_get_deployments(app, cluster: configuration.ClusterWithDevices, mock_deployments: dict):
    if app:
        pods = cluster.get_deployments(app=app)
    else:
        pods = cluster.get_deployments()
    if app is None:
        assert_that(pods).is_equal_to(mock_deployments)
    elif app == "dummy":
        assert_that(pods).is_equal_to(mock_deployments)
    else:
        assert_that(pods).is_empty()


@pytest.mark.parametrize("app", [None, "dummy", "non_existant_dummy"])
def test_get_services(app, cluster: configuration.ClusterWithDevices, mock_services: dict):
    if app:
        pods = cluster.get_services(app=app)
    else:
        pods = cluster.get_services()
    if app is None:
        assert_that(pods).is_equal_to(mock_services)
    elif app == "dummy":
        assert_that(pods).is_equal_to(mock_services)
    else:
        assert_that(pods).is_empty()


@pytest.mark.parametrize("app", [None, "dummy", "non_existant_dummy"])
def test_get_configmaps(app, cluster: configuration.ClusterWithDevices, mock_configmaps: dict):
    if app:
        pods = cluster.get_configmaps(app=app)
    else:
        pods = cluster.get_configmaps()
    if app is None:
        assert_that(pods).is_equal_to(mock_configmaps)
    elif app == "dummy":
        assert_that(pods).is_equal_to(mock_configmaps)
    else:
        assert_that(pods).is_empty()


@pytest.mark.parametrize("app", [None, "dummy", "non_existant_dummy"])
def test_get_statefulsets(
    app,
    mock_statefulsets: dict,
    cluster: configuration.ClusterWithDevices,
):
    if app:
        actual_ss = cluster.get_statefulsets(app=app)
    else:
        actual_ss = cluster.get_statefulsets()
    if app is None:
        assert_that(actual_ss).is_equal_to(mock_statefulsets)
    elif app == "dummy":
        assert_that(actual_ss).is_equal_to(mock_statefulsets)
    else:
        assert_that(actual_ss).is_empty()


@pytest.mark.usefixtures("setup_chartinfo")
def test_cluster_get_main_chart(
    cluster: configuration.ClusterWithDevices, charts_test_data: Chartsdata
):
    main_chart = cluster.chart
    assert main_chart
    assert_that((main_chart.name, main_chart.version)).is_equal_to(
        (charts_test_data.name, charts_test_data.version)
    )


@pytest.mark.usefixtures("setup_chartinfo")
def test_cluster_get_sub_charts(
    cluster: configuration.ClusterWithDevices, charts_test_data: Chartsdata
):
    sub_charts = cluster.sub_charts
    sub_chart_names = list(sub_charts.keys())
    expected_sub_chart_names = [item.name for item in charts_test_data.dependencies]
    assert_that(sub_chart_names).is_equal_to(expected_sub_chart_names)


@pytest.mark.usefixtures("setup_chartinfo")
@pytest.mark.parametrize(
    "resource_type",
    ["configmaps", "deployments", "statefulsets", "services", "pods"],
)
def test_chart(resource_type, test_chart: TestChart):
    chart = test_chart.sut_chart
    expected_resources = test_chart.input_chart.get_resources_for_chart(chart.name, resource_type)
    if resource_type == "configmaps":
        assert_that(chart.configmaps).is_equal_to(expected_resources)
    elif resource_type == "deployments":
        assert_that(chart.deployments.keys()).is_equal_to(expected_resources.keys())
    elif resource_type == "statefulsets":
        assert_that(chart.statefulsets.keys()).is_equal_to(expected_resources.keys())
    elif resource_type == "services":
        assert_that(chart.services).is_equal_to(expected_resources)
    elif resource_type == "pods":
        assert_that(chart.pods).is_equal_to(expected_resources)


def test_chart_health(
    current_chart_health,
    test_chart: TestChart,
):
    chart = test_chart.sut_chart
    health = chart.health
    assert_that(health).is_equal_to(current_chart_health)
    deployment_set_health = chart.deployments_health
    assert_that(deployment_set_health).is_equal_to(current_chart_health)
    statefulsets_health = chart.statefulsets_health
    assert_that(statefulsets_health).is_equal_to(current_chart_health)


def test_get_pod_set_phase(
    current_chart_health,
    test_chart: TestChart,
):
    pod_set = cast(
        helm.PodSet,
        cast(
            piping.Item,
            test_chart.sut_chart.deployments | piping.first_element_from_dict(),
        ).value,
    )
    if current_chart_health == "READY":
        expected_phase = "Running"
    elif current_chart_health == "PENDING":
        expected_phase = "Pending"
    elif current_chart_health == "ERROR":
        expected_phase = "Failed"
    else:
        expected_phase = "Unknown"
    assert_that(pod_set.phase).is_equal_to(expected_phase)


@pytest.mark.usefixtures("setup_chartinfo")
def test_chart_subcharts(cluster: configuration.ClusterWithDevices):
    sub_charts = cluster.sub_charts
    assert_that(sub_charts["ska-log-consumer"]).is_instance_of(helm.LogConsumerChart)


@pytest.mark.usefixtures("setup_chartinfo")
def test_tango_release(
    cluster: configuration.ClusterWithDevices,
):
    release = cluster.release
    assert release
    assert_that(release.devices_health).is_equal_to("READY")
    assert_that(release.health).is_equal_to("READY")


@pytest.mark.usefixtures("current_chart_health", "test_chart")
def test_release_health(
    current_chart_health,
    cluster: configuration.ClusterWithDevices,
):
    release = cluster.release
    assert release
    health = release.health
    assert_that(health).is_equal_to(current_chart_health)


@pytest.mark.usefixtures("setup_chartinfo")
def test_get_device_deployment_status(
    cluster_with_charts: configuration.ClusterWithDevices,
    charts_test_data: Chartsdata,
):
    device = next(iter(cluster_with_charts.devices.values()))
    status = device.get_device_deployment_status()
    assert_that(status).is_equal_to("Running")


@pytest.mark.usefixtures("setup_chartinfo")
def test_get_device_properties(
    cluster_with_charts: configuration.ClusterWithDevices,
    charts_test_data: Chartsdata,
):
    device = cast(tango_infra.Device, next(iter(cluster_with_charts.devices.values())))
    properties = device.properties
    assert_that(properties).is_equal_to({"Property1": ["property"], "Property2": ["1000"]})


@pytest.mark.usefixtures("setup_chartinfo")
def test_get_device_states(
    cluster_with_charts: configuration.ClusterWithDevices,
    charts_test_data: Chartsdata,
):
    chart = cluster_with_charts.chart
    assert chart
    device_states = chart.device_states
    assert_that(device_states).is_equal_to({"test-0/device-0/01": "ON", "test-1/device-1/01": "ON"})


@pytest.mark.usefixtures("setup_chartinfo")
def test_get_devices_health(
    current_chart_health: ChartsDataHealth,
    cluster_with_charts: configuration.ClusterWithDevices,
    charts_test_data: Chartsdata,
):
    release = cluster_with_charts.release
    assert release
    devices_health = release.devices_health
    expected_health = {
        "PENDING": "NOT_READY",
        "ERROR": "ERROR",
        "READY": "READY",
        "UNKNOWN": "READY",
    }[current_chart_health]
    assert_that(devices_health).is_equal_to(expected_health)


@pytest.mark.usefixtures("setup_chartinfo")
def test_get_umbrella_chart_device_servers(
    cluster_with_charts: configuration.ClusterWithDevices,
    charts_test_data: Chartsdata,
):
    release = cluster_with_charts.release
    assert release
    devices = release.devices
    assert_that(list(devices.keys())).is_equal_to(charts_test_data.devices)


@pytest.mark.usefixtures("setup_chartinfo")
def test_get_devices_in_state(
    current_chart_health: ChartsDataHealth,
    cluster_with_charts: configuration.ClusterWithDevices,
    charts_test_data: Chartsdata,
):
    release = cluster_with_charts.release
    assert release
    devices = {
        "PENDING": release.get_devices_not_ready,
        "ERROR": release.get_devices_in_error,
        "READY": release.get_devices_ready,
        "UNKNOWN": release.get_devices_ready,
    }[current_chart_health]()
    assert_that(list(devices.keys())).is_equal_to(charts_test_data.devices)
