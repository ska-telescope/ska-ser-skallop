"""Testing module that tests the generic MessageHandler class"""

from datetime import datetime

import mock
import pytest
from assertpy.assertpy import assert_that

from ska_ser_skallop.subscribing.base import BaseProducer, EventDataInt, MessageBoardBase
from ska_ser_skallop.subscribing.helpers import Tracer
from ska_ser_skallop.subscribing.message_handler import MessageHandler
from ska_ser_skallop.subscribing.subscription import Subscription

LIB = "ska_ser_skallop.subscribing.message_handler"


@pytest.fixture(name="message_board_mock")
def fxt_message_board_mock():
    return mock.create_autospec(MessageBoardBase, spec_set=True, instance=True)


@pytest.fixture(params=[True, False], name="enable_pre_handling_annotations")
def fxt_enable_pre_handling_annotations(request):
    return request.param


def test_create_message_handler(message_board_mock, enable_pre_handling_annotations):
    # given a message boord
    # when I create a new instance of a message handler
    handler = MessageHandler(
        message_board_mock,
        "dummy handler",
        enable_pre_handling_annotations=enable_pre_handling_annotations,
    )
    # I expect an initialised message handler
    assert not handler.cancel_at_next_event
    assert not handler.cancelled_by_base_class
    assert not handler.second_event_received
    assert not handler.first_event_received
    assert_that(handler.current_subscription).is_none()


@pytest.fixture(params=[True, False], name="first_event_received")
def fxt_first_event_received(request):
    return request.param


@pytest.fixture(params=[True, False], name="second_event_received")
def fxt_second_event_received(request):
    return request.param


@pytest.fixture(name="tracer_mock_cls")
def fxt_tracer_mock_cls():
    return mock.Mock(Tracer)


@pytest.fixture(name="message_handler")
def fxt_message_handler(
    tracer_mock_cls,
    message_board_mock,
    first_event_received,
    second_event_received,
):
    with mock.patch(f"{LIB}.helpers.Tracer") as the_tracer_mock_cls:
        tracer_mock_cls.return_value = the_tracer_mock_cls.return_value
        handler = MessageHandler(message_board_mock, "dummy handler")
        if second_event_received:
            handler._update_event_handler_state()
            handler._update_event_handler_state()
        elif first_event_received:
            handler._update_event_handler_state()
        yield handler


@pytest.fixture(name="mock_event")
def fxt_mock_event():
    return mock.create_autospec(EventDataInt("dummy", "dummy", "dummy"))


@pytest.fixture(name="mock_subscription")
def fxt_mock_subscription():
    sbscr = mock.create_autospec(Subscription, spec_set=True, instance=True)
    sbscr.describe.return_value = ("dummy" for _ in range(3))
    return sbscr


def test_handle_context_pre(
    message_handler,
    tracer_mock_cls,
    mock_event,
    mock_subscription,
    first_event_received,
    second_event_received,
):
    # given a message_handler
    handler: MessageHandler = message_handler
    tracer_mock = tracer_mock_cls.return_value
    # when I create a handling context with the message handler
    with handler.handle_context(mock_event, mock_subscription):
        # then I expect the following pre handlings to be done
        tracer_mock.message.assert_called()
        # event ordering state updates
        if first_event_received:
            # 1st event flag  remain then same
            assert_that(handler.first_event_received)
            # check that the second event reveived is flagged
            assert_that(handler.second_event_received)
        elif second_event_received:
            # flags remain the same
            assert_that(handler.second_event_received)
            assert_that(handler.first_event_received)
        else:
            # check that the first event reveived is flagged
            assert_that(handler.first_event_received)


def test_handle_context_post(message_handler, mock_event, mock_subscription, message_board_mock):
    # given a message_handler
    handler: MessageHandler = message_handler
    # when I create a handling context with the message handler
    with handler.handle_context(mock_event, mock_subscription):
        pass
    # then I expect the following post handlings to be done
    message_board_mock.task_done.assert_called()


def test_handle_event(
    message_handler,
    mock_event,
    mock_subscription,
    first_event_received,
    second_event_received,
    message_board_mock,
):
    # given a message_handler
    handler = message_handler
    # when I call handler event
    message_handler.handle_event(mock_event, mock_subscription)
    if first_event_received:
        # 1st event flag  remain then same
        assert_that(handler.first_event_received)
        # check that the second event reveived is flagged
        assert_that(handler.second_event_received)
    elif second_event_received:
        # flags remain the same
        assert_that(handler.second_event_received)
        assert_that(handler.first_event_received)
    else:
        # check that the first event reveived is flagged
        assert_that(handler.first_event_received)
    message_board_mock.task_done.assert_called()


def test_load_event(message_handler, mock_event, mock_subscription):
    # given a message_handler
    handler: MessageHandler = message_handler
    # when I load the event on the hanlder
    handler.load_event(mock_event, mock_subscription)
    # I expect the attributes containing event and subscription to be populated
    assert_that(handler.current_event).is_equal_to(mock_event)
    assert_that(handler.current_subscription).is_equal_to(mock_subscription)


def test_unsubscribe_all(message_handler, message_board_mock):
    # given a message_handler
    handler: MessageHandler = message_handler
    # when I unsubscribe
    handler.unsubscribe_all()
    # I expect the a call to the board to unsubscribe  all
    message_board_mock.remove_all_subscriptions.assert_called()


@pytest.fixture(params=[False, True], name="cancelled_by_base_class")
def fxt_cancelled_by_base_class(request, message_handler):
    if request.param:
        message_handler.cancelled_by_base_class = True
        return True
    return False


def test_unsubscribe(
    cancelled_by_base_class,
    message_handler,
    message_board_mock,
    mock_subscription,
):
    # given a message_handler
    handler: MessageHandler = message_handler
    # when I unsubscribe
    handler.unsubscribe(mock_subscription)
    # I expect the a call to the board to unsubscribe  all
    if cancelled_by_base_class:
        message_board_mock.remove_subscription.assert_not_called()
    else:
        message_board_mock.remove_subscription.assert_called_with(mock_subscription)


@pytest.fixture(params=[True, False], name="cancel_at_next_event")
def fxt_cancel_at_next_event(request, message_handler):
    if request.param:
        message_handler.cancel_at_next_event = True
        return True
    return False


def test_cancel_at_next_event(
    cancel_at_next_event,
    message_handler,
    mock_event,
    mock_subscription,
    message_board_mock,
):
    # given a message_handler
    handler: MessageHandler = message_handler
    # when I handle a new event
    handler.handle_event(mock_event, mock_subscription)
    # then I expect the event to be unsubscribed if it was set to be canceled at next
    # event
    if cancel_at_next_event:
        message_board_mock.remove_subscription.assert_called_with(mock_subscription)
        assert handler.cancelled_by_base_class
    else:
        message_board_mock.remove_subscription.assert_not_called()


@pytest.fixture(name="mock_producer")
def fxt_mock_producer():
    return mock.create_autospec(BaseProducer, spec_set=True, instance=True)


@pytest.fixture(name="mock_attr")
def fxt_mock_attr():
    return "dummy attr"


@pytest.mark.parametrize("print_header", [True, False])
def test_handle_timeout_by_printing_tracer(message_handler, mock_producer, mock_attr, print_header):
    # given a message_handler
    handler: MessageHandler = message_handler
    # when I call handle timeout
    result = handler.handle_timedout(mock_producer, mock_attr, print_header)
    # I expect a string  message to be reurned
    assert_that(result).is_type_of(str)


@pytest.fixture(name="mock_unpack_event")
def fxt_mock_unpack_event():
    with mock.patch(f"{LIB}.helpers.unpack_event", autospec=True) as the_mock:
        the_mock.return_value = ("dummy", "dummy", "dymmy", datetime.now())
        yield the_mock


@pytest.mark.usefixtures("mock_unpack_event")
@pytest.mark.parametrize("ignore_first", [True, False])
def test_print_event(
    message_handler,
    mock_event,
    ignore_first,
    second_event_received,
    message_board_mock,
):
    # given a message_handler
    handler: MessageHandler = message_handler
    # whe I call the handler to print an event
    result = handler.print_event(mock_event, ignore_first)
    if (not ignore_first) or (second_event_received):
        # I expect the message board to have logged that message
        message_board_mock.log.assert_called()
        # I expect the reurn results to have be a string
        assert_that(result).is_type_of(str)
