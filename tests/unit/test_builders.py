"""Testing module that tests event event handling builders module"""

from enum import Enum
from typing import List, NamedTuple

import pytest
from assertpy import assert_that
from mock import Mock

import ska_ser_skallop.event_handling.builders as sut
from ska_ser_skallop.connectors import configuration as base_conf
from ska_ser_skallop.connectors.mockingfactory import MockingFactory
from ska_ser_skallop.event_handling import base
from ska_ser_skallop.subscribing import configuration as sub_conf


class BuildContext(NamedTuple):
    attr: str
    device: base.Producer
    builder: sut.BaseBuilder
    polling: bool


class Polling(Enum):
    false = False
    true = True


@pytest.fixture(
    params=[Polling.false, Polling.true],
    name="build_context_for_polling_and_not_polling",
)
def fxt_build_context_for_polling_and_not_polling(request) -> BuildContext:
    builder = Mock(sut.BaseBuilder())
    return BuildContext("attr", base.Producer("Producer"), builder, request.param)


@pytest.fixture(name="build_context")
def fxt_build_context() -> BuildContext:
    builder = Mock(sut.BaseBuilder())
    return BuildContext("attr", base.Producer("Producer"), builder, False)


class ForAttrContext(NamedTuple):
    context: BuildContext
    subj: sut.ForAttr


@pytest.fixture(name="for_attr_context_polling_and_not_polling")
def fxt_for_attr_context_polling_and_not_polling(
    build_context_for_polling_and_not_polling: BuildContext,
) -> ForAttrContext:
    context = build_context_for_polling_and_not_polling
    subject = sut.ForAttr(context.attr, context.device, context.builder, context.polling)
    return ForAttrContext(context, subject)


@pytest.fixture(name="for_attr_context")
def fxt_for_attr_context(build_context: BuildContext) -> ForAttrContext:
    context = build_context
    subject = sut.ForAttr(context.attr, context.device, context.builder, context.polling)
    return ForAttrContext(context, subject)


@pytest.fixture(name="mock_handler")
def fxt_mock_handler() -> base.MessageHandler:
    return Mock(base.MessageHandler)


def test_for_attr_add_spec(for_attr_context_polling_and_not_polling: ForAttrContext, mock_handler):
    # given a device with a particular attribute set on producer
    context = for_attr_context_polling_and_not_polling.context
    device = context.device
    attr = context.attr
    builder = context.builder
    polling = context.polling
    # and a for attr object containing those items
    for_attr: sut.ForAttr = for_attr_context_polling_and_not_polling.subj
    # when I call a new spec with a dummy handler
    spec: sut.BuildSpec = for_attr._add_spec(mock_handler)
    # I expect a new spec to have been created
    assert_that(spec.attr).is_equal_to(attr)
    assert_that(spec.device).is_equal_to(device)
    assert_that(spec.polling).is_equal_to(polling)
    # I expect the spec to have been added to the builder
    builder.add_spec.assert_called_with(spec)


def get_called_spec_from_builder_mock(
    builder_mock: sut.BaseBuilder,
) -> sut.BuildSpec:
    (called_spec,) = builder_mock.add_spec.call_args.args
    return called_spec


@pytest.mark.parametrize("master", [True, False])
def test_for_attr_to_become_equal_to(master, for_attr_context: ForAttrContext):
    # given
    context = for_attr_context.context
    for_attr: sut.ForAttr = for_attr_context.subj
    builder = context.builder
    mock_value = "mock_value"
    # when
    for_attr.to_become_equal_to(mock_value, master)
    called_spec = get_called_spec_from_builder_mock(builder)
    assert_that(called_spec.handler).is_instance_of(sut.handlers.WaitUntilEqual)


@pytest.mark.parametrize("master", [True, False])
def test_for_attr_to_change_in_order(master, for_attr_context: ForAttrContext):
    # given
    context = for_attr_context.context
    for_attr: sut.ForAttr = for_attr_context.subj
    builder = context.builder
    mock_order = ["mock_value1", "mock_value2"]
    # when
    for_attr.to_change_in_order(mock_order, master)
    called_spec = get_called_spec_from_builder_mock(builder)
    assert_that(called_spec.handler).is_instance_of(sut.handlers.WaitForOrderedChange)


def test_for_attr_and_observe(for_attr_context: ForAttrContext):
    # given
    context = for_attr_context.context
    for_attr: sut.ForAttr = for_attr_context.subj
    builder = context.builder
    # when
    for_attr.and_observe()
    called_spec = get_called_spec_from_builder_mock(builder)
    assert_that(called_spec.handler).is_instance_of(sut.handlers.ObserveEvent)


class RequirementContext(NamedTuple):
    device_name: str
    attr: str
    value: str
    order: List
    device: base.Producer
    nr_of_specs: int


@pytest.fixture(name="requirement_context")
def fxt_requirement_context() -> RequirementContext:
    return RequirementContext(
        "device",
        "attr",
        "value",
        ["value1", "value2"],
        base.Producer("device"),
        4,
    )


@pytest.fixture(name="message_board_mock")
def fxt_message_board_mock():
    message_board_mock = Mock(sub_conf.MessageBoardBase())
    with sub_conf.patch_messageboard(message_board_mock, "test fixture: configure"):
        yield message_board_mock


@pytest.mark.usefixtures("message_board_mock")
@pytest.fixture(name="test_factory")
def fxt_test_factory(
    requirement_context: RequirementContext,
) -> MockingFactory:
    testing_factory = MockingFactory()
    testing_factory.inject_producer(requirement_context.device_name, requirement_context.device)
    for index in range(requirement_context.nr_of_specs):
        testing_factory.inject_producer(
            f"{requirement_context.device_name}-{index}",
            requirement_context.device,
        )
    with base_conf.patch_factory(testing_factory, "test fixture: configure"):
        yield testing_factory


@pytest.mark.usefixtures("test_factory")
def test_wating_on(requirement_context: RequirementContext):
    # given
    builder = sut.MessageBoardBuilder()
    device_name = requirement_context.device_name
    device = requirement_context.device
    attr = requirement_context.attr
    value = requirement_context.value
    # when
    spec: sut.BuildSpec = (
        builder.set_waiting_on(device_name).for_attribute(attr).to_become_equal_to(value)
    )
    # then
    assert_that(spec.attr).is_equal_to(attr)
    assert_that(spec.device).is_equal_to(device)
    assert_that(spec.handler).is_instance_of(sut.handlers.WaitUntilEqual)


@pytest.mark.usefixtures("test_factory")
def test_wating_for_ordered(requirement_context: RequirementContext):
    # given
    builder = sut.MessageBoardBuilder()
    device_name = requirement_context.device_name
    attr = requirement_context.attr
    order = requirement_context.order
    device = requirement_context.device
    # when
    spec: sut.BuildSpec = (
        builder.set_waiting_on(device_name).for_attribute(attr).to_change_in_order(order)
    )
    # then
    assert_that(spec.attr).is_equal_to(attr)
    assert_that(spec.device).is_equal_to(device)
    assert_that(spec.handler).is_instance_of(sut.handlers.WaitForOrderedChange)


@pytest.mark.usefixtures("test_factory")
def test_wait_and_observe(requirement_context: RequirementContext):
    # given
    builder = sut.MessageBoardBuilder()
    device_name = requirement_context.device_name
    attr = requirement_context.attr
    device = requirement_context.device
    # when
    spec: sut.BuildSpec = builder.set_waiting_on(device_name).for_attribute(attr).and_observe()
    # then
    assert_that(spec.attr).is_equal_to(attr)
    assert_that(spec.device).is_equal_to(device)
    assert_that(spec.handler).is_instance_of(sut.handlers.ObserveEvent)


class MessageBoardBuilderContext(NamedTuple):
    builder: sut.MessageBoardBuilder
    specs: List[RequirementContext]
    polling: bool
    nr_of_specs: int


@pytest.mark.usefixtures("test_factory")
@pytest.fixture(name="message_board_builder_context")
def fxt_message_board_builder_context(
    requirement_context: RequirementContext,
) -> MessageBoardBuilderContext:
    req = requirement_context
    nr_of_specs = req.nr_of_specs
    polling = False
    specs = [
        RequirementContext(
            f"{req.device_name}-{index}",
            req.attr,
            req.value,
            req.order,
            req.device,
            req.nr_of_specs,
        )
        for index in range(1, nr_of_specs)
    ]
    builder = sut.MessageBoardBuilder()
    for spec in specs:
        builder.set_waiting_on(spec.device_name).for_attribute(spec.attr).to_become_equal_to(
            spec.value
        )
    return MessageBoardBuilderContext(builder, specs, polling, nr_of_specs)


@pytest.mark.usefixtures("test_factory", "message_board_mock")
def test_setup_board(
    message_board_builder_context: MessageBoardBuilderContext,
):
    # given a messageboard builder
    builder = message_board_builder_context.builder
    # upon which a nr of specs have been loaded
    nr_of_specs = message_board_builder_context.nr_of_specs
    # when I call setup board
    builder.setup_board()
    # I expect the messageboard to have been called to add subscriptions equal to the
    # number of specs
    builder.board.add_subscription.call_count = nr_of_specs
