import pytest
from assertpy import assert_that
from mock import Mock

from ska_ser_skallop.event_handling.handlers import Recorder, WaitUntilEqual
from ska_ser_skallop.subscribing.base import EventDataInt
from ska_ser_skallop.subscribing.event_item import EventItem
from ska_ser_skallop.subscribing.message_board import MessageBoard
from ska_ser_skallop.subscribing.subscription import Subscription

BASETIME = 1659681488.129775


def generate_event_item(device: str, attribute: str, value: str, increment: int):
    return EventItem(
        EventDataInt(
            f"{device}{increment}",
            f"{attribute}{increment}",
            f"{value}{increment}",
            BASETIME + increment,
        ),
        Mock(Subscription),
        WaitUntilEqual(
            Mock(MessageBoard),
            f"{attribute}{increment}",
            f"{value}{increment}",
            f"{device}{increment}",
        ),
    )


@pytest.fixture(name="events")
def fxt_events() -> list[EventItem]:
    return [generate_event_item("dev", "att", "val", increment) for increment in range(1, 5)]


def handle_events(events: list[EventItem], recorder: Recorder):
    for event in events:
        handler = event.handler
        if handler:
            handler.handle_event(event.event, event.subscription)
            recorder.record(handler)


def test_recording(events: list[EventItem]):
    recorder = Recorder()
    handle_events(events, recorder)
    events = recorder.get_devices_transitioned_after("dev4")
    assert_that(events).is_length(1)
    recorder.assert_no_devices_transitioned_after("dev4")
    with pytest.raises(AssertionError):
        recorder.assert_no_devices_transitioned_after("dev1")
