import types
from typing import Any, Dict

from mock import Mock, patch

from ska_ser_skallop.connectors.mockingfactory import MockingFactory
from ska_ser_skallop.mvp_control.base import AbstractDeviceProxy
from ska_ser_skallop.mvp_control.infra_mon.infra import Cluster
from ska_ser_skallop.mvp_control.infra_mon.tango_infra import DevicesChart, set_factory
from ska_ser_skallop.mvp_control.model.mocks import MockDeviceProxy

from .conftest import MockAppsAPI, MockCoreAPI, MockCustomAPI, load_json_testdata


@patch("kubernetes.client.CoreV1Api", return_value=MockCoreAPI())
@patch("kubernetes.client.CustomObjectsApi", return_value=MockCustomAPI())
@patch("kubernetes.client.AppsV1Api", return_value=MockAppsAPI())
@patch("kubernetes.config.load_kube_config", return_value=Mock())
def test_devices_chart_devices_loading(
    mock_client_core, mock_client_custom, mock_client_apps, mock_config
):
    expected_devices = load_json_testdata("expected_devices_cbf.json")
    device_proxies: Dict[str, AbstractDeviceProxy] = {}

    def command_inout(self, cmd_name: str, cmd_param: Any = None, *args: Any, **kwargs: Any) -> Any:
        return [
            "bla",
            [
                cmd_param,
                "IOR:000",
                "5",
                "test",
                "ds-test-01-0",
                "13th March 2024 at 10:10:05",
                "?",
                "TestClass",
            ],
        ]

    for v in expected_devices:
        dp = MockDeviceProxy(name=v, model={})
        device_proxies[v] = dp

    dp = MockDeviceProxy(name="sys/database/2", model={})
    dp.command_inout = types.MethodType(command_inout, dp)
    device_proxies["sys/database/2"] = dp

    factory = MockingFactory(mock_device_proxies=device_proxies)
    set_factory(factory)
    c = DevicesChart(
        name="ska-mid-cbf-mcs", version="0.1.0", subsystem="cbfmcs-mid", cluster=Cluster()
    )
    devices = list(c.devices.keys())
    devices.sort()
    assert devices == expected_devices, "lists are inequal"
