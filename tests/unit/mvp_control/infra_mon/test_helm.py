from mock import Mock, patch

from ska_ser_skallop.mvp_control.infra_mon import helm

from .conftest import MockAppsAPI, MockCoreAPI, MockCustomAPI, load_json_testdata


@patch("kubernetes.client.CoreV1Api", return_value=MockCoreAPI())
@patch("kubernetes.client.CustomObjectsApi", return_value=MockCustomAPI())
@patch("kubernetes.client.AppsV1Api", return_value=MockAppsAPI())
@patch("kubernetes.config.load_kube_config", return_value=Mock())
def test_cluster_with_charts_sub_charts(
    mock_client_core, mock_client_custom, mock_client_apps, mock_config
):
    expected_charts = load_json_testdata("expected_charts_from_chartinfo.json")
    cluster = helm.ClusterWithCharts()
    sub_charts = list(cluster.sub_charts.keys())
    assert sub_charts == expected_charts, "chart lists are inequal"


@patch("kubernetes.client.CoreV1Api", return_value=MockCoreAPI())
@patch("kubernetes.client.CustomObjectsApi", return_value=MockCustomAPI())
@patch("kubernetes.client.AppsV1Api", return_value=MockAppsAPI())
@patch("kubernetes.config.load_kube_config", return_value=Mock())
def test_cluster_with_charts_release(
    mock_client_core, mock_client_custom, mock_client_apps, mock_config
):
    cluster = helm.ClusterWithCharts()
    assert "test" == cluster.release.name, "releases are inequal"


@patch("kubernetes.client.CoreV1Api", return_value=MockCoreAPI())
@patch("kubernetes.client.CustomObjectsApi", return_value=MockCustomAPI())
@patch("kubernetes.client.AppsV1Api", return_value=MockAppsAPI())
@patch("kubernetes.config.load_kube_config", return_value=Mock())
def test_cluster_with_charts_chart(
    mock_client_core, mock_client_custom, mock_client_apps, mock_config
):
    cluster = helm.ClusterWithCharts()
    assert cluster.chart.name == "ska-mid-itf", "main chart names are inequal"
    assert cluster.chart.version == "0.1.0", "main chart versions are inequal"
