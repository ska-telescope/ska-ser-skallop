from mock import Mock, patch

from ska_ser_skallop.mvp_control.infra_mon.infra import Cluster

from .conftest import MockAppsAPI, MockCoreAPI, MockCustomAPI, load_json_testdata


@patch("kubernetes.client.CoreV1Api", return_value=MockCoreAPI())
@patch("kubernetes.client.CustomObjectsApi", return_value=MockCustomAPI())
@patch("kubernetes.client.AppsV1Api", return_value=MockAppsAPI())
@patch("kubernetes.config.load_kube_config", return_value=Mock())
def test_chart_deviceservers_loading(
    mock_client_core, mock_client_custom, mock_client_apps, mock_config
):
    c = Cluster()
    ds_map = c.get_deviceservers_crds()
    ds = list(ds_map)
    expected = load_json_testdata("deviceserver_names.json")
    assert ds == expected
