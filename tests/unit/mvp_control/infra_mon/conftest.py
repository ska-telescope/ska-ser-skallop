import json
import pathlib
from dataclasses import dataclass
from typing import Any

from kubernetes.client import ApiClient
from kubernetes.client.models.v1_config_map_list import V1ConfigMapList
from kubernetes.client.models.v1_deployment_list import V1DeploymentList
from kubernetes.client.models.v1_pod_list import V1PodList
from kubernetes.client.models.v1_service_list import V1ServiceList
from kubernetes.client.models.v1_stateful_set_list import V1StatefulSetList
from mock import Mock

__testdata_dir = "tests/unit/mvp_control/infra_mon/testdata"


def load_json_testdata(name: str) -> dict:
    with open(pathlib.Path(__testdata_dir, name), encoding="utf-8") as json_file:
        return json.load(json_file)


def load_testdata(name: str) -> str:
    with open(pathlib.Path(__testdata_dir, name), encoding="utf-8") as f:
        return f.read()


@dataclass
class MockRestResponse:
    data: str


class MockCoreAPI(Mock):
    def list_namespaced_pod(self, namespace: str) -> V1PodList:
        td = load_testdata("pods.json")
        api = ApiClient()
        return api.deserialize(MockRestResponse(td), V1PodList)

    def list_namespaced_config_map(self, namespace: str) -> V1ConfigMapList:
        td = load_testdata("configmaps.json")
        api = ApiClient()
        return api.deserialize(MockRestResponse(td), V1ConfigMapList)

    def list_namespaced_service(self, namespace: str) -> V1ServiceList:
        td = load_testdata("services.json")
        api = ApiClient()
        return api.deserialize(MockRestResponse(td), V1ServiceList)


class MockCustomAPI(Mock):
    def list_namespaced_custom_object(self, group, version, namespace, plural, **kwargs) -> Any:
        return load_json_testdata("deviceservers.json")


class MockAppsAPI(Mock):
    def list_namespaced_deployment(self, namespace: str) -> V1DeploymentList:
        td = load_testdata("deployments.json")
        api = ApiClient()
        return api.deserialize(MockRestResponse(td), V1DeploymentList)

    def list_namespaced_stateful_set(self, namespace: str) -> V1StatefulSetList:
        td = load_testdata("stateful_sets.json")
        api = ApiClient()
        return api.deserialize(MockRestResponse(td), V1StatefulSetList)
