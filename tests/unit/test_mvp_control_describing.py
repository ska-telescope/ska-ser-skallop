"""Testing module that tests describing module of mvp control"""

import logging
from enum import Enum, auto
from typing import Dict, List, NamedTuple, Set, Tuple, Union

import mock
import pytest
from assertpy import assert_that

from ska_ser_skallop.connectors import configuration
from ska_ser_skallop.connectors.mockingfactory import MockingFactory
from ska_ser_skallop.connectors.tango.tangoqueries import get_new_tango_device_query
from ska_ser_skallop.mvp_control.base import AbstractDeviceProxy
from ska_ser_skallop.mvp_control.describing import mvp_names as names
from ska_ser_skallop.mvp_control.describing import mvp_states as states
from ska_ser_skallop.mvp_control.describing import names_spec
from ska_ser_skallop.subscribing.base import AttributeInt
from ska_ser_skallop.utils import env

logger = logging.getLogger(__name__)


LIB = "ska_ser_skallop.mvp_control.describing.mvp_states"


# subarrays_start_up_states = {device_name: "OFF" for device_name in names.SubArrays(1)}


@pytest.fixture(name="subarrays_start_up_states")
def fxt_subarrays_start_up_states():
    tel = names.TEL()
    return {device_name: "OFF" for device_name in tel.subarrays(1)}


@pytest.fixture(name="master_start_up_states")
def fxt_master_start_up_states():
    tel = names.TEL()
    devices_states = {device_name: "OFF" for device_name in tel.masters()}
    if tel.skamid:
        devices_states[tel.csp.controller] = "STANDBY"
        devices_states[tel.skamid.csp.cbf.controller] = "STANDBY"
        devices_states[tel.sdp.master] = "STANDBY"
    return devices_states


@pytest.fixture(name="master_on_states")
def fxt_master_on_states():
    tel = names.TEL()
    return {device_name: "ON" for device_name in tel.masters()}


@pytest.fixture(name="subarrays_on_states")
def fxt_subarrays_on_states():
    tel = names.TEL()
    return {device_name: "ON" for device_name in tel.subarrays(1)}


@pytest.fixture(name="subarrays_start_up_obs_states")
def fxt_subarrays_start_up_obs_states():
    tel = names.TEL()
    return {device_name: "EMPTY" for device_name in tel.subarrays(1)}


class State:
    def __init__(self, name: str):
        self.name = name


class MockDevices:
    states = {}

    def __init__(self, name: str) -> None:
        self._name = name

    def name(self) -> str:
        return self._name

    def read_attribute(self, attr: str):
        if attr == "state":
            val = self.states[self._name]
            return AttributeInt(name=attr, value=val)
        return None


class MockSubarrays(MockDevices):
    obsStates = {}

    def read_attribute(self, attr: str):
        result = super().read_attribute(attr)
        if result:
            return result
        if attr == "obsstate":
            val = self.obsStates[self._name]
            return AttributeInt(name=attr, value=val)
        return None


class MockTelescope(MockSubarrays):
    pass


def mock_master(name: str) -> MockDevices:
    return MockDevices(name)


def mock_subarray(name: str) -> MockSubarrays:
    return MockSubarrays(name)


@pytest.fixture(name="factory")
def fxt_factory():
    factory = MockingFactory()
    device_query = get_new_tango_device_query(factory)
    factory.inject_devices_query(device_query)
    with configuration.patch_factory(factory):
        yield factory


@pytest.fixture(name="mock_masters")
def fxt_mock_masters(factory: MockingFactory):
    masters_mock_cls = mock.Mock(AbstractDeviceProxy, wraps=MockDevices)
    factory.inject_device_proxy_constructor(masters_mock_cls)


@pytest.fixture(name="mock_telescope")
def fxt_mock_telescope(factory: MockingFactory):
    masters_mock_cls = mock.Mock(AbstractDeviceProxy, wraps=MockTelescope)
    factory.inject_device_proxy_constructor(masters_mock_cls)


@pytest.fixture(name="masters_in_standby")
def fxt_masters_in_standby(master_start_up_states):
    MockDevices.states = master_start_up_states


@pytest.fixture(name="subarrays_in_standby")
def fxt_subarrays_in_standby(subarrays_start_up_states, subarrays_start_up_obs_states):
    MockSubarrays.states = subarrays_start_up_states
    MockSubarrays.obsStates = subarrays_start_up_obs_states


@pytest.fixture(name="mock_subarrays")
def fxt_mock_subarrays(factory: MockingFactory):
    subarrays_mock_cls = mock.Mock(AbstractDeviceProxy, wraps=MockSubarrays)
    factory.inject_device_proxy_constructor(subarrays_mock_cls)


@pytest.fixture(name="skamid")
def fxt_skamid():
    env.set_telescope_as_mid()


@pytest.fixture(name="skalow")
def fxt_skalow():
    env.set_telescope_as_low()


@pytest.fixture(name="skamid_skalow", params=["skamid", "skalow"])
def fxt_skamid_skalow(request):
    if request.param == "skamid":
        env.set_telescope_as_mid()
    else:
        env.set_telescope_as_low()


class StartingState(Enum):
    standby = auto()
    running = auto()


class Context:
    def __init__(self, starting_state: StartingState):
        self.starting_state = starting_state


@pytest.fixture(name="context")
def fxt_context():
    return Context(StartingState.standby)


@pytest.fixture(name="telescope_in_standby")
def fxt_telescope_in_standby(
    subarrays_start_up_states,
    master_start_up_states,
    subarrays_start_up_obs_states,
):
    MockTelescope.states = {
        **subarrays_start_up_states,
        **master_start_up_states,
    }
    MockTelescope.obsStates = subarrays_start_up_obs_states


@pytest.fixture(name="telescope_in_on_state")
def fxt_telescope_in_on_state(
    context: Context,
    subarrays_on_states,
    master_on_states,
    subarrays_start_up_obs_states,
):
    MockTelescope.states = {**subarrays_on_states, **master_on_states}
    MockTelescope.obsStates = subarrays_start_up_obs_states
    context.starting_state = StartingState.running


@pytest.mark.usefixtures("skamid", "masters_in_standby", "mock_masters")
def test_master_state():
    master_states = states.get_master_states()
    assert any(master_states.are_in_state("STANDBY"))
    assert not all(master_states.are_in_state("OFF"))
    assert all(master_states.are_in_state("OFF", "STANDBY"))


@pytest.mark.usefixtures("skalow", "masters_in_standby", "mock_masters")
def test_master_state_low():
    master_states = states.get_master_states()
    assert all(master_states.are_in_state("OFF", "STANDBY"))


@pytest.mark.usefixtures("skamid_skalow", "subarrays_in_standby", "mock_subarrays")
def test_subarray_obs_state():
    subarray_obs_state = states.get_subarray_obs_states(1)
    assert all(subarray_obs_state.are_in_state("EMPTY"))


@pytest.mark.usefixtures(
    "skamid_skalow",
    "subarrays_in_standby",
    "mock_subarrays",
)
def test_subarray_state():
    subarray_obs_state = states.get_subarray_states(1)
    assert all(subarray_obs_state.are_in_state("OFF"))


@pytest.fixture(name="dirty_master")
def fxt_dirty_master(context: Context) -> Tuple[str, str]:
    if context.starting_state == StartingState.standby:
        return (f"{names.Mid.csp.controller}", "ON")
    return (f"{names.Mid.csp.controller}", "OFF")


@pytest.fixture(name="masters_dirty")
def fxt_masters_dirty(dirty_master, master_start_up_states):
    MockDevices.states = master_start_up_states
    dirty_master_name, dirty_master_val = dirty_master
    MockDevices.states[dirty_master_name] = dirty_master_val


@pytest.mark.usefixtures("skamid", "masters_dirty", "mock_masters")
def test_master_state_filtering(dirty_master):
    master_states = states.get_master_states()
    faulty = master_states.not_in_state("OFF", "STANDBY")
    correct = master_states.in_state("OFF", "STANDBY")
    assert_that(faulty).is_equal_to([dirty_master[0]])
    assert_that(dirty_master[0]).is_not_in(correct)


@pytest.fixture(name="dirty_subarray")
def fxt_dirty_subarray() -> Tuple[str, str]:
    return (str(names.Mid.tm.subarray(1)), "IDLE")


@pytest.fixture(name="faulty_subarray")
def fxt_faulty_subarray(context) -> Tuple[str, str]:
    if context.starting_state == StartingState.standby:
        return (str(names.Mid.tm.subarray(1)), "ON")
    return (str(names.Mid.tm.subarray(1)), "OFF")


class DirtyType(Enum):
    faulty_masters = auto()
    faulty_subarrays = auto()
    dirty_subarrays = auto()


class DirtyTelescopeContext(NamedTuple):
    dirty_element: Dict
    grouping: str


@pytest.fixture(
    params=[
        DirtyType.faulty_masters,
        DirtyType.faulty_subarrays,
        DirtyType.dirty_subarrays,
    ]
)
def telescope_dirty(request, dirty_master, dirty_subarray, faulty_subarray):
    condition = request.param
    if condition == DirtyType.faulty_masters:
        key, val = dirty_master
        MockTelescope.states[key] = val
    elif condition == DirtyType.faulty_subarrays:
        key, val = faulty_subarray
        MockTelescope.states[key] = val
    else:
        key, val = dirty_subarray
        MockTelescope.obsStates[key] = val
    return DirtyTelescopeContext({key: val}, condition.name)


@pytest.fixture(name="ska_spec", params=["skalow", "skamid"])
def fxt_ska_spec(
    request,
) -> Dict[names.DeviceName, Dict[str, Union[str, Set[str]]]]:
    if request.param == "skalow":
        return names_spec.ska_low
    return names_spec.ska_mid


def test_correct_device_names(ska_spec: Dict[names.DeviceName, Dict[str, Union[str, Set[str]]]]):
    errors: List[AssertionError] = []
    for device, spec in ska_spec.items():
        try:
            assert_that(f"{device}").is_equal_to(spec["name"])
            assert_that(set(device.tags)).is_equal_to(spec["tags"])
            correct_tag = next(iter(spec["tags"]))
            incorrect_tag = f"incorrect_shim{correct_tag}"
            assert_that(device.filter(correct_tag)).is_equal_to(device)
            assert_that(device.filter(incorrect_tag)).is_none()
        except AssertionError as error:
            errors.append(error)
    if errors:
        raise AssertionError(errors)
