import pytest
from assertpy import assert_that

from ska_ser_skallop.mvp_management.subarray_composition import generate_allocation_configuration
from ska_ser_skallop.mvp_management.subarray_configuration import generate_scan_configuration
from ska_ser_skallop.mvp_management.types import (
    Composition,
    CompositionByFile,
    ScanConfiguration,
    ScanConfigurationByFile,
)
from ska_ser_skallop.utils.combinations import combine

generate_alloc_configuration_args = {}
generate_alloc_configuration_args["configuration_types"] = [
    "standard",
    "non-standard",
]
generate_alloc_configuration_args["file_based"] = [None, False, True]


@pytest.mark.parametrize(**combine(generate_alloc_configuration_args))
def test_generate_alloc_configuration(configuration_types, file_based):
    if configuration_types == "non-standard":
        with pytest.raises(NotImplementedError):
            generate_allocation_configuration(configuration_types, file_based)
    else:
        result = generate_allocation_configuration(configuration_types, file_based)
        if file_based:
            assert_that(result).is_type_of(CompositionByFile)
        else:
            assert_that(result).is_type_of(Composition)


generate_scan_configuration_args = {}
generate_scan_configuration_args["configuration_types"] = [
    "standard",
    "non-standard",
]
generate_scan_configuration_args["file_based"] = [None, False, True]


@pytest.mark.parametrize(**combine(generate_scan_configuration_args))
def test_generate_scan_configuration(configuration_types, file_based):
    if configuration_types == "non-standard":
        with pytest.raises(NotImplementedError):
            generate_scan_configuration(configuration_types, file_based)
    else:
        result = generate_scan_configuration(configuration_types, file_based)
        if file_based:
            assert_that(result).is_type_of(ScanConfigurationByFile)
        else:
            assert_that(result).is_type_of(ScanConfiguration)
