import pytest
from mock import Mock

from ska_ser_skallop.mvp_control.entry_points import testing


@pytest.fixture(name="entry_point")
def fxt_entry_point() -> testing.MockedEntryPoint:
    return testing.MockedEntryPoint()


@pytest.fixture(name="mock_calling")
def fxt_mock_calling() -> Mock:
    return Mock()


def test_when_abort_subarray(entry_point: testing.MockedEntryPoint, mock_calling: Mock):
    @entry_point.when_abort_subarray
    def mock_call(*args, **kwargs):
        mock_calling()

    entry_point.abort_subarray(0)
    mock_calling.assert_called()


def test_when_configure_subarray(entry_point: testing.MockedEntryPoint, mock_calling: Mock):
    @entry_point.when_configure_subarray
    def mock_call(*args, **kwargs):
        mock_calling()

    entry_point.configure_subarray(0, [0], Mock(testing.ScanConfiguration), "", 0)
    mock_calling.assert_called()


def test_when_reset_subarray(entry_point: testing.MockedEntryPoint, mock_calling: Mock):
    @entry_point.when_reset_subarray
    def mock_call(*args, **kwargs):
        mock_calling()

    entry_point.reset_subarray(0)
    mock_calling.assert_called()


def test_when_set_telescope_to_running(entry_point: testing.MockedEntryPoint, mock_calling: Mock):
    @entry_point.when_set_telescope_to_running
    def mock_call(*args, **kwargs):
        mock_calling()

    entry_point.set_telescope_to_running()
    mock_calling.assert_called()


def test_when_tear_down_subarray(entry_point: testing.MockedEntryPoint, mock_calling: Mock):
    @entry_point.when_tear_down_subarray
    def mock_call(*args, **kwargs):
        mock_calling()

    entry_point.tear_down_subarray(0)
    mock_calling.assert_called()


def test_when_set_telescope_to_standby(entry_point: testing.MockedEntryPoint, mock_calling: Mock):
    @entry_point.when_set_telescope_to_standby
    def mock_call(*args, **kwargs):
        mock_calling()

    entry_point.set_telescope_to_standby()
    mock_calling.assert_called()


def test_when_scan(entry_point: testing.MockedEntryPoint, mock_calling: Mock):
    @entry_point.when_scan
    def mock_call(*args, **kwargs):
        mock_calling()

    entry_point.scan(0)
    mock_calling.assert_called()
