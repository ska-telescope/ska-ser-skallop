from unittest import mock

import pytest

from ska_ser_skallop.mvp_control.dishes.failure_handling import DirtyDishHandler
from ska_ser_skallop.mvp_control.rectifying.rectifier import patch_rectifier


@pytest.fixture(name="mock_rectifier")
def fxt_mock_rectifier():
    with patch_rectifier() as mock_rectifier:
        yield mock_rectifier


@mock.patch("ska_ser_skallop.mvp_control.dishes.failure_handling.Readiness")
def test_switch_off_cmd_is_called_when_devices_are_on(mock_readiness, mock_rectifier):
    mock_readiness.get_not_ok_conditions().get().in_state.return_value = [
        "ska-001",
        "ska-0002",
    ]
    dd_handler = DirtyDishHandler(mock_readiness)
    dd_handler.switch_off_any_on_dishes()
    mock_rectifier.switch_off_dish_masters.assert_called()


@mock.patch("ska_ser_skallop.mvp_control.dishes.failure_handling.Readiness")
def test_switch_off_cmd_is_not_called_when_devices_are_off(mock_readiness, mock_rectifier):
    mock_readiness.get_not_ok_conditions().get().in_state.return_value = None
    dd_handler = DirtyDishHandler(mock_readiness)
    dd_handler.switch_off_any_on_dishes()
    mock_rectifier.switch_off_dish_masters.assert_not_called()


@mock.patch("ska_ser_skallop.mvp_control.dishes.failure_handling.Readiness")
def test_switch_on_cmd_is_called_when_devices_are_off(mock_readiness, mock_rectifier):
    mock_readiness.get_not_ok_conditions().get().are_in_state.return_value = [
        "ska-001",
        "ska-0002",
    ]
    dd_handler = DirtyDishHandler(mock_readiness)
    dd_handler.switch_on_any_off_dishes()
    mock_rectifier.switch_on_dish_masters.assert_called()


@mock.patch("ska_ser_skallop.mvp_control.dishes.failure_handling.Readiness")
def test_switch_on_cmd_is_not_called_when_devices_are_off(mock_readiness, mock_rectifier):
    mock_readiness.get_not_ok_conditions().get().are_in_state.return_value = []
    dd_handler = DirtyDishHandler(mock_readiness)
    dd_handler.switch_on_any_off_dishes()
    mock_rectifier.switch_on_dish_masters.assert_not_called()


@mock.patch("ska_ser_skallop.mvp_control.dishes.failure_handling.Readiness")
def test_switch_on_cmd_is_called_when_devices_are_standbylp(mock_readiness, mock_rectifier):
    mock_readiness.get_not_ok_conditions().get().are_in_state.return_value = [
        "ska-001",
        "ska-0002",
    ]
    dd_handler = DirtyDishHandler(mock_readiness)
    dd_handler.switch_to_fp_any_lp_dishes()
    mock_rectifier.switch_on_dish_masters.assert_called()


@mock.patch("ska_ser_skallop.mvp_control.dishes.failure_handling.Readiness")
def test_switch_on_cmd_is_not_called_when_devices_are_not_in_standbylp(
    mock_readiness, mock_rectifier
):
    mock_readiness.get_not_ok_conditions().get().are_in_state.return_value = []
    dd_handler = DirtyDishHandler(mock_readiness)
    dd_handler.switch_to_fp_any_lp_dishes()
    mock_rectifier.switch_on_dish_masters.assert_not_called()
