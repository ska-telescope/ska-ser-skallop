from typing import Tuple

import pytest
from assertpy import assert_that
from mock import Mock, patch

from ska_ser_skallop.connectors.tangodb import TangoDB

mock_device_names = [
    "sys/database/2",
    "sys/tg_test/1",
]


@patch("ska_ser_skallop.connectors.tangodb.get_device_proxy")
@patch("ska_ser_skallop.connectors.tangodb.get_devices_query")
def test_init(mock_get_device_proxy, get_devices_query):
    TangoDB()
    mock_get_device_proxy.assert_called()
    get_devices_query.assert_called()


@pytest.fixture(name="tangodb_init")
@patch("ska_ser_skallop.connectors.tangodb.get_device_proxy")
@patch("ska_ser_skallop.connectors.tangodb.get_devices_query")
def fxt_tangodb_init(mock_get_devices_query, mock_get_device_proxy) -> Tuple[TangoDB, Mock, Mock]:
    return TangoDB(), mock_get_device_proxy, mock_get_devices_query


@pytest.fixture(name="tangodb")
def fxt_tangodb(tangodb_init) -> TangoDB:
    return tangodb_init[0]


@pytest.fixture(name="mock_get_device_proxy")
def fxt_mock_get_device_proxy(tangodb_init) -> Mock:
    return tangodb_init[1]


@pytest.fixture(name="mock_get_devices_query")
def fxt_mock_get_devices_query(tangodb_init) -> Mock:
    return tangodb_init[2]


@pytest.fixture(name="mock_devices_query")
def fxt_mock_devices_query(mock_get_devices_query) -> Mock:
    return mock_get_devices_query.return_value


@pytest.fixture(name="mock_devices")
def fxt_mock_devices(mock_get_device_proxy):
    mock_device = mock_get_device_proxy.return_value
    mock_device.command_inout.return_value = mock_device_names


@pytest.mark.usefixtures("mock_devices")
def test_get_property(tangodb: TangoDB):
    devices = tangodb.devices
    assert_that(devices).is_equal_to(mock_device_names)


def test_get_db_state(tangodb: TangoDB, mock_devices_query):
    tangodb.get_db_state()
    mock_devices_query.query.assert_called()
