from typing import NamedTuple
from unittest import mock

import pytest

from ska_ser_skallop.connectors import configuration as conf
from ska_ser_skallop.connectors.mockingfactory import MockingFactory
from ska_ser_skallop.mvp_control.configuration.types import ScanConfiguration, ScanConfigurationType
from ska_ser_skallop.mvp_control.entry_points import configuration as entry_conf
from ska_ser_skallop.mvp_control.entry_points.base import EntryPoint
from ska_ser_skallop.mvp_control.subarray.base import SBConfig
from ska_ser_skallop.mvp_control.subarray.configure import (
    EWhileConfiguring,
    NotReadyConfigure,
    assert_I_can_configure_a_subarray,
    configure_subarray,
    start_configuring_a_subarray,
)
from ska_ser_skallop.mvp_fixtures.base import ExecSettings
from ska_ser_skallop.subscribing.message_board import MessageBoard


@pytest.fixture(name="test_factory")
def fxt_test_factory():
    testing_factory = MockingFactory()
    with conf.patch_factory(testing_factory, "test fixture"):
        testing_factory.set_producer_spy()
        testing_factory.set_device_spy()
        yield testing_factory


@pytest.fixture(name="entrypoint_mock")
def fxt_entrypoint_mock() -> EntryPoint:
    with entry_conf.patch_entry_point(synched_impl=True) as entrypoint_mock:
        yield entrypoint_mock


class SubarrayArgs(NamedTuple):
    subarray_id: int
    receptors: list[int]
    scan_config: ScanConfiguration
    sb_config: SBConfig
    scan_duration: int
    settings: ExecSettings

    @property
    def args(self):
        return (
            self.subarray_id,
            self.scan_config,
            self.sb_config,
            self.scan_duration,
            self.settings,
        )


@pytest.fixture(name="exec_settings")
def fxt_exec_settings() -> ExecSettings:
    settings = ExecSettings()
    settings.time_out = 0.1
    return settings


@pytest.fixture(name="subarray_args")
def fxt_subarray_args(exec_settings):
    return SubarrayArgs(
        subarray_id=1,
        receptors=[1, 2],
        scan_config=ScanConfiguration(ScanConfigurationType.STANDARD),
        sb_config=SBConfig(sbid="sbid1"),
        scan_duration=2,
        settings=exec_settings,
    )


@pytest.mark.usefixtures("test_factory")
def test_configure_wait_timeout(subarray_args: SubarrayArgs):
    """Test that the wait times out, as the device does not exist."""
    with pytest.raises(EWhileConfiguring):
        configure_subarray(*subarray_args.args)  # type: ignore


@pytest.mark.usefixtures("test_factory")
@mock.patch("ska_ser_skallop.mvp_control.event_waiting.wait.wait")
def test_configure(mocked_wait, entrypoint_mock: EntryPoint, subarray_args: SubarrayArgs):
    """Test the configuration parameters"""
    # Check that the wait is called
    configure_subarray(*subarray_args.args)  # type: ignore
    assert mocked_wait.called
    wait_call = mocked_wait.call_args.call_list()[0]
    assert wait_call[1]["live_logging"] == subarray_args.settings.log_enabled
    assert wait_call[0][1] == subarray_args.settings.time_out
    assert isinstance(wait_call[0][0], MessageBoard)

    configure_subarray(*subarray_args.args)  # type: ignore
    assert entrypoint_mock.configure_subarray.called
    configure_subarray_args = entrypoint_mock.configure_subarray.call_args[0]
    assert configure_subarray_args[0] == subarray_args.subarray_id
    assert configure_subarray_args[1] == subarray_args.scan_config
    assert isinstance(configure_subarray_args[1], ScanConfiguration)


@pytest.mark.usefixtures("test_factory")
@mock.patch("ska_ser_skallop.mvp_control.subarray.configure.Job")
@mock.patch("ska_ser_skallop.mvp_control.event_waiting.wait.wait")
def test_start_configuring_a_subarray(
    mocked_wait,
    mocked_job,
    entrypoint_mock: EntryPoint,
    subarray_args: SubarrayArgs,
):
    """Test the configuration parameters"""
    # Check that the wait is called

    start_configuring_a_subarray(*subarray_args.args)  # type: ignore
    assert mocked_wait.called
    wait_call = mocked_wait.call_args.call_list()[0]
    assert wait_call[1]["live_logging"] == subarray_args.settings.log_enabled
    assert wait_call[0][1] == subarray_args.settings.time_out
    assert isinstance(wait_call[0][0], MessageBoard)

    # Check configure_subarray command for oet hidden behind entrypoint.
    start_configuring_a_subarray(*subarray_args.args)  # type: ignore
    assert mocked_job.called
    job_args = mocked_job.call_args[0]
    assert job_args[0] == entrypoint_mock.configure_subarray
    assert job_args[1] == subarray_args.subarray_id
    assert job_args[2] == subarray_args.scan_config
    assert isinstance(job_args[2], ScanConfiguration)


def test_assert_configure(test_factory: MockingFactory, subarray_args: SubarrayArgs):
    test_factory.set_devices_query()
    with pytest.raises(NotReadyConfigure):
        assert_I_can_configure_a_subarray(subarray_args.subarray_id, subarray_args.receptors)
