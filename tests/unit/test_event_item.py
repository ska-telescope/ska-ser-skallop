"""Testing module that tests the eventItem class"""

from datetime import datetime

import mock
import pytest
from assertpy import assert_that

from ska_ser_skallop.subscribing import base
from ska_ser_skallop.subscribing.event_item import EventItem

LIB = "ska_ser_skallop.subscribing.event_item"


class Spec:
    expected_date: datetime
    expected_device_name: str
    expected_attr_name: str
    expected_attr_value: int

    def __init__(self) -> None:
        self.expected_date: datetime = datetime.now()
        self.expected_device_name = "dummy_device_name"
        self.expected_attr_name = "dummy_attr_name"
        self.expected_attr_value = 0
        self.expected_errors = ["error1", "error2"]
        self.mocked_helpers = None
        self.event_error = False
        self.expected_described_result = set()

    def patch_helpers(self, mocked_helpers: mock.Mock):
        self.mocked_helpers = mocked_helpers
        self.expected_described_result = (
            self.expected_device_name,
            self.expected_attr_name,
            self.expected_attr_value,
            self.expected_date,
        )
        self.mocked_helpers.describe_event.return_value = self.expected_described_result
        self.mocked_helpers.get_attr_value_as_str.return_value = str(self.expected_attr_value)

    def patch_event_with_new_init_date(self, event: EventItem):
        event.init_date = self.expected_date

    # API specs

    def assert_describe_event_called(self, event_item: EventItem):
        self.mocked_helpers.describe_event.assert_called_with(event_item, self.expected_date)

    def assert_get_attr_value_as_str_called(self, attr: mock.Mock):
        self.mocked_helpers.get_attr_value_as_str.assert_called_with(attr)


@pytest.fixture(name="spec")
def fxt_spec():
    return Spec()


@pytest.fixture(name="attr_value")
def fxt_attr_value(spec):
    attr_value = mock.Mock(base.AttributeInt)
    attr_value.value = spec.expected_attr_value
    attr_value.name = spec.expected_attr_name
    attr_value.time = mock.Mock()
    attr_value.time.isoformat.return_value = spec.expected_date.isoformat()
    attr_value.time.todatetime.return_value = spec.expected_date
    return attr_value


@pytest.fixture(params=["without error", "with error"], name="event")
def fxt_event(spec: Spec, attr_value, request):
    event = base.EventDataInt(
        producer_name=f"name.{spec.expected_attr_name}",
        attr_name=f"name.{spec.expected_attr_name}",
    )
    event = mock.Mock(event, wraps=event)
    event.attr_name = f"name.{spec.expected_attr_name}"
    event.device.name.return_value = spec.expected_device_name
    if request.param == "without error":
        event.attr_value = attr_value
        spec.event_error = False
    else:
        event.attr_value = None
        event.err = True
        event.errors = spec.expected_errors
        spec.event_error = True
    return event


@pytest.fixture(name="subscription")
def fxt_subscription():
    return mock.Mock(base.SubscriptionBase)


@pytest.fixture(name="handler")
def fxt_handler():
    return mock.Mock(base.MessageHandlerBase)


def test_create_item(event, subscription, handler):
    # given an event, supscription and handler
    # when I create a new event item
    item = EventItem(event, subscription, handler)
    assert_that((item.event, item.subscription, item.handler)).is_equal_to(
        (event, subscription, handler)
    )


@pytest.fixture(name="event_item")
def fxt_event_item(event, subscription, handler, spec):
    with mock.patch(f"{LIB}.helpers") as mocked_helpers:
        spec.patch_helpers(mocked_helpers)
        event = EventItem(event, subscription, handler)
        spec.patch_event_with_new_init_date(event)
        yield event


def test_get_date_lodged(event_item, spec: Spec):
    result = event_item.get_date_lodged()
    assert_that(result).is_equal_to(spec.expected_date)


def test_get_date_init(event_item, spec: Spec):
    result = event_item.get_date_init()
    assert_that(result).is_equal_to(spec.expected_date)


def test_get_producer_name(event_item, spec: Spec):
    result = event_item.get_producer_name()
    assert_that(result).is_equal_to(spec.expected_device_name)


def test_get_attr_name(event_item, spec: Spec):
    result = event_item.get_attr_name()
    assert_that(result).is_equal_to(spec.expected_attr_name)


def test_get_attr_value_str(event_item, spec: Spec, attr_value):
    result = event_item.get_attr_value_str()
    if not spec.event_error:
        assert_that(result).is_equal_to(str(spec.expected_attr_value))
        spec.assert_get_attr_value_as_str_called(attr_value)
    else:
        assert_that(result).is_equal_to(str(spec.expected_errors))


def test_get_date_lodged_isoformat(event_item, spec: Spec):
    result = event_item.get_date_lodged_isoformat()
    assert_that(result).is_equal_to(str(spec.expected_date.isoformat()))


def test_describe(event_item, spec: Spec, event):
    result = event_item.describe()
    spec.assert_describe_event_called(event)
    assert_that(result).is_equal_to(spec.expected_described_result)


def test_handle_event_on_event_item(event_item, event, handler, subscription):
    # given an event item
    item: EventItem = event_item
    # when I call handle event on event Item
    item.handle_event()
    # I expect handle evenet to have been called on the handler
    handler.handle_event.assert_called_with(event, subscription)
    handler.print_event.assert_called_with(event)
