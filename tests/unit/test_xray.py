import json
from pathlib import Path
from typing import Dict, List, Set

import pytest
from assertpy.assertpy import assert_that
from mock import patch

from ska_ser_skallop.xray import execution_desc, execution_report, results


@pytest.fixture(name="data")
def fxt_data():
    return {
        "test_plans": ["test_plan1", "test_plan2"],
        "name": "name",
        "test_environments": ["test_environment1", "test_environment1"],
        "versions": ["version1", "version2"],
        "project_id": "project_id",
        "description": "description",
        "test_report": "test_report",
        "labels": ["label1", "label2"],
        "environmental_variables": [
            "environmental_variable1",
            "environmental_variable2",
        ],
    }


@pytest.fixture(name="environmental_variables")
def fxt_environmental_variables(data) -> List[str]:
    return data["environmental_variables"]


@pytest.fixture(name="test_exec_desc")
def fxt_test_exec_desc(data) -> execution_desc.TestExecDescTempl:
    return execution_desc.TestExecDescTempl(**data)


@pytest.fixture(name="mock_env_vars_from_os")
def fxt_mock_env_vars_from_os(environmental_variables):
    with patch("ska_ser_skallop.xray.execution_desc.os") as mock_os:
        mock_os.getenv.side_effect = environmental_variables
        yield mock_os


@pytest.mark.usefixtures("mock_env_vars_from_os")
def test_get_any_env_vars_desc(
    environmental_variables: List[str],
    test_exec_desc: execution_desc.TestExecDesc,
):
    result = execution_desc.get_any_env_vars_desc(test_exec_desc)
    assert_that(result).contains(*environmental_variables)


@pytest.fixture(name="chart_info", params=["from_str", "from_env"])
def fxt_chart_info(request, data: Dict):
    file = "tests/resources/example_chart_info.yaml"
    if request.param == "from_str":
        data["chart_info"] = file
        yield
    else:
        with patch("ska_ser_skallop.xray.execution_desc.os") as mock_os:
            data["chart_info"] = {"env": "foo", "maps_to": {"bar": file}}
            mock_os.getenv.side_effect = ["bar"]
            yield


@pytest.mark.usefixtures("chart_info")
def test_load_chart_info(test_exec_desc: execution_desc.TestExecDesc):
    result = execution_desc.get_any_chart_info(test_exec_desc)
    assert_that(result).contains(
        "ska-mid-0.9.1-dev",
        ".ska-tango-base-0.2.24",
    )


@pytest.fixture(
    name="test_name",
    params=["from_str", "from_env", "from_keywords", "from_tags"],
)
def fxt_test_name(request, data: Dict) -> str:
    name = data["name"]
    if request.param == "from_str":
        yield name
    elif request.param == "from_env":
        with patch("ska_ser_skallop.xray.execution_desc.os") as mock_os:
            data["name"] = {"env": "foo", "maps_to": {"bar": name}}
            mock_os.getenv.side_effect = ["bar"]
            yield name
    elif request.param == "from_keywords":
        data["name"] = {"select_from_keyword": {"XTP-3614": name}}
        yield name
    elif request.param == "from_tags":
        data["name"] = {"select_from_tag": {"XTP-3614": name}}
        yield name


@pytest.fixture(name="test_desc_file")
def fxt_test_desc_file(data, tmp_path: Path) -> Path:
    file_name = "test_desc_file.yaml"
    path = tmp_path / file_name
    path.write_text(json.dumps(data))
    return path


def test_load_name(
    test_name: str,
    test_desc_file: Path,
    keywords: Set[str],
    test_tags: Set[str],
):
    template = execution_desc.load_test_execution_template(test_desc_file)
    result = execution_desc.get_test_execution_description(template, keywords, test_tags)
    assert_that(result["name"]).is_equal_to(test_name)


@pytest.fixture(name="report_data")
def fxt_report_data() -> Dict:
    return {
        "duration": 1637164437.443158,
        "created": 19.233025312423706,
        "summary": {"passed": 19, "total": 19, "collected": 19},
        "tests": [
            {
                "nodeid": "central_node]",
                "lineno": 162,
                "outcome": "passed",
                "keywords": [
                    "__scenario__",
                    "XTP-3613",
                    "SKA_mid",
                    "ska_mid/tm_central/central_node",
                    "tests",
                    "test_internal_model_mid[ska_mid/tm_central/central_node]",
                    "XTP-3614",
                    "XTP-3615",
                    "pytestmark",
                    "parametrize",
                    "acceptance",
                    "post_deployment",
                    "test_central_node.py",
                    "acceptance/__init__.py",
                ],
            },
            {
                "nodeid": "central_node-On]",
                "lineno": 162,
                "outcome": "passed",
                "keywords": [
                    "__scenario__",
                    "SKA_mid",
                    "acceptance/__init__.py",
                    "tests",
                    "XTP-3614",
                    "XTP-3615",
                    "pytestmark",
                    "parametrize",
                    "acceptance",
                    "post_deployment",
                    "ska_mid/tm_central/central_node-On",
                    "XTP-3612",
                    "test_run_commands_mid[ska_mid/tm_central/central_node-On]",
                    "usefixtures",
                ],
            },
        ],
    }


@pytest.fixture(name="test_report")
def fxt_test_report(report_data) -> execution_report.ReportData:
    return execution_report.ReportData(**report_data)


def test_get_keywords(test_report):
    result = execution_report.get_keywords(test_report)
    assert_that(result).is_equal_to(
        {
            "test_central_node.py",
            "usefixtures",
            "post_deployment",
            "XTP-3612",
            "ska_mid/tm_central/central_node",
            "XTP-3615",
            "acceptance/__init__.py",
            "XTP-3613",
            "__scenario__",
            "pytestmark",
            "parametrize",
            "ska_mid/tm_central/central_node-On",
            "SKA_mid",
            "test_internal_model_mid[ska_mid/tm_central/central_node]",
            "tests",
            "acceptance",
            "XTP-3614",
            "test_run_commands_mid[ska_mid/tm_central/central_node-On]",
        }
    )


@pytest.fixture(name="keywords")
def fxt_keywords(test_report) -> Set[str]:
    return execution_report.get_keywords(test_report)


@pytest.fixture(name="test_results")
def fxt_test_results(tmp_path: Path):
    results = [
        {
            "keyword": "Feature",
            "uri": "integration/../features/centralnode.feature",
            "name": "Central Node acceptance",
            "id": "integration/../features/centralnode.feature",
            "description": "",
            "tags": [{"name": "XTP-3615", "line": 1}],
            "elements": [
                {
                    "keyword": "Scenario",
                    "id": "test_internal_model_mid[ska_mid/tm_central/central_node]",
                    "name": "Monitor Telescope Components",
                    "description": "",
                    "tags": [
                        {"name": "XTP-3613", "line": 12},
                        {"name": "XTP-3614", "line": 12},
                    ],
                    "type": "scenario",
                },
                {
                    "keyword": "Scenario",
                    "id": "test_run_commands_mid[ska_mid/tm_central/central_node-On]",
                    "name": "Ability to run commands on central node",
                    "description": "",
                    "tags": [
                        {"name": "XTP-3612", "line": 5},
                        {"name": "XTP-3614", "line": 5},
                    ],
                    "type": "scenario",
                },
            ],
        }
    ]
    test_results = tmp_path / "test_results.json"
    test_results.write_text(json.dumps(results))
    return test_results


def test_get_tags(test_results: Path):
    tags = results.load_results(test_results).tags
    assert_that(tags).is_equal_to({"XTP-3614"})


@pytest.fixture(name="test_tags")
def fxt_test_tags(test_results) -> Set[str]:
    return results.load_results(test_results).tags
