import logging
from typing import List

from assertpy import assert_that

from ska_ser_skallop.connectors.remoting.remote_devices import RemoteDeviceProxy
from ska_ser_skallop.connectors.remoting.tangobridge.tangobridge import TangoBridge

logger = logging.getLogger(__name__)


def test_command(
    test_device_mock,
    device_name: str,
    mock_tango_bridge: TangoBridge,
    commands_and_their_responses,
):
    proxy = RemoteDeviceProxy(device_name, mock_tango_bridge)
    for command, response in commands_and_their_responses:
        arg = command  # command and arg are echos
        result = proxy.command_inout(command, arg)
        assert_that(result).is_equal_to(response)


def test_load_commands(
    test_device_mock,
    device_name,
    mock_tango_bridge: TangoBridge,
    command_names: List[str],
):
    proxy = RemoteDeviceProxy(device_name, mock_tango_bridge)
    result = proxy._load_commands()
    assert_that([command.name for command in result]).is_equal_to(command_names)


def test_load_attributes(
    test_device_mock,
    device_name,
    mock_tango_bridge: TangoBridge,
    attribute_names: List[str],
):
    proxy = RemoteDeviceProxy(device_name, mock_tango_bridge)
    result = proxy._load_attributes()
    assert_that([attr.name for attr in result]).is_equal_to(attribute_names)


def test_read_attribute(
    test_device_mock,
    device_name,
    mock_tango_bridge: TangoBridge,
    attributes_and_their_responses,
):
    proxy = RemoteDeviceProxy(device_name, mock_tango_bridge)
    for attr, response in attributes_and_their_responses:
        result = proxy.read_attribute(attr)
        assert_that(result.value).is_equal_to(response)


def test_device_ping(test_device_mock, device_name, mock_tango_bridge: TangoBridge):
    proxy = RemoteDeviceProxy(device_name)
    result = proxy.ping()
    logger.info(result)
