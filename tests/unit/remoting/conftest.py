import datetime
from typing import Any, Callable, Dict, List, Tuple

import pytest

from ska_ser_skallop.connectors.remoting.tangobridge import queries
from ska_ser_skallop.connectors.remoting.tangobridge.factories import TBridgeFactory
from ska_ser_skallop.connectors.remoting.tangobridge.tangobridge import TangoBridge

from .mocks import MockSUTFactory


@pytest.fixture(name="device_name")
def fxt_device_name():
    return "sys/tg_test/1"


@pytest.fixture(name="attribute_name")
def fxt_attribute_name():
    return "double_scalar"


@pytest.fixture(name="mock_sut_factory")
def fxt_mock_sut_factory() -> TBridgeFactory:
    return MockSUTFactory()


Value = str
DeviceName = str
Attribute = str
AttributeEventGenerator = Callable[[Value, DeviceName, Attribute], Dict]


@pytest.fixture(name="ws_attribute_event_function")
def fxt_ws_attribute_event_function(mock_date) -> AttributeEventGenerator:
    def fxt_function(value: str, device_name: str, attr: str):
        return {
            "type": "data",
            "payload": {
                "data": {
                    "attributes": {
                        "device": device_name,
                        "attribute": attr,
                        "value": value,
                        "timestamp": mock_date,
                    }
                }
            },
        }

    return fxt_function


@pytest.fixture(name="mock_tango_bridge")
def fxt_mock_tango_bridge(mock_sut_factory: TBridgeFactory) -> TangoBridge:
    tango_bridge = TangoBridge(mock_sut_factory, 0.2)
    yield tango_bridge
    tango_bridge.tear_down_connections()


@pytest.fixture(name="fetch_attributes_query")
def fxt_fetch_attributes_query(device_name) -> queries.GQLQuery:
    return queries.fetch_attributes(device_name)


@pytest.fixture(name="fetch_commands_query")
def fxt_fetch_commands_query(device_name) -> queries.GQLQuery:
    return queries.fetch_commands(device_name)


@pytest.fixture(name="read_attribute_queries")
def fxt_read_attribute_queries(device_name, attribute_names) -> List[queries.GQLQuery]:
    return [queries.read_attribute(device_name, name) for name in attribute_names]


@pytest.fixture(name="commands")
def fxt_commands(device_name, command_names) -> List[queries.GQLQuery]:
    return [queries.command(device_name, name, name) for name in command_names]


@pytest.fixture(name="attribute_names")
def fxt_attribute_names():
    return ["state", "short_scalar", "boolean_scalar"]


@pytest.fixture(name="attribute_values")
def fxt_attribute_values():
    return [1.0, 1, True]


@pytest.fixture(name="attributes")
def fxt_attributes(attribute_names, attribute_values):
    return zip(attribute_names, attribute_values)


@pytest.fixture(name="command_names")
def fxt_command_names():
    return ["DevString", "DevFloat", "DevBoolean"]


@pytest.fixture(name="command_response_values")
def fxt_command_response_values():
    return ["DevString", 1.0, True]


@pytest.fixture(name="attribute_objects")
def fxt_attribute_objects(attribute_names):
    return {
        "attributes": [
            {"name": name, "label": name, "dataformat": name, "datatype": name}
            for name in attribute_names
        ]
    }


@pytest.fixture(name="fetch_attributes_result")
def fxt_fetch_attributes_result(attribute_objects):
    return {"data": {"device": attribute_objects}}


@pytest.fixture(name="command_objects")
def fxt_command_objects(command_names):
    return {
        "commands": [
            {
                "name": name,
                "intype": name,
            }
            for name in command_names
        ]
    }


@pytest.fixture(name="fetch_commands_result")
def fxt_fetch_commands_result(command_objects):
    return {"data": {"device": command_objects}}


@pytest.fixture(name="commands_and_their_responses")
def fxt_commands_and_their_responses(
    command_names: str, command_response_values: str
) -> List[Tuple[str, str]]:
    return list(zip(command_names, command_response_values))


@pytest.fixture(name="command_responses")
def fxt_command_responses(commands_and_their_responses) -> List[Dict]:
    return [
        {
            "data": {
                "executeCommand": {
                    "message": name,
                    "ok": True,
                    "output": response,
                }
            }
        }
        for name, response in commands_and_their_responses
    ]


@pytest.fixture(name="mock_date")
def fxt_mock_date():
    return datetime.datetime(1978, 1, 1, 1).timestamp()


@pytest.fixture(name="attribute_responses")
def fxt_attribute_responses(attributes, mock_date) -> List[Dict]:
    return [
        {"data": {"attributes": [{"name": name, "value": True, "timestamp": mock_date}]}}
        for name, value in attributes
    ]


@pytest.fixture(name="attributes_and_their_responses")
def fxt_attributes_and_their_responses(
    attribute_names: List[str], attribute_values: List[str]
) -> List[Tuple[str, str]]:
    return list(zip(attribute_names, attribute_values))


@pytest.fixture(name="mock_query_results")
def fxt_mock_query_results(
    fetch_attributes_query: queries.GQLQuery,
    fetch_attributes_result: Dict,
    fetch_commands_query: queries.GQLQuery,
    fetch_commands_result: Dict,
    read_attribute_queries: List[queries.GQLQuery],
    attribute_responses: List[str],
    commands: List[queries.GQLQuery],
    command_responses: List[Any],
):
    commands_mapping = {hash(key): value for key, value in zip(commands, command_responses)}
    attributes_mapping = {
        hash(key): value for key, value in zip(read_attribute_queries, attribute_responses)
    }

    mock_query_results = {
        hash(fetch_attributes_query): fetch_attributes_result,
        hash(fetch_commands_query): fetch_commands_result,
        **commands_mapping,
        **attributes_mapping,
    }
    return mock_query_results


@pytest.fixture(name="mock_query")
def fxt_mock_query(mock_query_results) -> Callable:
    def mock_query(query, variables, *_, **__):
        key = hash(queries.GQLQuery(query, variables))
        return mock_query_results[key]

    return mock_query


@pytest.fixture(name="test_device_mock")
def fxt_test_device_mock(mock_sut_factory: TBridgeFactory, mock_query):
    graphql_mock, _ = mock_sut_factory.get_graphql_client()
    graphql_mock.execute = mock_query
