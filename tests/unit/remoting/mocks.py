import asyncio
import json
from threading import Event, Lock
from typing import Any, List, NamedTuple

import websockets
from mock import Mock, patch
from python_graphql_client import GraphqlClient

from ska_ser_skallop.connectors.remoting.tangobridge.authentication import (
    AuthenticatedUser,
    Authenticator,
)
from ska_ser_skallop.connectors.remoting.tangobridge.configuration import Environment
from ska_ser_skallop.connectors.remoting.tangobridge.factories import TangoGQLClient, TBridgeFactory


class Act(NamedTuple):
    name: str
    event: Any


class Scene(NamedTuple):
    name: str
    trigger: Any
    actions: List[Act]


class Scenario:
    def __init__(self, scenes: List[Scene]) -> None:
        self.scene_count = 0
        self.index = 0
        self.current_scene = []
        self.scenes: List[Scene] = scenes
        self.state = "Pending"
        self.pending_scene = self.scenes[self.scene_count]

    def step(self):
        self.index += 1

    def get_next_act(self, event: Any) -> Any:
        if self.state == "Pending":
            if event != self.pending_scene.trigger:
                return None
            self.state = "Running"
            self.current_scene = self.pending_scene.actions
        return self.current_scene[self.index]


subscribe_to_A_trigger = {}

example_scenario = [
    Scene(
        "change from A to B",
        subscribe_to_A_trigger,
        [Act("Act1", {}), Act("Act2", {})],
    )
]


class Spy:
    def __init__(self, scenario: Scenario) -> None:
        self.scenario = scenario

    def push_event(self, event, caller: "WSMock"):
        response = self.scenario.get_next_act(event)
        if response:
            caller.push_event(event)


class WSMock:
    def __init__(self) -> None:
        self.queue = asyncio.Queue()
        self.running = Event()
        self.spies: List[Spy] = []
        self.running.set()
        self.lock = Lock()

    def add_scenario_spy(self, spy: Spy):
        self.spies.append(spy)

    def push_event(self, item):
        encoded = json.dumps(item)
        self.queue.put_nowait(encoded)

    async def send(self, message):
        self.queue.put_nowait(message)
        with self.lock:
            for spy in self.spies:
                spy.push_event(message, self)

    async def close(self):
        self.running.clear()

    async def __aiter__(self):
        while self.running.is_set():
            yield await self.queue.get()


class MockSUTFactory(TBridgeFactory):
    def get_graphql_client(self, *args, **kwargs):
        graphql_mock = self.mocks.get("graphql_mock")
        url = self.get_tango_gql_service_url()
        if graphql_mock:
            return TangoGQLClient(graphql_mock, url)
        graphql_mock = Mock(GraphqlClient)
        self.mocks["graphql_mock"] = graphql_mock
        return TangoGQLClient(graphql_mock, url)

    def get_new_authenticated_user(self) -> AuthenticatedUser:
        with patch(
            "ska_ser_skallop.connectors.remoting.tangobridge.authentication.requests." "post"
        ) as mock_post:
            mock_result = mock_post.return_value
            mock_result.status_code = 200
            mock_result.cookies.get_dict.return_value = {"taranta_jwt": "mock_cookies"}
            authenticator = Authenticator(self.env)
            self._authenticated_user = authenticator.get_authenticated_user()
        return self._authenticated_user

    @property
    def env(self):
        if self._env:
            return self._env
        self._env = Environment(
            username="username",
            password="password",
            domain="integration",
            kube_branch="green-skampi",
            telescope="mid",
            kubehost="k8s.skao.stfc",
        )
        return self._env

    def get_mock_websocket(self) -> WSMock:
        ws_mock = self.mocks.get("mock_ws")
        if ws_mock:
            return ws_mock
        if not self.controller:
            self.get_controller()
        ws_mock = WSMock()
        self.mocks["mock_ws"] = ws_mock
        return ws_mock

    def get_websockets(self):
        ws_mock = self.get_mock_websocket()
        mock = Mock(websockets)

        async def connect(*_, **__):
            return ws_mock

        mock.connect = connect
        return mock
