import logging
from queue import Queue
from typing import Callable, Dict

import pytest
from assertpy import assert_that

from ska_ser_skallop.connectors.remoting.tangobridge.subscribing import DeviceAttributeSubscriber
from ska_ser_skallop.connectors.remoting.tangobridge.wscontrol import WSController
from ska_ser_skallop.subscribing.helpers import describe_event

from .mocks import MockSUTFactory

logger = logging.getLogger(__name__)


# final testing code


def test_authenticator(mock_sut_factory):
    auth_user = mock_sut_factory.get_new_authenticated_user()
    assert_that(auth_user.cookies["taranta_jwt"]).is_equal_to("mock_cookies")


def log_event(event):
    results = describe_event(event)
    logger.info(f"{results[0]:<20}{results[1]:<20}{results[2]:<20}{results[3]:<20}")


# TODO generate end to end tests simulating different event scenarios
# def test_tango_bridge_subscription(device_name, attribute_name):
#    bridge = TangoBridge()
#    queue = Queue()
#    def cb(item):
#        queue.put(item)
#    #subscriber = TestSubscriber()
#    cb_subscription_id = bridge.add_subscription(device_name, attribute_name, cb)
#    #subscriber_subscription_id = bridge.add_subscription(
#    #    device_name, attribute_name, subscriber
#    #)
#    #buffer_subscription_id = bridge.add_subscription(device_name, attribute_name, 10)
#    count = 10
#    while count:
#        event = queue.get()
#        log_event(event)
#        #event = next(subscriber.events())
#        #log_event(event)
#        count -= 1
#    #events = bridge.get_events(device_name, attribute_name, buffer_subscription_id)
#    bridge.remove_subscription(device_name, attribute_name, cb_subscription_id)
#    #bridge.remove_subscription(device_name, attribute_name, subscriber_subscription_id)
#    #bridge.remove_subscription(device_name, attribute_name, buffer_subscription_id)
#    #for event in events:
#    #    log_event(event)
#    with pytest.raises(Empty):
#        item = queue.get(timeout = 1)
#    #with pytest.raises(Empty):
#    #    event = next(subscriber.events(1))
#    time.sleep(10)
#    bridge.teardown()
#
# def test_remote_device_proxy_subscription(device_name, attribute_name):
#    proxy = RemoteDeviceProxy(device_name)
#    #queue = Queue()
#    #def cb(item):
#    #    queue.put(item)
#    subscriber = LogSubscriber()
#    #cb_subscription_id = proxy.subscribe_event(attribute_name, 'any', cb)
#    subscriber_subscription_id = proxy.subscribe_event(
#        attribute_name, 'any', subscriber
#    )
#    #buffer_subscription_id = proxy.subscribe_event(attribute_name, 'any', 10)
#    count = 10
#    #while count:
#        #event = queue.get()
#        #log_event(event)
#    #    event = next(subscriber.events(2))
#    #    log_event(event)
#    #    count -= 1
#    subscriber.log_events(5)
#    #time.sleep(10)
#    #events = proxy.get_events(buffer_subscription_id)
#    #proxy.unsubscribe_event(cb_subscription_id)
#    proxy.unsubscribe_event(subscriber_subscription_id)
#    #proxy.unsubscribe_event(buffer_subscription_id)
#    #for event in events:
#    #    log_event(event)
#    #with pytest.raises(Empty):
#    #    item = queue.get(timeout = 1)
#    #with pytest.raises(Empty):
#    #    event = next(subscriber.events(1))
#    proxy.tear_down_connections()


@pytest.fixture(name="attribute_names")
def fxt_attribute_names():
    return ["short_scalar_ro", "short_scalar_rww"]


@pytest.mark.skip(reason="test fails in gitlab env but not in local env")
def test_subscribe_to_ws_controller(
    mock_sut_factory: MockSUTFactory,
    ws_attribute_event_function: Callable[[str, str, str], Dict],
):
    controller = WSController(mock_sut_factory)
    sub = DeviceAttributeSubscriber(controller, polling_rate=0.2)
    queue = Queue()
    sub.add_subscription("dummy", "attr", lambda data: queue.put_nowait(data.attr_value.value))
    event = ws_attribute_event_function("value", "dummy", "attr")
    ws = mock_sut_factory.get_mock_websocket()
    ws.push_event(event)
    result = queue.get(timeout=1)
    assert_that(result).is_equal_to("value")
