"""Testing module that tests the examples for explaining subscribing"""

import os
from typing import List

import pytest

from ska_ser_skallop.connectors import configuration as conf
from ska_ser_skallop.subscribing.base import EventDataInt
from ska_ser_skallop.subscribing.examples.subscribing_example import handle_events, subscribe
from ska_ser_skallop.subscribing.message_board import MessageBoard
from ska_ser_skallop.subscribing.producers import Producer

cwd = os.getcwd()
LIB = "ska_ser_skallop.subscribing.examples.subscribing_example"


@pytest.fixture(name="mock_producers")
def fxt_mock_producers() -> List[Producer]:
    return [Producer("sys/tg_test/1"), Producer("sys/tg_test/1")]


@pytest.fixture(name="testing_factory")
def fxt_test_factory(mock_producers: List[Producer]):
    with conf.patch_factory_for_testing() as testing_factory:
        for producer in mock_producers:
            testing_factory.inject_producer(producer.name(), producer)
        yield testing_factory


@pytest.fixture(name="expected_values")
def fxt_expected_values():
    return ["not_expected", "not_expected", "expected"]


@pytest.mark.parametrize("option", ["handle most recent", "block until all handled"])
@pytest.mark.usefixtures("testing_factory")
def test_example(option, mock_producers, expected_values):
    board = MessageBoard()
    subscribe(board)
    for val in expected_values:
        mock_producers[0].push_event("state", EventDataInt(mock_producers[0].name(), "state", val))
        mock_producers[1].push_event(
            "short_scalar",
            EventDataInt(mock_producers[1].name(), "short_scalar", val),
        )
    handle_events(board, option)
