"""Testing module that tests producers module"""

import pytest
from assertpy import assert_that
from mock import MagicMock

from ska_ser_skallop.subscribing import base
from ska_ser_skallop.subscribing.producers import Producer


@pytest.fixture(name="dummy_subscriber")
def fxt_dummy_subscriber():
    return MagicMock(base.Subscriber)


@pytest.mark.parametrize("polling", [False, True])
def test_subscribe_to_test_producer(polling, dummy_subscriber):
    # given a producer and a subscriber
    prod = Producer("dummy")
    # when that subscriber subscribe to events on an attribute
    if not polling:
        sub_id = prod.subscribe_event("dummy_attr", None, dummy_subscriber)
    else:
        sub_id = prod.subscribe_event("dummy_attr", None, 100)
    # I expect an id to be given as the first index 1
    assert_that(sub_id).is_equal_to(1)


@pytest.fixture(name="running_subscription")
def fxt_running_subscription(dummy_subscriber):
    prod = Producer("dummy")
    attr = "dummy_attr"
    sub_id = prod.subscribe_event(attr, None, dummy_subscriber)
    return prod, sub_id, attr


@pytest.fixture(name="dummy_event")
def fxt_dummy_event():
    return MagicMock(base.EventDataInt)


def test_push_event(running_subscription, dummy_subscriber, dummy_event):
    # given a running subscription
    producer, _, attr = running_subscription
    # when I push an event on the producer
    producer.push_event(attr, dummy_event)
    # I expect the dummy_subscriber to have been notifed with that event
    dummy_subscriber.push_event.assert_called_with(dummy_event)


def test_unsubscribe(running_subscription, dummy_subscriber, dummy_event):
    # given a running subscription given by an id
    producer, sub_id, attr = running_subscription
    # when I unsubscribe using that id
    producer: Producer = producer
    producer.unsubscribe_event(sub_id)
    # then I expect any subsequent events pushed to not be pused onto the subscriber
    producer.push_event(attr, dummy_event)
    dummy_subscriber.push_event.assert_not_called()


def test_describe_subscription(running_subscription, dummy_subscriber):
    # given a running subscription given by an id
    producer, sub_id, attr = running_subscription
    # when I call the producer to describe a subscription
    result = producer.describe_subscription(sub_id)
    # then I expect the result to be the same as the attr and the dummy subscriber
    assert_that(result).is_equal_to({"attr": attr, "subscriber": dummy_subscriber})


@pytest.fixture(name="running_polled_subscription")
def fxt_running_polled_subscription():
    prod = Producer("dummy")
    attr = "dummy_attr"
    sub_id = prod.subscribe_event(attr, None, 100)
    return prod, sub_id, attr


def test_push_event_polling(running_polled_subscription, dummy_event):
    # given a subscription set up as polling
    producer, sub_id, attr = running_polled_subscription
    # when I push an event on the producer
    producer.push_event(attr, dummy_event)
    # I expect to get that event when I call get events on that subscription
    result = producer.get_events(sub_id)
    assert_that(result).is_equal_to([dummy_event])
