from unittest import mock

import pytest

from ska_ser_skallop.connectors import configuration as conf
from ska_ser_skallop.connectors.mockingfactory import MockingFactory
from ska_ser_skallop.mvp_control.telescope.start_up import (
    NotReadyShutdown,
    NotReadyStartUp,
    assert_I_can_set_telescope_to_running,
    assert_I_can_set_telescope_to_standby,
)


@pytest.fixture(name="mocked_device_proxy")
def fxt_test_factory():
    testing_factory = MockingFactory()
    with conf.patch_factory(testing_factory, "test fixture"):
        mocked_device_proxy = testing_factory.set_device_spy()
        testing_factory.set_devices_query()
        yield mocked_device_proxy


@pytest.mark.xfail(reason="prelim tests gives false positives")
def test_devices_in_expected_state():
    """Test that if all the devices has state OFF and obsState empty, then we can set the
    telescope to running
    """
    with mock.patch(
        "ska_ser_skallop.mvp_control.describing.base.DeviceProxy"
    ) as mocked_device_proxy_class:
        obsState_mock = mock.MagicMock()
        obsState_mock.name = "EMPTY"

        State_mock = mock.MagicMock()
        State_mock.name = "OFF"

        DeviceProxy_mock = mock.MagicMock()
        DeviceProxy_mock.State.return_value = State_mock
        DeviceProxy_mock.obsState = obsState_mock
        mocked_device_proxy_class.return_value = DeviceProxy_mock
        assert_I_can_set_telescope_to_running()


@pytest.mark.xfail(reason="prelim tests gives false positives")
def test_obsState_wrong_state():
    """Test that if the obsState is not EMPTY then an exception is raised."""
    with pytest.raises(NotReadyStartUp) as excinfo:
        with mock.patch(
            "ska_ser_skallop.mvp_control.describing.base.DeviceProxy"
        ) as mocked_device_proxy_class:
            obsState_mock = mock.PropertyMock()
            obsState_mock.name = "NOTEMPTY"

            State_mock = mock.MagicMock()
            State_mock.name = "OFF"

            DeviceProxy_mock = mock.MagicMock()
            DeviceProxy_mock.State.return_value = State_mock
            DeviceProxy_mock.obsState = obsState_mock
            mocked_device_proxy_class.return_value = DeviceProxy_mock

            assert_I_can_set_telescope_to_running()

    for device in [
        "ska_mid/tm_subarray_node/1",
        "mid_sdp/elt/subarray_2",
        "mid_csp/elt/subarray_01",
    ]:
        assert f"expected {device} to be in ('EMPTY',) but instead was NOTEMPTY" in str(
            excinfo.value
        )


@pytest.mark.xfail(reason="prelim tests gives false positives")
def test_state_is_wrong():
    """Test that if the State is not OFF then an exception is raised."""
    with pytest.raises(NotReadyStartUp) as excinfo:
        with mock.patch(
            "ska_ser_skallop.mvp_control.describing.base.DeviceProxy"
        ) as mocked_device_proxy_class:
            obsState_mock = mock.PropertyMock()
            obsState_mock.name = "EMPTY"

            State_mock = mock.MagicMock()
            State_mock.name = "ON"

            DeviceProxy_mock = mock.MagicMock()
            DeviceProxy_mock.State.return_value = State_mock
            DeviceProxy_mock.obsState = obsState_mock
            mocked_device_proxy_class.return_value = DeviceProxy_mock

            assert_I_can_set_telescope_to_running()

    for device in ["mid_csp/elt/master", "mid_csp_cbf/sub_elt/master"]:
        assert f"expected {device} to be in ('OFF', 'STANDBY') but instead was ON" in str(
            excinfo.value
        )


def test_assert_I_can_set_telescope_to_standby_fails_when_devices_are_in_wrong_state(
    mocked_device_proxy,
):
    with pytest.raises(NotReadyShutdown):
        assert_I_can_set_telescope_to_standby(nr_of_subarrays=1, nr_of_receptors=1)
