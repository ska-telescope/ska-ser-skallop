"""
Testing module that tests the the basic functionality of generators in configuration
module
"""

from ska_ser_skallop.mvp_control.configuration import generators as gen

example1 = [
    [0, 1],
    [744, 0],
    [1488, 0],
    [2232, 0],
    [2976, 0],
    [3720, 0],
    [4464, 0],
    [5208, 0],
    [5952, 0],
    [6696, 0],
    [7440, 0],
    [8184, 0],
    [8928, 0],
    [9672, 0],
    [10416, 0],
    [11160, 0],
    [11904, 0],
    [12648, 0],
    [13392, 0],
    [14136, 0],
]

example3 = [[0, 0], [200, 1]]

example4 = [[0, 4], [200, 5]]


def test_gen_averaging():
    assert gen.generate_channel_averaging(744, 0, 1, 20) == example1


def test_distribute():
    assert gen.distribute(0, 200, 2) == [0, 200]


def test_merge():
    assert gen.merge([1, 2], [3, 4]) == [[1, 3], [2, 4]]


def test_merge_and_distribute():
    val = gen.merge(gen.distribute(0, 200, 2), gen.distribute(4, 1, 2))
    assert val == example4
