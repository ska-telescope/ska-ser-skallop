from contextlib import ExitStack

import pytest
from assertpy import assert_that

from ska_ser_skallop.mvp_control.describing.mvp_names import DeviceName, set_scope
from ska_ser_skallop.mvp_fixtures.context_management import TelescopeContext


@pytest.fixture(name="telescope_context")
def fxt_telescope():
    with ExitStack() as session_stack:
        yield TelescopeContext(session_stack)


@pytest.fixture(name="device_in_scope")
def fxt_device():
    return DeviceName("test_device", "in_scope")


@pytest.fixture(name="device_out_of_scope")
def fxt_device_out_of_scope():
    return DeviceName("test_device", "out_of_scope")


def test_set_scope_context(device_in_scope: DeviceName, device_out_of_scope: DeviceName):
    with set_scope("in_scope"):
        assert_that(device_in_scope.enabled).is_true()
        assert_that(device_out_of_scope.enabled).is_false()
    assert_that(device_in_scope.enabled).is_true()
    assert_that(device_out_of_scope.enabled).is_true()
