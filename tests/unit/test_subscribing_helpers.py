"""Testing module that tests the helpers module of subscribing"""

from datetime import datetime
from typing import List

import pytest
from assertpy import assert_that

from ska_ser_skallop.subscribing import helpers
from ska_ser_skallop.subscribing.base import AttributeInt, EventDataInt


@pytest.fixture(
    params=[
        "obsState",
        "obsstate",
        "dishpointingstate",
        "dishPointingstate",
        "pointingstate",
        "pointingState",
        "dishPointingState",
    ],
    name="named_attribute",
)
def fxt_named_attribute(request):
    devattr = AttributeInt(request.param, 0)
    devattr.name = request.param
    devattr.value = 0
    return devattr


@pytest.fixture(name="tracers")
def fxt_tracers() -> List[helpers.Tracer]:
    tracers = [helpers.Tracer() for _ in range(3)]
    for index in range(10):
        for i, tracer in enumerate(tracers):
            tracer.message(f"message {index} on tracer {i}")
    return tracers


def test_print_tracer(tracers):
    # given a set of tracers with messages
    # when I print all their messages
    result = helpers.print_tracers(tracers)
    # logging.info(f'\n{result}')
    assert_that(result).is_type_of(str)


def test_get_attr_value_as_str(named_attribute):
    assert_that(helpers.get_attr_value_as_str(named_attribute)).is_type_of(str)


def dummy_event():
    event = EventDataInt("dummy", "dummy", 0)
    return event


def error_event():
    event = EventDataInt("dummy", "dummy", None)
    event.attr_value = None
    event.attr_name = "dummy.dummy"
    event.err = True
    event.errors = ["dummy", "dummy"]
    return event


@pytest.fixture(params=[dummy_event(), error_event()], name="event_data")
def fxt_event_data(request):
    """
    Create a fixture to provide a set of event data.

    There are two cases: dummy_event and error_event.
    """
    return request.param


def test_get_device_name(event_data):
    assert_that(helpers.get_device_name(event_data)).is_type_of(str)


def test_get_attr_name(event_data):
    assert_that(helpers.get_attr_name(event_data)).is_type_of(str)


def test_get_attr_value_str(event_data):
    assert_that(helpers.get_attr_value_str(event_data)).is_type_of(str)


def test_get_date_lodged(event_data):
    assert_that(helpers.get_date_lodged(event_data)).is_type_of(datetime)


def test_describe_event(event_data):
    result = helpers.describe_event(event_data)
    assert all(isinstance(item, str) for item in result)


def test_unpack_event(event_data):
    device_name, attr, value, date = helpers.unpack_event(event_data)
    assert all(isinstance(item, str) for item in [device_name, attr, value])
    assert_that(date).is_type_of(datetime)


@pytest.fixture(name="tracer_with_messages")
def fxt_tracer_with_messages():
    # given a tracer with three messages written to it
    tracer = helpers.Tracer()
    # when I write a three messages to it:
    messages = ["message1", "message2", "message3"]
    for msg in messages:
        tracer.message(msg)
    return tracer, messages


@pytest.mark.parametrize("init_message", [None, "message0"])
def test_tracer_message(init_message):
    # given a tracer
    tracer = helpers.Tracer(init_message)
    # with n messages
    n_of_messages = len(tracer.messages)
    # when I send it a message
    tracer.message("message1")
    # then I expect it to be updated with a new message
    assert_that(tracer.messages).is_length(n_of_messages + 1)


def every_second_item(the_str, split=None):
    the_list = the_str.split(split)
    return the_list[1::2]


def every_first_item(the_str, split=None):
    the_list = the_str.split(split)
    return the_list[::2]


def is_isoformat_date(the_date):
    try:
        datetime.fromisoformat(the_date)
    except ValueError:
        return False
    return True


def test_tracer_print(tracer_with_messages):
    tracer, messages = tracer_with_messages
    result = tracer.print_messages()
    assert_that(every_second_item(result)).is_equal_to(messages)
    dates_with_colon = every_first_item(result)
    dates_without_colon = [item[:-1] for item in dates_with_colon]
    assert all(is_isoformat_date(item) for item in dates_without_colon)


def log_message_with_timestamp():
    return "log_message_with_timestamp", datetime(1, 1, 1, 0, 0)


def log_message_without_timestamp():
    return ("log_message_without_timestamp",)


def log_message_with_label():
    return "log_message_with_label", datetime(1, 1, 1, 0, 0), "log"


def log_message_without_timestamp_label():
    return "log_message_without_timestamp_label", None, "log"


@pytest.mark.parametrize(
    "message",
    [
        log_message_with_timestamp(),
        log_message_without_timestamp(),
        log_message_with_label(),
        log_message_without_timestamp_label(),
    ],
)
def test_logbook_log(message):
    # given a logbook
    logbook = helpers.LogBook()
    nr_of_messages = len(logbook.messages)
    # when I log a message
    logbook.log(*message)
    # I expect the message to have increased by 1
    assert_that(logbook.messages).is_length(nr_of_messages + 1)


@pytest.fixture(name="populated_logbook")
def fxt_populated_logbook():
    messages = [
        log_message_with_timestamp(),
        log_message_without_timestamp(),
        log_message_with_label(),
        log_message_without_timestamp_label(),
    ]
    logbook = helpers.LogBook()
    for msg in messages:
        logbook.log(*msg)
    ordered_messages = [
        log_message_with_timestamp(),
        log_message_with_label(),
        log_message_without_timestamp(),
        log_message_without_timestamp_label(),
    ]
    return logbook, ordered_messages


def with_log_label(message):
    if len(message) == 3:
        return bool(message[2] == "log")
    return False


@pytest.mark.parametrize("filter_log", [False, True])
def test_logbook_read(populated_logbook, filter_log):
    # given a pre populated logbook
    logbook, messages = populated_logbook
    # when I read it
    result = logbook.read(filter_log)
    if filter_log:
        assert_that(every_second_item(result)).is_equal_to(
            [m[0] for m in messages if not with_log_label(m)]
        )
    else:
        assert_that(every_second_item(result)).is_equal_to([m[0] for m in messages])
    assert all(is_isoformat_date(item) for item in every_first_item(result))
