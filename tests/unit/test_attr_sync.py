from typing import List

import mock
import pytest
from assertpy.assertpy import assert_that

from ska_ser_skallop.connectors import configuration
from ska_ser_skallop.connectors.mockingfactory import MockingFactory
from ska_ser_skallop.connectors.tango.tangoqueries import get_new_tango_device_query
from ska_ser_skallop.event_handling.handlers import WaitUntilEqual
from ska_ser_skallop.mvp_control.base import AbstractDeviceProxy
from ska_ser_skallop.mvp_control.event_waiting.attr_synching import wait_until_attrs_in_sync
from ska_ser_skallop.subscribing.base import AttributeInt, MessageBoardBase, SubscriptionBase

EMPTY = ["EMPTY" for _ in range(1, 4)]
RESOURCING = ["RESOURCING" for _ in range(1, 4)]
IDLE = ["IDLE" for _ in range(1, 4)]

env = {"obsstates": EMPTY}


@pytest.fixture(name="factory")
def fxt_factory():
    factory = MockingFactory()
    device_query = get_new_tango_device_query(factory)
    factory.inject_devices_query(device_query)
    with configuration.patch_factory(factory):
        yield factory


@pytest.fixture(name="test_devices")
def fxt_test_devices():
    return [
        "mid_csp/elt/subarray_01",
        "mid_sdp/elt/subarray_1",
        "ska_mid/tm_subarray_node/1",
    ]


@pytest.fixture(name="obsstates")
def fxt_obsstates():
    return ["obsstate", "obsstate", "obsstate"]


@pytest.fixture(name="mock_subscriptions")
def fxt_mock_subscriptions(test_devices: List[str], obsstates: List[str]):
    mock_subscriptions = set()
    for device_name, attr, value in zip(test_devices, obsstates, IDLE):
        mock_subscription = mock.Mock(SubscriptionBase())
        mock_subscription.producer = mock.Mock()
        mock_subscription.producer.name.return_value = device_name
        mock_subscription.attr = attr
        mock_subscription.handler = mock.Mock(WaitUntilEqual)
        mock_subscription.handler.desired_value = value
        mock_subscriptions.add(mock_subscription)
    return mock_subscriptions


class MockDevices:
    obsstates = {}

    def __init__(self, name: str) -> None:
        self._name = name

    def name(self) -> str:
        return self._name

    def read_attribute(self, attr: str):
        if attr == "obsstate":
            val = self.obsstates[self._name]
            return AttributeInt(name=attr, value=val)
        return None


@pytest.fixture(name="test_board")
def fxt_test_board(mock_subscriptions):
    message_board = mock.Mock(MessageBoardBase())
    message_board.archived_subscriptions = mock_subscriptions
    message_board.subscriptions = None
    return message_board


@pytest.fixture(name="generic_mock_device")
def fxt_generic_mock_device(test_devices, factory: MockingFactory):
    MockDevices.obsstates = dict(zip(test_devices, env["obsstates"]))
    device_cls = mock.Mock(AbstractDeviceProxy, wraps=MockDevices)
    factory.inject_device_proxy_constructor(device_cls)


@pytest.fixture(name="devices_in_IDLE")
def fxt_devices_in_idle():
    env["obsstates"] = IDLE


@pytest.mark.usefixtures("generic_mock_device")
@pytest.mark.usefixtures("devices_in_IDLE")
def test_dont_wait_if_all_in_sync(test_board):
    result = wait_until_attrs_in_sync(test_board, 0)
    assert_that(result).is_none()


@pytest.mark.usefixtures("generic_mock_device")
@pytest.fixture(name="devices_in_EMPTY")
def fxt_set_to_empty():
    env["obsstates"] = EMPTY


@pytest.mark.usefixtures("generic_mock_device")
@pytest.mark.usefixtures("devices_in_EMPTY")
def test_wait_if_not_in_sync(test_board):
    with pytest.raises(TimeoutError):
        wait_until_attrs_in_sync(test_board, 0)


class MockDevice(AbstractDeviceProxy):
    def __init__(self, name: str) -> None:
        super().__init__(name)
        self._name = name
        self._read_count = 0
        self.results = []

    def name(self) -> str:
        return self._name

    def read_attribute(self, attr_name: str):
        val = self.results[self._read_count]
        self._read_count += 1
        return AttributeInt(name=attr_name, value=val)

    def set_results(self, results: List[str]):
        self.results = results


@pytest.fixture(name="csp_subarray")
def fxt_csp_subarray():
    return device_mapping["mid_csp/elt/subarray_01"]


@pytest.fixture(name="sdp_subarray")
def fxt_sdp_subarray():
    return device_mapping["mid_sdp/elt/subarray_1"]


@pytest.fixture(name="subarray")
def fxt_subarray():
    return device_mapping["ska_mid/tm_subarray_node/1"]


device_mapping = {
    "mid_csp/elt/subarray_01": MockDevice("mid_csp/elt/subarray_01"),
    "mid_sdp/elt/subarray_1": MockDevice("mid_sdp/elt/subarray_1"),
    "ska_mid/tm_subarray_node/1": MockDevice("ska_mid/tm_subarray_node/1"),
}


def mock_device_constructor(_p0: str) -> AbstractDeviceProxy:
    return device_mapping[_p0]


@pytest.fixture(name="mock_device")
def fxt_mock_device(test_devices, factory: MockingFactory):
    MockDevices.obsstates = dict(zip(test_devices, env["obsstates"]))
    factory.inject_device_proxy_constructor(mock_device_constructor)


@pytest.mark.usefixtures("mock_device")
def test_varied_states(
    csp_subarray: MockDevice,
    sdp_subarray: MockDevice,
    subarray: MockDevice,
    test_board,
):
    csp_subarray.set_results(IDLE)
    sdp_subarray.set_results(["RESOURCING", "IDLE"])
    subarray.set_results(IDLE)
    result = wait_until_attrs_in_sync(test_board, 1)
    assert_that(result).is_equal_to(
        f"{sdp_subarray.name()}/obsstate expected to be IDLE but was found to be " "RESOURCING"
    )
