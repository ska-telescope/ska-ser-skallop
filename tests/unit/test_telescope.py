import mock
import pytest

from ska_ser_skallop.connectors import configuration as conf
from ska_ser_skallop.connectors.mockingfactory import MockingFactory
from ska_ser_skallop.mvp_control.entry_points import configuration as entry_conf
from ska_ser_skallop.mvp_control.entry_points.base import EntryPoint
from ska_ser_skallop.mvp_control.telescope.start_up import EWhileStartingUp, NotReadyStartUp
from ska_ser_skallop.mvp_fixtures.base import ExecSettings
from ska_ser_skallop.mvp_management.telescope_management import (
    run_a_telescope,
    tear_down_a_telescope,
)
from ska_ser_skallop.subscribing import configuration as subs_conf


@pytest.fixture(name="test_factory")
def fxt_test_factory():
    testing_factory = MockingFactory()
    with conf.patch_factory(testing_factory, "test fixture"):
        testing_factory.set_devices_query()
        testing_factory.set_producer_spy()
        testing_factory.set_device_spy()
        yield testing_factory


@pytest.fixture(name="exec_settings")
def fxt_exec_settings() -> ExecSettings:
    settings = ExecSettings()
    settings.time_out = 0.1
    return settings


@pytest.fixture(name="message_board_mock")
def fxt_message_board_mock():
    with subs_conf.patch_messageboard() as message_board_mock:
        yield message_board_mock


@pytest.fixture(name="entrypoint_mock")
def fxt_entrypoint_mock() -> EntryPoint:
    with entry_conf.patch_entry_point(synched_impl=True) as entrypoint_mock:
        yield entrypoint_mock


@pytest.mark.usefixtures("test_factory", "entrypoint_mock")
def test_run_a_telescope_fails_when_devices_are_in_the_wrong_state(
    exec_settings, test_factory: MockingFactory
):
    test_factory.get_device_proxy("spy")

    with pytest.raises(NotReadyStartUp):
        run_a_telescope(exec_settings)


@pytest.mark.usefixtures("test_factory", "entrypoint_mock")
# @mock.patch("ska_ser_skallop.mvp_control.telescope.start_up.assert_tmc_is_on")
@mock.patch(
    "ska_ser_skallop.mvp_control.telescope.start_up." "assert_I_can_set_telescope_to_running"
)
def test_run_a_telescope_succeeds_when_devices_are_in_the_expected_state(
    mock_assert, exec_settings, message_board_mock
):
    message_board_mock.subscriptions = None
    run_a_telescope(exec_settings)


@pytest.mark.usefixtures("test_factory", "entrypoint_mock", "message_board_mock")
@mock.patch(
    "ska_ser_skallop.mvp_control.telescope.start_up." "assert_I_can_set_telescope_to_standby"
)
def test_tear_down_a_telescope_succeeds_when_devices_are_in_the_expected_state(
    mock_assert, exec_settings
):
    tear_down_a_telescope(exec_settings)


@pytest.mark.usefixtures("test_factory", "entrypoint_mock", "message_board_mock")
@mock.patch(
    "ska_ser_skallop.mvp_management.telescope_management." "_handle_error_whilst_starting_up"
)
@mock.patch("ska_ser_skallop.mvp_control.telescope.start_up.set_telescope_to_running")
@mock.patch(
    "ska_ser_skallop.mvp_control.telescope.start_up." "assert_I_can_set_telescope_to_running"
)
def test_run_a_telescope_raises_startup_error(mock_assert, mock_running, error_meth, exec_settings):
    with pytest.raises(Exception):
        mock_running.side_effect = EWhileStartingUp(Exception)
        run_a_telescope(exec_settings)
        assert mock_running.called
        assert mock_assert.called
        assert error_meth.called


@pytest.mark.usefixtures("test_factory", "entrypoint_mock", "message_board_mock")
@mock.patch(
    "ska_ser_skallop.mvp_management.telescope_management." "_handle_error_whilst_starting_up"
)
@mock.patch("ska_ser_skallop.mvp_control.telescope.start_up.set_telescope_to_running")
@mock.patch(
    "ska_ser_skallop.mvp_control.telescope.start_up." "assert_I_can_set_telescope_to_running"
)
def test_run_a_telescope_raises_exception(mock_assert, mock_running, error_meth, exec_settings):
    with pytest.raises(Exception):
        mock_running.side_effect = Exception
        run_a_telescope(exec_settings)
        assert mock_running.called
        assert error_meth.called
