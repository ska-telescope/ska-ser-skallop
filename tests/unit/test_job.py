from ska_ser_skallop.mvp_control.subarray.job_scheduling import Job


def test_job():
    job = Job(lambda x: x + 1, 1)
    job.schedule_for_execution()
    res = job.wait_until_job_is_finished()
    assert res == 2
