"""Testing module that tests the EventsPusher and Subscription classes"""

from queue import Queue
from typing import Union

import mock
import pytest
from assertpy import assert_that

from ska_ser_skallop.subscribing import base
from ska_ser_skallop.subscribing.subscription import EventsPusher, Subscription

SUTLIB = "ska_ser_skallop.subscribing"


def assert_subscribed_called_on(
    device: mock.Mock,
    subscription: Subscription,
    pusher: base.EventsPusherBase,
):
    attr = subscription.attr
    if isinstance(pusher, int):
        device.subscribe_event.assert_called_with(attr, base.CHANGE_EVENT, pusher)
    elif isinstance(pusher, base.EventsPusherBase):
        device.subscribe_event.assert_called_with(attr, base.CHANGE_EVENT, pusher)


class SubscriptionContext:
    mock_events_pusher: mock.Mock
    mock_device: mock.Mock
    attr: str
    mock_handler: mock.Mock
    subscription: Subscription
    subscription_id: int

    def __init__(self, MockEventsPusher: mock.Mock) -> None:
        self.mock_events_pusher_cls = MockEventsPusher
        self.mock_events_pusher = MockEventsPusher.return_value
        self.mock_device = mock.Mock(base.Producer)
        self.subscription_id = 1
        self.mock_device.subscribe_event.return_value = self.subscription_id
        self.attr = "dummy"
        self.mock_events = [mock.Mock(base.Producer), mock.Mock(base.Producer)]
        self.mock_device.get_events.return_value = self.mock_events
        self.mock_handler = mock.Mock(base.MessageHandlerBase)
        self.subscription = Subscription(self.mock_device, self.attr, self.mock_handler)


@pytest.fixture(name="subscription_context")
def fxt_subscription_context():
    with mock.patch(f"{SUTLIB}.subscription.EventsPusher") as mock_events_pusher_cls:
        yield SubscriptionContext(mock_events_pusher_cls)


@pytest.fixture(name="message_queue")
def fxt_message_queue():
    return Queue()


def test_subscribe_by_call_back(subscription_context: SubscriptionContext, message_queue: Queue):
    # given a subscription
    context = subscription_context
    subscription = context.subscription
    # and a message queue
    # when I subscribe
    subscription.subscribe_by_callback(message_queue)
    # I expect a new eventsPusher to have been created
    context.mock_events_pusher_cls.assert_called()
    # I expect a new subscription to have been created with that events pusher
    assert_subscribed_called_on(context.mock_device, subscription, context.mock_events_pusher)
    # I expecte the dsubscription id to be updated
    assert_that(subscription.id).is_equal_to(context.subscription_id)


@pytest.fixture(name="running_subscription")
def fxt_running_subscription(subscription_context, message_queue):
    # given a subscription
    context: SubscriptionContext = subscription_context
    context.subscription.subscribe_by_callback(message_queue)
    return context


def test_unsubscribe(running_subscription):
    # given a running subscription
    context: SubscriptionContext = running_subscription
    subscription = context.subscription
    # when I unsubscribe
    subscription.unsubscribe()
    # I expect the device to  be unsubscribed
    context.mock_device.unsubscribe_event.assert_called_with(context.subscription_id)


def test_subscribe_by_polling(subscription_context):
    # given a subscription
    context: SubscriptionContext = subscription_context
    subscription = context.subscription
    # when I subscribe with a buffer of size 10
    subscription.subscribe_buffer(10)
    # I do not expect a new events pusher
    assert_that(context.mock_events_pusher.call_count).is_equal_to(0)
    # I expect a new subscription to have been created without events pusher
    assert_subscribed_called_on(context.mock_device, subscription, context.mock_events_pusher)


@pytest.fixture(name="polled_subscription")
def fxt_polled_subscription(subscription_context):
    # given a subscription
    context: SubscriptionContext = subscription_context
    subscription = context.subscription
    subscription.subscribe_buffer(10)
    return context


def test_poll_polling_subscription(polled_subscription):
    # given a polling subscription
    context: SubscriptionContext = polled_subscription
    subscription = context.subscription
    # when I then poll the subscription
    result = subscription.poll()
    (result_events, result_subscriptions, result_handlers) = tuple(
        zip(*[(item.event, item.subscription, item.handler) for item in result])
    )
    assert_that(list(result_events)).is_equal_to(context.mock_events)
    assert_that(list(result_subscriptions)).is_equal_to([subscription for _ in range(len(result))])
    assert_that(list(result_handlers)).is_equal_to(
        [context.mock_handler for _ in range(len(result))]
    )


class EventsPusherContext:
    pusher: EventsPusher
    queue: Queue
    subscription: mock.Mock
    handler: mock.Mock
    event: Union[base.EventDataInt, None]

    def __init__(self) -> None:
        self.queue = Queue()
        self.handler = mock.Mock(base.MessageHandlerBase)
        self.pusher = EventsPusher(self.queue, self.handler)
        self.subscription = mock.Mock(Subscription)
        self.subscription.describe.return_value = {
            "dummy_device_name",
            "dummy_attr",
            1,
        }
        self.idempotent = False
        self.event = None

    def set_subscription(self):
        self.pusher.set_subscription(self.subscription)
        self.idempotent = True

    def assert_event_placed_on_queue(self, event: Union[base.EventDataInt, None]):
        item = self.queue.get(timeout=0)
        assert_that(item.event).is_equal_to(event)
        assert_that(item.subscription).is_equal_to(self.subscription)
        assert_that(item.handler).is_equal_to(self.handler)


@pytest.fixture(params=["unidempotent", "idempotent"], name="events_pusher_context")
def fxt_events_pusher_context(request):
    context = EventsPusherContext()
    if request.param == "idempotent":
        context.set_subscription()
    return EventsPusherContext()


@pytest.fixture(name="event")
def fxt_event():
    return mock.Mock(base.EventDataInt("dummy", "dummy"))


def test_events_pusher(events_pusher_context, event):
    # given an events pusher with an existing subscription tied to it
    context: EventsPusherContext = events_pusher_context
    pusher = context.pusher
    # when I push an event
    pusher.push_event(event)
    # I expect an item to be in the queue if pushed idempotently
    if context.idempotent:
        context.assert_event_placed_on_queue(event)
    else:
        # I expect an event to have been stashed
        assert_that(context.pusher.stash).is_length(1)
        # I expect the event not to have been placed on the queue
        assert context.queue.empty


@pytest.fixture(name="stashed_events_pusher")
def fxt_stashed_events_pusher(event):
    context = EventsPusherContext()
    pusher = context.pusher
    pusher.push_event(event)
    context.event = event
    return context


def test_push_stashed_event(stashed_events_pusher):
    # given an event that has been pushed idempotently on a pusher
    context: EventsPusherContext = stashed_events_pusher
    pusher = context.pusher
    event = context.event
    subscription = context.subscription
    # when I set the pusher's subscription
    pusher.set_subscription(subscription)
    # I expect the event to have been placed on the queue
    context.assert_event_placed_on_queue(event)
    # I expect the pusher to have removed any remaining stashed events
    assert_that(pusher.stash).is_length(0)
