from queue import Queue
from typing import Generator, cast

import mock
import pytest
from assertpy import assert_that

from ska_ser_skallop.connectors.configuration import AbstractDeviceProxy, patch_factory_for_testing
from ska_ser_skallop.mvp_fixtures.context_management import ExitStack, StackableContext
from ska_ser_skallop.mvp_fixtures.reflector import ExecSettings, Reflector, TelescopeContext


class TestContext:
    def __init__(
        self,
        telescope_context: TelescopeContext,
        exec_settings: ExecSettings,
        mock_device: AbstractDeviceProxy,
        queue: Queue,
    ) -> None:
        self.telescope_context = telescope_context
        self.exec_settings = exec_settings
        self.mock_device_name = "sys/tg_test/1"
        self.mock_device_attribute = "string_scalar"
        self.mock_device = mock_device
        self.queue = queue

    def get_latest_events(self):
        if not self.queue.empty():
            return list(self.queue.queue)
        else:
            return None


@pytest.fixture(name="stack_context")
def fxt_stack_context() -> Generator[StackableContext, None, None]:
    with ExitStack() as stack:
        yield StackableContext(stack)


@pytest.fixture(name="context")
def fxt_context(
    stack_context: StackableContext,
) -> Generator[TestContext, None, None]:
    telescope_context_mock = mock.Mock(TelescopeContext, wraps=stack_context)
    exec_settings_mock = mock.Mock(ExecSettings)
    cast(mock.Mock, cast(ExecSettings, exec_settings_mock).exec_finished).return_value = False
    queue = Queue()

    def mock_write(_, val: str):
        queue.put(val)

    with patch_factory_for_testing() as mock_factory:
        mock_device = mock_factory.set_device_spy()
        cast(mock.Mock, mock_device.write_attribute).side_effect = mock_write
        yield TestContext(telescope_context_mock, exec_settings_mock, mock_device, queue)


@pytest.fixture(name="reflector")
def fxt_reflector(context: TestContext):
    return Reflector(
        context.telescope_context,
        context.exec_settings,
        context.mock_device_name,
        context.mock_device_attribute,
    )


@pytest.mark.parametrize("reflections", ["dummy", ["dummy1", "dummy2"]])
def test_reflect(reflections, reflector: Reflector, context: TestContext):
    reflector.reflect(reflections)
    reflections = [reflections] if isinstance(reflections, str) else reflections
    assert_that(context.get_latest_events()).is_equal_to(reflections)


@pytest.mark.parametrize("reflections", ["dummy", ["dummy1", "dummy2"]])
def test_reflect_concurrently(reflections, reflector: Reflector, context: TestContext):
    reflector.reflect(reflections, concurrent=True)
    reflector.block_until_finished(1)
    reflections = [reflections] if isinstance(reflections, str) else reflections
    assert_that(context.get_latest_events()).is_equal_to(reflections)
