"""Testing module that tests the basic set of event handling handlers"""

import logging
from contextlib import contextmanager

import mock
import pytest
from assertpy import assert_that

from ska_ser_skallop.connectors import configuration as conf
from ska_ser_skallop.connectors.mockingfactory import MockingFactory
from ska_ser_skallop.event_handling import base
from ska_ser_skallop.event_handling.handlers import (
    ObserveEvent,
    ObserveLogEvent,
    WaitForOrderedChange,
    WaitUntilEqual,
)
from ska_ser_skallop.mvp_control.event_waiting import set_to_wait
from ska_ser_skallop.subscribing.base import MessageBoardBase
from ska_ser_skallop.subscribing.helpers import Tracer

LIB = "event_handling.waiting"


@pytest.fixture(name="test_factory")
def fxt_test_factory():
    testing_factory = MockingFactory()
    with conf.patch_factory(testing_factory, "test fixture"):
        testing_factory.set_devices_query()
        testing_factory.set_producer_spy()
        testing_factory.set_device_spy()
        yield testing_factory


@pytest.fixture(name="attr")
def fxt_attr():
    return "dummy"


@pytest.fixture(name="device_name")
def fxt_device_name():
    return "dummy"


@pytest.fixture(name="mock_board")
def fxt_mock_board():
    return mock.Mock(MessageBoardBase())


@pytest.fixture(params=[True, False], name="master")
def fxt_master(request):
    return request.param


@pytest.fixture(params=[True, False], name="ignore_first")
def fxt_ignore_first(request):
    return request.param


@pytest.fixture(name="required_value")
def fxt_required_value():
    return "required_value"


@pytest.fixture(name="required_event")
def fxt_required_event(required_value, device_name, attr):
    return base.EventDataInt(device_name, attr, required_value)


@pytest.fixture(name="not_required_value")
def fxt_not_required_value():
    return "not required_value"


@pytest.fixture(name="not_required_event")
def fxt_not_required_event(not_required_value, device_name, attr):
    return base.EventDataInt(device_name, attr, not_required_value)


@pytest.fixture(name="wait_until_equal")
def fxt_wait_until_equal(
    mock_board, attr, required_value, device_name, master, ignore_first
) -> WaitUntilEqual:
    return WaitUntilEqual(mock_board, attr, required_value, device_name, master, ignore_first)


@pytest.fixture(name="mock_subscription")
def fxt_mock_subscription(device_name, attr):
    subscription = mock.Mock(
        base.SubscriptionBase,
    )
    subscription.describe.return_value = (device_name, attr, 0)
    return subscription


def test_wait_when_not_equal(wait_until_equal, not_required_event, mock_subscription, mock_board):
    # given
    wait: WaitUntilEqual = wait_until_equal
    # when I handle an event that is not what I am waiting for
    wait.handle_event(not_required_event, mock_subscription)
    # I do not expect the handler to unsubscribe
    mock_board.remove_subscription.assert_not_called()
    # I do not expect all other subscriptions to be removed
    mock_board.remove_all_subscriptions.assert_not_called()


def test_unsubscribe_when_equal(
    wait_until_equal,
    required_event,
    mock_subscription,
    mock_board,
    ignore_first,
    master,
):
    # given a handler for which  no events have been handled  yet
    wait: WaitUntilEqual = wait_until_equal
    # when I handle an event that is what I am waiting for
    wait.handle_event(required_event, mock_subscription)
    if ignore_first:
        # I do not expect the handler to unsubscribe if I set ignore first
        mock_board.remove_subscription.assert_not_called()
        # I do not expect all other subscriptions to be removed
        mock_board.remove_all_subscriptions.assert_not_called()
    else:
        # I expect the handler to unsubscribe
        mock_board.remove_subscription.assert_called()
        if master:
            # I expect the handler to unsubscribe all other subscriptions as well
            mock_board.remove_all_subscriptions.assert_called()


@pytest.fixture(name="wait_until_equal_second_event")
def fxt_wait_until_equal_second_event(wait_until_equal, not_required_event, mock_subscription):
    wait: WaitUntilEqual = wait_until_equal
    wait.handle_event(not_required_event, mock_subscription)
    return wait


def test_unsubscribe_when_equal_after_second(
    wait_until_equal_second_event,
    required_event,
    mock_subscription,
    mock_board,
):
    # given a handler for which a not expected event have been handled already
    wait: WaitUntilEqual = wait_until_equal_second_event
    # when I handle an event that is what I am waiting for
    wait.handle_event(required_event, mock_subscription)
    # I expect the handler to unsubscribe
    mock_board.remove_subscription.assert_called()


@pytest.fixture(name="expected_ordered_values")
def fxt_expected_ordered_values():
    return ["val1", "val2", "val3"]


@pytest.fixture(name="expected_not_ordered_values")
def fxt_expected_not_ordered_values():
    return ["val4", "val5", "val6"]


@pytest.fixture(name="expected_ordered_events")
def fxt_expected_ordered_events(device_name, attr, expected_ordered_values, ignore_first):
    events = [
        base.EventDataInt(device_name, attr, required_value)
        for required_value in expected_ordered_values
    ]
    if ignore_first:
        return [base.EventDataInt(device_name, attr, "ignored event")] + events
    return events


@pytest.fixture(name="not_expected_ordered_events")
def fxt_not_expected_ordered_events(device_name, attr, expected_not_ordered_values, ignore_first):
    events = [
        base.EventDataInt(device_name, attr, required_value)
        for required_value in expected_not_ordered_values
    ]
    if ignore_first:
        return [base.EventDataInt(device_name, attr, "ignored event")] + events
    return events


@pytest.fixture(name="wait_for_ordered_change")
def fxt_wait_for_ordered_change(
    mock_board,
    attr,
    expected_ordered_values,
    device_name,
    master,
    ignore_first,
):
    return WaitForOrderedChange(
        mock_board,
        attr,
        expected_ordered_values,
        device_name,
        master,
        ignore_first,
    )


def test_wait_when_not_equal_to_ordered_changes(
    wait_for_ordered_change,
    not_expected_ordered_events,
    mock_subscription,
    mock_board,
):
    # given a handler for which a not expected event have been handled already
    wait: WaitForOrderedChange = wait_for_ordered_change
    # when I handle a set of events not equal to what I have expected
    for event in not_expected_ordered_events:
        wait.handle_event(event, mock_subscription)
    # I do not expect the handler to unsubscribe
    mock_board.remove_subscription.assert_not_called()
    # I do not expect all other subscriptions to be removed
    mock_board.remove_all_subscriptions.assert_not_called()


@contextmanager
def log_when_fail(tracer: Tracer):
    try:
        yield
    except Exception as ex:
        logging.warning(tracer.print_messages())
        raise ex


def test_ubscubscribe_when_equal_to_ordered_changes(
    wait_for_ordered_change,
    expected_ordered_events,
    mock_subscription,
    mock_board,
    master,
):
    # given a handler for which a not expected event have been handled already
    wait: WaitForOrderedChange = wait_for_ordered_change
    # when I handle a set of events not equal to what I have expected
    for event in expected_ordered_events:
        wait.handle_event(event, mock_subscription)
    # I expect the handler to unsubscribe
    with log_when_fail(wait.tracer):
        mock_board.remove_subscription.assert_called()
        if master:
            # I expect the handler to unsubscribe all other subscriptions as well
            mock_board.remove_all_subscriptions.assert_called()


@pytest.fixture(name="consumer")
def fxt_consumer():
    return "dummy consumer"


@pytest.fixture(name="observe_log_event")
def fxt_observe_log_event(mock_board, attr, consumer):
    return ObserveLogEvent(mock_board, attr, consumer)


def test_observe_log_event_does_not_unsubscribe(
    observe_log_event, required_event, mock_subscription, mock_board
):
    # given a handler for which a not expected event have been handled already
    wait: ObserveLogEvent = observe_log_event
    # when I handle it
    wait.handle_event(required_event, mock_subscription)
    # I do not expect the handler to unsubscribe
    mock_board.remove_subscription.assert_not_called()
    # I do not expect all other subscriptions to be removed
    mock_board.remove_all_subscriptions.assert_not_called()


@pytest.fixture(name="observe_event")
def fxt_observe_event(mock_board, attr, device_name):
    return ObserveEvent(mock_board, attr, device_name)


def test_observe_log_event_print_with_label(observe_log_event, required_event, mock_board):
    # given a handler for which a not expected event have been handled already
    wait: ObserveEvent = observe_log_event
    # when I handle it
    result = wait.print_event(required_event)
    assert_that(result).is_type_of(str)
    mock_board.log.assert_called()


@pytest.mark.usefixtures("test_factory")
def test_waiting_for_startup():
    mocked_message_board_builder = mock.MagicMock()
    set_to_wait.set_wating_for_start_up(mocked_message_board_builder)
