[tool.poetry]
name = "ska-ser-skallop"
version = "2.31.4-rc1"
description = "Testing helpers and modules for running end to end tests against SKAO Software, deployed using SKAO Gitlab CI/CD pipelines."
authors = ["Gerhard le Roux <gerhardlr@gmail.com>"]
license = "BSD license"
include = ["src/ska_ser_skallop/scripts/env/setenv.sh"]
classifiers = ["Framework :: Pytest"]

[tool.poetry.dependencies]
python = "^3.10"
assertpy = "^1.1"
mock = "^5.1.0"
python-graphql-client = "^0.4.3"
requests = "^2.26.0"
pyyaml = "^6.0.0"
pytango = { version = "9.5.1", optional = true }
kubernetes = "^29.0.0"
pipe = "^2.2"
flake8 = "^7.0.0"
darglint = "^1.8.1"
python-dotenv = "^1.0.1"
schema = "0.7.5"
diagrams = "^0.23.4"
lxml = "^5.2.1"
typing-extensions = "^4.11.0"
pydantic = "^2.7.1"

[tool.poetry.group.dev]
optional = true

[tool.poetry.group.dev.dependencies]
ipython = "^7.27.0"
graphviz = "^0.17"
pydeps = "^1.9.14"
build = "^0.10.0"

[tool.poetry.group.docs]
optional = true

[tool.poetry.group.docs.dependencies]
sphinx = "^7.2.3"
sphinx-rtd-theme = "^2.0.0"
sphinx-autobuild = "^2021.3.14"
sphinxcontrib-websupport = "^1.2.4"
docutils = "^0.19"
markupsafe = "^2.0.1"
pygments = "^2.10.0"
rstcheck = "^3.3.1"
m2r = "^0.2.1"
importlib-metadata = "^4.8.1"
m2r2 = "^0.3.1"

[tool.poetry.group.tests]
optional = false

[tool.poetry.group.tests.dependencies]
pytest = "^8.1.1"
pytest-asyncio = "^0.23.6"
pytest-bdd = "^7.1.2"
pytest-cov = "^5.0.0"
pytest-forked = "^1.3.0"
pytest-json-report = "^1.4.0"
pytest-mock = "^3.14.0"
pytest-watch = "^4.2.0"
pytest-xdist = "^3.5.0"
pylint = "^3.1.0"
pylint-junit = "^0.3.4"
assertpy = "^1.1"
darglint = "^1.8.0"
flake8-docstrings = "^1.6.0"
flake8-formatter-junit-xml = "^0.0.6"
flake8-rst-docstrings = "^0.3.0"
mock = "^5.1.0"
pycodestyle = "^2.11.1"
black = "^24.4.2"

[build-system]
requires = ["poetry_core==1.1.0b3"]
build-backend = "poetry.core.masonry.api"

[[tool.poetry.source]]
name = 'ska'
url = 'https://artefact.skao.int/repository/pypi-all/simple'

[[tool.poetry.source]]
name = "PyPI-public"
url = 'https://pypi.org/simple'

[tool.poetry.extras]
bit = ["pytango"]

[tool.poetry.plugins.pytest11]
ska_ser_skallop = "ska_ser_skallop.mvp_fixtures.fixtures"


[tool.poetry.scripts]
setenv = { reference = "scripts/env/setenv.sh", type= "file" }
log-consumer = "ska_ser_skallop.log_consumer.log_consumer:main"
xray-inspect = "ska_ser_skallop.scripts.bdd_helper_scripts.xray_inspect:main"
xtp-pull = "ska_ser_skallop.scripts.bdd_helper_scripts.xtp_pull:main"
xtp-push = "ska_ser_skallop.scripts.bdd_helper_scripts.xtp_push:main"
xtp-compare = "ska_ser_skallop.scripts.bdd_helper_scripts.xtp_compare:main"
xtp-xray-upload = "ska_ser_skallop.scripts.bdd_helper_scripts.xray_upload:main"
data-manager = "ska_ser_skallop.scripts.bdd_test_data_manager.data_manager:main"
pingmvp = "ska_ser_skallop.scripts.env.pingmvp:main"
getrstats = "ska_ser_skallop.scripts.env.get_resource_stats:main"
upload-to-confluence = "ska_ser_skallop.scripts.confluence.upload_to_confluence:main"
generate-sut-diagrams = "ska_ser_skallop.scripts.confluence.generate_sut_diagrams:main"


[tool.pytest.ini_options]
addopts = "-p no:warnings"
testpaths = ["tests"]
log_cli_level = "INFO"
log_cli = "False"
junit_family = "xunit2"
python_classes = "!Test"

[tool.build_sphinx]
source-dir = "docs/src"
build-dir = "docs/_build"
all_files = 1
builder = "html"

[tool.isort]
profile = "black"
multi_line_output = 3
line_length = 100

[tool.flake8]
max-line-length = 100

[tool.black]
line_length = 100

[tool.pyright]
