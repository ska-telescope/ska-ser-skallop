{{/* vim: set filetype=mustache: */}}
{{/*
Create a install command from the provided values
*/}}
{{- define "devc.install" }}
    - bash
    - -c
    {{- if .Values.install.requirements }}
    - "pip3 install virtualenv && virtualenv venv && source venv/bin/activate && git clone {{ .Values.install.gitRepo }}/{{ .Values.install.project }}.git  && pip3 install {{ .Values.install.project }}/. -r /req/requirements.txt --extra-index-url {{ .Values.install.extraRepo }}"
    {{- else }}
    - "pip3 install virtualenv && virtualenv venv && source venv/bin/activate && git clone {{ .Values.install.gitRepo }}/{{ .Values.install.project }}.git  && pip3 install {{ .Values.install.project }}/. --extra-index-url {{ .Values.install.extraRepo }}"
    {{- end }}
{{- end }}
{{/*
Create a entry point command for container
*/}}
{{- define "devc.entrypoint" }}
    - bash
    - -c
    {{- if .Values.vscode.enabled }}
    - "pip3 install virtualenv && virtualenv venv && git clone {{ .Values.install.gitRepo }}/{{ .Values.install.project }}.git && cp -R /.vscode {{ .Values.install.project }}/.vscode && sleep infinity "
    {{- else }}
    - "pip3 install virtualenv && virtualenv venv && git clone {{ .Values.install.gitRepo }}/{{ .Values.install.project }}.git && sleep infinity "
    {{- end }}
{{- end }}
{{/*
Create a command to register a tango device
tango_admin --add-server LogConsumer/log LogConsumer LogConsumer/log/log01
<exec/inst> <class> <dev list (comma separated)>
*/}}
{{- define "devc.register" }}
    - tango_admin
    - --add-server
    - {{ .Values.tango.deviceServer.serverClass }}/{{ .Values.tango.deviceServer.serverInstance }}
    - {{ .Values.tango.deviceServer.serverClass }}
    - {{ .Values.tango.deviceServer.serverClass }}/{{ .Values.tango.deviceServer.serverInstance }}/{{ .Values.tango.deviceServer.deviceInstance }}
{{- end }}
{{/*
Create env vars
*/}}
{{- define "devc.env" }}
    {{- range .Values.env }}
    - name: {{ .name }}
      value: {{ .value }}
    {{- end }}
{{- end }}
{{/*
Create requirements file from values
*/}}
{{- define "devc.requirements"}}
    {{- range .Values.requirements}}
    {{ . }}
    {{- end }}
{{- end }}
{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "devc.chart" }}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end }}
{{/*
Generate global labels to identify resources related to the chart with
*/}}
{{- define "devc.labels"}}
    chart: {{ include "devc.chart" . }}
    app: {{ .Release.Name }}
    domain: {{ .Values.domain }}
{{- end }}
{{/*
Generate a tangobase name
*/}}
{{- define "devc.tangohost"}}
{{-  $tel := regexFind "(\\w+$)" .Release.Namespace -}}
{{-  $branch := .Release.Namespace | trimAll $tel | trimSuffix "-" | trimPrefix "ci-skampi-" -}}
{{- .Values.tango.dbhost }}-{{ .Values.targetRelease }}-{{ $branch }}:{{.Values.tango.dbhostPort -}}
{{- end}}
{{/*
Generate a SKUID service name
*/}}
{{- define "devc.skuidurl" -}}
{{-  $tel := regexFind "(\\w+$)" .Release.Namespace -}}
{{-  $branch := .Release.Namespace | trimAll $tel | trimSuffix "-" | trimPrefix "ci-skampi-" -}}
skuid-skuid-{{ .Release.Namespace }}-{{ .Values.targetRelease }}-{{ $tel }}-{{ $branch }}:{{.Values.skuid.targetPort}}
{{- end}}
