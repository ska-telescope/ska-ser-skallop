from tango.test_utils import DeviceTestContext

from ska_ser_skallop.log_consumer import log_consumer


def test_log_consumer():
    log_consumer_context = DeviceTestContext(log_consumer.LogConsumer)
    with log_consumer_context as consumer_proxy:
        assert consumer_proxy.message == "log consumer ready to accept log messages"
        consumer_proxy.Log("AB")
        assert consumer_proxy.message == "A\tB"
