import logging
import sys
import time

from tango import AttrQuality
from tango.server import Device, attribute, command

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "[%(asctime)s] (%(filename)s:%(lineno)d) %(levelname)s - %(message)s"
)
handler.setFormatter(formatter)
logger.addHandler(handler)


class LogConsumer(Device):
    def __init__(self, device_class, device_name):
        super().__init__(device_class, device_name)
        self.set_change_event("message", True, False)
        self.attr_message = "log consumer ready to accept log messages"

    def init_device(self):
        super().init_device()
        logger.info("device initialised")

    @attribute(dtype="str")
    def message(self):
        return self.attr_message

    def _push_message(self, message: str):
        self.push_change_event("message", message, time.time(), AttrQuality.ATTR_VALID)
        logger.info(f"mew message pushed: {message}")

    @command(dtype_in=[str])
    def Log(self, log_input):
        message = "\t".join(log_input)
        if self.attr_message != message:
            self.attr_message = message
            self._push_message(message)


def main():
    LogConsumer.run_server()


if __name__ == "__main__":
    main()
