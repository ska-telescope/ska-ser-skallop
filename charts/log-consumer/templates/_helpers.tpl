{{/* vim: set filetype=mustache: */}}
{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "log.chart" }}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end }}
{{/*
Generate global labels to identify resources related to the chart with
*/}}
{{- define "log.labels"}}
  chart: {{ include "log.chart" . }}
  app: {{ .Release.Name }}
  domain: {{ .Values.domain }}
{{- end }}
{{/*}}
Generate a tangobase name
*/}}
{{- define "log.tangohost"}}
  {{- if .Values.global }}
    {{- if .Values.global.tango_host }}
      {{-  .Values.global.tango_host }}
    {{- end }}
  {{- else }}
    {{-  $tel := regexFind "(\\w+$)" .Release.Namespace -}}
    {{-  $branch := .Release.Namespace | trimAll $tel | trimSuffix "-" | trimPrefix "ci-ska-skampi-" -}}
    {{- if $branch }}
      {{- .Values.tango.dbhost }}-{{ .Values.targetRelease }}-{{ $branch }}:{{.Values.tango.dbhostPort -}}
    {{- else }}
      {{- .Values.tango.dbhost }}-{{ .Values.targetRelease }}:{{.Values.tango.dbhostPort -}}
    {{- end }}
  {{- end }}
{{- end}}
{{/*
allow for a no op function maintaining pod in running forever so as to run seperete processes for dev pruposes
*/}}
{{- define "log.command" }}
  {{- if .Values.devMode }}
        - bash
        - -c
        - "sleep infinity"
  {{- else }}
        - python3
        - log_consumer.py
        - log
  {{- end}}
{{- end }}}}

