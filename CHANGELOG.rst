##########
Change Log
##########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

[Unreleased]
************

v2.31.3
*******

* AT-2194: Update test infrastructure
    - Fix handling of nr_of_subarrays and nr_of_receptors.
    - Update dish mode enums
    - Update dish manager tango device naming pattern

v2.31.1
*******

* AT-2077: Nodes in SUT dependency graphs scale with size of text labels

v2.31.0
*******

* AT-2053: Added ability to authenticate with confluence using bearer token.

v2.30.1
*******

* AT-2026: Use a version of docutils compatible with ska-ser-sphinx-theme.

v2.30.0
*******

* AT-1940: Update skallop to support the Tango operator:
    - Support loading Tango info from deviceserver custom resource definitions (used by the operator).
* AT-1945: Use subsystem rather than app label to select devices associated with a chart.
    - Add a chart to subsystem mapping for those charts where the subsystem != chart name
    - Improve chartinfo configmap loading by actually parsing the JSON rather than using regexes to extract the data.

v2.29.9
******

* AT-1940: Update dependencies
    - Update dependencies
    - Linting fixes
    - Throttle tango device retrieval: this was causing runtime errors due to too many threads being spawned.

v2.29.8
******
* AT-1870 - Switch to using non-deprecated gitlab templates.
    - Switched to using non-deprecated gitlab templates - this will allow us to make a release of skallop using the standard SKAO release process.
    - Removed unnecessary/unused gitlab jobs & make targets: most of the targets are already implemented in the make repo and the jobs were also duplicates of jobs in the templates repo.
    - Switched to using SKA_K8S_TOOLS_BUILD_DEPLOY image for the pipeline jobs: there isn't anything that specifically requires the use of a custom image.
    - Reformatted code to get linter to pass - all of the code changes are due to this.
    - Removed extra docker files: we only need one for the purposes of this repo.
    - Updated devcontainer to use this Dockerfile
    - Moved example values files to their respective chart dirs: this was causing helm linter to complain.
    - Updated make submodule.

v2.29.7
******
* AT-1702: Fixed dependency diagram generation.
    - The dependency diagram generation was including the platform dependents in the diagram which was creating incorrect diagrams.

v2.4.0
******

* Refactored `output_file_diff`
    - Renamed to `file_differences`
    - Now returning a bool (whether there are differences or not)
* Raising `XrayException` rather than `SystemExit` in methods

v2.3.0
******

* Update readiness and rectification logic on mvp_control package to be in line with new domain logic for TMC subsystem in MVP.
* Update configuration modules for subarray resource management and scan configuration to be in line with ADR 35
    See https://jira.skatelescope.org/browse/SP-1623 and https://jira.skatelescope.org/browse/SP-1643

v2.2.0
******

* Added new functionality to the xtp scripts:
    - all scripts request the user for the password (if not provided by the `-p` flag) on the terminal without echoing it.
    - `xtp-pull` and `xtp-compare` print in stdout whether the thing being downloaded/compared is a test set or a test scenario, e.g.:
        - `Dowloaded XTP-1839 [Type: Test]`
        - `Dowloaded XTP-1834 [Type: Test Set]`
    - `xtp-compare` output formatted as a unix diff.
    - added a `--dry-run` capability to the `xtp-pull` and `xtp-push` scripts, respectively.

v2.1.1
******

* Changed setenv script to handle new skampi name, see https://gitlab.com/ska-telescope/ska-ser-skallop/-/merge_requests/66.

v2.1.0
******

* Added `xtp-xray-upload` script to upload cucumber formatted result files to JIRA via Xray extension
* SAR-249

v2.0.0
******

* First release of ska-ser-skallop.
* Renamed package from old name skallop to ska_ser_skallop.
* Moving artefacts to a new repository https://artefact.skao.int/.

Added
-----

* Empty Python project directory structure
