How to scope mvp control 
========================
The mvp control package exercises a a number of commands on devices within the SUT based on an assumed composition
of the SUT. 
This can however be *"scoped"* according to a given set of tags as selection criteria allowing for mvp control over a subset of
components.


When to use it
--------------
When a particular end to end test exercises only a limited scope of the SUT or assumes only a subset of the end MVP is deployed.


How to use the feature
----------------------
The scope variable must be set as part of injecting the **set_session_exec_env** and **set_exec_env** fixtures.
These fixtures are essentially just a set of env values injected by the tester in order to set particular
configurational settings regarding the execution environment.

Since mvp control can occur on both a session base (i.e at the beginning and end of entire test session) as well
as for each test, selection tags can be set for both objects. 
However the user can also set the same for both session and test when leaving the value for *exec_env* `None` whilst 
setting a value for session.

The two examples below illustrates the concept:

    .. code-block:: python

        # session scope and scope will be different

        from ska_ser_skallop.mvp_fixtures.fixtures import fxt_types

        @pytest.fixture(name="set_session_env", scope="session")
        def fxt_set_session_exec_env(set_session_exec_env: fxt_types.set_session_exec_env):
            set_session_exec_env.session_scope = ["tm", "sdp"]


        @given("An TMC base API for commanding the telescope")
        def an_tmc_base_api_for_commanding_the_telescope(
            set_session_env,
            set_exec_env: fxt_types.set_exec_env,
        ):
            """An TMC base API for commanding the telescope."""
            # set_exec_env.session_scope will be equal to ["tm", "sdp"]
            set_exec_env.scope = ["tm"]


    .. code-block:: python

        # session scope will be the same as scope

        from ska_ser_skallop.mvp_fixtures.fixtures import fxt_types

        @given("An TMC base API for commanding the telescope")
        def an_tmc_base_api_for_commanding_the_telescope(
            set_exec_env: fxt_types.set_exec_env,
        ):
            """An TMC base API for commanding the telescope."""
            set_exec_env.scope = ["tm", "sdp"]
   
