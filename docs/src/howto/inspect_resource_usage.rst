How to inspect resource usage
============================
This section describes how one can inspect the ways in which
tango devices consume cpu and memory resources.

When to use it
--------------
When the resource specification of a pod (realizing a tango device) could be
under or over specified. When timeouts or devices become non responsive
during the execution of a test.

How to use the feature
----------------------
There are stages in which the resources can be inspected, leading to two
distinct ways of using the feature:

    #. At a "snapshot" of the general state of an entire deployment of a release
    #. During the execution of a specific test involving requests being serviced by a tango device.

The first stage can be done by running the binary script:

.. code-block:: bash

    getrstats


The second stage can be enabled by setting a new flag in the waiting setup for an event subscription, e.g.:


.. code-block:: python

    brd = get_message_board_builder()
    brd.set_waiting_on("my_device", mon_resources=True)
