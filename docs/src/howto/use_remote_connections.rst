How to Use Remote Connections
=============================

When to use it
--------------
When you want to run tests from your local dev machine without direct access to tangogql database in cluster.

What you need before starting
-----------------------------

#. System under Test (SUT) that includes a tangogql service (achieved by having the Taranta chart installed)
#. An auth service for getting secure access to tango gql ( usually part of Taranta service)
#. A VPN connection enabling access to the cluster LAN upon which the SUT is relevant
#. The latest skallop (`ska_ser_skallop`) package installed in your python environment

Setting up your environment
---------------------------

.. note::
   Remoting is indirectly enabled dependent on env variable flags set on your host.
   This ensures the same test can run without remoting on a different environment.
   However, the test must make use of the abstracted connectors given by the configuration module.

ENV variables needed
^^^^^^^^^^^^^^^^^^^^

.. list-table:: ENV variables for remoting
   :header-rows: 1

   * - ENV Name
     - Description
   * - TEST_ENV
     - This must always be set to BUILD_OUT to enable remoting
   * - TANGO_BRIDGE_IP
     - an alternative raw ip address to use for connecting to k8 cluster if SUT is not skampi based
   * - KUBE_HOST
     - the domain name hosting the k8 cluster (default k8s.skao.stfc)
   * - BYPASS_AUTH:
     - whether authentication must be skipped (default is not to skip) 
   * - TARANTA_USER:
     - the taranta/taranta username needed for authentication on tangogql service
   * - TARANTA_PASSWORD:
     - the taranta/TARANTA password needed for authentication on tangogql service
   * - KUBE_NAMESPACE:
     - a namespace to determine the path to taranta (otherwise derived from DOMAIN and KUBE_BRANCH)
   * - DOMAIN
     - the type of environment in which SUT is deployed nl: branch/integration/staging
   * - KUBE_BRANCH:
     - The branch name (in case of branch domain) used for the k8 deployment

.. note::
   KUBE_BRANCH is needed only when the DOMAIN variable is set to branch. The branch value is
   the name of the git branch used to deploy an instance of the SUT. If TANGO_BRIDGE_IP is given
   then the DOMAIN and KUBE_BRANCH variables become irrelevant.
  
.. _set-env-var-for-remote-connections:

Setting ENV variables using script
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If your SUT is a "skampi" MVP product, you can enable your host for remote testing by making use of the
`setenv.sh` script that is provided with the package after installation:

.. code-block:: bash

   . setenv.sh

Follow the prompts and choose accordingly:

.. code-block:: bash

   # select the type of k8 namespaced environment in which your SUT is running
   # by typing in the skampi branch used for deploying the SUT on demand (integration = master branch)
   # alternatively you can choose staging/staging-low which will refer to a deployment 
   # in a staged environment
   Name of local branch (or staging/staging-low):integration 
   # select the telescope type of the SUT (can be low or mid)
   Telescope variant:mid 
   # enter a Taranta username and password (taranta)
   taranta user name:
   taranta password:

The script will set (for the current bash terminal) a set of env variables that will enable it to run the test remotely.

.. code-block:: bash

   env variables set: 
   DOMAIN= #skampi branch/integration/staging/staging-low
   KUBE_BRANCH= # skampi branch or empty if domain = integration/staging/staging-low
   TEL= # mid/low
   TARANTA_USER= # xxxx
   TARANTA_PASSWORD= # xxx
   TEST_ENV=BUILD_OUT

You can use these env variables in future again by exporting them to your bashrc or setting them as a .env file for your IDE.

Testing if your environment is set up for remote testing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

After you have set up your env variables you can run this script to verify remoting will work:

.. code-block:: bash

   pingmvp

If it works you will get a result indicating the latency (in ms) it took to ping the Tango Database remotely from you dev machine.

Defining Tests to use abstracted connectors
-------------------------------------------

.. warning::

   Tests must be calling the SUT using abstracted connectors and not real tango proxies for remoting to work.

To ensure abstracted connectors are used, the configuration module within the
:py:mod:`~ska_ser_skallop.connectors` package must be used.

.. code-block:: python

   from ska_ser_skallop.connectors import configuration

   # gets an AbstractDeviceProxy instance
   device_proxy = configuration.get_device_proxy("my device")
   # gets an  AbstractDevicesQuery instance
   devices_query = configuration.get_devices_query()
   # gets an  Producer (which may be AbstractDeviceProxy as a subtype)
   producer = configuration.get_producer("my device")







