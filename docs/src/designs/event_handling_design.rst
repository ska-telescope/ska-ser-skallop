Event Handling
==============
The event handling module is an application of the subscribing module with the aim of handling tango events generated
during the course of a test running on the mvp. This is realized by means of specific event handlers (extending the base
event handler class) allowing for (amongst others) waiting for specific events, waiting for a transition of events and or
observing the events that occurred on a given set of tango device attributes. The interface is in the form of a builder
setting up a set of subscriptions on a messageboard according to the type of event handling desired. The builder in turn
creates "factory" objects that instantiate the necessary message handlers.

A more complex case is where the tester desires to cross reference events occurring as a result of different subscriptions
on devices (in order to test the correct ordering of events). The builder makes use of a event transition checker factory
that in turns creates an instance of an Occurrences object to which different handlers of events can record the occurrence of
state transitions events. Afterwards, the occurrence object can then be used to verify and observe the correct order of events.



Basic Domain Model
******************

The diagram below explains the concept by means of a basic UML domain model.

.. image:: /images/events_handling_design.jpg
    :width: 70%

