MVP Control Design
==================

Concepts
--------

The MVP control package facilitates a user to control a wide scope and variety of SUTs for testing purposes.
To understand the logical organisation of the packages and modules requires a conceptual model generalizing the
SUT on a very high level.

Within the context of the SKA, the SUT can be described as a Minimum Viable Product (MVP) that contains
three conceptual entities:

    #. A **Telescope**
    #. A set of **resources**
    #. One or more "collective groupings" of above resources into a **subarray**

The key functional unit is the subarray upon which observations are conducted. 
Having multiple subarrays provides for parallel and independent observations.

This idea is illustrated in diagram below. 
Note the entities are conceptual and does not relate one-to-one to packages/modules/classes within skallop.
However all the content of the mvp_control package centers around the concept of subarrays, their allocation to resources
and their configuration for running a particular scan.

.. image:: /images/domain_objects_model.jpg
    :width: 70%

Also note that the logical organization will to a large degree remain consistent with different levels and scope in which a particular SUT is
configured. For example if the telescope is SKA Mid or SKA low or if the SUT is focused only on SDP implementation.

The following sections will describe how these concepts are realised within the package.

Package Design
--------------
The high level design, depicting the essential entities and relationships involved is described in the diagram below.
Note the entities dealing with more utility like functions (e.g. naming and listing/collating of info etc.) are not shown.

.. image:: /images/conceptual_model.jpg
    :width: 100%

The skallop software interacts with the SUT via three possible routes: the main route (1) using the **EntryPoint** object, providing the tester with
a high level API for commanding the SUT whilst the other two routes enable the tester with (2) a means for performing more direct rectification of failed components
(using the **rectifier** object) and (3) a means for reading the states of various parts needed to access the overall readiness and health of the SUT (using the
**mvp_states** module).

These interactions are encapsulated into the domain objects (see above) exposing the tester with a public api for managing the state and readiness of the SUT in
a logically consistent manner. 

Thus, each domain object facing the tester are responsible for:

    #. Checking the readiness/condition of the object to be set to a specific state (using **mvp_states**)
    #. Setting the object to a specific state (using **EntryPoint**)
    #. Rectifying the object not being ready (using **rectifier**)

The specific subarray configuration, detailing how it should be allocated with resources (composed) and or configured to perform an observational scan, are contained
within a separate configuration sub package.

A key part involved with controlling the SUT involves ensuring testing scripts correctly wait for state transitions to occur.
Therefore the **EntryPoint** object has api calls setting up the testing script to wait for pre determined event/s before proceeding to the next step.
This is realised using the skallop **subscribing** and **event_handling** packages that result in a configured set of subscriptions to producer events with corresponding handlers
running in a waiting loop until desired events occurred.
The mvp_control package provides testers with a standard set of implementations for waiting for **EntryPoint** commands
in a separate event_waiting package. 
Those modules sets up a list of generic state machine business rules using the **event_handling** package resulting in a desired set of subscriptions to tango devices 
that will be waited for and handled using the **MessageBoard** object from the **subscription** package. 
Developers writing tests scripts may however implement their own business rules when creating their own concrete **EntryPoint** objects as long as it returns
the desired configured **MessageBoard** object that can be used to wait upon after the command was called.

.. note:: The mvp_control package provides a more specific :py:class:`EntryPoint` named :py:class:`SynchedEntryPoint` already implementing the standard waiting configurations
          that a tester may opt to use instead of his own set of waiting implementations.