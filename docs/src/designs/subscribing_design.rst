Subscribing
===========

The subscribing module is composed of a set of interdependent objects
(:py:class:`~ska_ser_skallop.subscribing.message_board.MessageBoard`,
:py:class:`~ska_ser_skallop.subscribing.event_item.EventItem`,
:py:class:`~ska_ser_skallop.subscribing.subscription.Subscription` and
:py:class:`~ska_ser_skallop.subscribing.message_handler.MessageHandler`)
to assist with subscribing to producers based on the "blackboard"
pattern. The central object is the
:py:class:`~ska_ser_skallop.subscribing.message_board.MessageBoard` that
holds items generated from events. These events are the result of
subscriptions
(:py:class:`~ska_ser_skallop.subscribing.subscription.Subscription`)
placed on a producer
(:py:class:`~ska_ser_skallop.subscribing.base.Producer`) object.
When a producer generates an event due to a subscription, the event is
packaged within an
:py:class:`~ska_ser_skallop.subscribing.event_item.EventItem` object
before posted to the
:py:class:`~ska_ser_skallop.subscribing.message_board.MessageBoard`.

The :py:class:`~ska_ser_skallop.subscribing.event_item.EventItem`
contains both the event data as well as the necessary objects to handle
the data:

    1. :py:class:`~ska_ser_skallop.subscribing.subscription.Subscription`:
       in case the event should cause the subscription to be removed
    2. :py:class:`~ska_ser_skallop.subscribing.message_handler.MessageHandler`:
       for conveniently tying specific instructions to handler events
       from a subscription

Basic Domain Model
******************

The diagram below explains the concept by means of a basic UML domain
model.

.. image:: /images/domain_model.jpg
    :width: 70%

A **Messageboard** gets populated by events as a consequence of
**Subscriptions** on *Producers* producing new events. For each event, a
means of handling it (or consuming of the event) is provided in the form
of a **Handler**. The handler could be for example something that checks
the event value against an expected order of events and unsubscribes a
subscription when a specific condition is met. The **Subscription**
object ties therefore not only a subscription to a **Producer**, but
also a **Handler** to the **Subscription**.

Basic Design
************
The :mod:`~ska_ser_skallop.subscribing` module defines a set of
interdependencies and relationships on abstract classes as a framework
for resting the implementation of these classes. In order to provide the
client software with maximum flexibility, a variety of ways is provided
for calling and acting upon these classes. However, care should be taken
to prevent circular dependencies. The basic structure is depicted as UML
diagram below:

.. image:: /images/base_dependencies.jpg
    :width: 100%

This framework is then used to create the actual implementations of the
classes as depicted in the diagram below:

.. image:: /images/interdependencies.jpg
    :width: 100%
