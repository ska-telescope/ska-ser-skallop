\.tangobridge package
=========================================================


Module contents
---------------

.. automodule:: ska_ser_skallop.connectors.remoting.tangobridge
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

\.authentication module
^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: ska_ser_skallop.connectors.remoting.tangobridge.authentication
   :members:
   :undoc-members:
   :show-inheritance:

\.configuration module
^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: ska_ser_skallop.connectors.remoting.tangobridge.configuration
   :members:
   :undoc-members:
   :show-inheritance:

\.control module
^^^^^^^^^^^^^^^^^

.. automodule:: ska_ser_skallop.connectors.remoting.tangobridge.control
   :members:
   :undoc-members:
   :show-inheritance:

\.factories module
^^^^^^^^^^^^^^^^^^

.. automodule:: ska_ser_skallop.connectors.remoting.tangobridge.factories
   :members:
   :undoc-members:
   :show-inheritance:

\.parsing module
^^^^^^^^^^^^^^^^

.. automodule:: ska_ser_skallop.connectors.remoting.tangobridge.parsing
   :members:
   :undoc-members:
   :show-inheritance:

\.queries module
^^^^^^^^^^^^^^^^^

.. automodule:: ska_ser_skallop.connectors.remoting.tangobridge.queries
   :members:
   :undoc-members:
   :show-inheritance:

\.restcontrol module
^^^^^^^^^^^^^^^^^^^^

.. automodule:: ska_ser_skallop.connectors.remoting.tangobridge.restcontrol
   :members:
   :undoc-members:
   :show-inheritance:

\.subscribing module
^^^^^^^^^^^^^^^^^^^^

.. automodule:: ska_ser_skallop.connectors.remoting.tangobridge.subscribing
   :members: 
   :undoc-members:
   :show-inheritance:

.. autodata:: ska_ser_skallop.connectors.remoting.tangobridge.subscribing.DeviceSubscriptionCallback
   :annotation:

\.tangobridge module
^^^^^^^^^^^^^^^^^^^^
.. automodule:: ska_ser_skallop.connectors.remoting.tangobridge.tangobridge
   :members:
   :undoc-members:
   :show-inheritance:

\.wscontrol module
^^^^^^^^^^^^^^^^^^

.. automodule:: ska_ser_skallop.connectors.remoting.tangobridge.wscontrol
   :members:
   :undoc-members:
   :show-inheritance:


