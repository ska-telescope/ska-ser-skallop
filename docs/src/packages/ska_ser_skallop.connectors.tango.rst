\.tango Package
==========================================
Module contents
---------------

.. automodule:: ska_ser_skallop.connectors.tango
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

\.tangoqueries submodule
^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: ska_ser_skallop.connectors.tango.tangoqueries
   :members:
   :undoc-members:
   :show-inheritance:


