.datatypes package
===================================

Submodules
----------

.datatypes.attributes module
---------------------------------------------

.. automodule:: ska_ser_skallop.datatypes.attributes
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: ska_ser_skallop.datatypes
   :members:
   :undoc-members:
   :show-inheritance:
