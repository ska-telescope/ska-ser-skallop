.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:

.. README =============================================================

.. This project most likely has it's own README. We include it here.

.. toctree::
   :maxdepth: 2
   :caption: Readme

.. mdinclude::   ../../README.md

.. HOWTO SECTION =================================================

..

.. toctree::
  :maxdepth: 4
  :caption: HOWTO

  howto/use_remote_connections.rst
  howto/use_skallop_fixtures.rst
  howto/use_mvp_management.rst
  howto/use_xtp_upload.rst
  howto/test_event_occurrences.rst
  howto/mock_entry_points.rst
  howto/scope_mvp_control.rst
  howto/inspect_resource_usage.rst


.. EXAMPLES SECTION =================================================

..

.. toctree::
  :maxdepth: 3
  :caption: Examples

  packages/examples.rst

.. SCRIPTS SECTION =================================================

..

.. toctree::
  :maxdepth: 3
  :caption: Scripts

  packages/scripts.rst
  packages/data_manager.rst
  packages/upload_to_confluence.rst

.. DESIGNS SECTION ==================================================

..

.. toctree::
  :maxdepth: 3
  :caption: designs

  designs/event_handling_design.rst
  designs/subscribing_design.rst
  designs/remoting.rst
  designs/fixtures.rst
  designs/mvp_management.rst
  designs/mvp_control.rst

.. MODULES SECTION ==================================================

..

.. toctree::
  :maxdepth: 3
  :caption: modules

  packages/modules.rst


.. COMMUNITY SECTION ==================================================
