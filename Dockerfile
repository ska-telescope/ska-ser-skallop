FROM artefact.skao.int/ska-tango-images-pytango-builder:9.4.3

RUN apt-get update && apt-get install -y openssh-client graphviz

RUN curl -sSL https://install.python-poetry.org | python3 -

RUN poetry config virtualenvs.in-project true

RUN pip install virtualenv
