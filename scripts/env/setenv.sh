read -e -p "Facility (e.g. stfc/itf/psi):" -i "itf" facility
if [ "$facility" = "itf" ] ; then
    host="10.164.10.1"
    TEL=""
    kube_hostname="integration.svc.miditf.internal.skao.int"
elif [ "$facility" = "stfc" ] ; then
    host="k8s.skao.stfc"
    kube_hostname="cluster.local"
else 
    host="k8s.skao.stfc"
    kube_hostname="cluster.local"
fi
read -e -p "connect using tangobridge? (y/n):" -i "n" tangobridge
if [ "$tangobridge" = "n" ] ; then
    read -e -p "tango db ds name:" -i "tango-databaseds" database_ds_name
    read -e -p "tang db service port:" -i "10000" database_ds_name_port
    read -e -p "disable pub/sub? (y/n):" -i "y" disable_pub_sub
    read -e -p "maintain telescope operational during entire test session?:" -i "no" maintain_on
    if [ "$maintain_on" = "yes" ] ; then
        unset DISABLE_MAINTAIN_ON
    else
        export DISABLE_MAINTAIN_ON="True"
    fi
    export TANGO_HOST=$database_ds_name'.'$kube_hostname':'$database_ds_name_port
    if [ "$disable_pub_sub" = "y" ] ; then
        export USE_ONLY_POLLING='True' 
        echo "env variables set: 
USE_ONLY_POLLING=$USE_ONLY_POLLING
TANGO_HOST=$TANGO_HOST
DISABLE_MAINTAIN_ON=$DISABLE_MAINTAIN_ON"
    else
        echo "env variables set: 
TANGO_HOST=$TANGO_HOST
DISABLE_MAINTAIN_ON=$DISABLE_MAINTAIN_ON"
    fi
else
    read -e -p "namespace prefix (e.g. ci-ska-skampi- or None):" -i "None" prefix
    read -e -p "Name of local branch (or staging/staging-low):" -i "integration" branch
    if [ "$branch" = "integration" ] ; then
        export DOMAIN='integration'
        export KUBE_BRANCH='integration'
        export KUBE_HOST=$host
    elif [ "$branch" = "staging" ] ; then
        export DOMAIN='staging'
        export KUBE_BRANCH='staging'
        export KUBE_HOST=$host
    elif [ "$branch" = "staging-low" ]; then
        export TANGO_BRIDGE_IP='psi-head.atnf.csiro.au'
        export DOMAIN='staging-low'
        export KUBE_BRANCH='staging-low'
        export KUBE_HOST=$TANGO_BRIDGE_IP
        TEL='-low'
    else
        export DOMAIN='branch'
        export KUBE_BRANCH=$branch
        export KUBE_HOST=$host
    fi
    if [ "$branch" != "staging-low" ]; then
        if [ "$facility" != "itf" ] ; then
            read -e -p "Telescope variant:" -i "mid" variant
            export TEL='-'$variant
        fi
    fi
    if [ "$prefix" = "None" ] ; then
        export KUBE_NAMESPACE=$branch$TEL
    else
        export KUBE_NAMESPACE=$prefix'-'$branch$TEL
    fi
    read -e -p "taranta user name:"  username
    export TARANTA_USER=$username
    read -e -p "Taranta password:"  password
    export TARANTA_PASSWORD=$password
    export TEST_ENV='BUILD_OUT'
    read -e -p "maintain telescope operational during entire test session?:" -i "no" maintain_on
    if [ "$maintain_on" = "yes" ] ; then
        unset DISABLE_MAINTAIN_ON
    else
        export DISABLE_MAINTAIN_ON="True"
    fi
echo "env variables set: 
DOMAIN=$DOMAIN
KUBE_BRANCH=$KUBE_BRANCH
TEL=$TEL
TARANTA_USER=$TARANTA_USER
TARANTA_PASSWORD=$TARANTA_PASSWORD
TEST_ENV=$TEST_ENV
DISABLE_MAINTAIN_ON=$DISABLE_MAINTAIN_ON
KUBE_BRANCH=$KUBE_BRANCH
KUBE_NAMESPACE=$KUBE_NAMESPACE
KUBE_HOST=$KUBE_HOST"
    if [ "$branch" = "staging-low" ]; then
        echo "TANGO_BRIDGE_IP='psi-head.atnf.csiro.au'"
    fi
fi
