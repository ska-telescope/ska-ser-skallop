Feature: Start the telescope

  Background:
    Given An TMC base API for commanding the telescope

  Scenario: Start up the telescope
    Given a telescope
    When I start up the telescope
    Then central node is in the ON state
