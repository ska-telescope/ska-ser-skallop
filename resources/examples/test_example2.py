import logging

import pytest

from ska_ser_skallop.connectors import configuration
from ska_ser_skallop.mvp_control.describing import mvp_names
from ska_ser_skallop.mvp_control.entry_points import types as conf_types
from ska_ser_skallop.mvp_fixtures.context_management import TelescopeContext

logger = logging.getLogger(__name__)


@pytest.fixture(name="maintain_on", scope="session", autouse=True)
def fxt_override_maintain_on():
    return True


@pytest.mark.usefixtures("allocated_subarray")
def test_subarray_node_is_in_idle():
    # allocated subarray uses default two dishes and a 'standard' resource allocation
    subarray_node_name = mvp_names.Mid.tm.subarray(1).__str__()
    subarray_node = configuration.get_device_proxy(subarray_node_name)
    assert subarray_node.obsState.name == "IDLE"  # type: ignore


def test_subarray_can_be_directly_created(
    running_telescope: TelescopeContext, tmp_path, sb_config, exec_settings
):
    # use running telescope to create a new allocated subarray
    subarray_id = 1
    receptors = [1, 2, 4, 4]
    composition = conf_types.CompositionByFile(tmp_path, conf_types.CompositionType.STANDARD)

    subarray = running_telescope.allocate_a_subarray(
        subarray_id,
        receptors,
        sb_config,
        exec_settings,
        composition=composition,
    )

    subarray_node_name = mvp_names.Mid.tm.subarray(1).__str__()
    subarray_node = configuration.get_device_proxy(subarray_node_name)
    assert subarray_node.obsState.name == "IDLE"  # type: ignore
    assert subarray.id == 1
