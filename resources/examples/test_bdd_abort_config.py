import logging
from types import SimpleNamespace

import pytest
from pytest_bdd import given, scenario, then, when

from ska_ser_skallop.event_handling import builders
from ska_ser_skallop.event_handling.logging import device_logging_context
from ska_ser_skallop.mvp_control.describing import mvp_names
from ska_ser_skallop.mvp_control.entry_points import types as conf_types
from ska_ser_skallop.mvp_control.entry_points.base import EntryPoint
from ska_ser_skallop.mvp_control.event_waiting import wait
from ska_ser_skallop.mvp_fixtures.base import ExecSettings
from ska_ser_skallop.mvp_fixtures.context_management import SubarrayContext, TelescopeContext
from ska_ser_skallop.mvp_fixtures.env_handling import ExecEnv

logger = logging.getLogger(__name__)


class Context(SimpleNamespace):
    pass


@pytest.fixture(name="context")
def fxt_context():
    return Context()


# background fixtures
@given("An TMC base API for commanding the telescope")
def set_entry_point(exec_env: ExecEnv):
    exec_env.entrypoint = "tmc"


@given("A running telescope for executing observations on a subarray")
def a_telescope(running_telescope: TelescopeContext):
    pass


@scenario("examples.feature", "Abort configuring")
def test_abort_config():
    """
    Test that we can abort a configuration.

    Scenario: Abort config
        Given a subarray allocated with resources in order to perform a scan
        And that subarray is busy configuring in order to get ready to perform a scan
        When I give the command to Abort
        Then I expect the subarray to cancel the configuration and go the aborted state
    """


@given("a subarray allocated with resources in order to perform a scan")
def allocate_subarray(allocated_subarray):
    pass


@given("that subarray is busy configuring in order to get ready to perform a scan")
def configure_subarray(tmp_path, allocated_subarray: SubarrayContext):
    duration = 2.0
    scan_config = conf_types.ScanConfigurationByFile(
        tmp_path,
        conf_types.ScanConfigurationType.STANDARD,
    )

    # note we are bypassing the automatic teardown as we are putting the subarray back
    # to idle afterwards
    allocated_subarray.set_to_configuring(scan_config, duration, clear_afterwards=False)


@when("I give the command to Abort")
def scan(
    allocated_subarray: SubarrayContext,
    exec_settings: ExecSettings,
    entry_point: EntryPoint,
    context,
):
    builder = builders.get_message_board_builder()
    subarray_node = str(mvp_names.Mid.tm.subarray(allocated_subarray.id))
    checker = (
        builder.check_that(subarray_node)
        .transits_according_to([("CONFIGURING", "ahead"), "ABORTING", "ABORTED"])
        .on_attr("obsState")
        .when_transit_occur_on(
            mvp_names.SubArrays(allocated_subarray.id).subtract("tm").subtract("cbf domain").list
        )
    )
    # allocated_subarray.clear_configuration_when_finished(exec_settings)
    allocated_subarray.reset_after_test(exec_settings)
    # exec_settings.capture_logs_from(subarray_node, DeviceLogLevel.LOG_DEBUG)
    allocated_subarray.push_context_onto_test(
        device_logging_context(builder, exec_settings.get_log_specs())
    )
    board = allocated_subarray.push_context_onto_test(wait.waiting_context(builder, exec_settings))
    context.board = board
    context.checker = checker
    exec_settings.touched = True
    entry_point.abort_subarray(allocated_subarray.id)


@then("I expect the subarray to cancel the configuration and go the aborted state")
def check_results(context):
    checker: builders.Occurrences = context.checker
    board: wait.MessageBoardBase = context.board
    try:
        wait.wait(context.board, 30, live_logging=False)
    except wait.EWhilstWaiting as exception:
        logs = board.play_log_book()
        logger.info(f"Log messages during waiting:\n{logs}")
        raise exception
    checking_logs = checker.print_outcome_for(checker.subject_device)
    logger.info(f"Results of checking:\n{checking_logs}")
    checker.assert_that(checker.subject_device).is_behind_all_on_transit("ABORTED")
