import logging
from types import SimpleNamespace

import pytest
from pytest_bdd import given, scenario, then, when

from ska_ser_skallop.event_handling import builders
from ska_ser_skallop.event_handling.logging import device_logging_context
from ska_ser_skallop.mvp_control.describing import mvp_names
from ska_ser_skallop.mvp_control.entry_points import types as conf_types
from ska_ser_skallop.mvp_control.entry_points.base import EntryPoint
from ska_ser_skallop.mvp_control.event_waiting import wait
from ska_ser_skallop.mvp_control.subarray.base import SBConfig
from ska_ser_skallop.mvp_fixtures.base import ExecSettings
from ska_ser_skallop.mvp_fixtures.context_management import SubarrayContext, TelescopeContext
from ska_ser_skallop.mvp_fixtures.env_handling import ExecEnv

logger = logging.getLogger(__name__)


class Context(SimpleNamespace):
    pass


@pytest.fixture(name="context")
def fxt_context():
    return Context()


# background fixtures
@given("An TMC base API for commanding the telescope")
def set_entry_point(exec_env: ExecEnv):
    exec_env.entrypoint = "tmc"


@given("A running telescope for executing observations on a subarray")
def a_telescope(running_telescope: TelescopeContext):
    pass


@scenario("examples.feature", "Run a scan using a predefined config")
def test_run_scan():
    """
    Test that we can run a scan using a predefined configuration.

    .. code-block: gherkin::

        Scenario: Run a scan using a predefined config
            Given subarray <subarray_id> that has been allocated
                <nr_of_dishes> according to <SB_config>
            And that subarray has been configured as per <scan_config>
                to run a scan
            When the subarray is commanded to perform a scan for 2
                seconds
            Then the subarray will run a scan for 2 seconds and
                afterwards return back to its original
    """


@given(
    "subarray <subarray_id> that has been allocated <nr_of_dishes> according to " "<SB_config>",
    target_fixture="subarray",
)
def allocate_subarray(
    nr_of_dishes,
    subarray_id,
    SB_config,
    running_telescope: TelescopeContext,
    sb_config: SBConfig,
    tmp_path,
    exec_settings: ExecSettings,
) -> SubarrayContext:
    subarray_id = int(subarray_id)
    nr_of_dishes = int(nr_of_dishes)
    receptors = list(range(1, int(nr_of_dishes) + 1))
    exec_settings.time_out = 30

    if SB_config == "standard":
        composition = conf_types.CompositionByFile(tmp_path, conf_types.CompositionType.STANDARD)
    else:
        raise NotImplementedError(f"Unknown configuration {SB_config}")

    return running_telescope.allocate_a_subarray(
        subarray_id, receptors, sb_config, exec_settings, composition
    )


@given("that subarray has been configured as per <scan_config> to run a scan")
def configure_subarray(scan_config, tmp_path, subarray: SubarrayContext):
    duration = 2.0
    if scan_config == "standard":
        scan_config = conf_types.ScanConfigurationByFile(
            tmp_path,
            conf_types.ScanConfigurationType.STANDARD,
        )
    else:
        raise NotImplementedError(f"{scan_config}")

    subarray.configure(scan_config, duration)


@when("the subarray is commanded to perform a scan for 2 seconds")
def scan(
    subarray: SubarrayContext,
    exec_settings: ExecSettings,
    entry_point: EntryPoint,
    context,
):
    builder = builders.get_message_board_builder()
    subarray_node = str(mvp_names.Mid.tm.subarray(subarray.id))
    checker = (
        builder.check_that(subarray_node)
        .transits_according_to(["READY", ("SCANNING", "ahead"), "READY"])
        .on_attr("obsState")
        .when_transit_occur_on(
            mvp_names.SubArrays(subarray.id).subtract("tm").subtract("cbf domain").list
        )
    )
    subarray.check_configuration_when_finished(exec_settings)
    # exec_settings.capture_logs_from(subarray_node, DeviceLogLevel.LOG_DEBUG)
    subarray.push_context_onto_test(device_logging_context(builder, exec_settings.get_log_specs()))
    board = subarray.push_context_onto_test(wait.waiting_context(builder, exec_settings))
    context.board = board
    context.checker = checker
    exec_settings.touched = True
    entry_point.scan(subarray.id)


@then("the subarray will run a scan for 2 seconds and afterwards return back to its " "original")
def check_results(context):
    checker: builders.Occurrences = context.checker
    board: wait.MessageBoardBase = context.board
    try:
        wait.wait(context.board, 30, live_logging=False)
    except wait.EWhilstWaiting as exception:
        logs = board.play_log_book()
        logger.info(f"Log messages during waiting:\n{logs}")
        raise exception
    checking_logs = checker.print_outcome_for(checker.subject_device)
    logger.info(f"Results of checking:\n{checking_logs}")
    checker.assert_that(checker.subject_device).is_behind_all_on_transit("READY")
