import logging
from types import SimpleNamespace

import pytest
from pytest_bdd import given, scenario, then, when

from ska_ser_skallop.event_handling import builders
from ska_ser_skallop.event_handling.logging import device_logging_context
from ska_ser_skallop.mvp_control.describing import mvp_names
from ska_ser_skallop.mvp_control.entry_points import types as conf_types
from ska_ser_skallop.mvp_control.entry_points.base import EntryPoint
from ska_ser_skallop.mvp_control.event_waiting import wait
from ska_ser_skallop.mvp_fixtures.base import ExecSettings
from ska_ser_skallop.mvp_fixtures.context_management import SubarrayContext, TelescopeContext
from ska_ser_skallop.mvp_fixtures.env_handling import ExecEnv

logger = logging.getLogger(__name__)


class Context(SimpleNamespace):
    pass


@pytest.fixture(name="context")
def fxt_context():
    return Context()


# background fixtures
@given("An TMC base API for commanding the telescope")
def set_entry_point(exec_env: ExecEnv):
    exec_env.entrypoint = "tmc"


@given("A running telescope for executing observations on a subarray")
def a_telescope(running_telescope: TelescopeContext):
    pass


@scenario("examples.feature", "Abort scanning")
def test_abort_scan():
    """
    Test we can abort a scan.

    Scenario: Abort scanning
        Given a subarray allocated and configured for running a scan
        And that subarray is busy performing scan
        When I give the command to Abort
        Then I expect the subarray to cancel the configuration and go the aborted state
    """


@given(
    "a subarray allocated and configured for running a scan",
    target_fixture="configured_subarray",
)
def configure_subarray(
    allocated_subarray: SubarrayContext,
    scan_config,
    tmp_path,
    exec_settings: ExecSettings,
):
    duration = 2.0
    if scan_config == "standard":
        scan_config = conf_types.ScanConfigurationByFile(
            tmp_path,
            conf_types.ScanConfigurationType.STANDARD,
        )
    else:
        raise NotImplementedError(f"{scan_config}")

    allocated_subarray.configure(scan_config, duration, exec_settings)
    return allocated_subarray


@given("that subarray is busy performing scan")
def set_subarray_to_scanning(configured_subarray: SubarrayContext, exec_settings: ExecSettings):
    configured_subarray.set_to_scanning(exec_settings, clear_afterwards=False)


@when("I give the command to Abort")
def abort(
    allocated_subarray: SubarrayContext,
    exec_settings: ExecSettings,
    entry_point: EntryPoint,
    context,
):
    builder = builders.get_message_board_builder()
    subarray_node = str(mvp_names.Mid.tm.subarray(allocated_subarray.id))
    checker = (
        builder.check_that(subarray_node)
        .transits_according_to([("SCANNING", "ahead"), "ABORTING", "ABORTED"])
        .on_attr("obsState")
        .when_transit_occur_on(
            mvp_names.SubArrays(allocated_subarray.id).subtract("tm").subtract("cbf domain").list
        )
    )
    # allocated_subarray.clear_configuration_when_finished(exec_settings)
    allocated_subarray.reset_after_test(exec_settings)
    # exec_settings.capture_logs_from(subarray_node, DeviceLogLevel.LOG_DEBUG)
    allocated_subarray.push_context_onto_test(
        device_logging_context(builder, exec_settings.get_log_specs())
    )
    board = allocated_subarray.push_context_onto_test(wait.waiting_context(builder, exec_settings))
    context.board = board
    context.checker = checker
    exec_settings.touched = True
    entry_point.abort_subarray(allocated_subarray.id)


@then("I expect the subarray to cancel the configuration and go the aborted state")
def check_results(context):
    checker: builders.Occurrences = context.checker
    board: wait.MessageBoardBase = context.board
    try:
        wait.wait(context.board, 30, live_logging=False)
    except wait.EWhilstWaiting as exception:
        logs = board.play_log_book()
        logger.info(f"Log messages during waiting:\n{logs}")
        raise exception
    checking_logs = checker.print_outcome_for(checker.subject_device)
    logger.info(f"Results of checking:\n{checking_logs}")
    checker.assert_that(checker.subject_device).is_behind_all_on_transit("ABORTED")
