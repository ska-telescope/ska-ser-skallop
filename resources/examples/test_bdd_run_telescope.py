from pytest_bdd import given, scenario, then, when

from ska_ser_skallop.connectors import configuration
from ska_ser_skallop.mvp_control.describing.mvp_names import Mid
from ska_ser_skallop.mvp_control.entry_points.base import EntryPoint
from ska_ser_skallop.mvp_fixtures.context_management import ExecSettings, TelescopeContext


@given("A mocked entry point")
def set_entry_point():
    pass


@scenario("run_telescope.feature", "Start up the telescope")
def test_start_telescope():
    pass


@given("a telescope")
def a_telescope():
    pass


@when("I start up the telescope")
def start_telescope(
    exec_settings: ExecSettings,
    standby_telescope: TelescopeContext,
    entry_point: EntryPoint,
):
    exec_settings.time_out = 0.1
    exec_settings.capture_logs_from(Mid.tm.subarray(1).__str__())
    with standby_telescope.wait_for_starting_up(exec_settings):
        entry_point.set_telescope_to_running()


@then("central node is in the ON state")
def check_central_node():
    central_node = configuration.get_device_proxy("ska_mid/tm_central/central_node")
    assert central_node.read_attribute("telescopestate").value == "ON"  # type: ignore
