Feature: Examples of using ska testing runway

  Examples:
    | nr_of_dishes  | subarray_id   | SB_config |
    | 2             | 1             | standard  |

  Background:
    Given A running telescope for executing observations on a subarray
    And An TMC base API for commanding the telescope

  Scenario: Allocate dishes to a subarray using a predefined config
		When I allocate <nr_of_dishes> dishes to subarray <subarray_id> using the <SB_config> SB configuration
		Then The subarray is in the condition that allows scan configurations to take place

  Scenario: Configure a scan using a predefined config
    Given subarray <subarray_id> that has been allocated <nr_of_dishes> according to <SB_config>
    When I configure the subarray to perform a <scan_config> scan
    Then the subarray is in the condition to run a scan

    Examples:
      | scan_config |
      | standard    |

  Scenario: Run a scan using a predefined config
    Given subarray <subarray_id> that has been allocated <nr_of_dishes> according to <SB_config>
    And that subarray has been configured as per <scan_config> to run a scan
    When the subarray is commanded to perform a scan for 2 seconds
    Then the subarray will run a scan for 2 seconds and afterwards return back to its original

    Examples:
      | scan_config |
      | standard    |


  Scenario: Abort configuring
    Given a subarray allocated with resources in order to perform a scan
    And that subarray is busy configuring in order to get ready to perform a scan
    When I give the command to Abort
    Then I expect the subarray to cancel the configuration and go the aborted state


  Scenario: Abort scanning
    Given a subarray allocated and configured for running a scan
    And that subarray is busy performing scan
    When I give the command to Abort
    Then I expect the subarray to cancel the configuration and go the aborted state

    Examples:
      | scan_config |
      | standard    |