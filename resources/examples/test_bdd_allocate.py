import logging
from types import SimpleNamespace

import pytest
from pytest_bdd import given, scenario, then, when

from ska_ser_skallop.event_handling import builders
from ska_ser_skallop.mvp_control.describing import mvp_names
from ska_ser_skallop.mvp_control.entry_points import types as conf_types
from ska_ser_skallop.mvp_control.entry_points.base import EntryPoint
from ska_ser_skallop.mvp_control.event_waiting import wait
from ska_ser_skallop.mvp_control.subarray.compose import SBConfig
from ska_ser_skallop.mvp_fixtures.base import ExecSettings
from ska_ser_skallop.mvp_fixtures.context_management import TelescopeContext
from ska_ser_skallop.mvp_fixtures.env_handling import ExecEnv

logger = logging.getLogger(__name__)


class Context(SimpleNamespace):
    pass


@pytest.fixture(name="context")
def fxt_context():
    return Context()


# background fixtures
@given("A running telescope for executing observations on a subarray")
def a_running_telescope(running_telescope: TelescopeContext):
    pass


@given("An TMC base API for commanding the telescope")
def set_entry_point(exec_env: ExecEnv):
    exec_env.entrypoint = "tmc"


@scenario(
    "examples.feature",
    "Allocate dishes to a subarray using a predefined config",
)
def test_allocate_subarray():
    """
    Test that we can allocate dishes to a subarray using a predefined configuration.

    Scenario: Allocate dishes to a subarray using a predefined config
                When I allocate <nr_of_dishes> dishes to subarray <subarray_id> using
                    the <SB_config> SB configuration
                Then The subarray is in the condition that allows scan configurations to
                    take place
    """


@when(
    "I allocate <nr_of_dishes> dishes to subarray <subarray_id> using the <SB_config> "
    "SB configuration"
)
def allocate(
    tmp_path,
    nr_of_dishes,
    subarray_id,
    SB_config,
    context,
    sb_config: SBConfig,
    exec_settings: ExecSettings,
    running_telescope: TelescopeContext,
    entry_point: EntryPoint,
):
    subarray_id = int(subarray_id)
    nr_of_dishes = int(nr_of_dishes)
    receptors = list(range(1, int(nr_of_dishes) + 1))
    SB_config = SB_config.lower()

    if SB_config == "standard":
        composition = conf_types.CompositionByFile(tmp_path, conf_types.CompositionType.STANDARD)
    else:
        raise NotImplementedError(f"Unknown configuration {SB_config}")

    builder = builders.get_message_board_builder()
    checker = (
        builder.check_that(str(mvp_names.Mid.tm.subarray(subarray_id)))
        .transits_according_to(["EMPTY", ("RESOURCING", "ahead"), "IDLE"])
        .on_attr("obsState")
        .when_transit_occur_on(
            mvp_names.SubArrays(subarray_id).subtract("tm").subtract("cbf domain").list
        )
    )

    running_telescope.release_subarray_when_finished(subarray_id, receptors, exec_settings)
    board = running_telescope.push_context_onto_test(wait.waiting_context(builder))
    context.board = board
    context.checker = checker
    exec_settings.touched = True
    entry_point.compose_subarray(
        subarray_id,
        receptors,
        composition,
        sb_config.sbid,
    )


@then("The subarray is in the condition that allows scan configurations to take place")
def check_ready_for_scan_config(context):
    checker: builders.Occurrences = context.checker
    board: wait.MessageBoardBase = context.board
    try:
        wait.wait(context.board, 30, live_logging=False)
    except wait.EWhilstWaiting as exception:
        logs = board.play_log_book()
        logger.info(f"Log messages during waiting:\n{logs}")
        raise exception
    checking_logs = checker.print_outcome_for(checker.subject_device)
    logger.info(f"Results of checking:\n{checking_logs}")
    checker.assert_that(checker.subject_device).is_ahead_of_all_on_transit("RESOURCING")
    checker.assert_that(checker.subject_device).is_behind_all_on_transit("IDLE")
