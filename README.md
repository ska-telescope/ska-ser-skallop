SKA Testing Runway
==================

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-ser-skallop/badge/?version=latest)](https://developer.skatelescope.org/projects/ska-ser-skallop/en/latest/?badge=latest)


This repository contains the set of python libraries, charts and other artifacts useful to create a "runway" for implementing end to end tests on SKA software.

Usage and Installation
----------------------

The ska-ser-skallop package can be installed either as a "build out" testing system (default) or as a "build in" testing system.
In order to run it as a build in testing system:
```
pip install ska-ser-skallop[bit]
```
This will install skar-ser-skallop together with pytango and related dependencies so that the test can run inside the kubernetes cluster as a pytango client.

Otherwise, you can run the test script outside the kubernetes cluster through a tango bridge connected to a tangogql service running as part of taranta.

```
pip install ska-ser-skallop
```


To use it as a build out system one can run tests on a remote cluster using the following bash script (linux only)

```
. setenv.sh
```

Assuming your vpn is set, follow the command prompts and select the branch, telescope and taranta username and password.

Once that has been set the output will give you a set of env variables (you can copy that in a .env file also for IDE use).

To test that you can connect to the cluster run:
```
pingmvp
```

SKALLOP package can be used as testing-runway for SKA-Mid as well as SKA-Low.

Differnt environment variables are used for SKA-Mid and SKA-Low testing:
1. ```SKA_TELESCOPE``` - This env variable gets automatically set by gitlab pipeline. Also, it can be utilised when MVP is tested on local machine. 
2. ```TEL``` - This env varibale gets automatically set by setenv.sh script. This script sets the Telescope_VARIANT which states the type of telescope to be tested. This is utilized when SKALLOP is independently tested.

Default values for environment variables:
* Ignore if previous test have already set the type
* If test case marker is for both then look at env vars
* If none is marked then look at env vars
* If env vars are inconsistent then default type is mid
* If no env vars have been set and no markers then default type is mid

@pytest.mark.skamid
def test_skamid():
    # example of test that will set telescope type as skamid

@pytest.mark.skalow
def test_skalow():
    # example of test that will set telescope type as skalow

Skallop Fixtures
^^^^^^^^^^^^^^^^

Skallop loads a set of fixtures automically when installed.
It also have some fixtures that will load automitically during an MVP test.
To enable that you need to create a fixture in your conftest.py as follow:

```
import pytest
@pytest.fixture(name="disable_fixtures", scope="session", autouse=True)
def fxt_disable_fixtures():
    return False
    
```



Development
-----------

Requirements
------------


Platform:
* Python Interpreter: > 3.8
* OS = Linux


Development tools:
* Make
* virtualenv
* pip
* setuptools
* pip-tools (via pip and virtualenv/conda)
* pipdeptree (via pip and virtualenv/conda)
* VScode (optional)
* conda (optional)

There are three layers of installations based on the required dependencies

1. requirements.txt: The minimum amount of dependencies necessary to deliver the software as a self standing shared library: `make install`
2. test-requirements.txt: The additional dependencies needed to run CI tests on the software: `make install_test`
2. doc-requirements.txt and dev-requirements.txt: The additional dependencies needed to create software documentation and perform development tasks. `make install_dev`

The requirements files are determined by the input dependencies defined in dependencies folder (e.g `requirements.in`, `dev-requirements.in` etc). Thus in order to determine the requirements, you define the needed packages in the `*.in` files which then translates to requirements files using the pip tools. This ensures a separation between physical configuration and functional configuration.

Set Up
------
(Linux based)

```
git clone https://gitlab.com/ska-telescope/ska-ser-skallop.git

cd ska-ser-skallop

```

The recommended virtualization tool is virtualenv:

```
make venv/
source ./venv/bin/activate
```
You can also use conda as your virtual environment tool. Once you have conda installed:

```
make cenv/
conda activate ./cenv
```

To update conda with new requirements:

```
make conda_update
```

Lastly, you can use docker as virtualization tool:

```
make devc
make attach_devc
```

This project manages dependencies using pip-tools. In order to activate pip tools you first need to install this into your newly create virtual environment:

```
make setup
```

Testing
-------

* To run tests during development: `make test_dev`
* Ci tests are run using the `make test` command that does not involve installation of development and documentation dependencies$1

Writing documentation
---------------------
Sphinx is used to generate documentation.
* Design: Located inside src
* API: docstrings are provided inside the modules and then automatically generated
inside the docs



Development
-----------

VSCode is recommended as a IDE tool with Linux as OS. In windows platforms remote containers can be used.
