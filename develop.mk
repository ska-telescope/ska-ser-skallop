# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c
IMAGE_NAME=ska-ser-skallop
IMAGE_TAG=latest
CONTAINER_NAME=dev-testing
PYTHON_VERSION=3.9

LIVE_LOG=false
test_dev: install_dev build/
	@python3 -m pytest -o log_cli=$(LIVE_LOG) --cov --json-report --json-report-file=build/reports/report.json \
	 --cov-report term  --cov-report xml:build/reports/code-coverage.xml --cov-report html:build/reports/coverage/html --junitxml=build/reports/unit-tests.xml

setup_graphviz: ##needed for creating graphviz diagrams from pipdeptree (may need sudo)
	apt-get install python-pydot python-pydot-ng

## development environment ##
dev_image: deployment/dev_container/Dockerfile
	make -C deployment/dev_container/ build

attach_devc:
	docker run -it -e LOCAL_USER_ID=`id -u $$USER` -v $$(pwd):/app/ devc:latest bash

TEST_NAME?=''
debug_attach_test:install_dev
	@echo 'wating for debug client to connect to port 5678...'
	@python -m debugpy --listen 5678 --wait-for-client -m pytest tests/$(TEST_NAME)

serve: install_dev
	python -m http.server --directory docs/build/html/

livehtml:
	make -C docs livehtml

## telepresence into kubernetes
telepresence:
	telepresence --run-shell --method inject-tcp


