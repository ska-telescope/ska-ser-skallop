"""Objects for setting up and configuring a subscribing messageboard.

This module provides the interfaces for configuring a messageboard in the
:py:mod:`ska_ser_skallop.subscribing` module for typical subscriptions
necessary to wait for events on device attributes. The configuration
instructions are done in a pseudo human like language format depending
on the type of instruction e.g.:

.. code-block: python::

    b.set_waiting_on(
        'mid_d0001/elt/master'
    ).for_attribute('State').to_become_equal_to('STANDBY')
"""

from abc import abstractmethod
from contextlib import contextmanager
from typing import List, Set, Tuple, Union

from ska_ser_skallop.connectors import configuration
from ska_ser_skallop.connectors.poller import get_resource_state_producer
from ska_ser_skallop.event_handling.handlers import ObserveEvent
from ska_ser_skallop.subscribing import configuration as subscribing_conf
from ska_ser_skallop.utils.generic_classes import Symbol
from ska_ser_skallop.utils.singleton import Singleton

from . import base, handlers
from .base import MessageBoardBase
from .occurrences import Occurrences


class BuildSpec:
    """Object containing all the necessary specifications for setting up a subscription."""

    def __init__(
        self,
        attr: str,
        device: base.Producer,
        handler: base.MessageHandler,
        polling: bool,
    ) -> None:
        """Initialise the object.

        :param attr: The device attribute to be subscribed to
        :param device: The device object to be subscribed to
        :param handler: The event's handler to use for subscription events
        :param polling: Flag whether subscription must be polled base or not

        """
        self.attr = attr
        self.device = device
        self.handler = handler
        self.polling = polling


class BaseBuilder:
    """Abstract class for a Builder."""

    board: MessageBoardBase

    def __init__(self, board=None) -> None:
        """Initialise the object.

        :param board: the messageboard to inject, defaults to None
            in which case a default MessageBoardBase object will be initialised and used.
        """
        if board is None:
            board = MessageBoardBase()
        self.board = board

    @abstractmethod
    def add_spec(self, spec: BuildSpec):
        """Add a build spec.

        (abstract)

        :param spec: The spec to be added
        """

    @staticmethod
    def get_device(device_name: Union[str, Symbol]) -> base.Producer:
        """Use a factory to get a implementation of device to use.

        :param device_name: The name identifying the device
        :return: The provided device object
        """
        return configuration.get_producer(str(device_name), fast_load=True)


class ForAttr:
    """Intermediate class for building specifications on a messageboard."""

    def __init__(
        self,
        attr: str,
        device: base.Producer,
        builder: BaseBuilder,
        polling: bool = False,
        mon_producer: None | base.Producer = None,
    ):
        """Initialise the object.

        :param attr: the attr as subject for building a subscriptions
        :param device: the device that will produces events on that attribute
        :param builder: the builder object to add the specification to
        :type builder: BaseBuilder
        :param polling: Flag whether subscription must be polled base or not, defaults to False
        :param mon_producer: whether we should use a producer for infra monitoring, defaults to None
        """
        self.attr = attr
        self.device: base.Producer = device
        self.builder = builder
        self.polling = polling
        self.mon_producer = mon_producer

    def _add_spec(self, handler: base.MessageHandler) -> BuildSpec:
        spec = BuildSpec(self.attr, self.device, handler, self.polling)
        self.builder.add_spec(spec)
        attr = "throttling"
        if self.mon_producer:
            handler = ObserveEvent(self.builder.board, attr, self.device.name())
            spec = BuildSpec(attr, self.mon_producer, handler, polling=False)
            self.builder.add_spec(spec)
        return spec

    def to_become_equal_to(
        self,
        value: Union[str, List[str]],
        master: bool = False,
        ignore_first: bool = True,
    ) -> BuildSpec:
        """Add a specification for an event subscription on a messageboard.

        This will be based on the given device and attribute in which each event will be handled by
        a type of handler that waits for an event (i.e the value of the event) to be
        equal to the expected value.

        :param value: the value that is expected
        :param master: whether the handling of these events should should
            affect all other subscriptions on the messageboard by unsubscribing all
            other auxillary subscriptions once the expected conditions have been met.
            Defaults to False.
        :param ignore_first: flag indicating whether first events should be
            disregarded, Defaults to True

        :return: returns the spec itself for admin purposes
        """
        board = self.builder.board
        handler = handlers.WaitUntilEqual(
            board, self.attr, value, self.device.name(), master, ignore_first
        )
        spec = self._add_spec(handler)
        return spec

    def to_change(self, master: bool = False) -> BuildSpec:
        """Add a specification for an event subscription on a messageboard.

        This will be based on the given device and attribute in which each event will be handled by
        a type of handler that waits for events (i.e. the value of the event) to have changed from
        it's original state to a new one.

        :param master: whether the handling of these events should should
            affect all other subscriptions on the messageboard by unsubscribing all
            other auxillary subscriptions once the expected conditions have been met.
            Defaults to False.

        :return: the spec itself for admin purposes
        """
        board = self.builder.board
        handler = handlers.WaitforChange(board, self.attr, self.device.name(), master)
        spec = self._add_spec(handler)
        return spec

    def to_change_in_order(self, order: List, master: bool = False) -> BuildSpec:
        """Add a specification for an event subscription on a messageboard.

        This will be based on the given device and attribute in which each event will be handled by
        a type of handler that waits for events (i.e. the value of the event) to occur
        in a specific order after which it will unsubscribe and end the waiting on the
        message board.

        :param order: the list of expected values (strings) in the provided order
        :param master: whether the handling of these events should should
            affect all other subscriptions on the messageboard by unsubscribing all
            other auxillary subscriptions once the expected conditions have been met.
            Defaults to False.

        :return: the spec itself for admin purposes
        """
        board = self.builder.board
        handler = handlers.WaitForOrderedChange(board, self.attr, order, self.device.name(), master)
        spec = self._add_spec(handler)
        return spec

    def and_observe(self) -> BuildSpec:
        """Add a specification for an event subscription on a messageboard.

        This will be based on the given device and attribute in which each event will be handled by
        a type of handler that only records the values of the generated events but do
        not wait for a specific event to occur. It therefore rests on the assumption
        that some other event handler will be responsible for removing the subscription
        and therefore end the waiting for events.

        :return: the spec itself for admin purposes
        """
        board = self.builder.board
        handler = handlers.ObserveEvent(board, self.attr, self.device.name())
        spec = self._add_spec(handler)
        return spec


class SetWaitingOn:
    """Intermediate object used to hold device data to be waited on.

    This will be used for generating subsequent specifications
    """

    def __init__(
        self,
        device: base.Producer,
        builder: BaseBuilder,
        mon_producer: None | base.Producer = None,
    ) -> None:
        """Initialise the object.

        :param device: The device object to use for subscriptions
        :param mon_producer: whether monitoring of resources should be used, defaults to None
        :param builder: the builder object to add the specification to
        """
        self.builder = builder
        self.device = device
        self._mon_producer = mon_producer

    def for_attribute(self, attr: str, polling=False) -> ForAttr:
        """
        Return an intermediate object containing device data and attributes.

        This will in turn then be used to generate a specification for
        subscriptions on a messageboard.

        :param attr: the attribute of the device to be waited on polling
            (bool, optional): whether the subscription on that device
            should be polled base or not. Defaults to False.
        :param polling: Wether to use polling for the subscription

        :return: the object that contains information about the device
            and attribute to be subscribed to
        """
        # ensure all attributes are send as lower case
        attr = attr.lower()
        return ForAttr(attr, self.device, self.builder, polling, self._mon_producer)


class TransitionChecker:
    """Class to coordinate the creation of subscriptions enabling transition checking.

    This allows for verifying the ordering of transitions to states on a device
    that derives its state from transitions on other devices being followed
    """

    def __init__(self, device: base.Producer, builder: BaseBuilder, polling=False) -> None:
        """Initialise the object.

        :param device: the device object that will be investigated.
        :param builder: the builder object to add the specification to
        :param polling: wether to use polling for the subscription, defaults to False

        """
        self.device = device
        self.builder = builder
        self.polling = polling

    def transits_according_to(self, transits: List[Union[str, Tuple[str, str]]]):
        """Specify the expected order of events occurring on the given device.

        TODO evaluate if using a tuple for transit events is redundant

        :param transits: the list containing the orderred events,
            items can either be a simple string or a tuple for which
            the second value indicates if the transit is expected to be behind or ahead
            of all devices.
        :return: Itself so as to chain specifications
        """
        self.transits = transits
        return self

    def on_attr(self, attr_to_check: str):
        """Specify the attribute that will be subscribed to for events.

        :param attr_to_check: the attribute that will be subscribed to for events.
        :return: Itself so as to chain specifications

        """
        attr_to_check = (
            attr_to_check.lower()
        )  # make all lower case in case for correct attribute name
        self.attr_to_check = attr_to_check
        return self

    def _add_spec(
        self,
        device: base.Producer,
        handler: base.MessageHandler,
        attr_to_check: str,
    ) -> BuildSpec:
        spec = BuildSpec(attr_to_check, device, handler, self.polling)
        self.builder.add_spec(spec)
        return spec

    def when_transit_occur_on(
        self,
        devices_to_follow: List[Union[str, Symbol]],
        ignore_first=True,
        devices_to_follow_attr=None,
    ) -> Occurrences:
        """Specify the devices to compare events (and their relative order of occurrence) against.

        :param devices_to_follow: The devices which will be compared against.
        :param ignore_first: Whether the first event should be ignored, defaults to True
        :param devices_to_follow_attr: if a different attribute name than the attribute for the
            device being investigated should be used for comparison then this should be given
            as string, defaults to None which means it must be the same.
        :return: An Occurrences object that will contain the occurrence of events resulting from
            the handling of subscription events on the messageboard and which can afterwards be used
            for evaluating the correct ordering
        """
        board = self.builder.board
        occurrences = Occurrences(self.transits)
        occurrences.set_subject_device(self.device.name())
        handler = handlers.WaitForOrderedChange(
            board,
            self.attr_to_check,
            occurrences.expected,
            self.device.name(),
            ignore_first=ignore_first,
            wire_tap=occurrences,
        )
        if not devices_to_follow_attr:
            devices_to_follow_attr = self.attr_to_check
        self._add_spec(
            self.device,
            handler,
            self.attr_to_check,
        )
        for device_name in devices_to_follow:
            handler = handlers.WaitForOrderedChange(
                board,
                devices_to_follow_attr,
                occurrences.expected,
                str(device_name),
                ignore_first=ignore_first,
                wire_tap=occurrences,
            )
            device = self.builder.get_device(device_name)
            self._add_spec(device, handler, devices_to_follow_attr)
        return occurrences


def get_message_board_builder():
    """Get an instance of MessageboardBuilder.

    Note this may be in interjected builder containing
    subscription specifications setup by previous calls.

    :returns: an instance of MessageboardBuilder
    """
    container = MessageBoardBuilderContext()  # type: ignore
    return container.builder


def get_new_message_board_builder():
    """Get an new instance of MessageboardBuilder.

    :returns: an instance of MessageboardBuilder
    """
    return MessageBoardBuilder()


def clear_supscription_specs():
    """Clear any subscription specifications that was previously done.

    This allows for a tear down command to be done without any possible subscription
    specifications remaining.
    """
    container = MessageBoardBuilderContext()  # type: ignore
    container.reset()


@contextmanager
def interject_builder(builder: "MessageBoardBuilder"):
    """Interject getting a messageboard with given builder.

    During the given context, any call to get a messageboard will result in the given
    builder instead of a new one.

    :param builder: The builder to give callers for the duration of the context.
    :yields: None
    """
    container = MessageBoardBuilderContext()  # type: ignore
    container.interject(builder)
    yield
    container.reset()


class MessageBoardBuilderContext(metaclass=Singleton):
    """Singleton class holding context of whether a new or interjected builder must be used."""

    def __init__(self) -> None:
        """Initialise object."""
        self._interjected_builder = None

    def interject(self, builder: "MessageBoardBuilder"):
        """Interject context to use a injected builder instead of a new one.

        :param builder: The builder to uses instead of a new instance
        """
        self._interjected_builder = builder

    def reset(self):
        """Reset the context to use a new builder when called for."""
        self.__init__()

    @property
    def builder(self):
        """Get a builder to provide to callers.

        This is normally a new instance unless a injected builder have been set.

        :returns: The builder to use.
        """
        if builder := self._interjected_builder:
            return builder
        return MessageBoardBuilder()


class MessageBoardBuilder(BaseBuilder):
    """Configure a messageboard for waiting on tango devices.

    This will be done based on subscriptions on device attributes and corresponding handlers
    set up to handle consequent events.
    """

    def __init__(self) -> None:
        """Initialise the object."""
        board = subscribing_conf.get_messageboard()
        # ensure factory is enabled
        super().__init__(board)
        self.specs: Set[BuildSpec] = set()
        self._mon_resources = False

    def set_wait_for_long_running_command_on(
        self, device_name: Union[str, Symbol], ignore_first=True
    ):
        """Generate a specification to wait for a long running command to be completed.

        :param device_name: _description_
        :type device_name: Union[str, Symbol]
        :param ignore_first: _description_, defaults to True
        :type ignore_first: bool, optional
        :returns: an WaitForLRCComplete instance
        """
        device = self.get_device(device_name)
        handler = handlers.WaitForLRCComplete(self.board, device, ignore_first=ignore_first)
        spec = BuildSpec("longrunningcommandstatus", device, handler, False)
        self.add_spec(spec)
        return handler

    def set_waiting_on(
        self, device_name: Union[str, Symbol], mon_resources: bool = False
    ) -> SetWaitingOn:
        """Generate a helper object upon which subsequent types of event waiting can be specified.

        The resulting object will contain a handle to the builder in order tor write back subsequent
        specifications.

        :param device_name: the qualified device name as a string
        :param mon_resources: wether the cpu throttling should also me monitored concurrently

        :return: the object used to hold the device information and generate subsequent event
            waiting specifications on.
        """
        device = self.get_device(device_name)
        mon_resources_producer = None
        if mon_resources:
            mon_resources_producer = get_resource_state_producer(str(device_name))
        return SetWaitingOn(device, self, mon_resources_producer)

    def set_logging_on(self, consumer_name: str):
        """Specify a subscription to be made for log message events on a log consumer.

        in other words specify a tango device that collects log messages from multiple
        devices. Note the devices for which the log consumer will listen to is
        configured separately.

        :param consumer_name: the name of the log consumer which will be subscribed to
        """
        handler = handlers.ObserveLogEvent(self.board, "message", consumer_name)
        consumer_device = configuration.get_producer(consumer_name, fast_load=True)
        spec = BuildSpec("message", consumer_device, handler, False)
        self.add_spec(spec)

    def add_spec(self, spec: BuildSpec) -> None:
        """Add a specification for a messageboard subscription to be build.

        :param spec: the specification for a messageboard subscription to be build
        """
        self.specs.add(spec)

    def check_that(self, device_name: Union[str, Symbol]) -> TransitionChecker:
        """Create an TransitionChecker object.

        This can be used to coordinate the creation of subscriptions that enable
        checking the ordering of transitions to states on a device that derives its
        state from transitions on other devices being followed

        :param device_name: the qualified device name as a string

        :returns: the object used to coordinate the creation of
            subscriptions
        """
        device = configuration.get_device_proxy(device_name, fast_load=True)
        return TransitionChecker(device, self)

    def play_spec(self) -> str:
        """Return a human readable description of all the current specifications loaded.

        This can be usefull for debugging purposes.

        :return: the human readable spec
        """
        desc = ""
        for _, spec in enumerate(self.specs):
            desc = f"{desc}\n{spec.handler.describe_self()}"
        return desc

    def setup_board(self):
        """Apply the specifications loaded and convert it to running subscriptions.

        :returns: a messageboard containing the running subscriptions.
        """
        for _, spec in enumerate(self.specs):
            handler = spec.handler
            polling = spec.polling
            self.board.add_subscription(spec.device, spec.attr, handler, polling)
        return self.board


class TransitChecking:
    """Class acting as a facade to setting up and checking afterwards correct transits."""

    def __init__(self, builder: MessageBoardBuilder) -> None:
        """Initialise object.

        :param builder: the injected builder to use  upon which specifications will be placed.
        """
        self._builder = builder
        self._checker = None

    def __bool__(self) -> bool:
        """Whether a tarnsit check specification have been specified.

        This implies a user has called the `check_that` command.

        :return: True if a transit check has been set.
        """
        return self._checker is not None

    @property
    def checker(self):
        """Return the checker to use if a transit check have been specified.

        :return: the checker if a transit check have been specified, otherwise None.
        """
        return self._checker

    def check_that(self, device_name: Union[str, Symbol]):
        """Initiate an transition checking specification.

        :param device_name: the qualified device name as a string

        :returns: Itself so as to chain specifications
        """
        self.transition_builder = self._builder.check_that(device_name)
        return self

    def transits_according_to(self, transits: List[Union[str, Tuple[str, str]]]):
        """Specify the expected order of events occurring on the given device.

        TODO evaluate if using a tuple for transit events is redundant

        :param transits: the list containing the orderred events,
            items can either be a simple string or a tuple for which
            the second value indicates if the transit is expected to be behind or ahead
            of all devices.
        :return: Itself so as to chain specifications
        """
        self.transition_builder.transits_according_to(transits)
        return self

    def on_attr(self, attr_to_check: str):
        """Specify the attribute that will be subscribed to for events.

        :param attr_to_check: the attribute that will be subscribed to for events.
        :return: Itself so as to chain specifications

        """
        self.transition_builder.on_attr(attr_to_check)
        return self

    def when_transit_occur_on(
        self,
        devices_to_follow: List[Union[str, Symbol]],
        ignore_first=True,
        devices_to_follow_attr=None,
    ):
        """Specify the devices to compare events (and their relative order of occurrence) against.

        Note this completes the specification chain and will result in a `checker`
        attribute being set.
        In otherwords transit checking is enabled from hereon and will result in
        specifications being loaded, by the next waiting context manager.

        :param devices_to_follow: The devices which will be compared against.
        :param ignore_first: Whether the first event should be ignored, defaults to True
        :param devices_to_follow_attr: if a different attribute name than the attribute for the
            device being investigated should be used for comparison then this should be given
            as string, defaults to None which means it must be the same.
        """
        self._checker = self.transition_builder.when_transit_occur_on(
            devices_to_follow, ignore_first, devices_to_follow_attr
        )
