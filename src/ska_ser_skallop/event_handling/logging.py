"""Module dealing with setting up log events on a tango log consumer."""

import functools
import logging
from contextlib import contextmanager
from enum import Enum
from typing import List, NamedTuple, Set, Tuple

from ska_ser_skallop.connectors import configuration
from ska_ser_skallop.event_handling import builders
from ska_ser_skallop.mvp_control.base import AbstractDeviceProxy
from ska_ser_skallop.utils.device_health import i_can_communicate_with

logger = logging.getLogger(__name__)


class DeviceLogLevel(Enum):
    """Duct typed wrapper representing a Tango Device Log level."""

    LOG_OFF = 0
    LOG_FATAL = 1
    LOG_ERROR = 2
    LOG_WARN = 3
    LOG_INFO = 4
    LOG_DEBUG = 5


class Log_Registrar:
    """Object to use for setting up and tearing down reception of log events."""

    def __init__(self, log_consumer_name: str = "logconsumer/log/log01") -> None:
        """Initialise the object.

        :param log_consumer_name: The tango FQDN name for the log consumer,
            defaults to "logconsumer/log/log01"
        """
        self.registrations: Set[Tuple[str, DeviceLogLevel]] = set()
        self.log_consumer_name = log_consumer_name

    def get_logging_target(self) -> str:
        """Return the log consumer name used by the object.

        :return: the log consumer name used by the object.
        :rtype: str
        """
        return self.log_consumer_name

    @property
    def consumer_available(self) -> bool:
        """Indicate if a log consumer device is available.

        :return: True if a log consumer device is available.
        """
        return i_can_communicate_with(self.log_consumer_name)

    @staticmethod
    def _get_device_server(device_name) -> AbstractDeviceProxy:
        db_device = configuration.get_device_proxy("sys/database/2", fast_load=True)
        result = db_device.command_inout("DbGetDeviceInfo", device_name)
        dserver_name = f"dserver/{result[1][3].lower()}"
        dserver = configuration.get_device_proxy(dserver_name, fast_load=True)
        return dserver

    def enable_logging(self, dev_name: str, logLevel: DeviceLogLevel):
        """Enable a tango device to transmit log messages to a log consumer device.

        :param dev_name: The FQDN of the device.
        :param logLevel: The level of filtering for messages that must be transmitted.
        """
        dserver = self._get_device_server(dev_name)
        current_level = dserver.command_inout("GetLoggingLevel", [dev_name])[0][0]
        current_level = DeviceLogLevel(current_level)
        dserver.command_inout("AddLoggingTarget", [dev_name, f"device::{self.log_consumer_name}"])
        dserver.command_inout("SetLoggingLevel", [[logLevel.value], [dev_name]])
        self.registrations.add((dev_name, current_level))

    def disable_logging(self):
        """Disable the current transmission of log messages to a log consumer device."""
        while self.registrations:
            dev_name, level = self.registrations.pop()
            dserver = self._get_device_server(dev_name)
            dserver.command_inout(
                "RemoveLoggingTarget",
                [dev_name, f"device::{self.log_consumer_name}"],
            )
            dserver.command_inout("SetLoggingLevel", [[level.value], [dev_name]])


class LogSpec:
    """Contains a list of specification to capture logs from remote tango devices."""

    def __init__(self) -> None:
        """Initialise the object."""
        self._specs: List[DeviceLogSpec] = []

    def add_log(
        self,
        device_name: str,
        log_level: DeviceLogLevel = DeviceLogLevel.LOG_INFO,
    ):
        """Add a new specification for the capturing of log messages.

        :param device_name: The FQDN for the tango device to be subscribed to.
        :param log_level: The level of filtering for messages that must be transmitted
            , defaults to DeviceLogLevel.LOG_INFO
        :return: Itself so as to be able to chain the addition of log specifications
        """
        self._specs.append(DeviceLogSpec(device_name, log_level))
        return self

    @property
    def specs(self) -> List["DeviceLogSpec"]:
        """Get the list of log specifications contained within the object.

        :return: the list of log specifications contained within the object.
        """
        return self._specs

    def add_logs(self, *device_names: str):
        """Add the specification for the group of devices to be logged.

        Note all devices given will be logged on DeviceLogLevel.LOG_INFO

        :param device_names: the list of device FQDNs to log.
        """
        for device_name in device_names:
            self._specs.append(DeviceLogSpec(device_name, DeviceLogLevel.LOG_INFO))


class DeviceLogSpec(NamedTuple):
    """Bundles a device name and its corresponding log level as log specification object."""

    device_name: str
    log_level: DeviceLogLevel = DeviceLogLevel.LOG_INFO


def log_devices(builder: builders.MessageBoardBuilder, device_logging_spec: LogSpec):
    """Decorate a function with the setting up and tearing down afterwards for device logging.

    :param builder: The builder object to which specification of handlers for
        logging will be added to.
    :param device_logging_spec: Specification of the device logging to be configured.
    :returns: decorated function
    """

    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            registrar = Log_Registrar()
            if registrar.consumer_available:
                for spec in device_logging_spec.specs:
                    registrar.enable_logging(spec.device_name, spec.log_level)
                builder.set_logging_on(registrar.get_logging_target())
                try:
                    result = func(*args, **kwargs)
                finally:
                    registrar.disable_logging()
            else:
                logger.warning(
                    f"log consumer {registrar.log_consumer_name} not available, log "
                    "capture disabled"
                )
                result = func(*args, **kwargs)
            return result

        if device_logging_spec.specs:
            return wrapper
        return func

    return decorator


# context manager
@contextmanager
def device_logging_context(builder: builders.MessageBoardBuilder, device_logging_spec: LogSpec):
    """Generate a context for setting up and tearing down of device logging.

    :param builder: The builder object to which specification of handlers for
        logging will be added to.
    :param device_logging_spec: Specification of the device logging to be configured.
    :yields: a context manager for setting up and tearing down afterwards.

    """
    registrar = Log_Registrar()
    if registrar.consumer_available:
        if device_logging_spec.specs:
            for spec in device_logging_spec.specs:
                registrar.enable_logging(spec.device_name, spec.log_level)
            builder.set_logging_on(registrar.get_logging_target())
        try:
            yield
        finally:
            registrar.disable_logging()
    else:
        logger.warning(
            f"log consumer {registrar.log_consumer_name} not available, log capture " "disabled"
        )
        yield


class LogChecking:
    """Facade object for managing log capturing during a particular context."""

    def __init__(self) -> None:
        """Initialise the object."""
        self._log_spec: LogSpec = LogSpec()

    def __bool__(self) -> bool:
        """Indicate if any log specifications have been set.

        :return: True if log specs have been loaded.
        """
        return bool(self._log_spec.specs)

    def capture_logs_from(
        self,
        device_name: str,
        log_level: DeviceLogLevel = DeviceLogLevel.LOG_INFO,
    ):
        """Add an entry to the internal remote device log capturing spec for a device.

        :param device_name: The device for which logs must be captured with.
        :type device_name: str
        :param log_level: The level at which logs
            must be captured, defaults to DeviceLogLevel.LOG_INFO
        """
        self._log_spec.add_log(device_name, log_level)

    def capture_logs_from_devices(self, *device_names: str):
        """Add a set of log capturing entries for a list of device names.

        Note the log level for each device will be fixed to `DeviceLogLevel.LOG_INFO`
        as a set of positional arguments.
        """  # noqa: DAR101
        self._log_spec.add_logs(*device_names)

    @contextmanager
    def device_logging_context(self, builder: builders.MessageBoardBuilder):
        """Generate a context for setting up and tearing down of device logging.

        :param builder: The builder object to which specification of handlers for
            logging will be added to.
        :yields: a context manager for setting up and tearing down afterwards.

        """
        with device_logging_context(builder, self._log_spec):
            yield
