"""Event handlers and configuration scripts for handling and checking correct occurrence of events.

This sub package is a particular usage of the :py:mod:`ska_ser_skallop.subscribing` package to
manage tango related events that occur during the execution of runtime tests. It
consists of a set of connectors calling the :py:mod:`ska_ser_skallop.subscribing` package
creating the necessary objects for managing tango events.

The example below illustrates the concept:

.. TODO: add example of using event handling

.. code-block:: python

    foo = 'bar'



"""
