"""Contains imported types and base objects used by event_handling package."""

__all__ = [
    "EventDataInt",
    "MessageBoard",
    "MessageBoardBase",
    "MessageHandler",
    "Producer",
    "SubscriptionBase",
    "Tracer",
    "print_tracers",
    "MessageHandlerBase",
]

from abc import abstractmethod

from ska_ser_skallop.subscribing.base import (
    EventDataInt,
    MessageBoardBase,
    MessageHandlerBase,
    Producer,
    SubscriptionBase,
)
from ska_ser_skallop.subscribing.helpers import Tracer, print_tracers
from ska_ser_skallop.subscribing.message_board import MessageBoard
from ska_ser_skallop.subscribing.message_handler import MessageHandler


class WireTap:
    """Class that can listen in to events occurring on subscriptions when handled."""

    @abstractmethod
    def tap_in_on_event(self, event: EventDataInt):
        """Listen in to a an event that have occurred whilst handling it.

        :param event: The event object that has occurred.
        """
        pass
