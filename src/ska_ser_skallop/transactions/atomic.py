"""module used to create a context manager that ensures a call is encapsulated by
waiting for specific events on tango to occur
"""

import logging
import time
from contextlib import contextmanager
from queue import Empty, Queue
from typing import Any, Dict, List, NamedTuple, Set, Union

from ska_ser_skallop.connectors import configuration
from ska_ser_skallop.mvp_control.base import AbstractDeviceProxy
from ska_ser_skallop.subscribing.base import CHANGE_EVENT
from ska_ser_skallop.subscribing.helpers import get_attr_value_as_str

logger = logging.getLogger(__name__)


class Subscription(NamedTuple):
    name: str
    sub_id: int
    proxy: AbstractDeviceProxy


class SyncError(Exception):
    pass


class Subscriptions:
    """class used to manage the occurrence of events from subscriptions and allowing
    a user to block until those events are qual to an expected value
    """

    first_event_received: Dict[str, bool]
    device_list: List[str]
    queue: Queue
    subscriptions: Set[Subscription]
    read_event_sync_elapsed_time = 2

    def __init__(
        self,
        devices: Union[str, List[str]],
        attr: str,
        expected_value: Union[str, List[str]],
    ) -> None:
        """
        Initialise a new instance.

        :param devices: a list of tango device names which will be
            subscribed to
        :param attr: the common attribute of that device that will be
            subscribed to
        :param expected_value: the desired value for an event which will
            be waited upon
        """
        # allow for list or str type as args
        self.attr = attr
        self.device_list = [devices] if isinstance(devices, str) else devices
        self.first_event_received_record = {device: False for device in self.device_list}
        self.queue = Queue()
        self.subscriptions = set()
        self.expected_value = expected_value

    def subscribe(self):
        """
        Initiate the subscriptions which will lead to events being pushed from the
        tango devices.
        """
        for device in self.device_list:
            proxy = configuration.get_device_proxy(device)
            sub_id = proxy.subscribe_event(self.attr, CHANGE_EVENT, self.queue.put)
            self.subscriptions.add(Subscription(device, sub_id, proxy))

    @staticmethod
    def _print_event(attr_value, device: str):
        value = get_attr_value_as_str(attr_value)
        time_of_occurrence = attr_value.time.todatetime()
        logger.info(f"{time_of_occurrence}      {device:<40}       {value:<20}")

    def _unsubscribe_device(self, device: str) -> Union[None, Subscription]:
        subscription_result = [
            subscription for subscription in self.subscriptions if subscription.name == device
        ]
        # ignore case of device not in subscriptions (in case of late events from
        # already unsubscribed device)
        if subscription_result:
            subscription = subscription_result[0]
            subscription.proxy.unsubscribe_event(subscription.sub_id)
            self.subscriptions.remove(subscription)
            return subscription
        return None

    def _unsubscribe_all(self):
        for _, sub_id, proxy in self.subscriptions:
            proxy.unsubscribe_event(sub_id)

    def _set_first_event_received_for(self, device: str):
        self.first_event_received_record[device] = True

    def _first_event_has_been_received(self, device: str, event) -> bool:
        if self.first_event_received_record[device]:
            return True
        attr_value = event.attr_value
        if attr_value:
            value = get_attr_value_as_str(attr_value)
            if value == self.expected_value:
                # this means the expected value occurred before subscription was
                # manifested
                self._set_first_event_received_for(device)
                return True
        return False

    def _handle_timeout(self, exception):
        stuck_subscriptions = set()
        while self.subscriptions:
            subscription = self.subscriptions.pop()
            device = subscription.name
            proxy = subscription.proxy
            attr_value, value = self.get_value_from_reading_the_attr(proxy)
            if value == self.expected_value:
                self._unsubscribe_device(device)
                logger.warning(
                    f"timed out waiting for {subscription.name} to be "
                    f"{self.expected_value}, however when reading the value it seems "
                    f"to be equal to {value}"
                )
                self._print_event(attr_value, device)
            else:
                stuck_subscriptions.add(subscription)
        if stuck_subscriptions:
            devices = [subscription.name for subscription in self.subscriptions]
            current_states = [
                {
                    "device": subscription.name,
                    "state": get_attr_value_as_str(subscription.proxy.read_attribute(self.attr)),
                }
                for subscription in self.subscriptions
            ]
            raise Exception(
                f"timed out waiting for {devices} to become {self.expected_value}, "
                f"current states: {current_states}"
            ) from exception

    def get_value_from_reading_the_attr(self, proxy: AbstractDeviceProxy):
        attr_value = proxy.read_attribute(self.attr)
        value = get_attr_value_as_str(attr_value)
        return attr_value, value

    def _get_latest_event(self, timeout) -> Any:
        try:
            return self.queue.get(timeout=timeout)
        except Empty as exception:
            self._handle_timeout(exception)
            return None

    def _check_device_read_in_sync(self, proxy: AbstractDeviceProxy):
        poll_period = self.read_event_sync_elapsed_time / 10
        _, value = self.get_value_from_reading_the_attr(proxy)
        if value != self.expected_value:
            for count in range(10):
                time.sleep(poll_period)
                _, value = self.get_value_from_reading_the_attr(proxy)
                if self._compare(value):
                    logger.exception(
                        f"got an event for {self.attr} == {self.expected_value}, but "
                        "reading the attr only gave the same results after "
                        f"{count*poll_period}s"
                    )
                    break
            if value != self.expected_value:
                raise SyncError(
                    f"got an event for {self.attr} ==  {self.expected_value} but "
                    f"reading the attr still gives {value} after "
                    f"{self.read_event_sync_elapsed_time}"
                )

    def wait_until_equal(self, timeout: int = 10):
        """
        Wait until the subscriptions result in events equaling the given desired value.

        :param timeout: the maximum amount of time (in seconds) to wait for an event
            before deeming it as faulty. Defaults to 10.
        """
        try:
            while self.subscriptions:
                # get any subscriptions and block if empty until timeout
                event = self._get_latest_event(timeout)
                if not event:
                    break
                device = event.device.name()
                # ignore the first event from a tango device subscription as it gets
                # generated a priori
                if self._first_event_has_been_received(device, event):
                    self._check_if_event_is_as_expected(event, device)
                else:
                    # sets the state to indicate the first event has occurred
                    self._set_first_event_received_for(device)
        finally:
            # in case of any error unsubscribe any remaining subscriptions
            self._unsubscribe_all()

    def _check_if_event_is_as_expected(self, event, device):
        attr_value = event.attr_value
        if attr_value:
            value = get_attr_value_as_str(attr_value)
            self._print_event(attr_value, device)
            # unsubscribe and remove from subscriptions
            if self._compare(value):
                subscription = self._unsubscribe_device(device)
                if subscription:
                    self._check_device_read_in_sync(subscription.proxy)
            else:
                logger.warning(
                    f"Event received for {device} with value:{value} that is not equal "
                    f"to desired: {self.expected_value}"
                )
        else:
            logger.warning(f"Event received with no {attr_value}: {event}")

    def _compare(self, value):
        return value == self.expected_value


@contextmanager
def atomic(
    devices: Union[str, List[str]],
    attr: str,
    expected_value: str,
    timeout: int = 10,
):
    """
    Block until a set of conditional state changes have occurred on a given attribute
    from a set of tango devices.

    :param devices: the list of tango devices for which the conditional state changes
        will be checked
    :param attr: the attribute that will be checked for a conditional state change on
        the given devices
    :param expected_value: the expected value of that state (expressed as a string value
        that one would get from an Enum.name() method)
    :param timeout: the maximum amount of time (in seconds) one should wait for no
        events to be generated from the devices. Defaults to 10 seconds
    """
    subscriptions = Subscriptions(devices, attr, expected_value)
    subscriptions.subscribe()
    yield
    subscriptions.wait_until_equal(timeout)
