import json
from typing import TypedDict, Union, cast

import requests

from ska_ser_skallop.utils.dictionary_validation import validated_typed_dict


@validated_typed_dict
class Part(TypedDict):
    name: str
    filename: str
    data: str


class MultiPartResults(TypedDict):
    id: str
    key: str
    self: str


class UploadError(Exception):
    pass


class Connection:
    multipart_endpoint = "/rest/raven/1.0/import/execution/cucumber/multipart"
    fixed_version_endpoint = "/rest/api/2/project/{}/versions"
    jira_get_issue_types = "/rest/api/2/issue/createmeta/{}/issuetypes"
    jira_get_usable_fields = "/rest/api/2/issue/createmeta/{}/issuetypes/{}"

    def __init__(self, host: str, token: str) -> None:
        self._base_headers = {"Authorization": f"Basic {token}"}
        self.host = host

    def push_multipart(self, *parts: Part) -> MultiPartResults:
        headers = self._base_headers.copy()
        # headers["Accept"] = "application/json"
        url = f"{self.host}{self.multipart_endpoint}"
        data = {part["name"]: (part["filename"], part["data"]) for part in parts}
        response = requests.post(url, files=data, headers=headers, timeout=30)
        if not response.ok:
            raise UploadError(f"Response error ({response.status_code}): {response.text}")
        test_exec_issue = cast(dict, json.loads(response.text)["testExecIssue"])
        return MultiPartResults(**test_exec_issue)

    def get_issue_types(self, proj_id: str) -> None:
        headers = self._base_headers.copy()
        headers["Accept"] = "application/json"
        url = f"{self.host}{self.jira_get_issue_types.format(proj_id)}"
        response = requests.get(
            url,
            headers=headers,
            timeout=30,
        )
        return json.loads(response.content)

    def get_issue_type_fields(self, proj_id: str, issue_type: str) -> None:
        headers = self._base_headers.copy()
        headers["Accept"] = "application/json"
        url = f"{self.host}{self.jira_get_usable_fields.format(proj_id, issue_type)}"
        response = requests.get(
            url,
            headers=headers,
            timeout=30,
        )
        return json.loads(response.content)


def inspect_jira(host: str, token: str, xray_key: str, issue_id: Union[None, str]) -> str:
    connection = Connection(host, token)
    if issue_id:
        result = connection.get_issue_type_fields(xray_key, issue_id)
        return json.dumps(result)
    result = connection.get_issue_types(xray_key)
    return json.dumps(result)
