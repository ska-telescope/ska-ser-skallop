import json
import logging
from pathlib import Path
from typing import Union

from . import execution_desc, execution_report, results
from .multipart_connection import Connection, MultiPartResults, Part

logger = logging.getLogger(__name__)


def _get_test_results(path: Path) -> Part:
    return Part(**{"name": "result", "filename": path.name, "data": path.read_text()})


def get_info(connection: "Connection", test_exec_path: Path, test_results_path: Path) -> Part:
    test_exec_template = execution_desc.load_test_execution_template(test_exec_path)
    report_path = Path(test_exec_template["test_report"])
    report_data = execution_report.get_report_data(report_path)
    keywords = execution_report.get_keywords(report_data)
    tags = results.load_results(test_results_path).tags
    test_exec_desc = execution_desc.get_test_execution_description(
        test_exec_template, keywords, tags
    )
    start_date, end_date = execution_report.get_dates(report_data)
    start_date_fmt, end_date_fmt = execution_report.format_dates(start_date, end_date)
    FLD_END_DATE = "customfield_11918"
    FLD_START_DATE = "customfield_11917"
    FLD_TEST_PLAN = "customfield_11927"
    FLD_TEST_ENV = "customfield_11925"
    proj_id = test_exec_desc["project_id"]
    env_variables = execution_desc.get_any_env_vars_desc(test_exec_desc)
    chart_info = execution_desc.get_any_chart_info(test_exec_desc)
    description = f'{test_exec_desc["description"]}\n{chart_info}{env_variables}'
    name = test_exec_desc["name"]
    data = {
        "fields": {
            "project": {"key": proj_id},
            "description": description,
            "summary": f"{name}: {start_date}",
            "issuetype": {"id": "11602"},
            "labels": test_exec_desc["labels"],
            FLD_START_DATE: start_date_fmt,
            FLD_END_DATE: end_date_fmt,
            FLD_TEST_PLAN: test_exec_desc["test_plans"],
            FLD_TEST_ENV: test_exec_desc["test_environments"],
        }
    }
    # optionals
    if version_keys := test_exec_desc.get("versions"):
        versions = [{"name": version_key} for version_key in version_keys]
        data["fields"]["versions"] = versions
        data["fields"]["fixVersions"] = versions
    return {"name": "info", "filename": "info.json", "data": json.dumps(data)}


def upload_multipart(
    host: str, token: str, results_file: Path, test_exec_desc_file: Path
) -> Union[MultiPartResults, None]:
    connection = Connection(host, token)
    results = _get_test_results(results_file)
    info = get_info(connection, test_exec_desc_file, results_file)
    if results["data"] != "[]":
        return connection.push_multipart(results, info)
    logger.warning("Tests results was empty, no data pushed")
    return None
