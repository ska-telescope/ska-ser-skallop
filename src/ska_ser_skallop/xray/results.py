import json
from collections import Counter
from pathlib import Path
from typing import List, Set, TypedDict

from ska_ser_skallop.utils.dictionary_validation import validated_typed_dict


class Tag(TypedDict):
    name: str
    line: int


class Element(TypedDict):
    keyword: str
    id: str
    name: str
    description: str
    tags: List[Tag]
    type: str


@validated_typed_dict
class Result(TypedDict):
    keyword: str
    uri: str
    name: str
    id: str
    description: str
    tags: List[Tag]
    elements: List[Element]


class ResultsContainer:
    def __init__(self, results: List[Result]) -> None:
        self.results = results

    @property
    def tags(self) -> Set[str]:
        """Return a set of exclusively owned tags found in each test.

        In other words return tags for which each test share with all others.

        :return: A set of exclusively owned tags
        """
        test_elements = [element for result in self.results for element in result["elements"]]
        tag_group = [tag["name"] for element in test_elements for tag in element["tags"]]
        tags = {
            tag_name
            for tag_name, tag_count in Counter(tag_group).items()
            if tag_count == len(test_elements)
        }
        return tags


def load_results(results_file: Path) -> ResultsContainer:
    data = [Result(**result) for result in json.loads(results_file.read_text())]
    return ResultsContainer(data)
