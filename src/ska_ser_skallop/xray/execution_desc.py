import json
import os
from pathlib import Path
from typing import Dict, List, Set, TypedDict, TypeVar, Union, cast

import yaml

from ska_ser_skallop.utils.dictionary_validation import validated_typed_dict


@validated_typed_dict
class SelectFromEnv(TypedDict):
    env: str
    maps_to: Dict[str, str]


@validated_typed_dict
class SelectFromKeyword(TypedDict):
    select_from_keyword: Dict[str, str]


@validated_typed_dict
class SelectFromTag(TypedDict):
    select_from_tag: Dict[str, str]


@validated_typed_dict
class TestExecDescTempl(TypedDict):
    name: Union[str, SelectFromEnv, SelectFromKeyword, SelectFromTag]
    test_plans: List[Union[str, SelectFromEnv, SelectFromKeyword, SelectFromTag]]
    test_environments: List[Union[str, SelectFromEnv, SelectFromKeyword, SelectFromTag]]
    versions: Union[List[Union[str, SelectFromEnv, SelectFromKeyword, SelectFromTag]], None]
    chart_info: Union[str, SelectFromEnv, SelectFromKeyword, SelectFromTag, None]
    project_id: str
    description: str
    test_report: str
    labels: List[Union[str, SelectFromEnv, SelectFromKeyword]]
    environmental_variables: List[str]


@validated_typed_dict
class TestExecDesc(TypedDict):
    name: str
    test_plans: List[str]
    test_environments: List[str]
    versions: Union[List[str], None]
    chart_info: Union[List[str], None]
    project_id: str
    description: str
    test_report: str
    labels: List[str]
    environmental_variables: List[str]


def load_test_execution_template(path: Path) -> TestExecDescTempl:
    """get the test test description in template format (no indirect values loaded)

    The data still needs to be parsed by loading indirect values from env or test report data.

    :return: The test description in template format
    """
    return _get_test_execution_description_from_path(path)


def get_test_execution_description(
    template: TestExecDescTempl,
    keywords: Set[str] = set(),
    tags: Set[str] = set(),
) -> TestExecDesc:
    """get the test descriptions as a typed dictionary parsed from a given json file

    :return: The parsed results as a typed dictionary
    """
    return _set_from_env_if_req(template, keywords, tags)


def get_any_env_vars_desc(desc: TestExecDesc) -> str:
    output = ""
    if envs := desc["environmental_variables"]:
        output += f"{output}\nThe following env variables have been set:\n"
        for env in envs:
            val = os.getenv(env, "Not found")
            output = f"{output}{env}:{val:>30}\n"
    return output


@validated_typed_dict
class ChartDependency(TypedDict):
    name: str
    version: str
    repository: str


@validated_typed_dict
class ChartInfo(TypedDict):
    apiVersion: str
    name: str
    description: str
    type: str
    version: str
    appVersion: str
    dependencies: List[ChartDependency]


def _load_chart_info(file_name: str) -> ChartInfo:
    with open(file_name, "r", encoding="utf-8") as chart_info:
        data = yaml.safe_load(chart_info)
    return ChartInfo(**data)


def _chart_info_as_str(chart: ChartInfo) -> str:
    output = "Product version info:\n"
    output += f"{chart['name']}-{chart['version']}\n"
    output += "Dependencies:\n"
    for dependency in chart["dependencies"]:
        output += f".{dependency['name']}-{dependency['version']}\n"
    return output


def get_any_chart_info(desc: TestExecDesc) -> str:
    if chart_info := desc.get("chart_info"):
        if isinstance(chart_info, str):
            chart = _load_chart_info(chart_info)
            return _chart_info_as_str(chart)
        if _dict_is_select_from_env(cast(dict, chart_info)):
            if chart_selection := _select_from_env(cast(SelectFromEnv, chart_info)):
                chart = _load_chart_info(chart_selection)
                return _chart_info_as_str(chart)
    return ""


def _select_from_keywords(spec: SelectFromKeyword, keywords: Set[str]) -> Union[str, None]:
    for keyword, value in spec["select_from_keyword"].items():
        if keyword in keywords:
            return value
    if default := spec["select_from_keyword"].get("default"):
        return default
    return None


def _select_from_tags(spec: SelectFromTag, tags: Set[str]) -> Union[str, None]:
    for tag, value in spec["select_from_tag"].items():
        if tag in tags:
            return value
    if default := spec["select_from_tag"].get("default"):
        return default
    return None


def _select_from_env(spec: SelectFromEnv) -> Union[str, None]:
    env_name = spec["env"]
    env_value = os.getenv(env_name)
    mapping = spec["maps_to"]
    if env_value:
        if value := mapping.get(env_value):
            return value
    if default := mapping.get("default"):
        return default
    return None


def _get_test_execution_description_from_path(path: Path) -> TestExecDescTempl:
    data = json.loads(path.read_text())
    return TestExecDescTempl(**data)


def _dict_is_select_from_env(input_dict: Dict) -> bool:
    return "maps_to" in input_dict.keys()


def _dict_is_select_from_keywords(input_dict: Dict) -> bool:
    return "select_from_keyword" in input_dict.keys()


def _dict_is_select_from_tags(input_dict: Dict) -> bool:
    return "select_from_tag" in input_dict.keys()


def _inner_set_from_env_if_required(val, keywords: Set[str], tags: Set[str]):
    if isinstance(val, dict):
        if _dict_is_select_from_env(val):
            val = cast(SelectFromEnv, val)
            new_val = _select_from_env(val)
        elif _dict_is_select_from_keywords(val):
            val = cast(SelectFromKeyword, val)
            new_val = _select_from_keywords(val, keywords)
        elif _dict_is_select_from_tags(val):
            val = cast(SelectFromTag, val)
            new_val = _select_from_tags(val, tags)
        else:
            new_val = _set_from_env_if_req(val, keywords, tags)
    elif isinstance(val, list):
        new_val = [_inner_set_from_env_if_required(item, keywords, tags) for item in val]
    elif val.__class__ == SelectFromEnv:
        new_val = val
    else:
        new_val = val
    return new_val


T = TypeVar("T")


def _set_from_env_if_req(
    input_dict: Union[TestExecDescTempl, T], keywords: Set[str], tags: Set[str]
) -> T:
    assert isinstance(input_dict, dict)
    updated_dict = input_dict.copy()
    for key, val in input_dict.items():
        updated_dict[key] = _inner_set_from_env_if_required(val, keywords, tags)
    return cast(T, updated_dict)
