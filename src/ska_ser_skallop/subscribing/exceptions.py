class EventTimedOut(Exception):
    """Used to indicate a timeout has occurred whilst waiting for events to come in
    from a set of subscriptions. The timeout message will contain a list of diagnostic
    messages from remaining subscriptions in order to assist with fault finding.
    """

    def __init__(self, message: str, remaining_events="") -> None:
        self.message = message
        self.remaining_events = remaining_events
