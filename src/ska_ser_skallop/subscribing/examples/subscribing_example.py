import logging
import sys

from ska_ser_skallop.connectors import configuration
from ska_ser_skallop.subscribing import configuration as sub_conf
from ska_ser_skallop.subscribing.base import MessageBoardBase
from ska_ser_skallop.subscribing.message_handler import MessageHandler

logger = logging.getLogger(__name__)


def subscribe(board: MessageBoardBase):
    # adds two subscriptions
    board.add_subscription(
        configuration.get_producer("sys/tg_test/1", fast_load=True),
        "state",
        MessageHandler(board),
    )
    board.add_subscription(
        configuration.get_producer("sys/tg_test/1", fast_load=True),
        "short_scalar",
        MessageHandler(board),
    )


def handle_events(board: MessageBoardBase, option):
    # handle next item on the messageboard...
    if option == "handle most recent":
        next(board.get_items(timeout=1)).handle_event()
    # or if you want to get the next 2 events
    elif option == "block until all handled":
        for index, item in enumerate(board.get_items()):
            item.handle_event()
            print(item.handler.print_event(item.event))
            if index == 2:
                board.remove_all_subscriptions()


def set_logging_for_stdout():
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)


if __name__ == "__main__":
    set_logging_for_stdout()
    brd = sub_conf.get_messageboard()
    subscribe(brd)
    handle_events(brd, "block until all handled")
