import enum
from typing import Callable, Union


class PointingState(enum.IntEnum):
    """Pointing state of the dish"""

    READY = 0
    SLEW = 1
    TRACK = 2
    SCAN = 3


OBSSTATE = {
    "EMPTY": 0,
    "RESOURCING": 1,
    "IDLE": 2,
    "CONFIGURING": 3,
    "READY": 4,
    "SCANNING": 5,
    "ABORTING": 6,
    "ABORTED": 7,
    "RESETTING": 8,
    "FAULT": 9,
    "RESTARTING": 10,
}

ObsState = enum.IntEnum(value="ObsState", names=list(OBSSTATE.items()))
DISHHEALTH = {"UNKNOWN": 0, "OK": 1, "DEGRADED": 2, "FAILED": 3}
DishHealth = enum.IntEnum(value="DishHealth", names=list(DISHHEALTH.items()))

CONFIGUREDBANDS = {
    "NONE": 7,
    "UNDEFINED": 9,
    "UNKNOWN": 0,
    "ERROR": 8,
    "B4": 4,
    "B1": 1,
    "B2": 2,
    "B3": 3,
    "B5b": 6,
    "B5a": 5,
}
ConfiguredBand = enum.IntEnum(value="ConfiguredBand", names=list(CONFIGUREDBANDS.items()))

DISHMODE = {
    "STARTUP": 0,
    "SHUTDOWN": 1,
    "STANDBY_LP": 2,
    "STANDBY_FP": 3,
    "MAINTENANCE": 4,
    "STOW": 5,
    "CONFIG": 6,
    "OPERATE": 7,
    "UNKNOWN": 8,
}

DishMode = enum.IntEnum(
    value="DishMode",
    names=list(DISHMODE.items()),
)

ADMINMODE = {
    "ONLINE": 0,
    "OFFLINE": 1,
    "MAINTENANCE": 2,
    "NOT_FITTED": 3,
    "RESERVED": 4,
}

AdminMode = enum.IntEnum(
    value="AdminMode",
    names=list(ADMINMODE.items()),
)


DISHMASTERPOINTINGSTATE = {
    "NONE": 4,
    "SCAN": 3,
    "TRACK": 2,
    "UNKNOWN": 5,
    "READY": 0,
    "SLEW": 1,
}

DishMasterPointingState = enum.IntEnum(
    value="DishMasterPointingState",
    names=list(DISHMASTERPOINTINGSTATE.items()),
)

DISHMASTERPOWERSTATE = {"UNKNOWN": 0, "OFF": 1, "UPS": 2, "LOW": 3, "FULL": 4}


DishMasterPowerState = enum.IntEnum(
    value="DishMasterPowerState",
    names=list(DISHMASTERPOWERSTATE.items()),
)

DISHMASTEROBSERVINGSTATE = {
    "UNKNOWN": 0,
    "IDLE": 1,
    "CONFIGURING": 2,
    "READY": 3,
    "SCANNING": 4,
}

DishMasterObservingState = enum.IntEnum(
    value="DishMasterObservingState",
    names=list(DISHMASTEROBSERVINGSTATE.items()),
)

DEVSTATES = {
    "ON": 0,
    "OFF": 1,
    "CLOSE": 2,
    "OPEN": 3,
    "INSERT": 4,
    "EXTRACT": 5,
    "MOVING": 6,
    "STANDBY": 7,
    "FAULT": 8,
    "INIT": 9,
    "RUNNING": 10,
    "ALARM": 11,
    "DISABLE": 12,
    "UNKNOWN": 13,
}


DevState = enum.IntEnum(value="DevState", names=list(DEVSTATES.items()))

TESTMODE = {"NONE": 0, "TEST": 1}

HEALTHSTATE = {"OK": 0, "DEGRADED": 1, "FAILED": 2, "UNKNOWN": 3}

HealthState = enum.IntEnum(value="HealthState", names=list(HEALTHSTATE.items()))

TestMode = enum.IntEnum(value="TestMode", names=list(TESTMODE.items()))


def to_def_state_from_str(value: str) -> DevState:
    return DevState(DEVSTATES[value])


def to_obs_state_from_str(value: str) -> DevState:
    return ObsState(OBSSTATE[value])


def to_dishealth_from_str(value: str) -> DevState:
    return DishHealth(DISHHEALTH[value])


def to_dishmode_from_str(value: str) -> DevState:
    return DishMode(DISHMODE[value])


def to_dishadmin_mode_from_str(value: str) -> DevState:
    return AdminMode(ADMINMODE[value])


def to_dishmasterpoint_state_from_str(value: str) -> DevState:
    return DishMasterPointingState(DISHMASTERPOINTINGSTATE[value])


def to_dishmasterpower_state_from_str(value: str) -> DevState:
    return DishMasterPowerState(DISHMASTERPOWERSTATE[value])


def to_dishmasterobserving_state_from_str(value: str) -> DevState:
    return DishMasterObservingState(DISHMASTEROBSERVINGSTATE[value])


DATATYPE_MAPPING = {
    "State": DevState,
    "dishMasterObservingState": DishMasterObservingState,
    "obsState": ObsState,
    "dishMasterPowerState": DishMasterPowerState,
    "dishMasterPointingState": DishMasterPointingState,
    "adminMode": AdminMode,
    "dishMode": DishMode,
    "dishHealth": DishHealth,
    "pbsState": ObsState,
    "pointingState": PointingState,
    "healthState": HealthState,
    "testMode": TestMode,
}


def get_enum_type(type_name: str) -> Union[None, Callable[[int], int]]:
    return DATATYPE_MAPPING.get(type_name)
