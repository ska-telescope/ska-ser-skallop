import re
from datetime import datetime


def get_int_id() -> int:
    return int(datetime.now().timestamp())


def get_id(id_pattern: str = "********-*****") -> str:
    """
    Generate a time-based unique id in virtue of the fact that
    subsequent requests will take place further ahead of time.

    :param id_pattern: the string pattern as to how the numbered id should
        be rendered. <prefix>pattern<suffix> '*' indicates a digit from
        the timestamp. Note a maximum of 16 digits is allowed. Defaults
        to '********-*****'. Note digits must be seperated by only one
        dash at a time.

    :return: the id rendered according to the requested pattern
    """
    prefix, suffix = re.split(r"(?=\*)[\*-]*(?<=\*)", id_pattern)
    id_pattern = re.findall(r"(?=\*)[\*-]*(?<=\*)", id_pattern)[0]
    length = id_pattern.count("*")
    assert length < 16, f"Unable to generate an id with {length} digits, the limit is 16"
    timestamp = str(datetime.now().timestamp()).replace(".", "")
    sections = id_pattern.split("-")
    unique_id = ""
    sections.reverse()
    for section in sections:
        section_length = len(section)
        section_id = timestamp[-section_length:]
        timestamp = timestamp[:-section_length]
        if unique_id:
            unique_id = f"{section_id}-{unique_id}"
        else:
            unique_id = section_id
    return f"{prefix}{unique_id}{suffix}"
