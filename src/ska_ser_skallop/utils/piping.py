"""Contains utility functions related to piping operations."""

import re
import time
from concurrent.futures import ThreadPoolExecutor
from functools import partial
from queue import Empty, Queue
from typing import Any, Callable, Dict, Generic, Iterable, List, Tuple, Type, TypeVar, Union, cast

from pipe import Pipe, select, traverse

T = TypeVar("T")
Y = TypeVar("Y")
U = TypeVar("U")


class Item(Generic[T]):
    def __init__(self, name: str, value: T):
        self.name = name
        self.value = value


@Pipe
def first_element(iterable: Union[Iterable[T], Dict[str, Y]]) -> Union[T, Item[Y], None]:
    if isinstance(iterable, dict):
        for key, val in iterable.items():
            key = str(key)
            return Item(key, cast(Y, val))
    else:
        for item in iterable:
            return item
    return None


@Pipe
def first_element_from_dict(iterable: Dict[str, Y]) -> Item[Y]:
    for key, val in iterable.items():
        key = str(key)
        return Item(key, cast(Y, val))
    raise Empty("no elements in given iterable")


@Pipe
def first_element_from_tuple_list(iterable: List[Tuple[str, Y]]) -> Union[None, Item[Y]]:
    for item in iterable:
        key, val = item
        return Item(key, cast(Y, val))
    return None


def as_item(item: Tuple[str, Any]) -> Item:
    return Item(*item)


@Pipe
def re_first_match(match: Union[re.Match[str], None]) -> str:
    if match:
        return match.group(0)
    return ""


def regex_find_first_match(pattern: re.Pattern, *args) -> Pipe:
    def pipe_wrapper(pattern: re.Pattern, args: Tuple, data: str) -> str:
        match = re.search(pattern.pattern, data, re.DOTALL)
        if match:
            return match.group(0)
        return ""

    return Pipe(partial(pipe_wrapper, pattern, args))


@Pipe
def multilines(data: str) -> List[str]:
    return data.splitlines()


def regex_true(pattern: re.Pattern) -> Callable[[str], bool]:
    def wrapper(
        pattern: re.Pattern,
        data: Union[str, Tuple[str, ...]],
    ) -> bool:
        if isinstance(data, tuple):
            data = data[0]
        return pattern.search(data) is not None

    return partial(wrapper, pattern)


def regex_false(pattern: re.Pattern) -> Callable[[str], bool]:
    def wrapper(pattern: re.Pattern, data: str) -> bool:
        return pattern.search(data) is None

    return partial(wrapper, pattern)


def regex_match(pattern: re.Pattern) -> Callable[[str], Tuple[str, ...]]:
    def wrapper(pattern: re.Pattern, data: str) -> Tuple[str, ...]:
        return tuple(match.group(0) for match in pattern.finditer(data))

    return partial(wrapper, pattern)


def pick(position: int) -> Callable[[Tuple[T]], T]:
    def wrapper(position: int, item: Tuple[T]) -> T:
        assert len(item) >= position
        return item[position - 1]

    return partial(wrapper, position)


def instantiate(class_: Type[T]) -> Callable[[Tuple], T]:
    def wrapper(class_: Type[T], args: Tuple) -> T:
        return class_(*args)

    return partial(wrapper, class_)


def append_to_tuple(*item: T) -> Callable[[Tuple[Y]], Tuple[Y, T]]:
    def wrapper(item: Tuple[T, ...], data: Tuple[Y]) -> Tuple[Y, T]:
        return (*data, *item)

    return partial(wrapper, item)


def preppend_to_tuple(*item: T) -> Callable[[Tuple[Y]], Tuple[T, Y]]:
    def wrapper(item: Tuple[T, ...], data: Tuple[Y]) -> Tuple[T, Y]:
        return (*item, *data)

    return partial(wrapper, item)


class _Task:
    def __init__(self, task_id: int, task: Callable[[], Any], task_queue: Queue):
        self.task_id = task_id
        self.task = task
        self.result = None
        self.error: Union[None, Exception] = None
        self._queue = task_queue

    def run_task(self):
        try:
            self.result = self.task()
        except Exception as exception:
            self.error = exception
        finally:
            self._queue.put(self)


class ConcurrentTaskExecutionError(Exception):
    def __init__(self, error_message: str, error_tasks: Dict) -> None:
        super().__init__(error_message)
        self.error_tasks = error_tasks


class SerializedTaskExecutionError(Exception):
    def __init__(self, error_message: str, error_tasks: Dict) -> None:
        super().__init__(error_message)
        self.error_tasks = error_tasks


def _parallelize(throttle: float = 0):
    def submit_jobs(tasks: List[Callable], throttle: float, time_out=10):
        central_queue: Queue[_Task] = Queue()
        to_do_tasks = {
            task_id: _Task(task_id, task, central_queue) for task_id, task in enumerate(tasks)
        }
        with ThreadPoolExecutor(max_workers=len(to_do_tasks)) as pool:
            finished_tasks: List[_Task] = []
            for task in to_do_tasks.values():
                if throttle:
                    time.sleep(throttle)
                pool.submit(task.run_task)
            while to_do_tasks:
                try:
                    task = central_queue.get(timeout=time_out)
                    to_do_tasks.pop(task.task_id)
                    finished_tasks.append(task)
                except Empty as empty_exception:
                    raise Exception(
                        f"Timeout after {time_out} waiting for {list(to_do_tasks.items())}"
                    ) from empty_exception

        finished_tasks.sort(key=lambda element: element.task_id)
        error_tasks = [task.error for task in finished_tasks if task.error]
        if error_tasks:
            raise Exception(error_tasks)
        return [task.result for task in finished_tasks]

    def wrapper(throttle: float, tasks: List[Callable]):
        for result in submit_jobs(tasks, throttle):
            yield result

    return Pipe(partial(wrapper, throttle))


def _as_callables(callable: Callable):
    def wrapper(callable: Callable, arg_list: Iterable):
        for args in arg_list:
            yield partial(callable, *args)

    return Pipe(partial(wrapper, callable))


@Pipe
def _as_tuple(arg_list: Iterable):
    for args in arg_list:
        if isinstance(args, Tuple):
            yield args
        else:
            yield (args,)


def parallelize(callable: Callable, throttle: float = 0):
    def wrapper(callable: Callable, throttle: float, arg_list: Iterable):
        return arg_list | _as_tuple | _as_callables(callable) | _parallelize(throttle)

    return Pipe(partial(wrapper, callable, throttle))


@Pipe
def _serialize(tasks: List[Callable]):
    finished_tasks: Queue[_Task] = Queue()
    to_do_tasks = {
        task_id: _Task(task_id, task, finished_tasks) for task_id, task in enumerate(tasks)
    }
    for task in to_do_tasks.values():
        task.run_task()
    error_tasks = {task_id: task.error.args for task_id, task in to_do_tasks.items() if task.error}
    if error_tasks:
        raise SerializedTaskExecutionError(
            f"Error in executing list of tasks {error_tasks}",
            error_tasks,
        )
    return [task.result for task in to_do_tasks.values()]


def serialize(callable: Callable):
    def wrapper(callable: Callable, arg_list: Iterable):
        return arg_list | _as_tuple | _as_callables(callable) | _serialize

    return Pipe(partial(wrapper, callable))


class _StaticTuple(Generic[T, U]):
    def __init__(self, item_a: Union[T, Tuple[T, ...]], item_b: Union[U, Tuple[U, ...]]):
        self._item_a: Tuple[T, ...] = cast(
            Tuple[T, ...],
            (item_a,) if not isinstance(item_a, tuple) else item_a,
        )
        self._item_b: Tuple[U, ...] = cast(
            Tuple[U, ...],
            (item_b,) if not isinstance(item_b, tuple) else item_b,
        )

    @property
    def items(self) -> Tuple[Union[T, U], ...]:
        return (*self._item_a, *self._item_b)


def _tuple_cross(
    iter_a: Iterable[Union[T, Tuple[T, ...]]],
    iter_b: Iterable[Union[U, Tuple[U, ...]]],
) -> Iterable[_StaticTuple[T, U]]:
    for item_b in iter_b:
        yield iter_a | select(lambda item_a, item_c=item_b: _StaticTuple(item_a, item_c))


def tuple_cross(
    iter_a: Iterable[Union[T, Tuple[T, ...]]],
    iter_b: Iterable[Union[U, Tuple[U, ...]]],
) -> Iterable[Tuple[Union[T, U], ...]]:
    return (
        _tuple_cross(iter_a, iter_b)
        | traverse
        | select(lambda item: cast(_StaticTuple, item).items)
    )
