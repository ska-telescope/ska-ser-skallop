from contextlib import contextmanager
from functools import partial
from typing import Callable, Generic, TypeVar

T = TypeVar("T")
Y = TypeVar("Y")


class ParetoItem(Generic[T]):
    def __init__(
        self,
        name: str,
        value: int | float,
        cumulative: int | float,
        reference: T,
    ):
        self.name = name
        self.value = value
        self.cumulative = cumulative
        self.reference = reference

    def cumulative_contribution(self, aggregate: float) -> float:
        return self.cumulative / aggregate

    def contribution(self, aggregate: float) -> float:
        return self.value / aggregate

    def print_status(self, aggregate: float) -> str:
        val = f"{self.value}"
        return f"{self.name:<40}{val:<10.1}{self.cumulative_contribution(aggregate):<20.1%}"


class ParetoList(Generic[T]):
    def __init__(self, data: dict[str, T], key_function: Callable[[T], float | int]) -> None:
        self._data = data
        self._key_function = key_function
        self._context: list[Callable[[T], float | int]] = []

    def _calc(self) -> list[ParetoItem[T]]:
        def _adapted(key_function: Callable[[T], float | int], item: tuple[str, T]):
            return key_function(item[1])

        adapted_key_function = partial(_adapted, self._key_function)
        data_items = list(self._data.items())
        data_items.sort(key=adapted_key_function, reverse=True)
        cumulative = 0
        items: list[ParetoItem[T]] = []
        for item_name, item in data_items:
            item_val = self._key_function(item)
            cumulative += item_val
            items.append(ParetoItem(item_name, item_val, cumulative, item))
        return items

    def _set_key_function(self, key_function: Callable[[T], float | int]):
        self._key_function = key_function

    @contextmanager
    def _from_key_function(self, key_function: Callable[[T], float | int]):
        self._context.append(self._key_function)
        self._key_function = key_function
        yield
        self._key_function = self._context.pop()

    def _get_aggregate(self, key_function: Callable[[T], float | int]):
        with self._from_key_function(key_function):
            return self.aggregate

    def _get_eighty(self, key_function: Callable[[T], float | int]):
        with self._from_key_function(key_function):
            return self.eighty

    def _get_twenty(self, key_function: Callable[[T], float | int]):
        with self._from_key_function(key_function):
            return self.twenty

    @property
    def aggregate(self):
        items = self._calc()
        return items[-1].cumulative

    @property
    def eighty(self):
        items = self._calc()
        eighty_list: list[ParetoItem[T]] = []
        for item in items:
            if item.cumulative_contribution(self.aggregate) < 0.8:
                eighty_list.append(item)
        return eighty_list

    @property
    def twenty(self):
        items = self._calc()
        twenty_index = round(len(items) * 0.2)
        return items[:twenty_index]
