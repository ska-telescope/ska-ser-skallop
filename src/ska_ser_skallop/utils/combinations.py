"""Utility for creating combinations of arguments"""

from typing import Any, Dict, List, Tuple, TypedDict


class CombinationResult(TypedDict):
    argnames: List[str]
    argvalues: List[Tuple]


def combine(keyword_args: Dict["str", List[Any]]) -> CombinationResult:
    """combines all the possible permutations of the values arguments may have.

    The result will be a list of tuples representing an instance of argument values.
    This result can then be uses as an input argument to a :py:func:`pytest.mark.parameterize`
    call e.g.

    .. code-block:: python

        my_test_args = {}
        my_test_args['arg1] = ['val1','val2']
        my_test_args['arg2] = [1, 2]

        @pytest.mark.parameterize(**combine(my_test_args))
        def my_test(arg1, arg2):
            result = my_func(arg1, arg2)



    :param keyword_args: A dictionary representing an argument with a list of
        possible values it may have.
    """
    result = CombinationResult(**{})
    result["argnames"] = list(keyword_args.keys())
    args_values = list(keyword_args.values())
    result["argvalues"] = cross(args_values.pop(0), args_values)
    return result


def _flatten(the_list: List[Tuple[Any, Any]]):
    flattened = []
    for first_item, second_item in the_list:
        if isinstance(second_item, tuple):
            flattened = [*flattened, (first_item, *second_item)]
        else:
            return the_list
    return flattened


def _innercross(list1: List[Any], list2: List[Any]) -> List[Any]:
    result = []
    for item in list1:
        same_val_list = [item for _ in range(len(list2))]
        crossed = list(zip(same_val_list, list2))
        flattened = _flatten(crossed)
        result = [*result, *flattened]
    return result


def cross(listA: List[Any], lists: List[List[Any]]) -> List[Any]:
    """cross multiply a given list with another set of crossed lists"

    if the lists is a single list it will cross with that one, otherwise
    it will recursively get the cross of the inner list first.

    :param listA: The top list to be crossed with the other ones.
    :param lists: A set of inner lists to be crossed with.
    """
    if len(lists) == 1:
        return _innercross(listA, lists.pop(0))
    # multiple lists
    recursively_crossed = cross(lists.pop(0), lists)
    return _innercross(listA, recursively_crossed)
