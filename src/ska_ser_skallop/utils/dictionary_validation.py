"""Module that helps with validating data typically red from a json input into a known
 dictionary structure (e.g. typeddict). Given a defined typeddict when the data is loaded
 dynamically into the dictionary, a validation is performed on the source data to verify
 the expected key names are correct and that the data types returned for those keys are
 the same as defined in the typedict.
 E.g. :

 .. code-block:: python

    @validated_typed_dict
    class MyDict(TypedDict):
        my_key: str

    def loadjson(path)->MyDict:
        return MyDict(**json.loads(path))

In the above example `MyDict(**json.loads(path))` will cause the `validated_typed_dict` to first
validate the input data before return the data structure.

The result is that programmers can safely assume that dictionary they use will always supply the
correct data for the given keys.

Key caveats:

    1. This module does not handle 'Forward references` e.g. when a type has been commented out
"""

import functools
import typing
from typing import Any, Dict, Set, Tuple


class ValidationError(AssertionError):
    pass


def is_typed_dict_wrapper(item) -> bool:
    return hasattr(item, "__annotations__")


def _validate(val, ann_type):
    type_origin = ann_type.__origin__ if hasattr(ann_type, "__origin__") else None
    if any([type_origin == typing.TypedDict, is_typed_dict_wrapper(ann_type)]):  # type: ignore
        inner_keys = ann_type.__annotations__.keys()  # type: ignore
        inner_field_types = ann_type.__annotations__
        (
            inner_optional_keys,
            inner_required_keys,
        ) = _split_into_optional_and_required(inner_keys, inner_field_types)
        _validate_dict(val, inner_required_keys, inner_optional_keys, inner_field_types)
    elif type_origin == list:  # type: ignore
        assert isinstance(val, list)
        for item in val:
            inner_annotated_types = ann_type.__args__
            for inner_annotated_type in inner_annotated_types:
                _validate(item, inner_annotated_type)
    elif type_origin == dict:
        assert isinstance(val, dict)
        for key, inner_val in val.items():
            inner_args = ann_type.__args__
            if len(inner_args) == 2:
                _validate(key, inner_args[0])
                _validate(inner_val, inner_args[1])
    elif type_origin == typing.Union:  # type: ignore
        options = ann_type.__args__
        errors = []
        for optional_type in options:
            if str(optional_type) != "<class 'NoneType'>":
                try:
                    _validate(val, optional_type)
                    return
                except ValidationError as error:
                    errors.append(error)
        raise ValidationError(errors)
    else:
        try:
            assert isinstance(val, ann_type), f"expected value: {val} to be of type {ann_type}"
        except AssertionError as error:
            raise ValidationError(error) from error


def _validate_dict(
    input_dict: Dict,
    required_keys: Set[str],
    optional_keys: Set[str],
    field_types: Dict[str, Any],
):
    for required_key in required_keys:
        val = input_dict.get(required_key)
        if val is None:
            raise ValidationError(
                f"expected key: '{required_key}' not found in given data from which to form a typed"
                f" dictionary, given keys are {list(input_dict.keys())}"
            )
        annotated_type = field_types[required_key]
        _validate(val, annotated_type)
    for optional_key in optional_keys:
        if val := input_dict.get(optional_key):
            annotated_type = field_types[optional_key]
            _validate(val, annotated_type)


def _split_into_optional_and_required(
    keys: Set[str], field_types: Dict[str, Any]
) -> Tuple[Set[str], Set[str]]:
    # treat an optional None annotated type is meaning the key is not needed
    optional = set()
    required = set()
    for key in keys:
        ann_type = field_types[key]
        type_origin = ann_type.__origin__ if hasattr(ann_type, "__origin__") else None
        if type_origin == typing.Union:  # type: ignore
            inner_types = ann_type.__args__
            type_in_inner_types_is_none = [
                str(inner_type) == "<class 'NoneType'>" for inner_type in inner_types
            ]
            if any(type_in_inner_types_is_none):
                optional.add(key)
                continue
        required.add(key)
    return optional, required


def validated_typed_dict(cls):
    @functools.wraps(cls)
    def factory_wrapper(*args, **kwargs):
        keys = cls.__annotations__.keys()  # type: ignore
        field_types = cls.__annotations__
        optional_keys, required_keys = _split_into_optional_and_required(keys, field_types)
        _validate_dict(kwargs, required_keys, optional_keys, field_types)
        return cls(*args, **kwargs)

    return factory_wrapper
