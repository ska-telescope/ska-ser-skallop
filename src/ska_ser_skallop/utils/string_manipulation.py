"""Helper functions for manipulating strings."""

import re


def trunc(input: str, marker: str, from_last=False):
    """Truncate a given string based on the first or last found position of a marker.

    :param input: the input value e.g. xyz/temp/
    :param marker: the marker e.g.  '/'
    :param from_last:  wether to truncate from last found position (True)
        or the first found position (False), defaults to False
    """
    if from_last:
        if marker == "/":
            pattern = re.compile(r"(.*)(?=\/\w*$)")
        else:
            pattern_str = r"(.*)(?=" + marker + r"\w*$)"
            pattern = re.compile(pattern_str)
        if result := pattern.findall(input):
            return result[0]
        return ""
    return input[: input.find(marker)]
