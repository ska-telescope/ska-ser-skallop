from typing import Callable, List

from mock import Mock

from ska_ser_skallop.mvp_control.configuration.types import Composition, ScanConfiguration
from ska_ser_skallop.mvp_control.model.mvp_model import MVPModel
from ska_ser_skallop.utils.singleton import Singleton

from .synched_entrypoint import EntryPoint, SynchedEntryPoint


class MockedEntryPoint(SynchedEntryPoint):
    """Stubbed Entrypoint for which a user can override each respective call."""

    def __init__(self, model: MVPModel = None) -> None:
        super().__init__()
        self._abort_subarray: Callable[[int], None] = lambda *args, **kwargs: None
        self._compose_subarray: Callable[[int, List[int], Composition, str], None] = (
            lambda *args, **kwargs: None
        )
        self._configure_subarray: Callable[
            [int, List[int], ScanConfiguration, str, float], None
        ] = lambda *args, **kwargs: None
        self._reset_subarray: Callable[[int], None] = lambda *args, **kwargs: None
        self._set_telescope_to_running: Callable[[], None] = lambda *args, **kwargs: None
        self._tear_down_subarray: Callable[[int], None] = lambda *args, **kwargs: None
        self._set_telescope_to_standby: Callable[[], None] = lambda *args, **kwargs: None
        self._scan: Callable[[int], None] = lambda *args, **kwargs: None
        self._clear_configuration: Callable[[int], None] = lambda *args, **kwargs: None
        if model is None:
            self.model = MVPModel()
        else:
            self.model = model
        self._wrap: EntryPoint = Mock(EntryPoint)
        self._wrapped = False
        self._spy_cls = None

    def set_spy(self, spy_cls: Callable[[], EntryPoint]):
        self._spy_cls = spy_cls

    def activate_spy(self):
        # only activate once
        if not self._wrapped:
            if self._spy_cls:
                wrap = self._spy_cls()
                self._wrap: EntryPoint = Mock(EntryPoint, wraps=wrap)
                self._wrapped = True

    @property
    def spy(self) -> EntryPoint:
        return self._wrap

    def when_abort_subarray(self, handler: Callable[[int], None]):
        """implement :py:meth:`abort_subarray` with given function.

        :param handler: function to call during abort
        """
        self._abort_subarray = handler

    def abort_subarray(self, sub_array_id: int):
        """Stubbed abort subarray command.

        :param sub_array_id: subarray id
        """
        self._abort_subarray(sub_array_id)
        self._wrap.abort_subarray(sub_array_id)

    def when_compose_subarray(self, handler: Callable[[int, List[int], Composition, str], None]):
        """implement :py:meth:`abort_subarray` with given function.

        :param handler: function to call during compose
        """
        self._compose_subarray = handler

    def compose_subarray(
        self,
        sub_array_id: int,
        dish_ids: List[int],
        composition: Composition,
        sb_id: str,
    ):
        """Stubbed abort compose_subarray commands.

        :param sub_array_id: subarray id
        :param dish_ids: Ids of dishes (in case of mid) to assign
        :param composition: Configuration data for assign resources
        :param sb_id: SB identification
        """
        self._compose_subarray(sub_array_id, dish_ids, composition, sb_id)
        self._wrap.compose_subarray(sub_array_id, dish_ids, composition, sb_id)

    def when_configure_subarray(
        self,
        handler: Callable[[int, List[int], ScanConfiguration, str, float], None],
    ):
        """implement :py:meth:`configure_subarray` with given function.

        :param handler: function to call during configure subarray
        """
        self._configure_subarray = handler

    def configure_subarray(
        self,
        sub_array_id: int,
        dish_ids: List[int],
        configuration: ScanConfiguration,
        sb_id: str,
        duration: float,
    ):
        """Stubbed configure subarray command

        :param sub_array_id: subarray id
        :param dish_ids: Ids of dishes (in case of mid) to assign
        :param configuration: Configuration data for aresource configuration
        :param sb_id: SB identification
        :param duration: the duration that a scan should take
        """
        self._configure_subarray(sub_array_id, dish_ids, configuration, sb_id, duration)
        self._wrap.configure_subarray(sub_array_id, dish_ids, configuration, sb_id, duration)

    def when_clear_configuration(self, handler: Callable[[int], None]):
        """implement :py:meth:`clear_configuration` with given function.

        :param handler: function to call during clear configuration
        """
        self._clear_configuration = handler

    def clear_configuration(self, sub_array_id: int):
        """Stubbed clear configuration.

        :param sub_array_id: subarray id
        """
        self._clear_configuration(sub_array_id)
        self._wrap.clear_configuration(sub_array_id)

    def when_reset_subarray(self, handler: Callable[[int], None]):
        """implement :py:meth:`reset_subarray` with given function.

        :param handler: function to call during configure subarray
        """
        self._reset_subarray = handler

    def reset_subarray(self, sub_array_id: int):
        """Stubbed reset subarray command

        :param sub_array_id: subarray id
        """
        self._reset_subarray(sub_array_id)
        self._wrap.reset_subarray(sub_array_id)

    def when_set_telescope_to_running(self, handler: Callable[[], None]):
        """implement :py:meth:`set_telescope_to_running` with given function.

        :param handler: function to call during configure subarray
        """
        self._set_telescope_to_running = handler

    def set_telescope_to_running(self):
        """Stubbed set telescope to running command"""
        self._set_telescope_to_running()
        self._wrap.set_telescope_to_running()

    def when_tear_down_subarray(self, handler: Callable[[int], None]):
        """implement :py:meth:`tear_down_subarray` with given function.

        :param handler: function to call during configure subarray
        """
        self._tear_down_subarray = handler

    def tear_down_subarray(self, sub_array_id: int):
        """Stubbed tear down subarray command

        :param sub_array_id: subarray id
        """
        self._tear_down_subarray(sub_array_id)
        self._wrap.tear_down_subarray(sub_array_id)

    def when_set_telescope_to_standby(self, handler: Callable[[], None]):
        """implement :py:meth:`set_telescope_to_standby` with given function.

        :param handler: function to call during configure subarray
        """
        self._set_telescope_to_standby = handler

    def set_telescope_to_standby(self):
        """Stubbed set telescope to standby command"""
        self._set_telescope_to_standby()
        self._wrap.set_telescope_to_standby()

    def when_scan(self, handler: Callable[[int], None]):
        """implement :py:meth:`scan` with given function.

        :param handler: function to call during configure subarray
        """
        self._scan = handler

    def scan(self, sub_array_id: int):
        """Stubbed scan command

        :param sub_array_id: subarray id
        """
        self._scan(sub_array_id)
        self._wrap.scan(sub_array_id)

    def set_wating_for_start_up(self):
        if self._wrapped:
            return self._wrap.set_wating_for_start_up()
        return super().set_wating_for_start_up()

    def set_waiting_for_assign_resources(
        self,
        sub_array_id: int,
    ):
        if self._wrapped:
            return self._wrap.set_waiting_for_assign_resources(sub_array_id)
        return super().set_waiting_for_assign_resources(sub_array_id)

    def set_waiting_for_release_resources(
        self,
        sub_array_id: int,
    ):
        if self._wrapped:
            return self._wrap.set_waiting_for_release_resources(sub_array_id)
        return super().set_waiting_for_release_resources(sub_array_id)

    def set_wating_for_shut_down(
        self,
    ):
        if self._wrapped:
            return self._wrap.set_wating_for_shut_down()
        return super().set_wating_for_shut_down()

    def set_waiting_for_configure(self, sub_array_id: int, receptors: List[int]):
        if self._wrapped:
            return self._wrap.set_waiting_for_configure(sub_array_id, receptors)
        return super().set_waiting_for_configure(sub_array_id, receptors)

    def set_waiting_until_configuring(self, sub_array_id: int, receptors: List[int]):
        if self._wrapped:
            return self._wrap.set_waiting_until_configuring(sub_array_id, receptors)
        return super().set_waiting_until_configuring(sub_array_id, receptors)

    def set_waiting_until_scanning(self, sub_array_id: int, receptors: List[int]):
        if self._wrapped:
            return self._wrap.set_waiting_until_scanning(sub_array_id, receptors)
        return super().set_waiting_until_scanning(sub_array_id, receptors)

    def set_waiting_for_clear_configure(self, sub_array_id: int, receptors: List[int]):
        if self._wrapped:
            return self._wrap.set_waiting_for_clear_configure(sub_array_id, receptors)
        return super().set_waiting_for_clear_configure(sub_array_id, receptors)

    def set_waiting_for_obsreset(self, sub_array_id: int, receptors: List[int]):
        if self._wrapped:
            return self._wrap.set_waiting_for_obsreset(sub_array_id, receptors)
        return super().set_waiting_for_obsreset(sub_array_id, receptors)

    def set_waiting_until_resourcing(
        self,
        sub_array_id: int,
    ):
        if self._wrapped:
            return self._wrap.set_waiting_until_resourcing(sub_array_id)
        return super().set_waiting_until_resourcing(sub_array_id)

    def set_wating_for_scan_completion(self, sub_array_id: int, receptors: List[int]):
        if self._wrapped:
            return self._wrap.set_wating_for_scan_completion(sub_array_id, receptors)
        return super().set_wating_for_scan_completion(sub_array_id, receptors)


def get_mocked_entry_point() -> MockedEntryPoint:
    entry_point = MockEntryPointContainer().entry_point

    return entry_point


def get_new_mocked_entry_point() -> MockedEntryPoint:
    container = MockEntryPointContainer()
    container.reset()
    return MockEntryPointContainer().entry_point


class MockEntryPointContainer(metaclass=Singleton):
    def __init__(self) -> None:
        self._entry_point = MockedEntryPoint()

        @self._entry_point.when_set_telescope_to_running
        def set_telescope_to_running():
            self._entry_point.model.set_states_for_telescope_running()

        @self._entry_point.when_set_telescope_to_standby
        def set_telescope_to_standby():
            self._entry_point.model.set_states_for_telescope_standby()

    def reset(self) -> None:
        self.__init__()

    @property
    def entry_point(self) -> MockedEntryPoint:
        return self._entry_point
