"""Module containing an implementation of Entrypoint as a composite of Observation Steps"""

from typing import List

from ska_ser_skallop.event_handling.builders import MessageBoardBuilder, get_message_board_builder
from ska_ser_skallop.mvp_control.configuration.types import Composition, ScanConfiguration

from . import base


class NoOpStep(
    base.SetOnlineStep,
    base.StartUpStep,
    base.AssignResourcesStep,
    base.ConfigureStep,
    base.ScanStep,
    base.AbortStep,
    base.ObsResetStep,
    base.RestartStep,
    base.WaitReadyStep,
):
    """Type of step that does not do anything."""

    def set_wait_for_do_set_online(self) -> MessageBoardBuilder:
        return get_message_board_builder()

    def set_wait_for_doing_set_online(self) -> MessageBoardBuilder:
        return get_message_board_builder()

    def do_set_online(self):
        pass

    def set_wait_for_undo_set_online(self) -> MessageBoardBuilder:
        return get_message_board_builder()

    def undo_set_online(self):
        pass

    def set_wait_for_do_startup(self) -> MessageBoardBuilder:
        return get_message_board_builder()

    def set_wait_for_doing_startup(self) -> MessageBoardBuilder:
        return get_message_board_builder()

    def do_startup(self):
        pass

    def set_wait_for_undo_startup(self) -> MessageBoardBuilder:
        return get_message_board_builder()

    def undo_startup(self):
        pass

    def do_assign_resources(  # type: ignore
        self,
        sub_array_id: int,
        dish_ids: List[int],
        composition: Composition,
        sb_id: str,
    ):
        pass

    def set_wait_for_do_assign_resources(self, sub_array_id: int) -> MessageBoardBuilder:
        return get_message_board_builder()

    def set_wait_for_doing_assign_resources(self, sub_array_id: int) -> MessageBoardBuilder:
        return get_message_board_builder()

    def undo_assign_resources(self, sub_array_id: int):  # type: ignore
        pass

    def set_wait_for_undo_resources(self, sub_array_id: int) -> MessageBoardBuilder:
        return get_message_board_builder()

    def do_configure(  # type: ignore
        self,
        sub_array_id: int,
        dish_ids: List[int],
        configuration: ScanConfiguration,
        sb_id: str,
        duration: float,
    ):
        pass

    def undo_configure(self, sub_array_id: int):  # type: ignore
        pass

    def set_wait_for_do_configure(  # type: ignore
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        return get_message_board_builder()

    def set_wait_for_doing_configure(  # type: ignore
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        return get_message_board_builder()

    def set_wait_for_undo_configure(  # type: ignore
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        return get_message_board_builder()

    def do_scan(self, sub_array_id: int):
        pass

    def undo_scan(self, sub_array_id: int):  # type: ignore
        pass

    def set_wait_for_do_scan(  # type: ignore
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        return get_message_board_builder()

    def set_wait_for_doing_scan(  # type: ignore
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        return get_message_board_builder()

    def set_wait_for_undo_scan(  # type: ignore
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        return get_message_board_builder()

    def do_obsreset(self, sub_array_id: int):  # type: ignore
        pass

    def undo_obsreset(self, sub_array_id: int):  # type: ignore
        pass

    def set_wait_for_do_obsreset(  # type: ignore
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        return get_message_board_builder()

    def set_wait_for_doing_obsreset(  # type: ignore
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        return get_message_board_builder()

    def set_wait_for_undo_obsreset(  # type: ignore
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        return get_message_board_builder()

    def do_restart(self, sub_array_id: int):  # type: ignore
        pass

    def undo_restart(self, sub_array_id: int):  # type: ignore
        pass

    def set_wait_for_do_restart(  # type: ignore
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        return get_message_board_builder()

    def set_wait_for_doing_restart(  # type: ignore
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        return get_message_board_builder()

    def set_wait_for_undo_restart(  # type: ignore
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        return get_message_board_builder()

    def do_abort(self, sub_array_id: int):  # type: ignore
        pass

    def undo_abort(self, sub_array_id: int):  # type: ignore
        pass

    def set_wait_for_do_abort(  # type: ignore
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        return get_message_board_builder()

    def set_wait_for_doing_abort(  # type: ignore
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        return get_message_board_builder()

    def set_wait_for_undo_abort(  # type: ignore
        self, sub_array_id: int, receptors: List[int]
    ) -> MessageBoardBuilder:
        return get_message_board_builder()

    def set_wait_for_sut_ready_for_session(self) -> MessageBoardBuilder:
        return get_message_board_builder()


class CompositeEntryPoint(base.EntryPoint):
    """An Base Implimentation of EntryPoint using ObservationStep objects.

    To implement a more specific Entrypoint for an interface point, override the
    particular step object (:py:class:`ObservationStep`)
    with a more specific class e.g.:

    .. code-block:: python

        def __init__(self) -> None:
            super().__init__()
            self.set_online_step = MySetOnlineStep()

    """

    set_online_step: base.SetOnlineStep = base.NotImplementedSetOnlineStep()
    start_up_step: base.StartUpStep = base.NotImplementedStartUpStep()
    assign_resources_step: base.AssignResourcesStep = base.NotImplementedAssignResourcesStep()
    configure_scan_step: base.ConfigureStep = base.NotImplementedConfigureStep()
    scan_step: base.ScanStep = base.NotImplementedScanStep()
    obsreset_step: base.ObsResetStep = base.NotImplementedObsResetStep()
    abort_step: base.AbortStep = base.NotImplementedAbortStep()
    restart_step: base.RestartStep = base.NotImplementedRestartStep()
    wait_ready: base.WaitReadyStep = NoOpStep()

    def set_waiting_for_offline_components_to_become_online(
        self,
    ) -> MessageBoardBuilder:
        return self.set_online_step.set_wait_for_do_set_online()

    def set_wait_for_sut_ready_for_session(
        self,
    ) -> MessageBoardBuilder:
        return self.wait_ready.set_wait_for_sut_ready_for_session()

    def set_offline_components_to_online(self):
        self.set_online_step.do_set_online()

    def set_telescope_to_running(self):
        self.start_up_step.do_startup()

    def set_telescope_to_standby(self):
        self.start_up_step.undo_startup()

    def tear_down_subarray(self, sub_array_id: int):
        self.assign_resources_step.undo_assign_resources(sub_array_id)

    def compose_subarray(
        self,
        sub_array_id: int,
        dish_ids: List[int],
        composition: Composition,
        sb_id: str,
    ):
        self.assign_resources_step.do_assign_resources(sub_array_id, dish_ids, composition, sb_id)

    def clear_configuration(self, sub_array_id: int):
        self.configure_scan_step.undo_configure(sub_array_id)

    def configure_subarray(
        self,
        sub_array_id: int,
        configuration: ScanConfiguration,
        sb_id: str,
        duration: float,
    ):
        self.configure_scan_step.do_configure(sub_array_id, configuration, sb_id, duration)

    def scan(self, sub_array_id: int):
        self.scan_step.do_scan(sub_array_id)

    def set_wating_for_scan_completion(self, sub_array_id: int) -> MessageBoardBuilder:
        return self.scan_step.set_wait_for_do_scan(sub_array_id)

    def set_wating_for_start_up(self) -> MessageBoardBuilder:
        return self.start_up_step.set_wait_for_do_startup()

    def set_waiting_for_assign_resources(self, sub_array_id: int) -> MessageBoardBuilder:
        return self.assign_resources_step.set_wait_for_do_assign_resources(sub_array_id)

    def set_waiting_for_configure(self, sub_array_id: int) -> MessageBoardBuilder:
        return self.configure_scan_step.set_wait_for_do_configure(sub_array_id)

    def set_waiting_until_configuring(self, sub_array_id: int) -> MessageBoardBuilder:
        return self.configure_scan_step.set_wait_for_doing_configure(sub_array_id)

    def set_waiting_until_scanning(self, sub_array_id: int) -> MessageBoardBuilder:
        return self.scan_step.set_wait_for_doing_scan(sub_array_id)

    def set_waiting_until_resourcing(self, sub_array_id: int) -> MessageBoardBuilder:
        return self.assign_resources_step.set_wait_for_doing_assign_resources(sub_array_id)

    def set_waiting_for_clear_configure(self, sub_array_id: int) -> MessageBoardBuilder:
        return self.configure_scan_step.set_wait_for_undo_configure(sub_array_id)

    def set_waiting_for_obsreset(self, sub_array_id: int) -> MessageBoardBuilder:
        return self.obsreset_step.set_wait_for_do_obsreset(sub_array_id)

    def set_waiting_for_restart(self, sub_array_id: int) -> MessageBoardBuilder:
        return self.restart_step.set_wait_for_do_restart(sub_array_id)

    def set_waiting_for_release_resources(self, sub_array_id: int) -> MessageBoardBuilder:
        return self.assign_resources_step.set_wait_for_undo_resources(sub_array_id)

    def set_wating_for_shut_down(
        self,
    ) -> MessageBoardBuilder:
        return self.start_up_step.set_wait_for_undo_startup()

    def abort_subarray(self, sub_array_id: int):
        self.abort_step.do_abort(sub_array_id)

    def reset_subarray(self, sub_array_id: int):
        self.obsreset_step.do_obsreset(sub_array_id)

    def restart_subarray(self, sub_array_id: int):
        self.restart_step.do_restart(sub_array_id)
