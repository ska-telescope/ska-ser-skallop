from typing import List

from ska_ser_skallop.connectors import configuration
from ska_ser_skallop.mvp_control.configuration import composition as comp
from ska_ser_skallop.mvp_control.configuration import configuration as conf
from ska_ser_skallop.mvp_control.configuration.types import Composition, ScanConfiguration
from ska_ser_skallop.mvp_control.entry_points import types as conf_types
from ska_ser_skallop.utils.env import i_can_import
from ska_ser_skallop.utils.singleton import Singleton

from .synched_entrypoint import SynchedEntryPoint

if i_can_import("ska.scripting"):
    from ska.scripting.domain import ResourceAllocation  # type: ignore
    from ska.scripting.domain import Dish, SubArray


class EntryPoint(SynchedEntryPoint, metaclass=Singleton):
    def set_telescope_to_running(self):
        # TODO: Using TMC API for TelescopeOn command as TelescopeOn can not be invoked
        # from oet for now.
        # Telescope().start_up()
        device_name = "ska_mid/tm_central/central_node"
        proxy = configuration.get_device_proxy(device_name)
        LOGGER.info(  # noqa: F821
            "Before Sending TelescopeOn command on CentralNode state :"
            + str(CentralNode.telescopeState)  # noqa: F821
        )
        proxy.TelescopeOn()

    def set_telescope_to_standby(self):
        # TODO: Using TMC API for TelescopeOff command as TelescopeOff can not be
        # invoked from oet for now.
        # Telescope().standby()
        device_name = "ska_mid/tm_central/central_node"
        proxy = configuration.get_device_proxy(device_name)
        proxy.TelescopeOff()
        LOGGER.info(  # noqa: F821
            "After Standby CentralNode State:" + str(CentralNode.telescopeState)  # noqa: F821
        )
        LOGGER.info("Standby the Telescope")  # noqa: F821

    def tear_down_subarray(self, sub_array_id: int):
        subarray = SubArray(sub_array_id)
        subarray.deallocate()

    def compose_subarray(
        self,
        sub_array_id: int,
        dish_ids: List[int],
        composition: Composition,
        sb_id: str,
    ):
        subarray = SubArray(sub_array_id)
        if isinstance(composition, conf_types.CompositionByFile):
            if composition.conf_type == conf_types.CompositionType.STANDARD:
                multi_dish_allocation = ResourceAllocation(dishes=[Dish(id) for id in dish_ids])
                resource_config_file = comp.generate_standard_comp_to_file(
                    sub_array_id, dish_ids, composition.location, sb_id
                )
                # Verify this api as in SKAMPI allocate_from_cdm is used
                subarray.allocate_from_file(resource_config_file, True, multi_dish_allocation)
                return
        raise NotImplementedError(f"allocate using composition {composition} not implemented")

    def configure_subarray(
        self,
        sub_array_id: int,
        dish_ids: List[int],
        configuration: ScanConfiguration,
        sb_id: str,
        duration: float,
    ):
        subarray = SubArray(sub_array_id)
        if isinstance(configuration, conf_types.ScanConfigurationByFile):
            if configuration.conf_type == conf_types.ScanConfigurationType.STANDARD:
                scan_config_file = conf.generate_standard_conf_to_file(
                    configuration.location, sub_array_id, sb_id, duration
                )
                subarray.configure_from_file(scan_config_file, 6, with_processing=False)

    def clear_configuration(self, sub_array_id: int):
        subarray = SubArray(sub_array_id)
        subarray.end()

    def abort_subarray(self, sub_array_id: int):
        subarray = SubArray(sub_array_id)
        subarray.abort()

    def reset_subarray(self, sub_array_id: int):
        subarray = SubArray(sub_array_id)
        subarray.reset()

    def scan(self, sub_array_id: int):
        subarray = SubArray(sub_array_id)
        subarray.scan()
