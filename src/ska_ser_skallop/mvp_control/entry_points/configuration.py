import inspect
import logging
from contextlib import contextmanager
from typing import Callable, Dict, List, NamedTuple, Union

import mock

from ska_ser_skallop.utils.env import get_entry_point_from_env
from ska_ser_skallop.utils.singleton import Singleton

from .base import EntryPoint
from .oet import EntryPoint as OetEntryPoint
from .synched_entrypoint import SynchedEntryPoint
from .tmc import EntryPoint as TmcEntryPoint

EntryPointConstructor = Callable[[], EntryPoint]

EntryPointMapping = Dict[Union[str, None], EntryPointConstructor]

logger = logging.getLogger(__name__)


def determine_entry_point(entry_point: Union[str, None] = None) -> Callable[[], EntryPoint]:
    if not entry_point:
        entry_point = get_entry_point_from_env()
    mapping: EntryPointMapping = {
        None: TmcEntryPoint,
        "tmc": TmcEntryPoint,
        "oet": OetEntryPoint,
    }
    if entry_point not in mapping:
        # in case if wrongly typed env var we use deafult
        logger.warning(f"does not recognize {entry_point} will select default tmc as entrypoint ")
        entry_point = None
    return mapping[entry_point]


def get_entry_point() -> EntryPoint:
    container = EntryPointContainer()  # type: ignore
    if entry_point := container.get_entry_point():
        return entry_point
    entry_point_cls = container.get_entry_point_cls()
    return entry_point_cls()


@contextmanager
def inject_entry_point(entry_point: Union[Callable[[], EntryPoint], EntryPoint], provided_by: str):
    container = EntryPointContainer()  # type: ignore
    container.stash_entry_point()
    if isinstance(entry_point, EntryPoint):
        container.inject_entry_point_instance(entry_point, provided_by)
        yield
        container.pop_stash()
    else:
        container.reset()
        container.inject_entry_point(entry_point, provided_by)
        yield
        container.pop_stash()


def set_entry_point(entry_point: Union[Callable[[], EntryPoint], EntryPoint], provided_by: str):
    container = EntryPointContainer()  # type: ignore
    if isinstance(entry_point, EntryPoint):
        container.inject_entry_point_instance(entry_point, provided_by)
    else:
        container.inject_entry_point(entry_point, provided_by)


def reset_configurations():
    container = EntryPointContainer()  # type: ignore
    container.reset()


@contextmanager
def patch_entry_point(
    mocked_entry_point: mock.Mock | None = None,
    wrapping: bool = False,
    synched_impl: bool = False,
):
    parent = inspect.getouterframes(inspect.currentframe())[1][3]
    if wrapping:
        entry_point_cls = determine_entry_point()
        entry_point_mock = mock.Mock(EntryPoint, wraps=entry_point_cls())
    elif synched_impl:
        entry_point_mock = mock.Mock(SynchedEntryPoint)
        entry_point_mock.return_value = mock.Mock(SynchedEntryPoint, wraps=SynchedEntryPoint())
    else:
        entry_point_mock = mock.Mock(EntryPoint) if not mocked_entry_point else mocked_entry_point
    container = EntryPointContainer()  # type: ignore
    container.reset()
    container.inject_entry_point(entry_point_mock, f"patched by {parent}")
    yield entry_point_mock.return_value
    container.reset()


class Provider(NamedTuple):
    provider: Callable[[], EntryPoint]
    provided_by: str


class ProviderInstance(NamedTuple):
    provider: EntryPoint
    provided_by: str


class StashedEntryPoint(NamedTuple):
    entry_point_cls: Union[None, Provider]
    entry_point: Union[None, ProviderInstance]


class EntryPointContainer(metaclass=Singleton):
    def __init__(self) -> None:
        self._entry_point_cls = None
        self._entry_point = None
        self._stash: List[StashedEntryPoint] = []

    def reset(self):
        self._entry_point_cls = None
        self._entry_point = None

    def stash_entry_point(self):
        """remove the current entry point injected and stash it onto a stack."""
        self._stash.append(StashedEntryPoint(self._entry_point_cls, self._entry_point))
        self._entry_point = None
        self._entry_point_cls = None

    def pop_stash(self):
        """Return the state to entry points stashed temporarily."""
        stashed = self._stash.pop()
        self._entry_point = stashed.entry_point
        self._entry_point_cls = stashed.entry_point_cls

    def get_entry_point(self) -> Union[None, EntryPoint]:
        """Retrieves the configured entry point as an instance (the object controlling the SUT). If
        this is the first time call, the entry point shall be determined from an env
        variable.
        """
        if self._entry_point:
            return self._entry_point.provider
        return None

    def get_entry_point_cls(self) -> Callable[[], EntryPoint]:
        """Retrieves the configured entry point as a class (the object controlling the SUT). If
        this is the first time call, the entry point shall be determined from an env
        variable.
        """
        if self._entry_point_cls:
            return self._entry_point_cls.provider
        entry_point_cls = determine_entry_point()
        parent = inspect.getouterframes(inspect.currentframe())[2][3]
        self.inject_entry_point(
            entry_point_cls,
            f"environment during dependency call from {parent}",
        )
        return entry_point_cls

    def inject_entry_point(self, entry_point: Callable[[], EntryPoint], provided_by: str):
        assert not self._entry_point, (
            f"An entry point ({type(self._entry_point.provider)}) has already been "
            f"defined to be used by {self._entry_point.provided_by}"
        )
        assert not self._entry_point_cls, (
            f"An entry point ({type(self._entry_point_cls.provider)}) has already been "
            f"defined to be used by {self._entry_point_cls.provided_by}"
        )
        self._entry_point_cls = Provider(entry_point, provided_by)

    def inject_entry_point_instance(self, entry_point: EntryPoint, provided_by: str):
        assert not self._entry_point_cls, (
            f"An entry point ({type(self._entry_point_cls.provider)}) has already been "
            f"defined to be used by {self._entry_point_cls.provided_by}"
        )
        assert not self._entry_point, (
            f"An entry point ({type(self._entry_point.provider)}) has already been "
            f"defined to be used by {self._entry_point.provided_by}"
        )
        self._entry_point = ProviderInstance(entry_point, provided_by)
