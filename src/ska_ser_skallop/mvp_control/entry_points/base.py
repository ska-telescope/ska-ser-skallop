import logging
from abc import abstractmethod
from typing import List

from ska_ser_skallop.event_handling.builders import MessageBoardBuilder
from ska_ser_skallop.mvp_control.configuration.types import Composition, ScanConfiguration

logger = logging.getLogger(__name__)


class EntryPoint:
    def __init__(self) -> None:
        pass

    @abstractmethod
    def set_waiting_for_offline_components_to_become_online(
        self,
    ) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wait_for_sut_ready_for_session(self) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_offline_components_to_online(self):
        pass

    @abstractmethod
    def set_telescope_to_running(self):
        pass

    @abstractmethod
    def set_telescope_to_standby(self):
        pass

    @abstractmethod
    def tear_down_subarray(self, sub_array_id: int):
        pass

    @abstractmethod
    def compose_subarray(
        self,
        sub_array_id: int,
        dish_ids: List[int],
        composition: Composition,
        sb_id: str,
    ):
        pass

    @abstractmethod
    def clear_configuration(self, sub_array_id: int):
        pass

    @abstractmethod
    def configure_subarray(
        self,
        sub_array_id: int,
        configuration: ScanConfiguration,
        sb_id: str,
        duration: float,
    ):
        pass

    @abstractmethod
    def scan(self, sub_array_id: int):
        pass

    @abstractmethod
    def set_wating_for_scan_completion(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wating_for_start_up(self) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_waiting_for_assign_resources(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_waiting_for_configure(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_waiting_until_configuring(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_waiting_until_scanning(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_waiting_until_resourcing(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_waiting_for_clear_configure(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_waiting_for_obsreset(self, sub_array_id: int) -> MessageBoardBuilder:
        """[summary]

        :param sub_array_id: [description]

        :return: [description]
        """

    @abstractmethod
    def set_waiting_for_restart(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_waiting_for_release_resources(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wating_for_shut_down(
        self,
    ) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def abort_subarray(self, sub_array_id: int):
        pass

    @abstractmethod
    def reset_subarray(self, sub_array_id: int):
        pass

    @abstractmethod
    def restart_subarray(self, sub_array_id: int):
        pass


class ObservationStep:
    """Defines the abstract logic for a step in an observation execution flow."""

    @abstractmethod
    def set_wait_for_do(self) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wait_for_doing(self) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def do(self):
        pass

    @abstractmethod
    def set_wait_for_undo(self) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def undo(self):
        pass


class SetOnlineStep:
    """Defines the abstract logic for a step in an observation execution flow."""

    @abstractmethod
    def set_wait_for_do_set_online(self) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wait_for_doing_set_online(self) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def do_set_online(self):
        pass

    @abstractmethod
    def set_wait_for_undo_set_online(self) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def undo_set_online(self):
        pass


class NotImplementedSetOnlineStep(SetOnlineStep):
    """Defines the abstract logic for a step in an observation execution flow."""

    def set_wait_for_do_set_online(self) -> MessageBoardBuilder:
        raise NotImplementedError()

    def set_wait_for_doing_set_online(self) -> MessageBoardBuilder:
        raise NotImplementedError()

    def do_set_online(self) -> None:
        raise NotImplementedError()

    def set_wait_for_undo_set_online(self) -> MessageBoardBuilder:
        raise NotImplementedError()

    def undo_set_online(self) -> None:
        raise NotImplementedError()


class StartUpStep:
    """Defines the abstract logic for a step in an observation execution flow."""

    @abstractmethod
    def set_wait_for_do_startup(self) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wait_for_doing_startup(self) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def do_startup(self):
        pass

    @abstractmethod
    def set_wait_for_undo_startup(self) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def undo_startup(self):
        pass


class NotImplementedStartUpStep(StartUpStep):
    """Defines the abstract logic for a step in an observation execution flow."""

    def set_wait_for_do_startup(self) -> MessageBoardBuilder:
        raise NotImplementedError()

    def set_wait_for_doing_startup(self) -> MessageBoardBuilder:
        raise NotImplementedError()

    def do_startup(self) -> None:
        raise NotImplementedError()

    def set_wait_for_undo_startup(self) -> MessageBoardBuilder:
        raise NotImplementedError()

    def undo_startup(self) -> None:
        raise NotImplementedError()


class AssignResourcesStep:
    @abstractmethod
    def do_assign_resources(
        self,
        sub_array_id: int,
        dish_ids: List[int],
        composition: Composition,
        sb_id: str,
    ):
        pass

    @abstractmethod
    def set_wait_for_do_assign_resources(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wait_for_doing_assign_resources(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def undo_assign_resources(self, sub_array_id: int):
        pass

    @abstractmethod
    def set_wait_for_undo_resources(self, sub_array_id: int) -> MessageBoardBuilder:
        pass


class NotImplementedAssignResourcesStep(AssignResourcesStep):
    def do_assign_resources(
        self,
        sub_array_id: int,
        dish_ids: List[int],
        composition: Composition,
        sb_id: str,
    ) -> None:
        raise NotImplementedError()

    def set_wait_for_do_assign_resources(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()

    def set_wait_for_doing_assign_resources(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()

    def undo_assign_resources(self, sub_array_id: int) -> None:
        raise NotImplementedError()

    def set_wait_for_undo_resources(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()


class ConfigureStep:
    @abstractmethod
    def do_configure(
        self,
        sub_array_id: int,
        configuration: ScanConfiguration,
        sb_id: str,
        duration: float,
    ):
        pass

    @abstractmethod
    def undo_configure(self, sub_array_id: int):
        pass

    @abstractmethod
    def set_wait_for_do_configure(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wait_for_doing_configure(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wait_for_undo_configure(self, sub_array_id: int) -> MessageBoardBuilder:
        pass


class NotImplementedConfigureStep(ConfigureStep):
    def do_configure(
        self,
        sub_array_id: int,
        configuration: ScanConfiguration,
        sb_id: str,
        duration: float,
    ) -> None:
        raise NotImplementedError()

    def undo_configure(self, sub_array_id: int) -> None:
        raise NotImplementedError()

    def set_wait_for_do_configure(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()

    def set_wait_for_doing_configure(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()

    def set_wait_for_undo_configure(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()


class ScanStep:
    @abstractmethod
    def do_scan(self, sub_array_id: int):
        pass

    @abstractmethod
    def undo_scan(self, sub_array_id: int):
        pass

    @abstractmethod
    def set_wait_for_do_scan(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wait_for_doing_scan(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wait_for_undo_scan(self, sub_array_id: int) -> MessageBoardBuilder:
        pass


class NotImplementedScanStep(ScanStep):
    def do_scan(self, sub_array_id: int) -> None:
        raise NotImplementedError()

    def undo_scan(self, sub_array_id: int) -> None:
        raise NotImplementedError()

    def set_wait_for_do_scan(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()

    def set_wait_for_doing_scan(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()

    def set_wait_for_undo_scan(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()


class ObsResetStep:
    @abstractmethod
    def do_obsreset(self, sub_array_id: int):
        pass

    @abstractmethod
    def undo_obsreset(self, sub_array_id: int):
        pass

    @abstractmethod
    def set_wait_for_do_obsreset(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wait_for_doing_obsreset(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wait_for_undo_obsreset(self, sub_array_id: int) -> MessageBoardBuilder:
        pass


class NotImplementedObsResetStep(ObsResetStep):
    def do_obsreset(self, sub_array_id: int) -> None:
        raise NotImplementedError()

    def undo_obsreset(self, sub_array_id: int) -> None:
        raise NotImplementedError()

    def set_wait_for_do_obsreset(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()

    def set_wait_for_doing_obsreset(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()

    def set_wait_for_undo_obsreset(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()


class RestartStep:
    @abstractmethod
    def do_restart(self, sub_array_id: int):
        pass

    @abstractmethod
    def undo_restart(self, sub_array_id: int):
        pass

    @abstractmethod
    def set_wait_for_do_restart(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wait_for_doing_restart(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wait_for_undo_restart(self, sub_array_id: int) -> MessageBoardBuilder:
        pass


class NotImplementedRestartStep(RestartStep):
    def do_restart(self, sub_array_id: int) -> None:
        raise NotImplementedError()

    def undo_restart(self, sub_array_id: int) -> None:
        raise NotImplementedError()

    def set_wait_for_do_restart(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()

    def set_wait_for_doing_restart(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()

    def set_wait_for_undo_restart(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()


class AbortStep:
    @abstractmethod
    def do_abort(self, sub_array_id: int):
        pass

    @abstractmethod
    def undo_abort(self, sub_array_id: int):
        pass

    @abstractmethod
    def set_wait_for_do_abort(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wait_for_doing_abort(self, sub_array_id: int) -> MessageBoardBuilder:
        pass

    @abstractmethod
    def set_wait_for_undo_abort(self, sub_array_id: int) -> MessageBoardBuilder:
        pass


class NotImplementedAbortStep(AbortStep):
    def do_abort(self, sub_array_id: int) -> None:
        raise NotImplementedError()

    def undo_abort(self, sub_array_id: int) -> None:
        raise NotImplementedError()

    def set_wait_for_do_abort(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()

    def set_wait_for_doing_abort(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()

    def set_wait_for_undo_abort(self, sub_array_id: int) -> MessageBoardBuilder:
        raise NotImplementedError()


class WaitReadyStep:
    @abstractmethod
    def set_wait_for_sut_ready_for_session(self) -> MessageBoardBuilder:
        pass


class NotImplementedWaitReadyStep:
    @abstractmethod
    def set_wait_for_sut_ready_for_session(self) -> MessageBoardBuilder:
        raise NotImplementedError()
