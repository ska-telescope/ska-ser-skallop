import logging

from ska_ser_skallop.mvp_control.describing.mvp_conditions import (
    ConditionLabel,
    ConditionLabels,
    Readiness,
)
from ska_ser_skallop.mvp_control.rectifying.rectifier import create_rectifier

logger = logging.getLogger(__name__)


class DishConditionLabels(ConditionLabels):
    @property
    def dish_masters_operational(self) -> ConditionLabel:
        """The current operational state of dish masters. They should all be in state
        'STANDBY_LP' when starting up and 'STANDBY_FP' when shutting down
        """
        return ConditionLabel(
            "Dish Masters operational state",
            "dish_masters_operational",
            "dishMode",
        )

    @property
    def dish_masters_dev_state(self) -> ConditionLabel:
        """
        The current dish state of dish masters.

        They should all be in either 'OFF', "STANDBY"
        """
        return ConditionLabel("Dish Masters dev state", "dish_masters_dev_state", "state")

    @property
    def dish_masters_health(self) -> ConditionLabel:
        """
        The current dish state of dish masters.

        They should all be in either "OK" or "DEGRADED".
        """
        return ConditionLabel("Dish Masters health state", "dish_masters_health", "healthState")


dish_labels = DishConditionLabels()


class DirtyDishHandler:
    def __init__(self, result: Readiness) -> None:
        not_ok_conditions = result.get_not_ok_conditions()
        self.dev_state_condition = not_ok_conditions.get(dish_labels.dish_masters_dev_state)
        self.ops_state_condition = not_ok_conditions.get(dish_labels.dish_masters_operational)
        self.masters_health_condition = not_ok_conditions.get(dish_labels.dish_masters_health)
        self.rectifier = create_rectifier()

    def not_ok(self) -> bool:
        return any([self.dish_dev_state_not_ok(), self.dish_ops_state_not_ok()])

    def dish_dev_state_not_ok(self) -> bool:
        return self.dev_state_condition is not None

    def dish_ops_state_not_ok(self) -> bool:
        return self.ops_state_condition is not None

    def switch_off_any_on_dishes(self):
        devices_in_off, devices_in_lp = [[], []]
        if self.dev_state_condition:
            devices_in_off = (
                self.dev_state_condition.in_state("ON") if self.dish_dev_state_not_ok() else []
            )
        if self.ops_state_condition:
            devices_in_lp = (
                self.ops_state_condition.in_state("STANDBY_FP")
                if self.dish_ops_state_not_ok()
                else []
            )
        if any([devices_in_off, devices_in_lp]):
            devices_to_switch_on = devices_in_off + devices_in_lp
            if devices_to_switch_on:
                logger.warning(
                    "It seems some of the dishes are ON (either in STANDBY_FP or ON) "
                    f"{devices_to_switch_on} will attempt to switch them off"
                )
                # note we use the same command for when we want to switch dishes ON
                self.rectifier.switch_off_dish_masters(devices_to_switch_on)

    def switch_on_any_off_dishes(self):
        # ignore if no dev state condition was given
        if self.dish_dev_state_not_ok():
            dishes = self.dev_state_condition
            if dishes:
                if dishes.is_not_ok():
                    if any(dishes.are_in_state("OFF")):
                        devices_in_off = dishes.in_state("OFF")
                        logger.warning(
                            "It seems some of the dishes are ON (either in STANDBY_FP "
                            f"or ON) {devices_in_off} will attempt to switch them off"
                        )
                        # note we use the same command for when we want to switch dishes
                        # ON
                        self.rectifier.switch_on_dish_masters(devices_in_off)

    def switch_to_fp_any_lp_dishes(self):
        # ignore if no op state condition was given
        if self.dish_ops_state_not_ok():
            dishes = self.ops_state_condition
            if dishes:
                if dishes.is_not_ok():
                    if any(dishes.are_in_state("STANDBY_LP")):
                        # bring back dishes to FP so that we can do coordinated shutdown
                        standby_lp_dishes = dishes.in_state("STANDBY_LP")
                        logger.warning(
                            "It seems some of the dishes are in STANDBY_LP "
                            f"{standby_lp_dishes} will attempt to put them into "
                            "STANDBY_FP "
                        )
                        # note we use the same command for when we want to switch dishes
                        # ON
                        self.rectifier.switch_on_dish_masters(standby_lp_dishes)
