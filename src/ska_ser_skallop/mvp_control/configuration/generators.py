from typing import List


def distribute(start: int, seperation: int, size: int) -> List:
    return list(range(start, seperation * size + start, seperation))


def identity(val: int, size: int) -> List:
    return [val for _ in range(size)]


def merge(A: List, B: List):
    assert len(A) == len(B)
    return [list(each_pair) for each_pair in zip(A, B)]


def generate_channel_averaging(x_seperation: int, y_seperation: int, start: int, size: int):
    return merge(distribute(0, x_seperation, size), [start] + identity(0, size - 1))
