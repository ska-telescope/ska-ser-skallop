"""Module that defines a class for managing different orthogonal conditions about a
running MVP in order to aggregate the states of various devices into a combined
condition of FAULTY, OK or DEGRADED
"""

import logging
import time
from enum import Enum, auto
from functools import reduce
from typing import (
    Any,
    Callable,
    Dict,
    ItemsView,
    KeysView,
    List,
    NamedTuple,
    Set,
    Tuple,
    Union,
    ValuesView,
)

from ska_ser_skallop.mvp_control.describing.inspections import (
    DevList,
    DevState,
    Grouping,
    ListInspection,
    ListInspections,
)

logger = logging.getLogger(__name__)


class ConditionLabel(NamedTuple):
    """Tuple containing data to uniquely identify (label) a particular condition
    measured along a particular dimension (e.g. device state)
    """

    desc: str
    """
    A human readable description of the label defining what aspect is
    being measured (e.g. "The obsState of all subarray devices")
    """

    label: str
    """
    A unique string used as a dictionary key for seperating different
    conditions making up the readiness of an item
    """

    dim: str = "state"
    """
    A tag identifying a name for the dimension which the condition is
    based on (in case for example cross referencing dimensions on different
    objects), default is 'state'
    """


class ConditionState(Enum):
    """Used to specify the current state of a condition. OK means all the reported
    states are what they are supposed to be. INCONSISTENT means some of them are in the
    wrong state and WRONG means all of the states are in the wrong (but consistent)
    state. Lastly, UNKNOWN means the state can not be determined as it is unable to
    communicate with the MVP or has not yet done so.

    """

    OK = auto()
    INCONSISTENT = auto()
    WRONG = auto()
    UNKNOWN = auto()


class Getter:
    """Wrapper object that contains a specific get function used to populate a condition
    with state values.
    """

    _fn: Callable[..., Union[ListInspection, ListInspections]]
    _args: Tuple[Any, ...]

    def __init__(
        self,
        fn: Callable[..., Union[ListInspection, ListInspections]],
        *args: Any,
    ) -> None:
        """
        Initialise a Getter object with a specific get function that must return
        either a ListInspection or Listinspections Object.

        :param fn: the getter function defined as a callable returning
            ListInspection or Listinspections.
        """
        self._fn = fn
        self._args = args

    def get(self) -> Union[ListInspection, ListInspections]:
        """execute the getter function and return the results as ListInspection/Listinspections

        :return: The result of the get function compiled as
            ListInspection/Listinpections
        """
        return self._fn(*self._args)


class ConditionLabels:
    """A class that can be inherited by the client to define specific labels (defined as
    properties on that class) identifying a particular set of "orthogonal" conditions
    for a given  group of objects. Each property must return an `ConditionLabel` object,
    for example:

    .. code-block:: python

       MyConConditionLabels(ConditionLabels):

           @property
           def my_label1(self)->ConditionLabel:
               return ConditionLabel(
                   "my label 1", my_label_1, attribute_on_which_condition_is_based
                )

    """

    @classmethod
    def as_labels(cls) -> set[str]:
        """returns the properties of the class as a set of string labels

        :return: set of class property labels
        """
        attrs = [i for i in cls.__dict__ if i[:1] != "_"]
        return {attr for attr in attrs if attr != "as_labels"}


class Condition:
    """A Class containing the data (or state values) and logical set of rules to
    determine the condition (:py:class:`ConditionState`) for a specific item. Condition
    objects are made to be created by a separate factory function that provides it with
    a object for fetching the state values (:py:class:`Getter`) and the desired value/s
    those state values should be.

    Note that an Condition object can be logically evaluated, the truth condition
    implying condition state is ok (:py:class:`ConditionState.OK`) and false the
    opposite:

    .. code-block:: python

        subarray_condition = get_subarray_condition() # result is of type Condition
        if not subarray_condition:
            handle_condition_not_ok(subarray_condition)

    """

    data: Union[ListInspection, ListInspections]
    state: ConditionState
    get_state: Callable[[], None]
    _grouping: Grouping

    def __init__(self, getter: Getter, *desired_value: str, desired_equal: bool = True) -> None:
        self.state = ConditionState.UNKNOWN
        self.data = ListInspection({})
        self._desired_equal = desired_equal
        self._getter = getter
        self.desired_value = desired_value
        self._grouping = {}
        self._ignored = False

    @property
    def value(self) -> Union[List[DevState], DevState]:
        """The aggregate value of all the devices used to determine the condition if
        they were "rolled up" into a set of unique values. In other words if all the
        devices were in state "ON" the result would be simply "ON".
        If there were some elements who were also "OFF" the result would be
        ["ON", "OFF"]

        :return: unique state or list of unique states of devices
        """
        return self.data.value

    @property
    def grouping(self) -> Grouping:
        """Organises the current device states (used to determine the condition) by
        grouping them based on having a common state value. E.g. all devices in the "ON"
        state.
        Note that if the current device states have not been fetched yet, it will first
        collect the values by means of the :py:func:`~Condition.update_condition`
        method.

        :return: The grouping structured as a dictionary keyed on the state value
            with values organized into

                1. the count of devices having that state value
                2. the list of devices having that state value

        """
        if not self.data:
            self.update_condition()
        return self._grouping

    def describe_unhealthy_state(self) -> Union[str, None]:
        """Returns an human readable description of the condition if it is not ok.
        For example if an exception needs raising, this method can produce the error
        message:

        .. code-block:: python

            if (description := subarray_condition.describe_unhealthy_state()):
                raise Exception(description)

        :return: The description if the condition is calculated as not ok.
            Returns None if it is ok.
        """
        if not self.data:
            self.update_condition()
        if self.is_not_ok():
            if self.is_wrong():
                return f" all items in {self.data.value}"
            wrong_states_descriptions = [
                f"{v.items} to be {k}"
                for k, v in self._grouping.items()
                if k not in self.desired_value
            ]
            if wrong_states_descriptions:
                if len(wrong_states_descriptions) > 1:
                    wrong_states_descriptions = reduce(
                        _aggregate_string_with_and, wrong_states_descriptions
                    )
                    return f":\n{wrong_states_descriptions}"
                return f" {wrong_states_descriptions[0]}"

        return None

    @property
    def devices(self) -> DevList:
        """Returns the list of devices used in determining the condition

        :return: A list of string-formatted device names (FDN names)
        """
        return self.data.devices

    def is_ok(self) -> bool:
        """Whether or not the condition is ok (:py:class:`ConditionState.OK`), as
        determined by a predefined set of logical rules.

        :return: whether it is ok.
        """
        return self.state == ConditionState.OK

    def is_not_ok(self) -> bool:
        """Whether or not the condition is not ok (:py:class:`ConditionState.OK`), as
        determined by a predefined set of logical rules.

        return: whether it is not ok
        """
        return self.state is not ConditionState.OK

    def is_wrong(self) -> bool:
        """Whether or not the condition is wrong (:py:class:`ConditionState.WRONG`), as
        determined by a predefined set of logical rules.

        :return: whether it is wrong
        """
        return self.state == ConditionState.WRONG

    def is_inconsistent(self) -> bool:
        """Whether or not the condition is inconsistent
        (:py:class:`ConditionState.INCONSISTENT`), as determined by a predefined set
        of logical rules.

        :return: whether it is inconsistent
        """
        return self.state == ConditionState.INCONSISTENT

    def _load_state(self):
        self.data = self._getter.get()

    def _calc_state(self) -> bool:
        assert self.data
        if self._desired_equal:
            if all(self.data.are_in_state(*self.desired_value)):
                self.state = ConditionState.OK
                return True
            if (
                all(self.data.are_not_in_state(*self.desired_value))
                and self.data.are_all_the_same()
            ):
                self.state = ConditionState.WRONG
            else:
                self.state = ConditionState.INCONSISTENT
            return False
        # for the case that we are looking for values not to be equal to desired values
        if all(self.data.are_not_in_state(*self.desired_value)):
            self.state = ConditionState.OK
            return True
        if all(self.data.are_in_state(*self.desired_value)) and self.data.are_all_the_same():
            self.state = ConditionState.WRONG
        else:
            self.state = ConditionState.INCONSISTENT
        return False

    def in_state(self, *state: str) -> DevList:
        r"""Returns a selection of devices, making up the condition, that are in any of
        the state values given by the \*state argument. This allows for a more human
        readable way of applying logic, for example:

        .. code-block:: python

            rectify_faulty(subarray_condition.in_state('FAULT'))


        :return: A list of device names (strings) that are in the given state
        """
        return self.data.in_state(*state)

    def are_in_state(self, *state: str) -> List[bool]:
        r"""Returns a list of boolean values indicating whether or not a particular
        device from those used to determine the condition state are in any of the states
        given by the \*state argument. In other words if all the elements were
        satisfying the criteria, then an example would result into [True, True, True].
        This allows for writing logical comparisons in an more human readable way e.g.

        .. code-block:: python

            if any(subarray_condition.are_in_state('OFF','STANDBY')):
                do_someting()
            elif all(subarray_condition.are_in_state('ON')):
                do_someting_else()


        :return: The result of applying the logic on all the constituting
            elements grouped as a list of boolean values.
        """
        return self.data.are_in_state(*state)

    def are_not_in_state(self, *state: str) -> List[bool]:
        r"""Returns a list of boolean values indicating whether or not a particular
        device from those used to determine the condition state are **not** in any of
        the states given by the \*state argument. In other words if all the elements
        were satisfying the criteria, then an example would result into
        [True, True, True]. This allows for writing logical comparisons in an more human
        readable way e.g.

        .. code-block:: python

            if any(subarray_condition.are_in_state('OFF','STANDBY')):
                do_someting()
            elif all(subarray_condition.are_in_state('ON')):
                do_someting_else()


        :return: The result of applying the logic on all the constituting
            elements grouped as a list of boolean values.
        """
        return self.data.are_not_in_state(*state)

    def not_in_state(self, *state: str) -> DevList:
        r"""Returns a selection of devices, from those making the condition, that are
        not in any of the state values given by the \*state argument. This allows for a
        more human readable way of applying logic, for example:

        .. code-block:: python

            switch_on(subarray_condition.not_in_state('ON'))


        :return: A list of device names (strings) that are not in the given state
        """
        return self.data.not_in_state(*state)

    def update_condition(self, *desired_value: str) -> bool:
        r"""Calculate (or recalculate) the aggregate condition by fetching the relevant
        state data and then calculating the condition based on a comparison with the
        \*desired_value input arguments. The result would be that the state values have
        been populated into the object (for subsequent analysis) and the aggregate
        condition state value would be given to allow client code a quick and simple
        manner of determining the overall health. For example:

        .. code-block:: python

            if (result_ok := subarray_condition.update_condition():
                if not result_ok:
                    if subarray_condition.is_wrong():
                        handle_wrong_state(subarray_condition)
                    elif any(subarray_condition.are_in_state('FAULTY'):
                        rectify_faulty(subarray_condition.in_state('FAULTY'))
                    elif subarray_condition.is_inconsistent():
                        switch_off(subarray_condition.in_state('ON'))

        :return: a logical flag first indicating whether data was successfully fetched,
            if so it will return a flag indicating whether the condition is ok or not
        """
        if desired_value:
            self.desired_value = desired_value
        self._load_state()
        if (
            self.data
        ):  # if settings resulted in an entire catogory of data needing to be filtered out, it will
            # be ignored
            self._grouping = self.data.get_grouping()
            return self._calc_state()
        self._ignored = True
        return True

    @property
    def ignored(self) -> bool:
        """Wether the condition is being ignored due to it  been filtered out of scope.

        :return: True of the condition is being ignored
        """
        return self._ignored

    def __bool__(self) -> bool:
        return self.update_condition()

    def items(self) -> ItemsView[str, ListInspection]:
        """Returns an ItemsView of the state data whereby each item represent a
        particular key value pair, each value representing a ListInspection result (e.g
        device names with device values). This allows for iterating over each group of
        devices with values:

        .. code-block:: python

            for device_grouping, device_states in subarray_condition.items():
                if devices_states.are_in_state('ON'):
                    logger.log(f'devices for {device_grouping} are ON')



        :return: An itemview containing state for a particular key in the data.
        """
        if isinstance(self.data, ListInspection):
            return ListInspections({"1": self.data}).items()
        return self.data.items()

    def keys(self) -> KeysView[str]:
        """Returns the keys, if any, used to separate the condition data into mutually
        exclusive set.

        :return: The keys as strings
        """
        if isinstance(self.data, ListInspection):
            return ListInspections({"1": self.data}).keys()
        return self.data.keys()

    def values(self) -> ValuesView[ListInspection]:
        """Returns the values representing separate device states
        for each key naming that particular value set.

        :return: the state values of a group of devices
        """
        if isinstance(self.data, ListInspection):
            return ListInspections({"1": self.data}).values()
        return self.data.values()


class Conditional(NamedTuple):
    """The wrapping of a :py:class:`Condition` with metadata giving it a
    general description as well as a dimension upon which the condition is
    measured (e.g. ObsState, State, Pointing etc.)
    """

    desc: str
    cond: Condition
    dim: str = "state"


class _ConditionDescription(NamedTuple):
    label: str
    desc: str
    unhealthy_states: Union[str, None]
    desired: Tuple[str, str]
    state: ConditionState


def _aggregate_string_with_and(line1: str, line2: str) -> str:
    return f"{line1} and,\n{line2}"


def _aggregate_string(line1: str, line2: str) -> str:
    return f"{line1}\n{line2}"


Label = str
Conditions = Dict[Label, Conditional]


class FilteredConditions:
    def __init__(self, conditions: Conditions) -> None:
        """A wrapper of an provided Conditions objects allowing for the get operations
        to be performed on it as if it is a simple list. In addition it also have a
        simple includes method for checking if an label is contained in the conditions.
        This allows for an more human readable way for expressing a predicate e.g.

        .. code-block:: python

            # returns an FilteredConditions object
            wrong_conditions = telescope_readiness.get_wrong_conditions()

            # dish_masters_state is of type ConditionLabel
            if wrong_conditions.includes(dish_masters_state):
                rectify_wrong_dish_master_states(wrong_conditions[dish_masters_state])

        :param conditions: [description]
        """
        self.conditions = conditions

    def includes(self, condition_label: ConditionLabel) -> bool:
        """Whether or not a particular condition, identified by its label, is included
        in the returned data.

        :param condition_label: The label that identifies the particular
            condition being interested in

        :return: [description]
        """
        label = condition_label.label
        return label in self.conditions.keys()

    def __getitem__(self, condition_label: ConditionLabel) -> Condition:
        label = condition_label.label
        return self.conditions[label].cond

    def get(self, condition_label: ConditionLabel) -> Union[Condition, None]:
        """Gets the condition as identified by a unique label given.

        :param condition_label: The label that identifies the particular
            condition being interested in

        :returns: The Condition if it was found, otherwise None
        """
        label = condition_label.label
        conditional = self.conditions.get(label)
        return conditional.cond if conditional else None


class MalformedException(Exception):
    """
    Exception indicating a Readiness condition has not been constructed in the proper
    sequence
    """


class Readiness:
    """An aggregate of multiple conditions in the form of a dictionary like object in
    order to create an object indicating the 'readiness' of something based on a set of
    conditions being 'ok'. A readiness object is build by means of a sequence of methods
    called on it as a form of specifying what conditions will be deemed necessary to be
    ok before determining the state as 'ready'. Setting a particular condition requires
    a :py:class:`ConditionLabel` to be used as a key for getting the condition results
    similar to using a `dict` object. A useful way for doing this is to create a
    namespace object with predefined labels to be used on a particular readiness
    evaluation. The :py:class:`ConditionLabels` class can be used as a base for setting
    your customized labels. The second dependency is a means to fetch the desired state
    values in the result of a
    :py:class:`~ska_ser_skallop.mvp_control.describing.inspections.ListInspection` or
    :py:class:`~ska_ser_skallop.mvp_control.describing.inspections.ListInspections`
    object (refer to :py:mod:`~ska_ser_skallop.mvp_control.describing.mvp_states`) for a
    list of ready made fetcher functions that you can use. Lastly each condition needs
    to come with the desired values expected in order for it to be deemed in the
    condition ok. The example below illustrates the concept:

    .. code-block:: python

        # an object of type ConditionLabels used to contain a particular set of labels
        tel_labels = TelescopeConditionLabels()
        telescope_ready = Readiness(tel_labels)
        fetch_master_states = mvp_states.get_master_states
        telescope_ready.expect(
            *tel_labels.masters_health
        ).state(fetch_master_states).to_be('OFF','STANDBY')

    Note that the Readiness object does not realise the fetching of state values until
    it's condition is requested as part of a logical expression e.g.:

    .. code-block:: python

        # data is fetched as part of a __bool__ method being called on it
        if not telescope_ready:
            handle_telescope_not_ready(telescope_ready)

        # or a more explicit evaluation

        if not telescope_ready.update():
            handle_telescope_not_ready(telescope_ready)

    """

    conditions: Dict[str, Conditional]

    def __init__(self, *labels: Union[Set[str], ConditionLabels]) -> None:
        self.conditions: Dict[Label, Conditional] = {}
        self._constructed_expection_description = ""
        self._constructed_expection_getter = None
        self._constructed_expectation_label = ""
        self._constructed_expectation_dim = "state"
        self.labels = {"general"}
        for label_set in labels:
            label_set = (
                label_set.as_labels() if isinstance(label_set, ConditionLabels) else label_set
            )
            self.labels = {*label_set, *self.labels}

    def _get_ignored_conditions(self) -> List[Conditional]:
        return [conditional for conditional in self.conditions.values() if conditional.cond.ignored]

    def get_not_ok_conditions(self) -> FilteredConditions:
        """Return a subset of conditions (organized as a dictionary with labels as keys)
        which are not ok.

        :return: A dictionary of conditions not ok, keyed with the same
            labels as for Readiness
        """
        return FilteredConditions(
            {k: v for k, v in self.conditions.items() if v.cond.state is not ConditionState.OK}
        )

    def get_inconsistent_conditions(self) -> FilteredConditions:
        """Return a subset of conditions (organized as a dictionary with labels as keys)
        which are inconsistent.

        :return: A dictionary of inconsistent conditions, keyed with the
            same labels as for Readiness
        """
        return FilteredConditions(
            {
                k: v
                for k, v in self.conditions.items()
                if v.cond.state == ConditionState.INCONSISTENT
            }
        )

    def get_wrong_conditions(self) -> FilteredConditions:
        """Return a subset of conditions (organized as a dictionary with labels as keys)
        which are wrong.

        :return: A dictionary of wrong conditions, keyed with the same
            labels as for Readiness
        """
        return FilteredConditions(
            {k: v for k, v in self.conditions.items() if v.cond.state == ConditionState.WRONG}
        )

    def __getitem__(self, condition_label: ConditionLabel) -> Condition:
        return self.conditions[condition_label.label].cond

    def get(self, condition_label: ConditionLabel) -> Union[Condition, None]:
        """return a specific condition as per given label and None if it doesn't exist

        :param condition_label: The Label that identifies the condition

        :return: The requested condition, None if it doesn't exist
        """
        conditional = self.conditions.get(condition_label.label)
        label = conditional.cond if conditional else None
        return label

    def update(self) -> bool:
        """Evaluates the overall readiness by calling a fetch and logical comparison on
        the returned data for each contained condition making up the object.

        :return: whether the object is ready or not
        """
        return all(conditional.cond for conditional in self.conditions.values())

    @property
    def aggregate_results(self) -> ListInspections:
        """A property representing the combination of the results for conditions on the
        same dimension. Each dimension contains the states of devices as an
        :py:class:`~.inspections.ListInspection` object containing the results of
        separate conditions (but same dimensions). For example if the object contains
        two separate conditions, A containing data1 and B containing data2, both in the
        dimension 'state', then it will combine them into a single ListInspection set =
        data1 + data2. Each separate dimension gets combined seperately to form a
        :py:class:`~.inspections.ListInspections` object keyed by the particular
        dimension upon which they were combined.

        For example:

        .. code-block:: python

            # assume the dimension 'state' exists
            device_states = readiness.aggregate_results['state']
            if any(device_states.are_in_state('OFF','STANDBY')):
                switch_on(device_states.in_state('OFF','STANDBY'))

            # assume the dimension 'obsstate' exists
            device_obs_states = readiness.aggregate_results['obsstate']
            if any(device_obs_states.are_in_state('IDLE')):
                release_resources(device_states.in_state('IDLE'))

        :return: A dictionary keyed by the existing dimensions labeled on
            the conditions with values combined across conditions for each dimension
        """
        dimensions = {cnd.dim for cnd in self.conditions.values()}
        aggregate_results = ListInspections({})
        for dim in dimensions:
            filtered_results = [cnd.cond.data for cnd in self.conditions.values() if cnd.dim == dim]
            aggregate_results[dim] = reduce(lambda x, y: x + y, filtered_results)
        return aggregate_results

    @property
    def aggregate_condition(
        self,
    ) -> Dict[str, Union[List[DevState], DevState]]:
        """A derived property from :py:func:`~Readiness.aggregate_results` returning only
        the aggregate values for the state values for each
        :py:class:`~.inspections.ListInspection` object in a given dimension.
        This allows for a quick 'inspection' on the results across a given dimension
        with the assumption that all states on a particular dimension needs to be the
        same e.g.:

        .. code-block:: python

            if 'FAULT' in readiness.aggregate_condition['state']:
                rectify_faulty(readiness.select('state','FAULTY'))


        :return: A dictionary keyed by the name of a dimension and
            valued by a set or (single valued string) of device states
        """
        return {
            dimension: inspection.value for dimension, inspection in self.aggregate_results.items()
        }

    def filter_by(self, dimension: str) -> ListInspection:
        """Return a list of devices and their states as a
        :py:class:`~.inspections.ListInspection` object that are from a given dimension

        :param dimension: The specific dimension upon which the grouping must take place
            (e.g dimension = 'state')

        :return: The results in the form of devices and their current state
            values combined into a ListInspection object.
        """
        return self.aggregate_results[dimension]

    def select(self, dimension: str, *value: str) -> DevList:
        r"""return a list of devices (empty if none) that are according to a given
        dimension and having a given value.

        :param dimension: the dimension over which the selection should take place
            \*value (Tuple[str]): a list of possible values for selecting a device

        :return: [description]
        """
        return self.filter_by(dimension).in_state(*value)

    def _set_labels(self, labels: Set[str]) -> None:
        self.labels = {*labels, "general"}

    def _load_condition(
        self,
        desc: str,
        condition: Condition,
        label: str = "general",
        dim: str = "state",
    ) -> None:
        assert label in self.labels, f"unrecognized label {label} can only be one of {self.labels}"
        assert label not in self.conditions, "condition of type {label} already loaded"
        self.conditions[label] = Conditional(desc, condition, dim)

    def __bool__(self) -> bool:
        re_tries = 6
        wait_period = 0.5
        is_ready = False
        unreadiness_message = ""
        for attempt in range(re_tries):
            is_ready = all(conditional.cond for conditional in self.conditions.values())
            if attempt == 0:
                if ignored_list := self._get_ignored_conditions():
                    logger.warning(
                        "The following conditions have been ignored as a consequence of filtering:"
                        f"\n{[ignored.desc for ignored in ignored_list]}"
                    )
                if is_ready:
                    return True
            elif is_ready:
                logger.warning(
                    "the readiness of this condition was transitory: it changed"
                    " from being not ready to being ready over a period of"
                    f" {attempt*wait_period} second/s."
                    " This may have been caused by a racing condition caused by a previous test"
                    " not properly waiting before starting with current one."
                    f" Original unreadiness: \n {unreadiness_message}"
                )
                return True
            unreadiness_message = self.describe_why_not_ready
            time.sleep(wait_period)
        return is_ready

    def expect(self, description: str, label: str = "general", dim: str = "state") -> "Readiness":
        """Initialise the construction of a required condition by setting the name of
        the label and dimension that will be used to identify the condition with. A
        common way is to use a derived :py:class:`ConditionLabels` object with a
        predefined attribute as a label (Note the use of the asterisk to unpack both the
        description, label and dimension needed as arguments) e.g:

        .. code-block:: python

            telescope_ready.expect(*tel_labels.masters_health)

        The expect method must be followed by the :py:func:`~Readiness.state` method.
        Note the result will return the same object allowing to chain the construction
        of an condition into a single statement.

        :param description: a human readable description of the meaning of the
            condition is
        :param label: a label by which the particular condition will be
            identified. Defaults to "general".
            dim (str, optional): a dimension by which the particular condition will be
            tagged and grouped with potential others. Defaults to "state".

        :return: the same object so as to form a chain of steps
        """
        self._constructed_expection_description = description
        self._constructed_expectation_label = label
        self._constructed_expectation_dim = dim
        return self

    def describe_why_not_ready(self) -> str:
        """Generates a human readable description of why the object have been evaluated
        as not ready. This could be used in the generation of exception messages.

        :return: A description combining the descriptions of all the conditions that
            were not ok into a single paragraph.
        """
        description = ""
        descriptions = [
            _ConditionDescription(
                label,
                conditional.desc,
                conditional.cond.describe_unhealthy_state(),
                conditional.cond.desired_value,
                conditional.cond.state,
            )
            for label, conditional in self.conditions.items()
            if conditional.cond.describe_unhealthy_state()
        ]
        descriptions = [
            f"{item.label} - {item.state.name} state: Expected {item.desc} to be "
            f"{item.desired} but instead found{item.unhealthy_states}"
            for item in descriptions
        ]
        if descriptions:
            return reduce(_aggregate_string, descriptions)
        return description

    def state(
        self,
        get_function: Callable[..., Union[ListInspection, ListInspections]],
        *get_args: Any,
    ) -> "Readiness":
        """The second step (typically chained) in constructing a condition by defining
        the function to be used to collect the state results and return them as a
        :py:class:`~.inspections.ListInspection`. Refer to
        :py:mod:`~ska_ser_skallop.mvp_control.describing.mvp_states`) for a list of
        ready made functions that could be used to get state values from a particular
        group of devices. This step should be preceded by the
        :py:func:`~Readiness.expect` method and succeeded by the
        :py:func:`~Readiness.to_be` method. The results will return the same object
        allowing for the chaining of the steps into a single statement.

        :return: the same object so as to form a chain of steps

        :raises MalformedException: if this step has not been preceded by a
            :py:func:`~Readiness.expect` method

        """
        if not self._constructed_expection_description:
            raise MalformedException("malformed construction, first call expect")
        self._constructed_expection_getter = Getter(get_function, *get_args)
        return self

    def to_be(self, *desired_values: str):
        """The final step in constructing a condition by providing the particular
        desired value/s needed for the condition to be deemed ok. This step must be
        preceded by the :py:func:`~Readiness.state` method

        :raises MalformedException: if this step has not been preceded by a
            :py:func:`~Readiness.state` method
        """
        desc = self._constructed_expection_description
        getter = self._constructed_expection_getter
        label = self._constructed_expectation_label
        dim = self._constructed_expectation_dim
        if not getter:
            raise MalformedException("malformed construction, first call state and or expect")
        self._load_condition(desc, Condition(getter, *desired_values), label, dim)

    def to_not_be(self, *desired_values: str):
        desc = self._constructed_expection_description
        getter = self._constructed_expection_getter
        label = self._constructed_expectation_label
        dim = self._constructed_expectation_dim
        if not getter:
            raise Exception("malformed construction, first call state and or expect")
        self._load_condition(
            desc,
            Condition(getter, *desired_values, desired_equal=False),
            label,
            dim,
        )


class NotReady(Exception):
    def __init__(self, message: str, result: Readiness) -> None:
        self.result = result
        super().__init__(message)
