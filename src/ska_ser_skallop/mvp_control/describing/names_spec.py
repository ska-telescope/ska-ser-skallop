from typing import Dict, Set, Union

from . import mvp_names as names

mid = names.Mid()
ska_mid: Dict[names.DeviceName, Dict[str, Union[str, Set[str]]]] = {
    mid.tm.central_node: {
        "name": "ska_mid/tm_central/central_node",
        "tags": {"tmc scope", "master domain", "tm"},
    },
    mid.tm.csp_leaf_node: {
        "name": "ska_mid/tm_leaf_node/csp_master",
        "tags": {
            "master domain",
            "leaf nodes",
            "tmc scope",
            "csp control",
            "tm",
        },
    },
    mid.tm.sdp_leaf_node: {
        "name": "ska_mid/tm_leaf_node/sdp_master",
        "tags": {
            "master domain",
            "leaf nodes",
            "tmc scope",
            "sdp control",
            "tm",
        },
    },
    mid.tm.subarray(1): {
        "name": "ska_mid/tm_subarray_node/1",
        "tags": {"tmc scope", "subarrays", "tm"},
    },
    mid.tm.subarray(1).sdp_leaf_node: {
        "name": "ska_mid/tm_leaf_node/sdp_subarray01",
        "tags": {"sdp domain", "subarrays", "leaf nodes", "tmc scope", "tm"},
    },
    mid.tm.subarray(1).csp_leaf_node: {
        "name": "ska_mid/tm_leaf_node/csp_subarray01",
        "tags": {"subarrays", "tm", "leaf nodes", "tmc scope", "csp domain"},
    },
    mid.tm.subarray(2): {
        "name": "ska_mid/tm_subarray_node/2",
        "tags": {"tmc scope", "subarrays", "tm"},
    },
    mid.tm.subarray(2).sdp_leaf_node: {
        "name": "ska_mid/tm_leaf_node/sdp_subarray02",
        "tags": {"sdp domain", "subarrays", "leaf nodes", "tmc scope", "tm"},
    },
    mid.tm.subarray(2).csp_leaf_node: {
        "name": "ska_mid/tm_leaf_node/csp_subarray02",
        "tags": {"subarrays", "tm", "leaf nodes", "tmc scope", "csp domain"},
    },
    mid.tm.subarray(3): {
        "name": "ska_mid/tm_subarray_node/3",
        "tags": {"tmc scope", "subarrays", "tm"},
    },
    mid.tm.subarray(3).sdp_leaf_node: {
        "name": "ska_mid/tm_leaf_node/sdp_subarray03",
        "tags": {"sdp domain", "subarrays", "leaf nodes", "tmc scope", "tm"},
    },
    mid.tm.subarray(3).csp_leaf_node: {
        "name": "ska_mid/tm_leaf_node/csp_subarray03",
        "tags": {"subarrays", "tm", "leaf nodes", "tmc scope", "csp domain"},
    },
    mid.csp.controller: {
        "name": "mid-csp/control/0",
        "tags": {"master domain", "csp controller", "csp", "csp scope"},
    },
    mid.csp.subarray(1): {
        "name": "mid-csp/subarray/01",
        "tags": {"csp", "csp scope"},
    },
    mid.csp.cbf.controller: {
        "name": "mid_csp_cbf/sub_elt/controller",
        "tags": {"cbf", "cbf scope", "csp scope"},
    },
    mid.csp.cbf.fsp(1): {
        "name": "mid_csp_cbf/fsp/01",
        "tags": {"fsp", "cbf scope", "csp scope"},
    },
    mid.csp.cbf.fsp(1).correlator: {
        "name": "mid_csp_cbf/fsp_corr/01",
        "tags": {"cbf domain", "cbf scope", "correlator", "fsp", "csp domain"},
    },
    mid.csp.cbf.fsp(1).correlator.subarray(1): {
        "name": "mid_csp_cbf/fspcorrsubarray/01_01",
        "tags": {
            "cbf domain",
            "fsp domain",
            "cbf scope",
            "correlator",
            "fsp",
            "csp domain",
        },
    },
    mid.csp.cbf.fsp(1).correlator.subarray(2): {
        "name": "mid_csp_cbf/fspcorrsubarray/01_02",
        "tags": {
            "cbf domain",
            "fsp domain",
            "cbf scope",
            "correlator",
            "fsp",
            "csp domain",
        },
    },
    mid.csp.cbf.fsp(1).correlator.subarray(3): {
        "name": "mid_csp_cbf/fspcorrsubarray/01_03",
        "tags": {
            "cbf domain",
            "fsp domain",
            "cbf scope",
            "correlator",
            "fsp",
            "csp domain",
        },
    },
    mid.csp.cbf.fsp(2): {
        "name": "mid_csp_cbf/fsp/02",
        "tags": {"fsp", "cbf scope", "csp scope"},
    },
    mid.csp.cbf.fsp(2).correlator: {
        "name": "mid_csp_cbf/fsp_corr/02",
        "tags": {"cbf domain", "cbf scope", "correlator", "fsp", "csp domain"},
    },
    mid.csp.cbf.fsp(2).correlator.subarray(1): {
        "name": "mid_csp_cbf/fspcorrsubarray/02_01",
        "tags": {
            "cbf domain",
            "fsp domain",
            "cbf scope",
            "correlator",
            "fsp",
            "csp domain",
        },
    },
    mid.csp.cbf.fsp(2).correlator.subarray(2): {
        "name": "mid_csp_cbf/fspcorrsubarray/02_02",
        "tags": {
            "cbf domain",
            "fsp domain",
            "cbf scope",
            "correlator",
            "fsp",
            "csp domain",
        },
    },
    mid.csp.cbf.fsp(2).correlator.subarray(3): {
        "name": "mid_csp_cbf/fspcorrsubarray/02_03",
        "tags": {
            "cbf domain",
            "fsp domain",
            "cbf scope",
            "correlator",
            "fsp",
            "csp domain",
        },
    },
    mid.csp.cbf.fsp(1).pulsar_search: {
        "name": "mid_csp_cbf/fsp_pss/01",
        "tags": {
            "pulsar search",
            "cbf domain",
            "cbf scope",
            "fsp",
            "csp domain",
        },
    },
    mid.csp.cbf.fsp(1).pulsar_search.subarray(1): {
        "name": "mid_csp_cbf/fsppsssubarray/01_01",
        "tags": {
            "pulsar search",
            "cbf domain",
            "fsp domain",
            "cbf scope",
            "fsp",
            "csp domain",
        },
    },
    mid.csp.cbf.fsp(1).pulsar_search.subarray(2): {
        "name": "mid_csp_cbf/fsppsssubarray/01_02",
        "tags": {
            "pulsar search",
            "cbf domain",
            "fsp domain",
            "cbf scope",
            "fsp",
            "csp domain",
        },
    },
    mid.csp.cbf.fsp(1).pulsar_search.subarray(3): {
        "name": "mid_csp_cbf/fsppsssubarray/01_03",
        "tags": {
            "pulsar search",
            "cbf domain",
            "fsp domain",
            "cbf scope",
            "fsp",
            "csp domain",
        },
    },
    mid.csp.cbf.fsp(2).pulsar_search: {
        "name": "mid_csp_cbf/fsp_pss/02",
        "tags": {
            "pulsar search",
            "cbf domain",
            "cbf scope",
            "fsp",
            "csp domain",
        },
    },
    mid.csp.cbf.fsp(2).pulsar_search.subarray(1): {
        "name": "mid_csp_cbf/fsppsssubarray/02_01",
        "tags": {
            "pulsar search",
            "cbf domain",
            "fsp domain",
            "cbf scope",
            "fsp",
            "csp domain",
        },
    },
    mid.csp.cbf.fsp(2).pulsar_search.subarray(2): {
        "name": "mid_csp_cbf/fsppsssubarray/02_02",
        "tags": {
            "pulsar search",
            "cbf domain",
            "fsp domain",
            "cbf scope",
            "fsp",
            "csp domain",
        },
    },
    mid.csp.cbf.fsp(2).pulsar_search.subarray(3): {
        "name": "mid_csp_cbf/fsppsssubarray/02_03",
        "tags": {
            "pulsar search",
            "cbf domain",
            "fsp domain",
            "cbf scope",
            "fsp",
            "csp domain",
        },
    },
    mid.csp.cbf.fsp(1).vlbi(1): {
        "name": "mid_csp_cbf/fsp_pst/01",
        "tags": {"fsp", "cbf scope", "csp scope"},
    },
    mid.csp.cbf.fsp(1).vlbi(2): {
        "name": "mid_csp_cbf/fsp_pst/02",
        "tags": {"fsp", "cbf scope", "csp scope"},
    },
    mid.csp.cbf.fsp(2).vlbi(1): {
        "name": "mid_csp_cbf/fsp_pst/01",
        "tags": {"fsp", "cbf scope", "csp scope"},
    },
    mid.csp.cbf.fsp(2).vlbi(2): {
        "name": "mid_csp_cbf/fsp_pst/02",
        "tags": {"fsp", "cbf scope", "csp scope"},
    },
    mid.csp.cbf.fsp(1).pulsar_timing(1): {
        "name": "mid_csp_cbf/fsp_pst/01",
        "tags": {"fsp", "cbf scope", "csp scope"},
    },
    mid.csp.cbf.fsp(1).pulsar_timing(2): {
        "name": "mid_csp_cbf/fsp_pst/02",
        "tags": {"fsp", "cbf scope", "csp scope"},
    },
    mid.csp.cbf.fsp(2).pulsar_timing(1): {
        "name": "mid_csp_cbf/fsp_pst/01",
        "tags": {"fsp", "cbf scope", "csp scope"},
    },
    mid.csp.cbf.fsp(2).pulsar_timing(2): {
        "name": "mid_csp_cbf/fsp_pst/02",
        "tags": {"fsp", "cbf scope", "csp scope"},
    },
    mid.csp.cbf.vcc(1).band(1): {
        "name": "mid_csp_cbf/vcc_band12/001",
        "tags": {
            "sensor domain",
            "cbf",
            "cbf scope",
            "csp scope",
            "vcc",
            "csp",
        },
    },
    mid.csp.cbf.vcc(1).band(2): {
        "name": "mid_csp_cbf/vcc_band3/001",
        "tags": {
            "sensor domain",
            "cbf",
            "cbf scope",
            "csp scope",
            "vcc",
            "csp",
        },
    },
    mid.csp.cbf.vcc(1).band(3): {
        "name": "mid_csp_cbf/vcc_band4/001",
        "tags": {
            "sensor domain",
            "cbf",
            "cbf scope",
            "csp scope",
            "vcc",
            "csp",
        },
    },
    mid.csp.cbf.vcc(1).band(4): {
        "name": "mid_csp_cbf/vcc_band5/001",
        "tags": {
            "sensor domain",
            "cbf",
            "cbf scope",
            "csp scope",
            "vcc",
            "csp",
        },
    },
    mid.csp.cbf.vcc(1).sw(1): {
        "name": "mid_csp_cbf/vcc_sw1/001",
        "tags": {
            "sensor domain",
            "cbf",
            "cbf scope",
            "csp scope",
            "vcc",
            "csp",
        },
    },
    mid.csp.cbf.vcc(1).sw(2): {
        "name": "mid_csp_cbf/vcc_sw2/001",
        "tags": {
            "sensor domain",
            "cbf",
            "cbf scope",
            "csp scope",
            "vcc",
            "csp",
        },
    },
    mid.csp.cbf.vcc(2).band(1): {
        "name": "mid_csp_cbf/vcc_band12/002",
        "tags": {
            "csp scope",
            "cbf",
            "cbf scope",
            "sensor domain",
            "vcc",
            "csp",
        },
    },
    mid.csp.cbf.vcc(2).band(2): {
        "name": "mid_csp_cbf/vcc_band3/002",
        "tags": {
            "csp scope",
            "cbf",
            "cbf scope",
            "sensor domain",
            "vcc",
            "csp",
        },
    },
    mid.csp.cbf.vcc(2).band(3): {
        "name": "mid_csp_cbf/vcc_band4/002",
        "tags": {
            "csp scope",
            "cbf",
            "cbf scope",
            "sensor domain",
            "vcc",
            "csp",
        },
    },
    mid.csp.cbf.vcc(2).band(4): {
        "name": "mid_csp_cbf/vcc_band5/002",
        "tags": {
            "csp scope",
            "cbf",
            "cbf scope",
            "sensor domain",
            "vcc",
            "csp",
        },
    },
    mid.csp.cbf.vcc(2).sw(1): {
        "name": "mid_csp_cbf/vcc_sw1/002",
        "tags": {
            "csp scope",
            "cbf",
            "cbf scope",
            "sensor domain",
            "vcc",
            "csp",
        },
    },
    mid.csp.cbf.vcc(2).sw(2): {
        "name": "mid_csp_cbf/vcc_sw2/002",
        "tags": {
            "csp scope",
            "cbf",
            "cbf scope",
            "sensor domain",
            "vcc",
            "csp",
        },
    },
}

low = names.Low()
ska_low: Dict[names.DeviceName, Dict[str, Union[str, Set[str]]]] = {
    low.tm.central_node: {
        "name": "ska_low/tm_central/central_node",
        "tags": {"master domain", "tm", "tmc scope"},
    },
    low.tm.subarray(1): {
        "name": "ska_low/tm_subarray_node/1",
        "tags": {"tmc scope", "subarrays", "tm"},
    },
    low.tm.subarray(1).mccs_leaf_node: {
        "name": "ska_low/tm_leaf_node/mccs_subarray01",
        "tags": {"tmc scope", "tm", "mccs domain", "leaf nodes", "subarrays"},
    },
    low.mccs.station(1): {
        "name": "low-mccs/station/001",
        "tags": {"mccs", "mccs scope", "stations"},
    },
    low.mccs.station(2): {
        "name": "low-mccs/station/002",
        "tags": {"mccs", "mccs scope", "stations"},
    },
    low.mccs.tile(1): {
        "name": "low-mccs/tile/0001",
        "tags": {"tiles", "mccs", "mccs scope"},
    },
    low.mccs.tile(2): {
        "name": "low-mccs/tile/0002",
        "tags": {"tiles", "mccs", "mccs scope"},
    },
    low.mccs.tile(3): {
        "name": "low-mccs/tile/0003",
        "tags": {"tiles", "mccs", "mccs scope"},
    },
    low.mccs.antenna(1): {
        "name": "low-mccs/antenna/000001",
        "tags": {"mccs", "antennae", "mccs scope"},
    },
    low.mccs.antenna(2): {
        "name": "low-mccs/antenna/000002",
        "tags": {"mccs", "antennae", "mccs scope"},
    },
    low.mccs.antenna(3): {
        "name": "low-mccs/antenna/000003",
        "tags": {"mccs", "antennae", "mccs scope"},
    },
    low.mccs.apiu(2): {
        "name": "low-mccs/apiu/002",
        "tags": {"mccs", "mccs scope", "apius"},
    },
    low.mccs.beam(1): {
        "name": "low-mccs/beam/001",
        "tags": {"mccs", "mccs scope", "beams"},
    },
    low.mccs.beam(2): {
        "name": "low-mccs/beam/002",
        "tags": {"mccs", "mccs scope", "beams"},
    },
    low.mccs.beam(3): {
        "name": "low-mccs/beam/003",
        "tags": {"mccs", "mccs scope", "beams"},
    },
    low.mccs.subarraybeam(1): {
        "name": "low-mccs/subarraybeam/01",
        "tags": {"subarraybeams", "mccs", "mccs scope"},
    },
    low.mccs.subarraybeam(2): {
        "name": "low-mccs/subarraybeam/02",
        "tags": {"subarraybeams", "mccs", "mccs scope"},
    },
    low.mccs.subarraybeam(3): {
        "name": "low-mccs/subarraybeam/03",
        "tags": {"subarraybeams", "mccs", "mccs scope"},
    },
    low.mccs.subrack(1): {
        "name": "low-mccs/subrack/01",
        "tags": {"mccs", "subracks", "mccs scope"},
    },
    low.sdp.master: {
        "name": "low-sdp/control/0",
        "tags": {"master domain", "sdp scope", "sdp"},
    },
    low.sdp.subarray(1): {
        "name": "low-sdp/subarray/01",
        "tags": {"sdp domain", "sdp scope", "sdp"},
    },
    low.sdp.subarray(2): {
        "name": "low-sdp/subarray/02",
        "tags": {"sdp domain", "sdp scope", "sdp"},
    },
    low.sdp.subarray(3): {
        "name": "low-sdp/subarray/03",
        "tags": {"sdp domain", "sdp scope", "sdp"},
    },
}
