"""A set of base objects populated with state from multiple devices. It comes with logic
to interpet the collective states of these devices, thus providing a means to apply a
domain logic on the system as a whole.
"""

from collections import Counter
from functools import reduce
from typing import Any, Dict, List, NamedTuple, Union

from ska_ser_skallop.mvp_control.describing.mvp_names import DomainList
from ska_ser_skallop.utils.generic_classes import Symbol

DevName = str
DevState = str


DevStates = Dict[DevName, DevState]


class DevicesInState(NamedTuple):
    count: int  # type: ignore
    items: List[DevName]


Grouping = Dict[DevState, DevicesInState]


class DevList(list[str]):
    """A derivation from the standard list in that it is composed out of tango devices
    and have additional logical operations in the context of manipulating list of tango
    devices.
    """

    def __init__(self, *items: Union[str, List[str]]) -> None:
        the_list: list[str] = []
        for item in items:
            the_list = [*the_list, *item] if isinstance(item, list) else [*the_list, item]
        super().__init__(the_list)

    def __sub__(self, operand: List[str]) -> "DevList":
        """allows for one list of tango devices to be subtracted (as a set operation)
        from a given operand list list. e.g:

        .. code-block:: python

            new_list = list_a - list_b

        The result (`new_list`) will be a mutually exclusive set of elements in list_a.
        In other words only those elements in list_a that are not in list_b

        :param operand: the operand list containing the elements to be removed
            if they exist

        :return: the result of the subtraction as the remainder if all the elements
            in other were removed from it
        """
        return DevList([item for item in self if item not in operand])

    def __add__(self, operand: Union[List[str], "DevList"]) -> "DevList":
        """the aggregation of one list with another by adding elements to the first
        operand that are only found in the second operand.
        (Common elements are ignored). E.g.:

        .. code-block:: python

            new_list = list_a + list_b


        :param operand: the list which must be aggregated with the first

        :return: The result of the operation as a list of all the elements in the
            first and other list
        """
        operand = operand if isinstance(operand, DevList) else DevList(operand)
        return DevList([*self, *(operand - self)])

    def filter_out(self, domain: DomainList, *tags: str) -> "DevList":
        """
        Remove elements that belong to a given `DomainList` and have certain tags.

        For example,

        .. code-block:: python

            subarray_devices = SubArrays(1)
            # e.g [
            #         'ska_mid/tm_subarray_node/1',
            #         'ska_mid/tm_leaf_node/csp_subarray01',
            #         ...
            #     ]
            faulty_subarray_devices_excluding_csp = faulty_devices.filter_out(
                subbarray_devices, 'csp'
            )

        :param domain: any type of `DomainList` object set to collectively talk about a
            related set of devices (e.g. "all the subarray devices" will be designated
            by the `SubArrays()` object.)

        :return: the result of the operation which will be a list of devices without
            those indicated by the DomainList and corresponding tags.
        """
        selector = domain.filter(*tags).list
        return self.__sub__(selector)

    def select(self, domain: DomainList, *tags: str) -> "DevList":
        """
        Remove elements that don't belong to a given list and have certain tags.

        :param domain: any type of `DomainList` object set to collectively talk about a
            related set of devices (e.g. "all the subarray devices" will be designated
            by the `SubArrays()` object.)

        :return: the subset of devices selected
        """
        selector = domain.filter(*tags).list
        return DevList([item for item in self if item in selector])


class Inspection:
    """The base element of a particular state of a device on a particular attribute,
    that is used to perform certain basic logical inspections on (e.g. is in state "ON")
    """

    def __init__(self, name: Union[str, Symbol], val: str) -> None:
        self._val = val
        if isinstance(name, Symbol):
            self._name = name.__str__()
        else:
            self._name = name

    def is_in_state(self, *state: str) -> bool:
        """Whether the value of the state for that element equals any of the given
        values in the state argument.
        E.g.:

        .. code-block:: python

            device.is_in_state('ON','OFF') # true/false

        :return: the boolean result indicating if the device is in any of the given
            states
        """
        return self._val in state

    def in_state(self, *state: str) -> Dict[str, str]:
        r"""returns a dictionary of devices and their state values that are in any of
        the given state values specified by the \*state argument.

        :return: A dictionary of the device name (key) and the state value (value)
            equal to the given state values in the arguments
        """
        return {self._name: self._val} if self._val in state else {}

    def not_in_state(self, *state: str) -> Dict[str, str]:
        r"""returns a dictionary of devices and their state values that are **not** in
        any of the given state values specified by the \*state argument.

        :return: A dictionary of the device name (key) and the state value (value) not
            equal to the given state values in the arguments
        """
        return {self._name: self._val} if self._val not in state else {}

    def _generate_faulty_message(self, *state: str) -> list[str]:
        faulty = self.not_in_state(*state)
        return [
            f"expected {device} to be in {state} but instead was {actual}"
            for device, actual in faulty.items()
        ]

    def as_dict(self) -> Dict[DevName, DevState]:
        return {self._name: self._val}


class ListInspection:
    """Similar to Inspection except except the unit of analysis is now a list of
    elements containing the state of an an attribute for a set of devices, each having
    that attribute.
    """

    def __init__(self, the_list: Dict[DevName, DevState]) -> None:
        self.list = the_list

    def are_all_the_same(self) -> bool:
        """Whether the states of all the the devices are exactly the same

        :return: The boolean result of the operation
        """
        for value in self.list.values():
            if any(self.are_not_in_state(value)):
                return False
        return True

    @property
    def value(self) -> Union[List[DevState], DevState]:
        """The aggregate value of all the devices in the list (as an property type) if
        they were "rolled up" into a set of unique values. In other words if all the
        devices were in state "ON" the result would be simply "ON". If there were some
        elements who were also "OFF" the result would be ["ON", "OFF"]

        :return: Returns either a single value (if all
            devices were the same state) or a list representing the common set of states
            found in the list.
        """
        value = list(self.get_grouping().keys())
        if len(value) == 1:
            return value[0]
        return value

    @property
    def devices(self) -> DevList:
        """
        A property representing the devices in the object without their state values.

        :return: a list of devices
        """
        return DevList(list(self.list.keys()))

    def get_grouping(self) -> Grouping:
        """Generates a grouping of devices based on having a common state value. E.g.
        all devices in the "ON" state.

        :return: The grouping structured as a dictionary keyed on the state value
            with values organized into

                1. the count of devices having that state value
                2. the list of devices having that state value

        """
        counts = Counter(self.list.values())
        grouping = {}
        for state, count in counts.items():
            keys = [key for key in self.list.keys() if self.list[key] == state]
            grouping[state] = DevicesInState(count, keys)
        return grouping

    def are_in_state(self, *state: str) -> List[bool]:
        r"""returns a list of boolean values indicating whether or not a particular
        element are in any of the states given by the \*state argument. In other words
        if all the elements were satisfying the criteria, then an example would result
        into [True, True, True]. This allows for writing logical comaprisons in a more
        human readable way e.g.

        .. code-block:: python

            if any(subarrays_obsstate.are_in_state('OFF','STANDBY')):
                do_someting()
            elif all(subarrays_obsstate.are_in_state('ON')):
                do_someting_else()


        :return: The result of applying the logic on all the constituting
            elements grouped as a list of boolean values.
        """
        return [val in state for val in self.list.values()]

    def in_state(self, *state: str) -> DevList:
        r"""Returns a selection of devices that are in any of the state values given by
        the \*state argument. This allows for a more human readable way of applying
        logic, for example:

        .. code-block:: python

            rectify_faulty(devices.in_state('FAULT'))


        :return: A list of device names (strings) that are in the given state
        """
        return DevList([key for key, val in self.list.items() if val in state])

    def not_in_state(self, *state: str) -> DevList:
        r"""Returns a selection of devices that are not in any of the state values given
        by the \*state argument. This allows for a more human readable way of applying
        logic, for example:

        .. code-block:: python

            switch_on(devices.not_in_state('ON'))


        :return: A list of device names (strings) that are not in the given state
        """
        return DevList([key for key, val in self.list.items() if val not in state])

    def are_not_in_state(self, *state: str) -> List[bool]:
        r"""returns a list of boolean values indicating whether or not a particular
        element are in any of the states given by the \*state argument. In other words
        if all the elements were satisfying the criteria, then an example would result
        into [True, True, True]. This allows for writing logical comaprisons in a more
        human readable way e.g.

        .. code-block:: python

            if any(subarrays_obsstate.are_in_state('OFF','STANDBY')):
                do_someting()
            elif all(subarrays_obsstate.are_in_state('ON')):
                do_someting_else()


        :return: The result of applying the logic on all the constituting
            elements grouped as a list of boolean values.
        """
        return [val not in state for val in self.list.values()]

    def __bool__(self) -> bool:
        """whether or not the device list is empty

        :return: Whether or not the device list is empty
        """
        return self.list != {}

    def select(self, *state: str) -> "ListInspection":
        r"""Returns a new ListInspection for a subset of the current one for which the
        state of the elements are equal to any of those given by \*state
        """
        new_dict = {key: val for key, val in self.list.items() if val in state}
        return ListInspection(new_dict)

    def deselect(self, *state: str) -> "ListInspection":
        r"""Returns a new ListInspection for a subset of the current one for which the
        state of the elements are not equal to any of those given by \*state
        """
        new_dict = {key: val for key, val in self.list.items() if val not in state}
        return ListInspection(new_dict)

    def generate_faulty_message(self, *state: str) -> list[str]:
        r"""Returns a list of messages based on the fact that the states are not equal
        to those given in the arguments.
        This is for use in generating message during exceptions. E.g.:

        .. code-block:: python

            if any(sub_array_devices.are_not_in_state("ON")):
                raise Exception(sub_array_devices.generate_faulty_message("ON"))

        :return: A list of faulty messages for each element not having a state equal to
            those given in the \*state argument
        """
        faulty = self.deselect(*state).list
        return [
            f"expected {device} to be in {state} but instead was {actual}"
            for device, actual in faulty.items()
        ]

    def __add__(self, operand: Union["ListInspection", Dict[DevName, DevState]]):
        """Combines two Listinspections into one aggregate list containing the device
        and device state value for the elements coming from the original two items.
        E.g.:

        .. code-block:: python

            csp_and_sdp_obs_states = sdp_obs_state + csp_obs_state

        :param operand: the other list to be combined

        :return: The result of combining both as a single ListInspection
            containing elements from either
        """
        inspections = ListInspections()
        inspections["1"] = self
        inspections["2"] = operand
        return inspections.flatten()

    def __sub__(self, operand: Union["ListInspection", Dict[DevName, DevState]]):
        """Remove devices (and their state values) from the first operand list that are
        also in the second operand list. E.g:

        .. code-block:: python

            csp_obs_state = csp_and_sdp_obs_states - sdp_obs_state


        :param operand: The second list containing elements that should
            be removed from the first.

        :return: the result after subtracting common elements belonging to both
        """
        first_list = self.list
        if not isinstance(operand, dict):
            # assume it has to be type ListInspection
            second_list = operand.list
        else:
            second_list = operand
        devices_from_second_list = second_list.keys()
        return ListInspection(
            {
                device: device_state
                for device, device_state in first_list
                if device not in devices_from_second_list
            }
        )


class ListInspections(dict[str, ListInspection | ListInspection | Any]):
    """A composite structure of multiple ListInspection objects (or recursively
    ListInspections) that is 'ducked typed' into behaving like a listInspection class.
    I.e. it allows to be handled in the same way as the more simpler ListInspection
    (are_in_state,in_state  etc ) except the multiplicity is implicitly taken into
    account. ListInspections are useful as a container of state values from distinct
    sets of devices that can be inspected seperately as well as collectively. E.g.:

    .. code-block:: python

        tmc_subarray_obs_state = ListInspection({
            'ska_mid/tm_subarray_node/1': 'EMPTY'
        })

        tmc_subarray_leaf_nodes_obs_state = ListInspection({
            'ska_mid/tm_leaf_node/csp_subarray01': 'IDLE',
            'ska_mid/tm_leaf_node/sdp_subarray01': 'IDLE'
        })

        tmc_subarrays_obs_state = ListInspections({
            'subarray_node': tmc_subarray_obs_state,
            'leaf_nodes': tmc_subarray_leaf_nodes_obs_state
        })

        # look at them as a single collection
        # ['ska_mid/tm_leaf_node/csp_subarray01', 'ska_mid/tm_leaf_node/sdp_subarray01']
        tmc_subarrays_obs_state.in_state('IDLE')

        # look at only the subarray node ['ska_mid/tm_subarray_node/1']
        tmc_subarrays_obs_state['subarray_node'].in_state("EMPTY")

        # Note it is also possible to create a ListInspections dictionary with key
        # values as ListInspections types , allowing for a recursive structure

        telescope_subarray_obs_states = ListInspections({
            "tmc_subarrays": tmc_subarrays_obs_state
        })

        telescope_subarray_obs_states["tmc_subarrays"]["subarray_node"].in_state(
            "EMPTY"
        )

    """

    def flatten(self) -> ListInspection:
        inspections = [
            val.list for val in self.values() if isinstance(val, (ListInspection, ListInspections))
        ]
        if inspections:
            return ListInspection(reduce(lambda x, y: {**x, **y}, inspections))
        return ListInspection({})

    @property
    def list(self) -> dict[DevName, DevState]:
        """A property that returns the dictionary as a flat list combining all the keys
        into a single top level

        :return: Dictionary keyed by device name with the state value
        """
        return self.flatten().list

    @property
    def value(self) -> Union[List[DevState], DevState]:
        """A property that returns the aggregate value formed by flattening the devices
        with their values into a single dictionary and taking the set of values for the
        states found in that list. Thus if all the state values are the same the result
        wil be a single element, otherwise a list of the actual values. This is useful
        for performing aggregate logic onto the collection as a whole e.g:

        .. code-block:: python

            if subarrays_obsstate.value == 'IDLE':
                handle_idle()
            elif subarrays_obsstate.value == ['RESOURCING','CONFIGURING']:
                handle_stuck()



        :return: Either a single value (if all devices are the same) or a list of values
        """
        return self.flatten().value

    @property
    def devices(self) -> DevList:
        """A property that returns a list of all the device names contained within this
        object indirectly due to the lists within.

        :return: A list of device names as strings
        """
        return self.flatten().devices

    def are_in_state(self, *state: str) -> List[bool]:
        r"""returns a list of boolean values indicating whether or not a particular
        element (from all the elements combined from the inner lists) are in any of the
        states given by the \*state argument. In other words if all the elements were
        satisfying the criteria, then an example would result into [True, True, True].
        This allows for writing logical comparisons in a more human readable way e.g.

        .. code-block:: python

            if any(subarrays_obsstate.are_in_state('OFF','STANDBY')):
                do_someting()
            elif all(subarrays_obsstate.are_in_state('ON')):
                do_someting_else()

        :return: The result of applying the logic on all the constituting
            elements grouped as a list of boolean values.
        """  # type: ignore
        return self.flatten().are_in_state(*state)

    def in_state(self, *state: str) -> DevList:
        r"""Returns a selection of devices (from all the elements combined from the
        inner lists) that are in any of the state values given by the \*state argument.
        This allows for a more human readable way of applying logic, for example:

        .. code-block:: python

            rectify_faulty(devices.in_state('FAULT'))

        :return: A list of device names (strings) that are in the given state
        """
        return self.flatten().in_state(*state)

    def not_in_state(self, *state: str) -> DevList:
        r"""Returns a selection of devices (from all the elements combined from the
        inner lists) that are not in any of the state values given by the \*state
        argument. This allows for a more human readable way of applying logic, for
        example:

        .. code-block:: python

            switch_on(devices.not_in_state('ON'))

        :return: A list of device names (strings) that are not in the given state
        """
        return self.flatten().not_in_state(*state)

    def are_not_in_state(self, *state: str) -> List[bool]:
        r"""returns a list of boolean values indicating whether or not a particular
        element (from all the elements combined from the inner lists) are **not** in any
        of the states given by the \*state argument. In other words if all the elements
        were satisfying the criteria, then an example would result into
        [True, True, True]. This allows for writing logical comparisons in a more human
        readable way e.g.

        .. code-block:: python

            if any(subarrays_obsstate.are_not_in_state('OFF','STANDBY')):
                do_someting()
            elif all(subarrays_obsstate.are_in_state('OFF','STANDBY')):
                do_someting_else()

        :return: The result of applying the logic on all the constituting
            elements grouped as a list of boolean values.
        """
        return self.flatten().are_not_in_state(*state)

    def __setitem__(
        self,
        k: str,
        v: Union[Dict[str, str], ListInspection, "ListInspections"],
    ) -> None:
        if not isinstance(v, ListInspections) and not isinstance(v, ListInspection):
            if isinstance(v, dict):
                v = ListInspection(v)
        super().__setitem__(k, v)

    def __add__(
        self, operand: Union[Dict[str, str], ListInspection, "ListInspections"]
    ) -> "ListInspections":
        """combine two ListInspections as a binary nested composition of two sub
        ListInspections, the two lists are keyed as a sting index with 1 denoting the
        first operand and 2 the second.

        :param operand: the second list to be combined into the ListInspections

        :return: an aggregated ListInspections
        """
        agg = ListInspections()
        agg["1"] = self
        agg["2"] = operand
        return agg

    def __sub__(
        self, operand: Union[Dict[str, str], ListInspection, "ListInspections"]
    ) -> "ListInspection":
        """Removes elements from the first operand that also belongs to the second
        operand

        :param operand: The second list containing elements with their
            state values to be removed from the first operand

        :return: the result of the operation which is a new ListInspection
            object for which elements belonging to the second list have been removed
        """
        first_inspection = self.flatten()
        second_inspection = operand.flatten() if isinstance(operand, ListInspections) else operand
        return first_inspection - second_inspection

    def are_all_the_same(self) -> bool:
        """Whether or not the state values for all the elements in all the inner lists
        are the same

        :return: the result of the logical check, True = all events are the same
        """
        return self.flatten().are_all_the_same()

    def get_grouping(self) -> Grouping:
        """Generates a grouping of all the inner devices contained in the object
        based on them having a common state value. E.g. all devices in the "ON" state.

        :return: The grouping structured as a dictionary keyed on the state value
            with values organized into

                1. the count of devices having that state value
                2. the list of devices having that state value

        """
        return self.flatten().get_grouping()

    def generate_faulty_message(self, *state: str):
        r"""Returns a list of messages based on the fact that the states are not equal
        to those given in the arguments.
        This is for use in generating message during exceptions. E.g.:

        .. code-block:: python

            if any(sub_array_devices.are_not_in_state("ON")):
                raise Exception(sub_array_devices.generate_faulty_message("ON"))


        :return: A list of faulty messages for each element not having a state equal to
            those given in the \*state argument
        """
        faulty = self.flatten().deselect(*state).list
        return [
            f"expected {device} to be in {state} but instead was {actual}"
            for device, actual in faulty.items()
        ]
