import enum
from typing import Any, List, Union, cast

from ska_ser_skallop.connectors import configuration
from ska_ser_skallop.mvp_control.describing import mvp_names as names
from ska_ser_skallop.mvp_control.describing.inspections import (
    Inspection,
    ListInspection,
    ListInspections,
)
from ska_ser_skallop.mvp_control.describing.mvp_names import Low, Mid
from ska_ser_skallop.utils import env
from ska_ser_skallop.utils.generic_classes import Symbol

# from typing import TypedDict only if python 3.8


def _get_devices_data(devices: List[str], attr: str):
    if devices:
        fetcher = configuration.get_devices_query()
        return fetcher.query(devices, attr)
    return cast(dict[str, Any], {})


def _get_attr_val_from_a_device(device_name: Union[str, Symbol], attr: str):
    proxy = configuration.get_device_proxy(device_name)
    value = proxy.read_attribute(attr).value
    if isinstance(value, enum.Enum):
        return value.name
    if isinstance(value, str):
        return value
    return str(value)


class MasterStates(ListInspection):
    pass


def get_master_states() -> MasterStates:
    data = _get_devices_data(names.Masters().filter(*names.SCOPE).list, "state")
    return MasterStates(data)


def get_csp_sdp_master_states() -> MasterStates:
    data = _get_devices_data(names.Masters().filter(*names.SCOPE).subtract("tm").list, "state")
    return MasterStates(data)


def get_centralnode_state() -> ListInspection:
    # fetcher = configuration.get_devices_query()
    # data = fetcher.query(names.Masters().filter("central node"), "state")
    # return MasterStates(data)
    if env.telescope_type_is_mid():
        central_node_state = get_device_state(Mid.tm.central_node.filter(*names.SCOPE))
        if central_node_state:
            return ListInspection(central_node_state.as_dict())
        return ListInspection({})
    central_node_state = get_device_state(Low.tm.central_node.filter(*names.SCOPE))
    if central_node_state:
        return ListInspection(central_node_state.as_dict())
    return ListInspection({})


def get_centralnode_telescopestate() -> ListInspection:
    central_node_telescopestate = get_device_attr(
        Mid.tm.central_node.filter(*names.SCOPE), "telescopeState"
    )
    if central_node_telescopestate:
        return ListInspection(central_node_telescopestate.as_dict())
    return ListInspection({})


class DishesState(ListInspection):
    pass


def get_dishes_state(dishes: Union[int, List[int]]):
    dish_ids = list(range(1, dishes + 1)) if isinstance(dishes, int) else dishes
    dish_names = Mid.dishes(dish_ids).filter(*names.SCOPE)
    data = _get_devices_data(dish_names.list, "state")
    return DishesState(data)


def get_receptor_pointing(receptors: List[int]) -> ListInspections:
    the_list = ListInspections()
    for receptor in receptors:
        dish = Mid.dish(receptor).filter(*names.SCOPE)
        if dish:
            the_list[f"{receptor}"] = {
                dish.__str__(): _get_attr_val_from_a_device(dish, "pointingstate"),
            }
    return the_list


class DishesOpState(ListInspection):
    pass


def get_dishes_ops_state(dishes: Union[int, List[int]]):
    dish_ids = list(range(1, dishes + 1)) if isinstance(dishes, int) else dishes
    dish_names = Mid.dishes(dish_ids).filter(*names.SCOPE)
    data = _get_devices_data(dish_names.list, "dishmode")
    return DishesOpState(data)


class DishesHealth(ListInspection):
    pass


def get_dishes_health(dishes: Union[int, List[int]]):
    dish_ids = list(range(1, dishes + 1)) if isinstance(dishes, int) else dishes
    dish_names = Mid.dishes(dish_ids).filter(*names.SCOPE)
    data = _get_devices_data(dish_names.list, "healthstate")
    return DishesHealth(data)


class SensorStates(ListInspection):
    pass


def get_sensor_states(dishes: Union[int, List[int]]) -> ListInspections:
    the_list = ListInspections()
    dishes = list(range(1, dishes + 1)) if isinstance(dishes, int) else dishes
    for dish_nr in dishes:
        data = _get_devices_data(
            names.Sensors(dish_nr).filter(*names.SCOPE).subtract("vcc").subtract("dishes").list,
            "state",
        )
        the_list[f"{dish_nr}"] = SensorStates(data)
    return the_list


def get_vcc_sensor_states(dishes: Union[int, List[int]]) -> ListInspections:
    the_list = ListInspections()
    dishes = list(range(1, dishes + 1)) if isinstance(dishes, int) else dishes
    for dish_nr in dishes:
        data = _get_devices_data(
            names.Sensors(dish_nr).filter(*names.SCOPE).filter("vcc").list,
            "state",
        )
        the_list[f"{dish_nr}"] = SensorStates(data)
    return the_list


def get_tmc_master_leaf_nodes() -> ListInspection:
    data = _get_devices_data(
        names.Masters().filter(*names.SCOPE).filter("leaf nodes").list, "state"
    )
    return MasterStates(data)


def get_tmc_sensor_nodes(dishes: Union[int, List[int]]) -> ListInspections:
    the_list = ListInspections()
    dishes = list(range(1, dishes + 1)) if isinstance(dishes, int) else dishes
    for dish_nr in dishes:
        data = _get_devices_data(
            names.Sensors(dish_nr).filter(*names.SCOPE).filter("tm").list,
            "state",
        )
        the_list[f"{dish_nr}"] = SensorStates(data)
    return the_list


class SubArrayStates(ListInspection):
    pass


def get_subarray_states(sub_id: int, *filter_tags: str) -> SubArrayStates:
    data = _get_devices_data(
        names.SubArrays(sub_id).filter(*names.SCOPE).filter(*filter_tags).list,
        "state",
    )
    return SubArrayStates(data)


def get_subarrays_states(nr_of_subarrays: int, *filter_tags: str) -> ListInspections:
    the_list = ListInspections()
    for subarray_id in range(1, nr_of_subarrays + 1):
        the_list[f"{subarray_id}"] = get_subarray_states(subarray_id, *filter_tags)
    return the_list


class SubArrayObsStates(ListInspection):
    pass


def get_subarray_obs_states(sub_id: int) -> SubArrayObsStates:
    subarray_controllers = (
        names.SubArrays(sub_id).filter(*names.SCOPE).subtract("leaf nodes").subtract("fsp")
    )
    data = _get_devices_data(subarray_controllers.list, "obsstate")
    return SubArrayObsStates(data)


def get_subarrays_obs_states(nr_of_subarrays: int = 2) -> ListInspections:
    the_list = ListInspections()
    for sub_id in range(1, nr_of_subarrays + 1):
        the_list[f"{sub_id}"] = get_subarray_obs_states(sub_id)
    return the_list


def get_device_state(device_name: Union[str, Symbol, None]) -> Union[Inspection, None]:
    if device_name:
        return Inspection(device_name, _get_attr_val_from_a_device(device_name, "state"))
    return None


def get_device_attr(device_name: Union[str, Symbol, None], attr: str) -> Union[Inspection, None]:
    if device_name:
        return Inspection(device_name, _get_attr_val_from_a_device(device_name, attr))
    return None
