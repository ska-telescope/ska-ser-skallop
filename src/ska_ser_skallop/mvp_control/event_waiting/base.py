from typing import NamedTuple, Union


class SideCarException(Exception):
    """Used to signal an general exception occurred during a particular
    routine, the routine of which is specified by having a more specific exception class
    derived from this one. The handler of the exception can then retrieve the original
    general exception that was tagged along with the additional knowledge of when it
    occurred
    """

    def __init__(self, exc: Exception) -> None:
        self.exc = exc


class PlaySpec(NamedTuple):
    enabled: bool = True
    filter_logs: bool = True
    log_filter_pattern: str = ""

    def __bool__(self) -> bool:
        return self.enabled

    def replicate(
        self,
        enabled: Union[bool, None] = None,
        filter_logs: Union[bool, None] = None,
        log_filter_pattern: Union[str, None] = None,
    ) -> "PlaySpec":
        """Generate a replica of original tuple with updated values as given by args.
        :return: the updated spec as a replica of original
        """
        if enabled is None:
            enabled = self.enabled
        if filter_logs is None:
            filter_logs = self.filter_logs
        if log_filter_pattern is None:
            log_filter_pattern = self.log_filter_pattern
        return PlaySpec(enabled, filter_logs, log_filter_pattern)
