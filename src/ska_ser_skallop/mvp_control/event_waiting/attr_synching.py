import logging
from functools import reduce
from time import sleep
from typing import Any, Dict, List, NamedTuple, Set, Union

from ska_ser_skallop.connectors import configuration
from ska_ser_skallop.event_handling.handlers import WaitUntilEqual
from ska_ser_skallop.mvp_control.base import Spec
from ska_ser_skallop.subscribing.base import MessageBoardBase, SubscriptionBase

logger = logging.getLogger(__name__)


class AwaitAble(NamedTuple):
    device_name: str
    attr: str
    value: Union[str, List[str]]

    @property
    def qfd_name(self) -> str:
        """gets a name for device qaulified by it's attribute

        :return: a qualified name in the pattern '<device_name>/<attr>'
        """
        return f"{self.device_name}/{self.attr}"

    def __hash__(self) -> int:
        if isinstance(self.value, str):
            value_for_hash = "".join(self.value)
        else:
            value_for_hash = self.value
        return hash(f"{self.device_name}{self.attr}{value_for_hash}")


def _get_devices_awaited(
    subscriptions: Set[SubscriptionBase],
) -> Set[AwaitAble]:
    return {
        AwaitAble(
            subscription.producer.name(),
            subscription.attr,
            subscription.handler.desired_value,
        )
        for subscription in subscriptions
        if isinstance(subscription.handler, WaitUntilEqual)
    }


def _print_outcome(awaitables: Set[AwaitAble], result: Dict[str, Any]) -> str:
    return reduce(
        lambda x, y: f"{x}\n{y}",
        [
            f"{awaitable.qfd_name} expected to be "
            f"{awaitable.value} but was found to be {result[awaitable.qfd_name]}"
            for awaitable in awaitables
        ],
    )


def _not__equal(current_value: str, expected_value: Union[str, List[str]]):
    if isinstance(expected_value, str):
        return current_value != expected_value
    return current_value not in expected_value


def wait_until_attrs_in_sync(board: MessageBoardBase, timeout: float):
    """
    Wait for attributes to be in sync.

    Given a message board for which all subscriptions have completed,
    check that for each subscription explicitly waiting for an attr
    value will give the same when directly read. If the values are not
    the same then wait for a timeout period until they are.

    :param board: the messageboard that just completed waiting (it
        should have no more remaining subscriptions)
    :param timeout: the maximum amount of time to wait
    """
    assert not board.subscriptions
    pollperiod = 0.2
    awaitables = _get_devices_awaited(board.archived_subscriptions)
    timeout_count = timeout
    out_of_sync = False
    outcome = None
    while awaitables:
        devices_spec = [
            Spec(**{"name": awaitable.device_name, "attr": awaitable.attr})
            for awaitable in awaitables
        ]
        device_query = configuration.get_devices_query()
        result = device_query.complex_query(devices_spec, timeout)
        updated_awaitables = {
            awaitable
            for awaitable in awaitables
            if _not__equal(result[awaitable.qfd_name], awaitable.value)
        }
        if updated_awaitables:
            if not out_of_sync:
                outcome = _print_outcome(updated_awaitables, result)
                logger.warning(
                    "the values obtained by change events from the following devices "
                    f"differed from those when it was directly read:\n{outcome}"
                )
                out_of_sync = True
        else:
            return outcome
        sleep(pollperiod)
        timeout_count = timeout_count - pollperiod
        if timeout_count < 0:
            raise TimeoutError(
                f"Timeout ({timeout}s) waiting for devices to be in sync with that "
                f"being red - outcome:\n{outcome}"
            )
        awaitables = updated_awaitables
