import functools
import logging
import os
import time
from contextlib import contextmanager
from threading import Event
from typing import Callable, Dict, Iterator, NamedTuple, Set, Tuple, Union

from ska_ser_skallop.event_handling.builders import MessageBoardBuilder
from ska_ser_skallop.event_handling.handlers import Recorder
from ska_ser_skallop.mvp_control.event_waiting import base
from ska_ser_skallop.mvp_control.event_waiting.set_to_wait import (
    WatchSpec,
    set_observe,
    set_waiting_for,
)
from ska_ser_skallop.mvp_fixtures.base import ExecSettings
from ska_ser_skallop.subscribing.base import MessageBoardBase

from .attr_synching import wait_until_attrs_in_sync

logger = logging.getLogger(__name__)


def concurrent_wait(
    board: MessageBoardBase,
    stop_signal: Event,
    polling: float,
    empty_subscriptions_signal: Event,
    live_logging: bool = False,
    recorder: Union[None, Recorder] = None,
):
    """Waiting task that can be used to concurrently handle incoming events.


    :param board: The Messageboard upon which to retrieve events
    :param stop_signal: An Event item that is used to get a signal to stop the thread
    :param live_logging: wether log messages should be returned whilst they are received,
        defaults to False
    """
    if os.getenv("DEBUG_WAIT"):
        logger.info(f"starting wait with subscriptions: {board.describe()}")
    while not stop_signal.wait(timeout=polling):
        if items := board.get_current_items():
            for item in items:
                handler = item.handler
                if handler:
                    handler.handle_event(item.event, item.subscription)
                    if live_logging:
                        message = handler.print_event(item.event, ignore_first=False)
                        logger.info(message)
                    if recorder:
                        recorder.record(handler)
        if not board.subscriptions:
            empty_subscriptions_signal.set()
    board.remove_all_subscriptions()


def wait(
    board: MessageBoardBase,
    timeout: float,
    live_logging: bool = False,
    attr_synching: bool = False,
    recorder: Union[None, Recorder] = None,
) -> Union[str, None]:
    if os.getenv("DEBUG_WAIT"):
        logger.info(f"starting wait with subscriptions: {board.describe()}")
    try:
        for item in board.get_items(timeout):
            handler = item.handler
            if handler:
                handler.handle_event(item.event, item.subscription)
                message = handler.print_event(item.event, ignore_first=False)
                if live_logging:
                    if message:  # ignore emoptry strings (if filtered)
                        logger.info(message)
                if recorder:
                    recorder.record(handler)
        if attr_synching:
            return wait_until_attrs_in_sync(board, timeout)
        return None
    except Exception as err:
        raise EWhilstWaiting(err) from err


def print_logbook(board: MessageBoardBase, spec: base.PlaySpec) -> None:
    logbook = board.play_log_book(spec.filter_logs, spec.log_filter_pattern)
    logger.info(f"Log Messages during waiting:\n{logbook}")


class EWhilstRunning(base.SideCarException):
    pass


class EWhilstWaiting(base.SideCarException):
    pass


class FunctionToRun(NamedTuple):
    function: Callable
    args: Tuple
    kwargs: Dict

    def run(self):
        try:
            return self.function(*self.args, **self.kwargs)
        except Exception as err:
            raise EWhilstRunning(err) from err


def wait_after_running(
    board: MessageBoardBase,
    function_to_run: FunctionToRun,
    timeout: float,
    log_enabled: bool,
    wait_when_faulty: bool = False,
    play_logbook: base.PlaySpec = base.PlaySpec(),
    attr_synching: bool = False,
    recorder: Union[None, Recorder] = None,
):
    try:
        if log_enabled:
            logger.info("Starting execution...")
        result = function_to_run.run()
        if log_enabled:
            logger.info("task finished, initiating waiting")
        wait(
            board,
            timeout,
            live_logging=log_enabled,
            attr_synching=attr_synching,
            recorder=recorder,
        )
        if log_enabled:
            logger.info("wait completed")
        if play_logbook:
            print_logbook(board, play_logbook)
        return result
    except EWhilstRunning as err:
        try:
            if wait_when_faulty:
                logger.warning(
                    f"Execution failed ({str(err)[:40]}...) waiting for state "
                    "monitoring to finish..."
                )
                logger.exception(err.exc)
                wait(
                    board,
                    timeout,
                    live_logging=log_enabled,
                    attr_synching=attr_synching,
                    recorder=recorder,
                )
                if play_logbook:
                    print_logbook(board, play_logbook)
            else:
                logger.warning(
                    f"Execution failed ({str(err)[:40]}...) will cancel subscriptions "
                    "and abort waiting..."
                )
                logger.exception(err.exc)
                board.remove_all_subscriptions()
                if play_logbook:
                    print_logbook(board, play_logbook)
            raise err.exc
        except EWhilstWaiting as second_exception:
            logger.warning(
                "Additional exception whilst trying to wait for "
                f"events:\n{str(second_exception.exc)[:30]}...supressing as an "
                "exception"
            )
            logger.exception(second_exception.exc)
            if play_logbook:
                print_logbook(board, play_logbook)
            raise err.exc
    except EWhilstWaiting as err:
        logger.warning(f"exception occurred in execution of waiting: {err.args}")
        logger.exception(err.exc)
        if play_logbook:
            print_logbook(board, play_logbook)
        raise err.exc


# decorator for generic state logging
def sync_states(
    spec: Set[WatchSpec],
    timeout=10,
    log_enabled=False,
    observe_only=False,
    attr_synching=False,
    recorder: Union[None, Recorder] = None,
):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            builder = set_observe(spec) if observe_only else set_waiting_for(spec)
            board = builder.setup_board()
            function_to_run = FunctionToRun(func, args, kwargs)
            return wait_after_running(
                board,
                function_to_run,
                timeout,
                log_enabled,
                attr_synching,
                recorder=recorder,
            )

        return wrapper

    return decorator


def wait_for_it(
    builder: MessageBoardBuilder,
    timeout: float = 10,
    log_enabled: bool = False,
    play_logbook: base.PlaySpec = base.PlaySpec(),
    wait_when_faulty=False,
    attr_synching=False,
    recorder: Union[None, Recorder] = None,
):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            if log_enabled:
                logger.info(
                    "initiating subscriptions for message board so as to wait " "afterwards..."
                )
            board = builder.setup_board()
            function_to_run = FunctionToRun(func, args, kwargs)
            return wait_after_running(
                board,
                function_to_run,
                timeout,
                log_enabled,
                wait_when_faulty,
                play_logbook,
                attr_synching,
                recorder=recorder,
            )

        return wrapper

    return decorator


@contextmanager
def waiting_context(
    builder: MessageBoardBuilder,
    settings: ExecSettings = None,
    print_when_fail=True,
) -> Iterator[MessageBoardBase]:
    board = builder.setup_board()
    yield board
    if settings:
        if settings.exec_failed():
            if print_when_fail:
                # wait for 1 second to fetch log events
                time.sleep(1)
                board.remove_all_subscriptions()
                messages = board.play_log_book(filter_log=False)
                logger.info(messages)
    board.remove_all_subscriptions()


@contextmanager
def wait_for(
    builder: MessageBoardBuilder,
    timeout: float = 25,
    live_logging: bool = False,
    print_when_fail: bool = True,
    attr_synching: bool = False,
    recorder: Union[None, Recorder] = None,
):
    board = builder.setup_board()
    yield board
    try:
        wait(board, timeout, live_logging, attr_synching, recorder)
    except EWhilstWaiting as exception:
        if print_when_fail:
            # wait for 1 second to fetch log events
            time.sleep(1)
            board.remove_all_subscriptions()
            messages = board.play_log_book(filter_log=False)
            logger.info(messages)
        raise exception
    finally:
        board.remove_all_subscriptions()
