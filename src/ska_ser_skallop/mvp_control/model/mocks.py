from enum import Enum
from typing import Any, Callable, Dict, List, Set, Union, cast

import mock
from assertpy import assert_that

from ska_ser_skallop.connectors.mockingfactory import MockingFactory
from ska_ser_skallop.mvp_control.base import AbstractDeviceProxy, AbstractDevicesQuery, Spec
from ska_ser_skallop.subscribing.base import AttributeInt, EventDataInt, Subscriber
from ska_ser_skallop.subscribing.producers import Producer
from ska_ser_skallop.utils.generic_classes import Symbol


class _Subscriber(Subscriber):
    def __init__(self, callback: Callable[[EventDataInt], None]) -> None:
        super().__init__()
        self.callback = callback

    def push_event(self, event: EventDataInt) -> None:
        super().push_event(event)
        return self.callback(event)


def as_string(attr: Any) -> str:
    if isinstance(attr, Enum):
        return attr.name
    return str(attr)


class MockDevicesQuery(AbstractDevicesQuery):
    def __init__(self) -> None:
        self._device_list: Set["AbstractDeviceProxy"] = set()
        self.spy = cast("AbstractDevicesQuery", mock.Mock(AbstractDevicesQuery, wraps=self))

    def add_device(self, device: AbstractDeviceProxy):
        self._device_list.add(device)

    def query(self, device_list: List[str], attr: str, timeout: float = 3) -> dict[str, Any]:
        return {
            device_name: as_string(device.read_attribute(attr).value)
            for device_name in device_list
            for device in self._device_list
            if device.name() == device_name
        }

    def complex_query(self, device_specs: List[Spec], timeout: float = 3) -> Dict[str, Any]:
        return {
            f'{spec["name"]}/{spec["attr"]}': as_string(device.read_attribute(spec["attr"]).value)
            for spec in device_specs
            for device in self._device_list
            if device.name() == spec["name"]
        }


class MockDeviceProxy(AbstractDeviceProxy, Producer):
    """Mock implementation of a device proxy based on values set."""

    def __init__(self, name: Union[str, Symbol], model: Dict[str, Any]) -> None:
        """Initialise object.

        :param name: The FQDN name for the device
        :param model: a dictionary representing the device and its attributes
        """
        if isinstance(name, Symbol):
            name = name.__str__()
        super().__init__(name)
        self._name = name
        self._model = model
        self.spy = cast("AbstractDeviceProxy", mock.Mock(AbstractDeviceProxy, wraps=self))

    def inject_into_factory(self, factory: MockingFactory):
        """inject the device (as a mock) into the given factory.

        :param factory: the testing factory.
        """
        factory.inject_device_proxy_mock(self._name, self.spy)
        factory.inject_producer(self._name, self.spy)
        if factory.devices_query_set:
            devices_query = factory.get_devices_query()
            assert isinstance(devices_query, MockDevicesQuery)
        else:
            devices_query = MockDevicesQuery()
            factory.inject_devices_query(devices_query)
        devices_query.add_device(self)

    def ping(self) -> float:
        """Implements ping command"""
        return 1.0

    def command_inout(self, cmd_name: str, cmd_param: Any = None, *args: Any, **kwargs: Any) -> Any:
        """Implements command_inout

        :param cmd_name: see base
        :param cmd_param: see base
        :return: see base
        """
        return cmd_name, cmd_param

    def State(self) -> str:
        """Implements state command

        :return: see base
        """
        assert_that(self._model, description="state attribute exists on Mock").contains_key("state")
        return self._model["state"]

    def read_attribute(self, attr_name: str, *args: Any, **kwargs: Any) -> AttributeInt:
        """Implements read_attribute command

        :param attr_name: see base
        :return: see base

        """
        attr_name = attr_name.lower()
        assert_that(
            self._model,
            description=f"attribute exists on Mock for {self._name}",
        ).contains_key(attr_name)
        value = self._model[attr_name]
        return AttributeInt(attr_name, value)

    def set_attribute(self, attr_name: str, attr_value: Any):
        """set the attribute value of the device mock (this will result in a push event)

        :param attr_name: The name of the attribute that must be defined
        :param attr_value: see base
        """
        assert_that(self._model, description="attribute exists on Mock").contains_key(attr_name)
        self._model[attr_name] = attr_value
        attribute_event = EventDataInt(self._name, attr_name, attr_value)
        self.push_event(attr_name, attribute_event)

    def subscribe_event(
        self,
        attr: str,
        event_type: Any,
        subscriber: Union[Subscriber, int, Callable[[EventDataInt], None]],
    ) -> int:
        """Implement a subscription.
        Note a subscription will result in a event being pushed immediately.

        :param attr: The name of the attribute to subscribe to
        :param event_type: A dummy event type that will be ignored by the mock
        :param subscriber: see base
        :return: the id (as an integer) of teh subscription
        """
        assert_that(self._model, description="attribute exists on Mock").contains_key(attr)
        if not isinstance(subscriber, (Subscriber, int)):
            subscriber = _Subscriber(subscriber)
        sub_id = super().subscribe_event(attr, event_type, subscriber)
        value = self._model[attr]
        attribute_event = EventDataInt(self._name, attr, value)
        self.push_event(attr, attribute_event)
        return sub_id
