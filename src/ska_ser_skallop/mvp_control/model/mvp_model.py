from typing import Any, List, Union

from ska_ser_skallop.connectors.mockingfactory import MockingFactory
from ska_ser_skallop.mvp_control.base import AbstractDeviceProxy
from ska_ser_skallop.mvp_control.describing import mvp_names as names

from .mocks import MockDeviceProxy


class BaseModel:
    def __init__(self, *devices: Union[MockDeviceProxy, "BaseModel", None]) -> None:
        self._devices = [device for device in devices if isinstance(device, MockDeviceProxy)]
        self._sub_systems = [
            sub_system for sub_system in devices if isinstance(sub_system, BaseModel)
        ]

    def inject_factory(self, factory: MockingFactory):
        for device in self._devices:
            device.inject_into_factory(factory)
        for sub_system in self._sub_systems:
            sub_system.inject_factory(factory)

    def __bool__(self) -> bool:
        return any([len(self._devices) > 0, len(self._sub_systems) > 0])


device_model = {"state": "OFF"}
master_model = {**device_model.copy()}
subarray_model = {"obsstate": "EMPTY", **device_model.copy()}
central_node_model = {"telescopestate": "STANDBY", **device_model.copy()}
central_node_model["state"] = "ON"
dish_model = {
    "dishmode": "STANDBY_LP",
    "healthstate": "OK",
    **device_model.copy(),
}


class SDP(BaseModel):
    def __init__(self) -> None:
        tel = names.get_tel()
        self.master = MockDeviceProxy(tel.sdp.master, master_model.copy())
        self.subarray1 = MockDeviceProxy(tel.sdp.subarray(1), subarray_model.copy())
        self.subarray2 = MockDeviceProxy(tel.sdp.subarray(2), subarray_model.copy())
        self.subarray3 = MockDeviceProxy(tel.sdp.subarray(3), subarray_model.copy())
        self.subarray4 = MockDeviceProxy(tel.sdp.subarray(4), subarray_model.copy())
        super().__init__(
            self.master,
            self.subarray1,
            self.subarray2,
            self.subarray3,
            self.subarray4,
        )

    def set_states_for_telescope_running(self):
        self.master.set_attribute("state", "ON")
        self.subarray1.set_attribute("state", "ON")
        self.subarray2.set_attribute("state", "ON")
        self.subarray3.set_attribute("state", "ON")
        self.subarray4.set_attribute("state", "ON")

    def set_states_for_telescope_standby(self):
        self.master.set_attribute("state", "OFF")
        self.subarray1.set_attribute("state", "OFF")
        self.subarray2.set_attribute("state", "OFF")
        self.subarray3.set_attribute("state", "OFF")
        self.subarray4.set_attribute("state", "OFF")

    def set_states_for_subarray_idle(self):
        self.subarray1.set_attribute("obsstate", "IDLE")
        self.subarray2.set_attribute("obsstate", "IDLE")
        self.subarray3.set_attribute("obsstate", "IDLE")
        self.subarray4.set_attribute("obsstate", "IDLE")

    def set_states_for_subarray_ready(self):
        self.subarray1.set_attribute("obsstate", "READY")
        self.subarray2.set_attribute("obsstate", "READY")
        self.subarray3.set_attribute("obsstate", "READY")
        self.subarray4.set_attribute("obsstate", "READY")


class TMC(BaseModel):
    """Model of TMC element."""

    def __init__(self) -> None:
        tel = names.get_tel()
        tmc_subarray_model = {**subarray_model}
        tmc_subarray_model["state"] = "ON"
        tm_master_model = {**master_model}
        tm_master_model["state"] = "ON"
        self.central_node = MockDeviceProxy(tel.tm.central_node, central_node_model.copy())
        leafnodes = []
        if mid := tel.skamid:
            self.sdp_leaf_node = MockDeviceProxy(mid.tm.sdp_leaf_node, tm_master_model.copy())
            self.csp_leaf_node = MockDeviceProxy(mid.tm.csp_leaf_node, tm_master_model.copy())
            leafnodes = [self.sdp_leaf_node, self.csp_leaf_node]
        self.subarray1 = MockDeviceProxy(tel.tm.subarray(1), tmc_subarray_model.copy())
        self.subarray2 = MockDeviceProxy(tel.tm.subarray(2), tmc_subarray_model.copy())
        self.subarray3 = MockDeviceProxy(tel.tm.subarray(3), tmc_subarray_model.copy())
        self.subarray4 = MockDeviceProxy(tel.tm.subarray(4), tmc_subarray_model.copy())
        super().__init__(
            self.central_node,
            self.subarray1,
            self.subarray2,
            self.subarray3,
            self.subarray4,
            *leafnodes,
        )

    def set_states_for_telescope_running(self):
        self.central_node.set_attribute("telescopestate", "ON")

    def set_states_for_telescope_standby(self):
        self.central_node.set_attribute("telescopestate", "OFF")

    def set_states_for_subarray_idle(self):
        self.subarray1.set_attribute("obsstate", "IDLE")
        self.subarray2.set_attribute("obsstate", "IDLE")
        self.subarray3.set_attribute("obsstate", "IDLE")
        self.subarray4.set_attribute("obsstate", "IDLE")

    def set_states_for_subarray_ready(self):
        self.subarray1.set_attribute("obsstate", "READY")
        self.subarray2.set_attribute("obsstate", "READY")
        self.subarray3.set_attribute("obsstate", "READY")
        self.subarray4.set_attribute("obsstate", "READY")


class PSS(BaseModel):
    def __init__(self, fsp_id: int) -> None:
        self._fsp_id = fsp_id
        tel = names.get_tel()
        self.controller = MockDeviceProxy(
            tel.csp.cbf.fsp(self._fsp_id).pulsar_search, device_model.copy()
        )
        self.subarray1 = MockDeviceProxy(
            tel.csp.cbf.fsp(self._fsp_id).pulsar_search.subarray(1),
            subarray_model.copy(),
        )
        self.subarray2 = MockDeviceProxy(
            tel.csp.cbf.fsp(self._fsp_id).pulsar_search.subarray(2),
            subarray_model.copy(),
        )
        self.subarray3 = MockDeviceProxy(
            tel.csp.cbf.fsp(self._fsp_id).pulsar_search.subarray(3),
            subarray_model.copy(),
        )
        super().__init__(self.controller, self.subarray1, self.subarray2, self.subarray3)

    def set_states_for_telescope_running(self):
        self.controller.set_attribute("state", "ON")
        self.subarray1.set_attribute("state", "ON")
        self.subarray2.set_attribute("state", "ON")
        self.subarray3.set_attribute("state", "ON")

    def set_states_for_telescope_standby(self):
        self.controller.set_attribute("state", "OFF")
        self.subarray1.set_attribute("state", "OFF")
        self.subarray2.set_attribute("state", "OFF")
        self.subarray3.set_attribute("state", "OFF")

    def set_states_for_subarray_idle(self):
        self.subarray1.set_attribute("obsstate", "IDLE")
        self.subarray2.set_attribute("obsstate", "IDLE")
        self.subarray3.set_attribute("obsstate", "IDLE")

    def set_states_for_subarray_ready(self):
        self.subarray1.set_attribute("obsstate", "READY")
        self.subarray2.set_attribute("obsstate", "READY")
        self.subarray3.set_attribute("obsstate", "READY")


class FSP(BaseModel):
    def __init__(self, fsp_id: int) -> None:
        self._id = fsp_id
        tel = names.get_tel()
        self.vlbi1 = MockDeviceProxy(tel.csp.cbf.fsp(self._id).vlbi(1), device_model.copy())
        self.vlbi2 = MockDeviceProxy(tel.csp.cbf.fsp(self._id).vlbi(2), device_model.copy())
        self.pulsar_timing1 = MockDeviceProxy(
            tel.csp.cbf.fsp(self._id).pulsar_timing(1), device_model.copy()
        )
        self.pulsar_timing2 = MockDeviceProxy(
            tel.csp.cbf.fsp(self._id).pulsar_timing(2), device_model.copy()
        )
        self.pss = PSS(self._id)
        super().__init__(
            self.vlbi1,
            self.vlbi2,
            self.pulsar_timing1,
            self.pulsar_timing2,
            self.pss,
        )

    def set_states_for_telescope_running(self):
        self.vlbi1.set_attribute("state", "ON")
        self.vlbi2.set_attribute("state", "ON")
        self.pulsar_timing1.set_attribute("state", "ON")
        self.pulsar_timing2.set_attribute("state", "ON")
        self.pss.set_states_for_telescope_running()

    def set_states_for_telescope_standby(self):
        self.vlbi1.set_attribute("state", "OFF")
        self.vlbi2.set_attribute("state", "OFF")
        self.pulsar_timing1.set_attribute("state", "OFF")
        self.pulsar_timing2.set_attribute("state", "OFF")
        self.pss.set_states_for_telescope_standby()


class CBFLow(BaseModel):
    """Model of CBF element."""

    def __init__(self) -> None:
        tel = names.get_tel()
        self.controller = MockDeviceProxy(tel.csp.cbf.controller, master_model.copy())
        self.subarray1 = MockDeviceProxy(tel.csp.cbf.subarray(1), subarray_model.copy())

        super().__init__(
            self.controller,
            self.subarray1,
        )

    def set_states_for_telescope_running(self):
        self.controller.set_attribute("state", "ON")
        self.subarray1.set_attribute("state", "ON")

    def set_states_for_telescope_standby(self):
        self.controller.set_attribute("state", "OFF")
        self.subarray1.set_attribute("state", "OFF")

    def set_states_for_subarray_idle(self):
        self.subarray1.set_attribute("obsstate", "IDLE")

    def set_states_for_subarray_ready(self):
        self.subarray1.set_attribute("obsstate", "READY")


class CBF(BaseModel):
    """Model of CBF element."""

    def __init__(self) -> None:
        tel = names.get_tel()
        self.controller = MockDeviceProxy(tel.csp.cbf.controller, master_model.copy())
        self.subarray1 = MockDeviceProxy(tel.csp.cbf.subarray(1), subarray_model.copy())
        self.subarray2 = MockDeviceProxy(tel.csp.cbf.subarray(2), subarray_model.copy())
        self.subarray3 = MockDeviceProxy(tel.csp.cbf.subarray(3), subarray_model.copy())
        self.subarray4 = MockDeviceProxy(tel.csp.cbf.subarray(4), subarray_model.copy())
        self.fsp1 = FSP(1)
        self.fsp2 = FSP(1)
        super().__init__(
            self.controller,
            self.subarray1,
            self.subarray2,
            self.subarray3,
            self.subarray4,
            self.fsp1,
            self.fsp2,
        )

    def set_states_for_telescope_running(self):
        if self:
            self.controller.set_attribute("state", "ON")
            self.subarray1.set_attribute("state", "ON")
            self.subarray2.set_attribute("state", "ON")
            self.subarray3.set_attribute("state", "ON")
            self.subarray4.set_attribute("state", "ON")
            self.fsp1.set_states_for_telescope_running()
            self.fsp2.set_states_for_telescope_running()

    def set_states_for_telescope_standby(self):
        if self:
            self.controller.set_attribute("state", "OFF")
            self.subarray1.set_attribute("state", "OFF")
            self.subarray2.set_attribute("state", "OFF")
            self.subarray3.set_attribute("state", "OFF")
            self.subarray4.set_attribute("state", "OFF")
            self.fsp1.set_states_for_telescope_standby()
            self.fsp2.set_states_for_telescope_standby()

    def set_states_for_subarray_idle(self):
        if self:
            self.subarray1.set_attribute("obsstate", "IDLE")
            self.subarray2.set_attribute("obsstate", "IDLE")
            self.subarray3.set_attribute("obsstate", "IDLE")

    def set_states_for_subarray_ready(self):
        if self:
            self.subarray1.set_attribute("obsstate", "READY")
            self.subarray2.set_attribute("obsstate", "READY")
            self.subarray3.set_attribute("obsstate", "READY")


class CSP(BaseModel):
    """Model of CSP element."""

    def __init__(self) -> None:
        tel = names.get_tel()
        self.controller = MockDeviceProxy(tel.csp.controller, master_model.copy())
        self.subarray1 = MockDeviceProxy(tel.csp.subarray(1), subarray_model.copy())
        self.subarray2 = MockDeviceProxy(tel.csp.subarray(2), subarray_model.copy())
        self.subarray3 = MockDeviceProxy(tel.csp.subarray(3), subarray_model.copy())
        self.subarray4 = MockDeviceProxy(tel.csp.subarray(4), subarray_model.copy())
        if tel.skamid:
            self.cbf = CBF()
        else:
            self.cbf = CBFLow()
        super().__init__(
            self.controller,
            self.subarray1,
            self.subarray2,
            self.subarray3,
            self.subarray4,
            self.cbf,
        )

    def set_states_for_telescope_running(self):
        self.controller.set_attribute("state", "ON")
        self.subarray1.set_attribute("state", "ON")
        self.subarray2.set_attribute("state", "ON")
        self.subarray3.set_attribute("state", "ON")
        self.subarray4.set_attribute("state", "ON")
        if self.cbf:
            self.cbf.set_states_for_telescope_running()

    def set_states_for_telescope_standby(self):
        self.controller.set_attribute("state", "OFF")
        self.subarray1.set_attribute("state", "OFF")
        self.subarray2.set_attribute("state", "OFF")
        self.subarray3.set_attribute("state", "OFF")
        self.subarray4.set_attribute("state", "OFF")
        if self.cbf:
            self.cbf.set_states_for_telescope_standby()

    def set_states_for_subarray_idle(self):
        self.subarray1.set_attribute("obsstate", "IDLE")
        self.subarray2.set_attribute("obsstate", "IDLE")
        self.subarray3.set_attribute("obsstate", "IDLE")

    def set_states_for_subarray_ready(self):
        self.subarray1.set_attribute("obsstate", "READY")
        self.subarray2.set_attribute("obsstate", "READY")
        self.subarray3.set_attribute("obsstate", "READY")


class MCCS(BaseModel, AbstractDeviceProxy):
    """Model of MCCS components."""

    def __init__(self) -> None:
        tel = names.get_tel()
        if tel.skalow:
            self.controller = MockDeviceProxy(tel.skalow.mccs.master, master_model.copy())
            self.subarray1 = MockDeviceProxy(tel.skalow.mccs.subarray(1), subarray_model.copy())
            self.subarray2 = MockDeviceProxy(tel.skalow.mccs.subarray(2), subarray_model.copy())
            self.subarray3 = MockDeviceProxy(tel.skalow.mccs.subarray(3), subarray_model.copy())
            self.subarray4 = MockDeviceProxy(tel.skalow.mccs.subarray(4), subarray_model.copy())
            super().__init__(
                self.controller,
                self.subarray1,
                self.subarray2,
                self.subarray3,
                self.subarray4,
            )
        else:
            super().__init__()

    def set_states_for_telescope_running(self):
        self.controller.set_attribute("state", "ON")
        self.subarray1.set_attribute("state", "ON")
        self.subarray2.set_attribute("state", "ON")
        self.subarray3.set_attribute("state", "ON")
        self.subarray4.set_attribute("state", "ON")

    def set_states_for_telescope_standby(self):
        self.controller.set_attribute("state", "OFF")
        self.subarray1.set_attribute("state", "OFF")
        self.subarray2.set_attribute("state", "OFF")
        self.subarray3.set_attribute("state", "OFF")
        self.subarray4.set_attribute("state", "OFF")

    def set_states_for_subarray_idle(self):
        self.subarray1.set_attribute("obsstate", "IDLE")
        self.subarray2.set_attribute("obsstate", "IDLE")
        self.subarray3.set_attribute("obsstate", "IDLE")

    def set_states_for_subarray_ready(self):
        self.subarray1.set_attribute("obsstate", "READY")
        self.subarray2.set_attribute("obsstate", "READY")
        self.subarray3.set_attribute("obsstate", "READY")


class Dishes(BaseModel, AbstractDeviceProxy):
    """Model of Dishes."""

    def __init__(self, nr_of_dishes: int = 4) -> None:
        self._dishes = [
            MockDeviceProxy(names.Mid.dish(dish_id), dish_model)
            for dish_id in range(1, nr_of_dishes + 1)
        ]
        super().__init__(*self._dishes)

    def set_attribute(self, attr_name: str, attr_value: Any, dish_ids: List[int] = []):
        if dish_ids:
            for dish_id in dish_ids:
                self._dishes[dish_id - 1].set_attribute(attr_name, attr_value)
        else:
            for dish in self._dishes:
                dish.set_attribute(attr_name, attr_value)

    def dish(self, dish_id: int) -> MockDeviceProxy:
        return self._dishes[dish_id]

    def set_states_for_telescope_running(self):
        self.set_attribute("state", "ON")
        self.set_attribute("dishmode", "OPERATE")

    def set_states_for_telescope_standby(self):
        self.set_attribute("dishmode", "STANDBY_LP")
        self.set_attribute("state", "STANDBY")


class MVPModel:
    """Model of MVP."""

    sdp: SDP
    tmc: TMC
    csp: CSP
    dishes: Dishes
    mccs: MCCS

    def __init__(self) -> None:
        self.sdp = SDP()
        self.tmc = TMC()
        self.csp = CSP()
        self.dishes = Dishes()
        self.mccs = MCCS()

    def inject_factory(self, factory: MockingFactory):
        self.sdp.inject_factory(factory)
        self.tmc.inject_factory(factory)
        self.csp.inject_factory(factory)
        if self.dishes:
            self.dishes.inject_factory(factory)
        if self.mccs:
            self.mccs.inject_factory(factory)

    def set_states_for_telescope_running(self):
        self.dishes.set_states_for_telescope_running()
        self.sdp.set_states_for_telescope_running()
        self.csp.set_states_for_telescope_running()
        if self.mccs:
            self.mccs.set_states_for_telescope_running()
        self.tmc.set_states_for_telescope_running()

    def set_states_for_telescope_standby(self):
        self.dishes.set_states_for_telescope_standby()
        self.sdp.set_states_for_telescope_standby()
        self.csp.set_states_for_telescope_standby()
        self.mccs.set_states_for_telescope_standby()
        self.tmc.set_states_for_telescope_standby()

    def set_states_for_subarray_idle(self):
        self.dishes.set_states_for_telescope_standby()
        self.sdp.set_states_for_telescope_standby()
        self.csp.set_states_for_telescope_standby()
        self.mccs.set_states_for_telescope_standby()
        self.tmc.set_states_for_telescope_standby()
