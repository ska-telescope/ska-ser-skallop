import logging
from contextlib import contextmanager

from ska_ser_skallop.mvp_control import base
from ska_ser_skallop.mvp_control.describing import mvp_states as states
from ska_ser_skallop.mvp_control.describing.mvp_conditions import NotReady, Readiness
from ska_ser_skallop.mvp_control.dishes.failure_handling import dish_labels
from ska_ser_skallop.mvp_control.entry_points import configuration
from ska_ser_skallop.mvp_control.event_waiting import wait
from ska_ser_skallop.mvp_control.subarray.failure_handling import sub_labels
from ska_ser_skallop.mvp_control.telescope.failure_handling import tel_labels
from ska_ser_skallop.mvp_fixtures.base import ExecSettings
from ska_ser_skallop.utils import env

logger = logging.getLogger(__name__)


class EWhileStartingUp(base.SideCarException):
    pass


class EWhileShuttingDown(base.SideCarException):
    pass


class NotReadyStartUp(NotReady):
    pass


class NotReadyShutdown(NotReady):
    pass


class NotReadyRunning(NotReady):
    pass


def assert_telescope_running(nr_of_subarrays, nr_of_receptors):
    try:
        assert_I_can_set_telescope_to_standby(nr_of_subarrays, nr_of_receptors)
    except NotReadyShutdown as exception:
        raise NotReadyRunning(exception.args[0], exception.result)


def assert_I_can_set_telescope_to_running(nr_of_subarrays, nr_of_receptors):
    telescope_ready = Readiness(tel_labels, sub_labels, dish_labels)
    # logger.info(f"master_states: {master_states}")

    if env.telescope_type_is_mid():
        # first check tmc central_node is ON as it is on the top of control hierarchy
        telescope_ready.expect(*tel_labels.cn_state).state(states.get_centralnode_state).to_be("ON")
        # logger.info(f"get_centralnode_state: {states.get_centralnode_state}")

        # then check that TMC Master Leaf Nodes are ON
        telescope_ready.expect(*tel_labels.tmc_master_leaf_nodes).state(
            states.get_tmc_master_leaf_nodes
        ).to_be("ON")
        # logger.info(f"get_tmc_master_leaf_nodes: {states.get_tmc_master_leaf_nodes}")

        # then check that TMC Dish Master Leaf Nodes are ON - possibly make this switchable
        telescope_ready.expect(*tel_labels.tmc_sensor_leafnodes_health).state(
            states.get_tmc_sensor_nodes, nr_of_receptors
        ).to_be("ON")
        # logger.info(
        #     f"get_tmc_sensor_nodes: {states.get_tmc_sensor_nodes(nr_of_receptors)}"
        # )

        # then check that TMC Subarray Node and Subarray Leaf Nodes are ON
        telescope_ready.expect(*sub_labels.tmc_subarrays_health).state(
            states.get_subarrays_states, nr_of_subarrays, "tm"
        ).to_be("ON")
        # logger.info(
        #     "get_subarrays_states: "
        #     f"{states.get_subarrays_states(nr_of_receptors,str_tm)}"
        # )

        # then check that CSP and SDP masters are OFF/STANDBY
        telescope_ready.expect(*tel_labels.csp_sdp_master_health).state(
            states.get_csp_sdp_master_states
        ).to_be("OFF", "STANDBY", "ON")

        # then check that VCC devices are OFF/STANDBY
        # telescope_ready.expect(*tel_labels.vcc_sensor_health).state(
        #     states.get_vcc_sensor_states, nr_of_receptors
        # ).to_be("OFF", "STANDBY")

        # then check that CSP Subarray is OFF/STANDBY
        telescope_ready.expect(*sub_labels.csp_subarray_health).state(
            states.get_subarrays_states, nr_of_subarrays, "csp"
        ).to_be("OFF", "STANDBY")
        # logger.info(
        #     "get_subarrays_states CSP: "
        #     f"{states.get_subarrays_states(nr_of_receptors,str_csp)}"
        # )

        # then check that SDP Subarray is OFF/STANDBY
        telescope_ready.expect(*sub_labels.sdp_subarray_health).state(
            states.get_subarrays_states, nr_of_subarrays, "sdp"
        ).to_be("OFF", "STANDBY")
        # logger.info(
        #     "get_subarrays_states SDP: "
        #     f"{states.get_subarrays_states(nr_of_receptors,str_sdp)}"
        # )

        # then check that Dishes are healthy
        telescope_ready.expect(*dish_labels.dish_masters_operational).state(
            states.get_dishes_ops_state, nr_of_receptors
        ).to_be("STANDBY_LP")
        telescope_ready.expect(*dish_labels.dish_masters_health).state(
            states.get_dishes_health, nr_of_receptors
        ).to_be("OK", "DEGRADED")
        telescope_ready.expect(*dish_labels.dish_masters_dev_state).state(
            states.get_dishes_state, nr_of_receptors
        ).to_be("OFF", "STANDBY")
        # logger.info(f"get_dishes_state: {states.get_dishes_state(nr_of_receptors)}")

        # then check that Central Node telescopeState is STANDBY
        telescope_ready.expect(*tel_labels.cn_telescope_state).state(
            states.get_centralnode_telescopestate
        ).to_be("STANDBY")
        # logger.info(
        #     f"get_centralnode_telescopestate: {states.get_centralnode_telescopestate}"
        # )

    if env.telescope_type_is_low():
        nr_of_subarrays = 1
        # first check tmc central_node is ON as it is on the top of control hierarchy
        telescope_ready.expect(*tel_labels.cn_state).state(states.get_centralnode_state).to_be("ON")
        # logger.info(f"get_centralnode_state: {states.get_centralnode_state}")

        # then check that TMC Master Leaf Nodes are ON
        telescope_ready.expect(*tel_labels.tmc_master_leaf_nodes).state(
            states.get_tmc_master_leaf_nodes
        ).to_be("ON")
        # logger.info(f"get_tmc_master_leaf_nodes: {states.get_tmc_master_leaf_nodes}")

        # then check that TMC Dish Master Leaf Nodes are ON
        telescope_ready.expect(*tel_labels.tmc_sensor_leafnodes_health).state(
            states.get_tmc_sensor_nodes, nr_of_receptors
        ).to_be("ON")
        # logger.info(
        #     f"get_tmc_sensor_nodes: {states.get_tmc_sensor_nodes(nr_of_receptors)}"
        # )

        # then check that TMC Subarray Node and Subarray Leaf Nodes are ON
        telescope_ready.expect(*sub_labels.tmc_subarrays_health).state(
            states.get_subarrays_states, nr_of_subarrays, "tm"
        ).to_be("ON")
        # logger.info(
        #     "get_subarrays_states: "
        #     f"{states.get_subarrays_states(nr_of_receptors,str_tm)}"
        # )

        # then check that CSP and SDP masters are OFF/STANDBY
        telescope_ready.expect(*tel_labels.csp_sdp_master_health).state(
            states.get_csp_sdp_master_states
        ).to_not_be("FAULT")

    # then check the observation state of subarrays
    telescope_ready.expect(*sub_labels.subarrays_obs_state).state(
        states.get_subarrays_obs_states, nr_of_subarrays
    ).to_be("EMPTY")

    if not telescope_ready:
        raise NotReadyStartUp(telescope_ready.describe_why_not_ready(), telescope_ready)


def assert_tmc_is_on():
    telescope_ready = Readiness(tel_labels, sub_labels, dish_labels)
    # first check the general health state of the system
    telescope_ready.expect(*tel_labels.cn_state).state(states.get_centralnode_state).to_be("ON")


def assert_I_can_set_telescope_to_standby(nr_of_subarrays, nr_of_receptors):
    telescope_ready = Readiness(tel_labels, sub_labels, dish_labels)
    # first check tmc central_node is ON as it is on the top of control hierarchy
    telescope_ready.expect(*tel_labels.cn_state).state(states.get_centralnode_state).to_be("ON")

    # then check that TMC Master Leaf Nodes are ON
    telescope_ready.expect(*tel_labels.tmc_master_leaf_nodes).state(
        states.get_tmc_master_leaf_nodes
    ).to_be("ON")

    if env.telescope_type_is_mid():
        # then check that CSP and SDP masters are ON
        telescope_ready.expect(*tel_labels.csp_sdp_master_health).state(
            states.get_csp_sdp_master_states
        ).to_be("ON")

        # then check that TMC Dish Master Leaf Nodes are ON
        telescope_ready.expect(*tel_labels.tmc_sensor_leafnodes_health).state(
            states.get_tmc_sensor_nodes, nr_of_receptors
        ).to_be("ON")

        # then check that VCC devices are ON
        # telescope_ready.expect(*tel_labels.vcc_sensor_health).state(
        #     states.get_vcc_sensor_states, nr_of_receptors
        # ).to_be("ON")

        # then check that CSP Subarray is ON
        telescope_ready.expect(*sub_labels.csp_subarray_health).state(
            states.get_subarrays_states, nr_of_subarrays, "csp"
        ).to_be("ON")

        # then check that SDP Subarray is ON
        telescope_ready.expect(*sub_labels.sdp_subarray_health).state(
            states.get_subarrays_states, nr_of_subarrays, "sdp"
        ).to_be("ON")

        # then check that Dishes are healthy
        telescope_ready.expect(*dish_labels.dish_masters_operational).state(
            states.get_dishes_ops_state, nr_of_receptors
        ).to_be("OPERATE")
        telescope_ready.expect(*dish_labels.dish_masters_health).state(
            states.get_dishes_health, nr_of_receptors
        ).to_be("OK", "DEGRADED")
        telescope_ready.expect(*dish_labels.dish_masters_dev_state).state(
            states.get_dishes_state, nr_of_receptors
        ).to_be("ON")

        # then check that Central Node telescopeState is ON
        telescope_ready.expect(*tel_labels.cn_telescope_state).state(
            states.get_centralnode_telescopestate
        ).to_be("ON")

    if env.telescope_type_is_low():
        nr_of_subarrays = 1

        # then check that CSP and SDP masters are OFF/STANDBY
        telescope_ready.expect(*tel_labels.csp_sdp_master_health).state(
            states.get_csp_sdp_master_states
        ).to_be("ON")

    # then check that TMC Subarray Node and Subarray Leaf Nodes are ON
    telescope_ready.expect(*sub_labels.tmc_subarrays_health).state(
        states.get_subarrays_states, nr_of_subarrays, "tm"
    ).to_be("ON")

    # then check the observation state of subarrays
    telescope_ready.expect(*sub_labels.subarrays_obs_state).state(
        states.get_subarrays_obs_states, nr_of_subarrays
    ).to_be("EMPTY")

    if not telescope_ready:
        raise NotReadyShutdown(telescope_ready.describe_why_not_ready(), telescope_ready)


def set_telescope_to_running(settings: ExecSettings, entry_point: configuration.EntryPoint = None):
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    builder = entry_point.set_wating_for_start_up()

    @base.except_it(EWhileStartingUp)
    @wait.wait_for_it(
        builder,
        settings.time_out,
        settings.log_enabled,
        settings.play_logbook,
        attr_synching=settings.attr_synching,
    )
    def execute() -> None:
        settings.touched = True
        entry_point.set_telescope_to_running()

    execute()
    # TODO SKB-16 remove sleep one synchronisation with startup problem is resolved


def set_telescope_to_standby(settings: ExecSettings, entry_point: configuration.EntryPoint = None):
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    builder = entry_point.set_wating_for_shut_down()

    @base.except_it(EWhileShuttingDown)
    @wait.wait_for_it(
        builder,
        settings.time_out,
        settings.log_enabled,
        settings.play_logbook,
        settings.wait_when_faulty,
        attr_synching=settings.attr_synching,
    )
    def execute() -> None:
        settings.touched = True
        entry_point.set_telescope_to_standby()

    execute()
    # TODO SKB-16: remove sleep one synchronization with startup problem is resolved


@contextmanager
def wait_for_shutting_down(settings: ExecSettings, entry_point: configuration.EntryPoint = None):
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    builder = entry_point.set_wating_for_shut_down()
    with wait.wait_for(
        builder,
        settings.time_out,
        settings.log_enabled,
        attr_synching=settings.attr_synching,
        recorder=settings.recorder,
    ) as board:
        yield
    if settings.play_logbook:
        wait.print_logbook(board, settings.play_logbook)


@contextmanager
def wait_for_starting_up(settings: ExecSettings, entry_point: configuration.EntryPoint = None):
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    builder = entry_point.set_wating_for_start_up()
    with wait.wait_for(
        builder,
        settings.time_out,
        settings.log_enabled,
        attr_synching=settings.attr_synching,
        recorder=settings.recorder,
    ) as board:
        yield
    if settings.play_logbook:
        wait.print_logbook(board, settings.play_logbook)


@contextmanager
def wait_for_going_online(settings: ExecSettings, entry_point: configuration.EntryPoint = None):
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    if builder := entry_point.set_waiting_for_offline_components_to_become_online():
        with wait.wait_for(
            builder,
            settings.time_out,
            settings.log_enabled,
            attr_synching=settings.attr_synching,
            recorder=settings.recorder,
        ) as board:
            yield
        if settings.play_logbook:
            wait.print_logbook(board, settings.play_logbook)
    else:
        yield


def set_telescope_to_online(settings: ExecSettings):
    entry_point = configuration.get_entry_point()
    with wait_for_going_online(settings, entry_point):
        entry_point.set_offline_components_to_online()


def wait_sut_ready_for_session(settings: ExecSettings):
    entry_point = configuration.get_entry_point()
    builder = entry_point.set_wait_for_sut_ready_for_session()
    if builder.specs:
        board = builder.setup_board()
        wait.wait(
            board,
            settings.time_out,
            settings.log_enabled,
            attr_synching=settings.attr_synching,
            recorder=settings.recorder,
        )
