from types import SimpleNamespace

from ska_ser_skallop.event_handling import logging as log
from ska_ser_skallop.mvp_control.event_waiting import base as waiting_base


class TestRunSettings(SimpleNamespace):
    touched = False
    log_enabled: bool = False
    play_logbook: waiting_base.PlaySpec = waiting_base.PlaySpec(False)
    wait_when_faulty: bool = False
    time_out: float = 15
    log_spec: log.LogSpec = log.LogSpec()
    factory = None
