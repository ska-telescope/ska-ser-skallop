import logging
from typing import List

from ska_ser_skallop.connectors.base import AbstractFactory
from ska_ser_skallop.connectors.configuration import FactoryContainer
from ska_ser_skallop.mvp_control.describing import mvp_names
from ska_ser_skallop.mvp_control.describing.mvp_conditions import (
    ConditionLabel,
    ConditionLabels,
    Readiness,
)
from ska_ser_skallop.mvp_control.entry_points import configuration
from ska_ser_skallop.mvp_control.rectifying.rectifier import create_rectifier

logger = logging.getLogger(__name__)


class TelescopeConditionLabels(ConditionLabels):
    @property
    def masters_health(self) -> ConditionLabel:
        """The dev state health of tango devices responsible for general control over a
        major subsystem it should be in state OFF or STANDBY when starting up and ON
        when shutting down
        """
        return ConditionLabel("all masters' dev state", "masters_health", dim="masters")

    @property
    def csp_sdp_master_health(self) -> ConditionLabel:
        """The dev state health of CSP and SDP Masters. They should be in state OFF or
        STANDBY when starting up and ON when shutting down
        """
        return ConditionLabel("csp and sdp master dev states", "csp_sdp_master_health")

    @property
    def cn_telescope_state(self) -> ConditionLabel:
        """The telescopeState of CentralNode device is overall aggregated state of the
        telescope. It should be OFF or STANDBY when starting up and ON when shutting
        down
        """
        return ConditionLabel("central node' telescope state", "cn_telescope_state")

    @property
    def cn_state(self) -> ConditionLabel:
        """The State of CentralNode device is overall aggregated state of the TMC."""
        return ConditionLabel("central node dev state", "cn_state")

    @property
    def tmc_master_leaf_nodes(self) -> ConditionLabel:
        """The health state of all tmc devices related to a sdp and or csp master, they
        should all be in a state of 'ON' when starting and when shutting down.
        """
        return ConditionLabel(
            "all tmc devices related to sdp and csp master",
            "tmc_master_leaf_nodes",
            dim="masters",
        )

    @property
    def tmc_sensor_leafnodes_health(self) -> ConditionLabel:
        """The health state of all tmc devices related to a specific set of receptors,
        they should all be in a state of 'ON' when starting and when shutting down.
        """
        return ConditionLabel(
            "all tmc devices related to receptors",
            "tmc_sensor_leafnodes_health",
        )

    @property
    def resources_health(self) -> ConditionLabel:
        """The health state of devices related to a specific set of receptors (excluding
        the actual dish masters), they should all be in a state of 'OFF',"STANDBY"
        when starting up and ON when shutting down.
        """
        return ConditionLabel("all resources devices dev state", "resources_health")

    @property
    def vcc_sensor_health(self) -> ConditionLabel:
        """The health state of vcc devices related to a specific set of receptors, they
        should all be in a state of 'OFF',"STANDBY" when starting up and ON when
        shutting down.
        """
        return ConditionLabel("vcc sensors dev state", "vcc_sensor_health")


tel_labels = TelescopeConditionLabels()


def get_factory() -> AbstractFactory:
    return FactoryContainer().factory


class DirtyTelescopeHandler:
    def __init__(self, result: Readiness):
        self.telescope_not_ready = result
        self.entry_point = configuration.get_entry_point()
        self.not_ok_conditions = self.telescope_not_ready.get_not_ok_conditions()
        self.aggregate_state = self.telescope_not_ready.aggregate_condition["state"]
        # populate cn_telescope_state and cn_state only if they are not ok =>
        # None implies they are ok
        self.cn_telescope_state = self.not_ok_conditions.get(tel_labels.cn_telescope_state)
        self.cn_state = self.not_ok_conditions.get(tel_labels.cn_state)
        self.csp_sdp_master_health = self.not_ok_conditions.get(tel_labels.csp_sdp_master_health)
        self.rectifier = create_rectifier()

    @property
    def telescope_state_not_ok(self) -> bool:
        return self.cn_telescope_state is not None

    @property
    def telescope_on(self) -> bool:
        if self.telescope_controllable:
            if not self.csp_sdp_master_health:
                return True
            return False
        return False

    @property
    def cn_state_not_ok(self) -> bool:
        return self.cn_state is not None

    @property
    def telescope_controllable(self) -> bool:
        return not any([self.cn_state_not_ok, self.telescope_state_not_ok])

    def exit_if_already_off(self):
        if self.telescope_state_not_ok:
            assert self.cn_telescope_state
            if self.cn_telescope_state.are_in_state("OFF"):
                logger.warning(
                    "It seems the entire telescope is already off will not attempt to " "shutdown"
                )
                return

    def switch_off_any_on_resources(self):
        label = tel_labels.resources_health
        if self.not_ok_conditions.includes(label):
            not_ok_resources = self.not_ok_conditions[label]
            if any(not_ok_resources.are_in_state("ON")):
                on_resources = not_ok_resources.in_state("ON")
                logger.warning(
                    f"It seems some of the resources are already ON ({on_resources}) "
                    "will attempt to switch them off"
                )
                self.rectifier.switch_off_resources(on_resources)

    def switch_on_any_off_resources(self):
        label = tel_labels.resources_health
        if self.not_ok_conditions.includes(label):
            not_ok_resources = self.not_ok_conditions[label]
            if any(not_ok_resources.are_in_state("OFF")):
                off_resources = not_ok_resources.in_state("OFF")
                logger.warning(
                    f"It seems some of the resources are in OFF {off_resources} will "
                    "attempt to switch them ON"
                )
                self.rectifier.switch_on_resources(off_resources)

    def clear_all_faulty_to_be(self, state: str):
        if "FAULT" in self.aggregate_state:
            logger.warning("It seems some elements are in fault, will attempt to clear them")
            faulty_devices = self.telescope_not_ready.select("state", "FAULT")
            self.rectifier.rectify_faulty_devices(faulty_devices, state)

    def is_telescope_in_wrong_state(self, state: List[str]):
        return self.aggregate_state in state

    def switch_on_any_off_masters(self):
        label = tel_labels.csp_sdp_master_health
        if self.not_ok_conditions.includes(label):
            not_ok_masters = self.not_ok_conditions[label]
            if any(not_ok_masters.are_in_state("OFF", "STANDBY")):
                masters_off = not_ok_masters.in_state("OFF", "STANDBY")
                logger.warning(
                    f"It seems some of the masters are in OFF or STANDBY {masters_off} "
                    "will attempt to switch them ON"
                )
                self.rectifier.switch_on(masters_off)

    def switch_off_any_on_masters(self):
        label = tel_labels.csp_sdp_master_health
        if self.not_ok_conditions.includes(label):
            not_ok_masters = self.not_ok_conditions[label]
            if any(not_ok_masters.are_in_state("ON")):
                # if some of the masters are on we switch them off
                masters_on = not_ok_masters.in_state("ON")
                logger.warning(
                    f"it seems some of the masters, {masters_on} are already on, will "
                    "switch them off..."
                )
                masters = mvp_names.Masters()
                self.rectifier.standby_masters(masters_on.select(masters, "csp"))
                self.rectifier.switch_off(masters_on.select(masters, "sdp"))
