import json
import logging
from abc import abstractmethod
from threading import Lock
from typing import Dict, Iterable, Literal, Tuple, TypeVar, Union, cast

from pipe import select

from ska_ser_skallop.mvp_control.infra_mon.deviceservers_crd import (
    DeviceServerCRD,
    DeviceServersCRD,
)

from . import base, infra

T = TypeVar("T")


class PodSet:
    """Generic class to get info about a k8 Deployment/Statefulset in terms if a group of pods."""

    def __init__(
        self,
        resource: Union[base.V1Deployment, base.V1StatefulSet],
        cluster: infra.Cluster,
    ) -> None:
        """Init object.

        :param resource: Kube resource object as loaded from kubernetes namespace in cluster
        :param cluster: The reference to a kubernetes wrapper for a namespaced cluster
        """
        self.spec = cast(Union[base.V1DeploymentSpec, base.V1StatefulSetSpec], resource.spec)
        self._selector = cast(base.V1LabelSelector, self.spec.selector)
        self.status = cast(
            Union[base.V1DeploymentStatus, base.V1StatefulSetStatus],
            resource.status,
        )
        self._cluster = cluster
        self.resource = resource
        self.resource_type: Literal["Deployment", "Statefulset"] = (
            "Deployment" if isinstance(resource, base.V1Deployment) else "Statefulset"
        )

    def _phase_running(self, pod: base.V1Pod):
        return cast(base.V1PodStatus, pod.status).phase == "Running"

    def _phase_pending(self, pod: base.V1Pod):
        return cast(base.V1PodStatus, pod.status).phase == "Pending"

    def _phase_failed(self, pod: base.V1Pod):
        return cast(base.V1PodStatus, pod.status).phase == "Failed"

    @property
    def phase(
        self,
    ) -> Literal["Running", "Pending", "Succeeded", "Failed", "Unknown"]:
        """Provide the aggregate state representing the phase of the pod/s being controlled.

        :return: the aggregate state representing the phase of the pod/s being controlled.
        """
        # pylint: disable=no-value-for-parameter
        if all(self.pods.values() | select(self._phase_running)):
            return "Running"
        if any(self.pods.values() | select(self._phase_pending)):
            return "Pending"
        if any(self.pods.values() | select(self._phase_failed)):
            return "Failed"
        return "Unknown"

    @property
    def pods(self) -> Dict[str, base.V1Pod]:
        """Provide the set of pods being controlled.
        :return: the set of pods being controlled.
        """
        return self._cluster.get_pods(**cast(Dict, self._selector.match_labels))


class Chart:
    """Represents a set of kubernetes resources belonging to a chart."""

    def __new__(cls: type["Chart"], name: str, *_, **__) -> "Chart":
        if derived_chart_cls := _derived_charts.get(name):
            return object.__new__(derived_chart_cls)
        return object.__new__(cls)

    def __init__(self, name: str, version: str, subsystem: str, cluster: infra.Cluster) -> None:
        self.name = name
        self.version = version
        self._cluster = cluster
        self._selector = {"subsystem": subsystem}
        self._config_maps_lock = Lock()
        self._config_maps: None | Dict[str, base.V1ConfigMap] = None
        self._pods_lock = Lock()
        self._pods: None | Dict[str, base.V1Pod] = None
        self._deployments_lock = Lock()
        self._deployments: None | Dict[str, PodSet] = None
        self._statefulsets_lock = Lock()
        self._statefulsets: None | Dict[str, PodSet] = None
        self._services_lock = Lock()
        self._services: None | Dict[str, base.V1Service] = None
        self._deviceservers_crds_lock = Lock()
        self._deviceservers_crds: None | Dict[str, DeviceServersCRD] = None

    def _status_ready(self, resource: PodSet) -> bool:
        spec = cast(Union[base.V1DeploymentSpec, base.V1StatefulSetSpec], resource.spec)
        status = cast(
            Union[base.V1DeploymentStatus, base.V1StatefulSetStatus],
            resource.status,
        )
        return spec.replicas == status.ready_replicas

    def _status_error(self, resource: PodSet) -> bool:
        return resource.phase == "Failed"

    def _status_pending(self, resource: PodSet) -> bool:
        return resource.phase == "Pending"

    def _resource_health(self, resources: Iterable[PodSet]) -> base.ResourceHealth:
        # pylint: disable=no-value-for-parameter
        if all(resources | select(self._status_ready)):
            return "READY"
        if any(resources | select(self._status_error)):
            return "ERROR"
        if any(resources | select(self._status_pending)):
            return "PENDING"
        return "UNKNOWN"

    @property
    def deployments_health(self) -> base.ResourceHealth:
        """Get the aggregate health of deployments deployed as part of this chart.

        :return: the aggregate health of deployments deployed as part of this chart.
        """
        if self.deployments:
            return self._resource_health(self.deployments.values())
        return "UNKNOWN"

    @property
    def statefulsets_health(self) -> base.ResourceHealth:
        """Get the aggregate health of statefulsets deployed as part of this chart.

        :return: the aggregate health of statefulsets deployed as part of this chart.
        """
        if self.statefulsets:
            return self._resource_health(self.statefulsets.values())
        return "UNKNOWN"

    @property
    def health(self) -> base.ResourceHealth:
        """Get the aggregate health of the chart based on deployments and statefulsets.

        :return: the aggregate health of the chart.
        """
        aggregate_resources = [
            *self.deployments.values(),
            *self.statefulsets.values(),
        ]
        return self._resource_health(aggregate_resources)

    @property
    def pods(self) -> Dict[str, base.V1Pod]:
        """Get all the pods being deployed as part of this chart.

        :return: the pods being deployed as part of this chart.
        """
        with self._pods_lock:
            if self._pods is not None:
                return self._pods
            self._pods = self._cluster.get_pods(**self._selector)
            return self._pods

    def _set_as_pod_tuple(
        self, item: Tuple[str, Union[base.V1Deployment, base.V1StatefulSet]]
    ) -> Tuple[str, PodSet]:
        return (item[0], PodSet(item[1], self._cluster))

    @property
    def deployments(self) -> Dict[str, PodSet]:
        """Get all the deployments deployed as part of this chart.

        :return: the deployments being deployed as part of this chart.
        """
        # pylint: disable=no-value-for-parameter
        with self._deployments_lock:
            if self._deployments is not None:
                return self._deployments
            self._deployments = dict(
                list(self._cluster.get_deployments(**self._selector).items())
                | select(self._set_as_pod_tuple)
            )
            return self._deployments

    @property
    def services(self) -> Dict[str, base.V1Service]:
        """Get all the services managing deployments/statefulsets within this chart.

        :return: the services managing deployments/statefulsets within this chart.
        """
        with self._services_lock:
            if self._services is not None:
                return self._services
            self._services = self._cluster.get_services(**self._selector)
            return self._services

    @property
    def configmaps(self) -> Dict[str, base.V1ConfigMap]:
        """Get all config files used as resources within this chart's deployments/statefulsets.

        :return: all configuration files used as resources within this chart's
            deployments/statefulsets.
        """
        with self._config_maps_lock:
            if self._config_maps is not None:
                return self._config_maps
            try:
                self._config_maps = self._cluster.get_configmaps(**self._selector)
            except Exception as error:
                logging.exception(error)
                self._config_maps = None
                return None
            return self._config_maps

    @property
    def statefulsets(self) -> Dict[str, PodSet]:
        """Get all the statefulsets deployed as part of this chart.

        :return: the statefulsets deployed as part of this chart.
        """
        # pylint: disable=no-value-for-parameter
        with self._statefulsets_lock:
            if self._statefulsets is not None:
                return self._statefulsets
            self._statefulsets = dict(
                list(self._cluster.get_statefulsets(**self._selector).items())
                | select(lambda item: (item[0], PodSet(item[1], self._cluster)))
            )
            return self._statefulsets

    @property
    def deviceservers_crds(self) -> Dict[str, DeviceServerCRD]:
        """Get all deviceserver crds used within this chart.

        :return: all deviceservers custom resource definitions.
        """
        with self._deviceservers_crds_lock:
            if self._deviceservers_crds is not None:
                return self._deviceservers_crds
            try:
                self._deviceservers_crds = self._cluster.get_deviceservers_crds(**self._selector)
            except Exception as error:
                logging.exception(error)
                self._deviceservers_crds = None
                return {}
            return self._deviceservers_crds


class LogConsumerChart(Chart):
    """Derived chart aimed specifically at the Log Consumer app."""

    def __init__(
        self, name: str, version: str, subsystem: str, cluster: "ClusterWithCharts"
    ) -> None:
        super().__init__(name, version, subsystem, cluster)
        self._selector = {"name": "log-consumer"}


_derived_charts = {"ska-log-consumer": LogConsumerChart}


class BaseClusterWithCharts:
    """Abstract class resembling an cluster object that can only retrieve charts and sub charts.

    This is to prevent circular dependencies/self referencing deadlocks between an
    object(e.g. Release) needing to use the cluster to get info about charts but for
    which the chart itself which may reference the same object.
    """

    @property
    @abstractmethod
    def chart(self) -> Union[None, Chart]:
        """"""

    @property
    @abstractmethod
    def sub_charts(self) -> Dict[str, Chart]:
        """"""

    @abstractmethod
    def subsystem_for_chart(self, name: str) -> str:
        pass


def default_chart_subsystem_mapping() -> Dict[str, str]:
    return {
        "ska-tango-alarmhandler": "ska-tango-alarmhandler",
        "ska-tango-archiver": "ska-tango-archiver",
        "ska-mid-cbf-mcs": "cbfmcs-mid",
        "ska-tmc-mid": "ska-tmc-mid",
        "ska-csp-lmc-mid": "csp-lmc",
        "ska-tango-base": "ska-tango-base",
    }


class ClusterWithCharts(infra.Cluster, BaseClusterWithCharts):
    """A Derived Cluster that can reference helm chart like objects."""

    chart_cls = Chart

    def __init__(self, chart_subsystem_mapping: Dict[str, str] = None) -> None:
        super().__init__()
        if chart_subsystem_mapping is None:
            self._chart_subsystem_mapping = default_chart_subsystem_mapping()
        else:
            self._chart_subsystem_mapping = chart_subsystem_mapping
        self._chartinfo = {}

    @property
    def release(self) -> Union[None, "Release"]:
        """Returns the the 'umbrella' chart created to deploy a set of related charts.

        :return: the Release representing the 'umbrella' chart created to deploy a set of
            related charts, None if no helm chart was used.
        """
        if not self.chartinfo:
            return None
        return Release(self.chartinfo["release"], self)

    @property
    def chart(self) -> Union[None, Chart]:
        """Returns the 'umbrella' chart created to deploy a set of related charts.

        :return:  the 'umbrella' chart created to deploy a set of related charts, None
            if no helm chart was used.
        """
        # pylint: disable=no-value-for-parameter
        if not self.chartinfo:
            return None
        name = self.chartinfo["name"]
        version = self.chartinfo["version"]
        subsystem = self.subsystem_for_chart(name)
        return self.chart_cls(name, version, subsystem, self)

    def get_new_chart(self, name: str, version: str) -> "Chart":
        subsystem = self.subsystem_for_chart(name)
        return self.chart_cls(name, version, subsystem, self)

    def subsystem_for_chart(self, name: str) -> str:
        return self._chart_subsystem_mapping.get(name, name)

    @property
    def sub_charts(self) -> Dict[str, Chart]:
        """Get the set of internal charts that made up the 'umbrella' chart.

        :return: the set of internal charts that made up the 'umbrella' chart.
        """
        # pylint: disable=no-value-for-parameter
        sub_charts: Dict[str, Chart] = {}
        print("data", self.chartinfo)
        for sub_chart_info in self.chartinfo.get("dependencies", []):
            sub_chart = self.get_new_chart(sub_chart_info["name"], sub_chart_info["version"])
            sub_charts[sub_chart.name] = sub_chart
        return sub_charts

    @property
    def chartinfo(self) -> Dict:
        if self._chartinfo:
            return self._chartinfo
        if chart_info := self.get_configmaps(name=r"re/chartinfo-/"):
            config_map: base.V1ConfigMap = next(iter(chart_info.values()))
            self._chartinfo = json.loads(config_map.data["chartinfo.json"])
            return self._chartinfo
        return {}


class Release:
    """Represents an 'umbrella' chart that was used to deploy a set of helm charts on a cluster."""

    def __init__(self, name: str, cluster: BaseClusterWithCharts) -> None:
        self.name = name
        self._cluster = cluster

    @property
    def chart(self) -> Chart:
        """Returns the 'umbrella' chart used to create the release instance

        :return:  the 'umbrella' chart created to deploy a set of related charts
        """
        chart = self._cluster.chart
        assert (
            chart
        ), "Incorrectly formed release object, as there seems to exist no chart for this release."
        return chart

    @property
    def sub_charts(self) -> Dict[str, Chart]:
        """Get the set of charts within this release.

        :return: the set of charts within this release.
        """
        return self._cluster.sub_charts

    @staticmethod
    def _chart_ready(chart: Chart) -> bool:
        return chart.health == "READY"

    @staticmethod
    def _chart_in_error(chart: Chart) -> bool:
        return chart.health == "ERROR"

    @staticmethod
    def _chart_in_pending(chart: Chart) -> bool:
        return chart.health == "PENDING"

    @staticmethod
    def _chart_in_unknown(chart: Chart) -> bool:
        return chart.health == "UNKNOWN"

    @property
    def health(self) -> base.ResourceHealth:
        """Aggregate the entire health of the release based on the health of it's subcharts.

        :return: the entire health of the release based on the health of it's subcharts.
        """
        # pylint: disable=no-value-for-parameter
        charts = self._cluster.sub_charts.values()
        if all(charts | select(self._chart_ready)):
            return "READY"
        if any(charts | select(self._chart_in_error)):
            return "ERROR"
        if any(charts | select(self._chart_in_pending)):
            return "PENDING"
        return "UNKNOWN"
