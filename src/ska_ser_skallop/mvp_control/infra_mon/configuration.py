from typing import Dict

from .base import AbstractFactory, DeviceAllocation
from .tango_infra import ClusterWithDevices, DevicesChart, TangoBasedRelease, set_factory

__all__ = ("DevicesChart", "DeviceAllocation")

_factory: AbstractFactory | None = None


def set_factory_provider(factory: AbstractFactory):
    global _factory
    _factory = factory


def get_factory():
    global _factory
    assert _factory, "you first need to provide a factory implementation"
    return _factory


def get_mvp_cluster(chart_subsystem_mapping: Dict[str, str] = None) -> ClusterWithDevices:
    # we set the factory first so that release knows
    # how to generate correct devices
    factory = get_factory()
    set_factory(factory)
    return ClusterWithDevices(chart_subsystem_mapping)


def get_mvp_release(chart_subsystem_mapping: Dict[str, str] = None) -> TangoBasedRelease:
    cluster = get_mvp_cluster(chart_subsystem_mapping)
    release = cluster.release
    assert release, "Unable to get a release on the current cluster"
    return release
