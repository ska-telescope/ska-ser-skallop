import re
from typing import NewType

from ska_ser_skallop.utils.pareto import ParetoItem, ParetoList

Seconds = NewType("Seconds", float)
MilliSeconds = NewType("MilliSeconds", float)
Percentage = NewType("MilliSeconds", float)
GigaBytes = NewType("GigaBytes", float)


class MemStats:
    _pattern_select_anon = re.compile(r"(?<=anon\s)(\d*)", re.MULTILINE)
    _pattern_kernel_stack = re.compile(r"(?<=kernel_stack\s)(\d*)", re.MULTILINE)

    def __init__(self, data: str) -> None:
        self._data = data

    @property
    def anon_in_gb(self) -> GigaBytes | None:
        if result := self._pattern_select_anon.findall(self._data):
            return GigaBytes(float(result[0]) / (1024 * 1024 * 1024))

    @property
    def kernel_stack_in_mb(self) -> GigaBytes | None:
        if result := self._pattern_kernel_stack.findall(self._data):
            return GigaBytes(float(result[0]) / (1024 * 1024))

    def get_printable_stats(self) -> str:
        if all(
            [
                self.anon_in_gb is not None,
                self.kernel_stack_in_mb is not None,
            ]
        ):
            anon = f"{self.anon_in_gb:.2f}GB"
            kernel = f"{self.kernel_stack_in_mb:.2f}MB"
            stats = f"Anonymous: {anon:<20} " f"Kernel: {kernel:<5} "
            return (
                f"\n***************************************stats*********************************\n"
                f"{stats}"
                "\n*****************************************************************************\n"
            )
        return ""


class CPUStats:
    _pattern_select_nr_throttled = re.compile(r"(?<=nr_throttled\s)(\d*)", re.MULTILINE)
    _pattern_select_time_throttled = re.compile(r"(?<=throttled_usec\s)(\d*)", re.MULTILINE)
    _pattern_select_nr_of_periods = re.compile(r"(?<=nr_periods\s)(\d*)", re.MULTILINE)
    _pattern_select_system_use = re.compile(r"(?<=system_usec\s)(\d*)", re.MULTILINE)
    _pattern_select_user_use = re.compile(r"(?<=user_usec\s)(\d*)", re.MULTILINE)
    _pattern_select_use = re.compile(r"(?<=usage_usec\s)(\d*)", re.MULTILINE)

    def __init__(self, data: str) -> None:
        self._data = data

    @property
    def nr_throttled(self) -> int | None:
        if result := self._pattern_select_nr_throttled.findall(self._data):
            return int(result[0])

    @property
    def time_throttled_in_seconds(self) -> Seconds | None:
        if result := self._pattern_select_time_throttled.findall(self._data):
            return Seconds(float(result[0]) / (1000 * 1000))

    @property
    def throttling_latency_in_ms(self) -> MilliSeconds | None:
        if self.time_throttled_in_seconds and self.nr_throttled:
            return MilliSeconds(self.time_throttled_in_seconds / float(self.nr_throttled) * 1000)

    @property
    def nr_periods(self) -> int | None:
        if result := self._pattern_select_nr_of_periods.findall(self._data):
            return int(result[0])

    @property
    def os_usage_in_seconds(self) -> Seconds | None:
        if result := self._pattern_select_system_use.findall(self._data):
            return Seconds(float(result[0]) / (1000 * 1000))

    @property
    def app_usage_in_seconds(self) -> Seconds | None:
        if result := self._pattern_select_user_use.findall(self._data):
            return Seconds(float(result[0]) / (1000 * 1000))

    @property
    def usage_in_seconds(self) -> Seconds | None:
        if result := self._pattern_select_use.findall(self._data):
            return Seconds(float(result[0]) / (1000 * 1000))

    @property
    def average_usage_per_period_in_ms(self) -> MilliSeconds | None:
        if self.usage_in_seconds and self.nr_periods:
            return MilliSeconds((self.usage_in_seconds / float(self.nr_periods) * 1000))

    @property
    def proportion_in_os(self) -> None | Percentage:
        if self.os_usage_in_seconds and self.usage_in_seconds:
            return Percentage(self.os_usage_in_seconds / self.usage_in_seconds)

    @property
    def proportion_in_app(self) -> None | Percentage:
        if self.app_usage_in_seconds and self.usage_in_seconds:
            return Percentage(self.app_usage_in_seconds / self.usage_in_seconds)

    @property
    def throttling_severity(self) -> None | Percentage:
        if self.time_throttled_in_seconds and self.usage_in_seconds:
            return Percentage(
                self.time_throttled_in_seconds
                / (self.time_throttled_in_seconds + self.usage_in_seconds)
            )

    @property
    def effective_time_in_ms(self) -> None | MilliSeconds:
        if self.time_throttled_in_seconds and self.usage_in_seconds and self.nr_periods:
            return MilliSeconds(
                ((self.time_throttled_in_seconds + self.usage_in_seconds) / self.nr_periods) * 1000
            )

    def get_printable_stats(self) -> str:
        if all(
            [
                self.throttling_latency_in_ms is not None,
                self.throttling_latency_in_ms is not None,
                self.time_throttled_in_seconds is not None,
                self.usage_in_seconds is not None,
                self.average_usage_per_period_in_ms is not None,
                self.effective_time_in_ms is not None,
            ]
        ):
            severity = f"{self.throttling_severity:.0%}"
            latency = f"{self.throttling_latency_in_ms:.0f}ms"
            time_throttled = f"{self.time_throttled_in_seconds:.0f}s"
            total_cpu_time = f"{self.usage_in_seconds:.0f}s"
            average_cpu_time = f"{self.average_usage_per_period_in_ms:.0f}ms"
            average_effective_time = f"{self.effective_time_in_ms:.0f}ms"
            stats = (
                f"Total cpu time: {total_cpu_time:<5} "
                f"Average cpu time: {average_cpu_time:<5} "
                f"Average effective time: {average_effective_time:<5} "
                "\n"
                f"Total time throttled: {time_throttled:<5} "
                f"Throttling severity: {severity:<5} "
                f"Throttling latency: {latency:<5} "
            )
            return (
                f"\n***************************************stats*********************************\n"
                f"{stats}"
                "\n*****************************************************************************\n"
            )
        return ""


class DeviceProfile:
    def __init__(
        self,
        mem_stats: dict[str, MemStats],
        cpu_stats: dict[str, CPUStats],
        chart_name: str,
    ) -> None:
        self._mem_stats = mem_stats
        self._cpu_stats = cpu_stats
        self.chart_name = chart_name

    @property
    def mem_stats(self) -> dict[str, MemStats]:
        return self._mem_stats

    @property
    def cpu_stats(self) -> dict[str, CPUStats]:
        return self._cpu_stats


class DevicesProfile(ParetoList[DeviceProfile], DeviceProfile):
    @staticmethod
    def _get_cpu_usage_in_seconds(profile: DeviceProfile) -> float:
        value = 0
        for val in profile.cpu_stats.values():
            if val.app_usage_in_seconds is not None:
                value += val.app_usage_in_seconds
        return value

    @staticmethod
    def _get_mem_usage_in_gb(profile: DeviceProfile) -> float:
        value = 0
        for val in profile.mem_stats.values():
            if val.anon_in_gb is not None:
                value += val.anon_in_gb
        return value

    def __init__(self, data: dict[str, DeviceProfile]) -> None:
        super().__init__(data, self._get_cpu_usage_in_seconds)

    def get_twenty_percent_most_cpu_usage(self):
        return self._get_twenty(self._get_cpu_usage_in_seconds)

    def get_twenty_percent_most_mem_usage(self):
        return self._get_twenty(self._get_mem_usage_in_gb)

    @staticmethod
    def _print_cpu_status(item: ParetoItem[DeviceProfile], aggregate: float) -> str:
        val = f"{item.value:.2f}s"
        cum_contribution = f"{item.cumulative_contribution(aggregate):.1%} (cumulative)"
        contribution = item.contribution(aggregate)
        cpu_stats = list(item.reference.cpu_stats.items())
        chart = item.reference.chart_name
        if len(cpu_stats) > 1:
            throttling_severity_list = []
            for name, cpu_stat in cpu_stats:
                if cpu_stat.throttling_severity is not None:
                    throttling_severity = cpu_stat.throttling_severity
                else:
                    throttling_severity = 0.0
                throttling_severity_list.append(f"{throttling_severity: .2%} ({name})")
            throttling_severity = " ".join(throttling_severity_list)
        else:
            cpu_stat = cpu_stats[0][1]
            if cpu_stat.throttling_severity is not None:
                throttling_severity = f"{cpu_stat.throttling_severity:.2%}"
            else:
                throttling_severity = 0.0
        return (
            f"{item.name:<40}{val:<10}{contribution:<10.2%}{cum_contribution:<20}"
            f"throttling: {throttling_severity:<10}"
            f"({chart})"
        )

    @staticmethod
    def _print_mem_status(item: ParetoItem[DeviceProfile], aggregate: float) -> str:
        val = f"{item.value:.3f}GB"
        cum_contribution = f"{item.cumulative_contribution(aggregate):.1%} (cumulative)"
        contribution = item.contribution(aggregate)
        chart = item.reference.chart_name
        return f"{item.name:<40}{val:<10}{contribution:<10.2%}{cum_contribution:<20} ({chart})"

    def get_summary(self) -> str:
        predicate = self._get_cpu_usage_in_seconds
        aggregate_cpu = self._get_aggregate(predicate)
        cpu_list = "\n    ".join(
            [self._print_cpu_status(item, aggregate_cpu) for item in self._get_eighty(predicate)]
        )
        predicate = self._get_mem_usage_in_gb
        aggregate_mem = self._get_aggregate(predicate)
        mem_list = "\n    ".join(
            [self._print_mem_status(item, aggregate_mem) for item in self._get_eighty(predicate)]
        )

        return (
            "\n***************resource usage summary***************\n"
            "****CPU:*****\n"
            f"    total: {aggregate_cpu:.1f} seconds\n"
            f"    {cpu_list}"
            "\n****Memory:****\n"
            f"    total: {aggregate_mem:.1f} gb\n"
            f"    {mem_list}"
            "\n******************************************************\n"
        )
