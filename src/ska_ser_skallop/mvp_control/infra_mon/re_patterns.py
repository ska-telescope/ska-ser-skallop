import re
from types import SimpleNamespace


class re_patterns(SimpleNamespace):
    start_with_equals = (re.compile(r"(?<==\s)(.*)"), re.DOTALL)
    select_dependencies = (re.compile(r"(?<=dependencies: )(.*)"), re.DOTALL)
    deselect_dependencies = (re.compile(r"(.*)(?=dependencies)"), re.DOTALL)
    contains_words = re.compile(r"\w")
    select_name_and_version = re.compile(r"(?<=:\s')([\w-]+|[\d\.]+)(?='\s)")
    select_name_or_version = re.compile(r"(?<=:\s')([\d\w\.-]*)(?=',|\w)")
    select_release_name = re.compile(r"(?<=chartinfo-)(.*)")
    select_json_file = re.compile(r"\w+\.json")
