"""Module dealing with more specific infra objects responsible for running tango devices."""

import json
import logging
import re
from abc import abstractmethod
from collections import UserDict
from functools import partial
from threading import Lock
from typing import Any, Callable, Dict, List, Literal, NamedTuple, Union, cast

from pipe import chain, select, traverse, where

from ska_ser_skallop.utils import piping

from . import base, helm
from .re_patterns import re_patterns
from .resource_stats import CPUStats, DeviceProfile, DevicesProfile, MemStats

logger = logging.getLogger(__name__)

_factory: base.AbstractFactory | None = None


def set_factory(factory: base.AbstractFactory):
    global _factory
    _factory = factory


def get_device_proxy(name: str, *args: Any, **kwargs: Any):
    global _factory
    assert _factory, "you need to first set a factory provider"
    return _factory.get_device_proxy(name, *args, **kwargs)


def get_devices_query():
    global _factory
    assert _factory, "you need to first set a factory provider"
    return _factory.get_devices_query()


class DeviceMapper:
    """Abstract class representing a cluster like object that can retrieve device information.

    This can be useful for objects making up a cluster that need to reference back in order to
    retrieve information about devices without creating circular referencing/deadlocking.
    """

    name: str

    @abstractmethod
    def get_device_deployment_status(
        self, device: str
    ) -> Literal["Running", "Pending", "Succeeded", "Failed", "Unknown"]:
        pass

    @property
    @abstractmethod
    def device_allocation(self) -> Dict[str, base.DeviceAllocation]:
        pass

    @property
    @abstractmethod
    def pods(self) -> Dict[str, base.V1Pod]:
        pass

    @abstractmethod
    def get_cpu_stats(self, pod: base.V1Pod) -> None | dict[str, CPUStats]:
        pass

    @abstractmethod
    def get_mem_stats(self, pod: base.V1Pod) -> None | dict[str, MemStats]:
        pass


class DeviceServerClass:
    """Represents information about a Tango Device Server."""

    def __init__(self, name: str, data: Dict, mapper: DeviceMapper) -> None:
        self.name = name
        self._data = data
        self._mapper = mapper
        self.chart = mapper.name

    @property
    def instances(self) -> Dict[str, "DeviceServer"]:
        """Get the particular class instances.

        :return: the particular class instances (e.g. exported servers).
        """
        return dict(
            list(self._data.items())
            | select(piping.append_to_tuple(self.name, self._mapper))
            | select(piping.instantiate(DeviceServer))
            | select(lambda item: (cast(DeviceServer, item).name, item))
        )


class Device:
    """Represents a Tango device object in the context of a kube k8s pod running in a cluster."""

    def __init__(
        self,
        name: str,
        data: Dict[str, Any],
        device_class: str,
        device_server: str,
        instance: str,
        device_mapper: DeviceMapper,
    ) -> None:
        self.name = name
        self._data = data
        self.device_class = device_class
        self.device_server = device_server
        self.instance = instance
        self._mapper = device_mapper
        self.chart = self._mapper.name
        self._pods = self._mapper.pods

    def __str__(self) -> str:
        return (
            f"{{name: {self.name}, class: {self.device_class}, "
            f"instance: {self.instance}, server: {self.device_server}, data: {self._data}}}"
        )

    def __repr__(self):
        return self.__str__()

    def get_profile(self) -> Union[None, "DeviceProfile"]:
        allocation = self._mapper.device_allocation[self.name]
        if pod := self._mapper.pods.get(allocation.pod):
            mem_stats = self._mapper.get_mem_stats(pod)
            cpu_stats = self._mapper.get_cpu_stats(pod)
            if any([mem_stats is None, cpu_stats is None]):
                return None
            return DeviceProfile(mem_stats, cpu_stats, self.chart)  # type: ignore
        return None

    @property
    def properties(self) -> Dict[str, Any]:
        """Get any properties specified at deployment time related to this Device.

        :return: the properties specified at deployment time related to this Device.
        """
        if properties := self._data.get("properties"):
            return properties
        return {}

    def get_device_deployment_status(
        self,
    ) -> Literal["Running", "Pending", "Succeeded", "Failed", "Unknown"]:
        """Get the status of the tango device in terms of its k8 deployemt within a pod.

        :return: the status of the tango device in terms of its k8 deployemt within a pod.
        """
        return self._mapper.get_device_deployment_status(self.name)


class DeviceClassInServer:
    """Represents the device as a class (not instance) used within a specific device server."""

    def __init__(
        self,
        name: str,
        data: Dict[str, Any],
        server: str,
        mapper: DeviceMapper,
    ) -> None:
        self.name = name
        self.server = server
        self._data = data
        self._mapper = mapper
        self.chart = mapper.name

    @property
    def devices_in_server(self) -> Dict[str, Device]:
        """Get a list of tango device instances of this class within this server instance.

        :return: a list of tango device instances of this class within this server instance.
        """
        # pylint: disable=no-value-for-parameter
        return dict(
            list(self._data.items())
            | select(piping.append_to_tuple(self.name, self.server, "", self._mapper))
            | select(piping.instantiate(Device))
            | select(lambda item: (cast(Device, item).name, item))
        )


class DeviceServer:
    def __init__(
        self,
        name: str,
        data: Dict[str, Any],
        server_class: str,
        mapper: DeviceMapper,
    ) -> None:
        self.name = name
        first_key = list(data.keys())[0]
        if first_key.find("/") > 0:
            # in case the devicserver has been created as a singleton device
            data = {first_key: data}
        self.server_class = server_class
        self._data = data
        self._mapper = mapper
        self.chart = mapper.name

    @property
    def qualified_name(self) -> str:
        """Get a qualified name namespaced by its class.

        :return: a qualified name namespaced by its class.
        """
        return f"{self.server_class}/{self.name}"

    @property
    def device_classes(self) -> Dict[str, DeviceClassInServer]:
        """Get the device classes (not instances) used within this specific device server instance.

        :return: the device classes (not instances) used within this specific device server.
        """
        # pylint: disable=no-value-for-parameter
        return dict(
            list(self._data.items())
            | select(piping.append_to_tuple(self.name, self._mapper))
            | select(piping.instantiate(DeviceClassInServer))
            | select(lambda item: (cast(DeviceServer, item).name, item))
        )

    @property
    def devices(self) -> Dict[str, Device]:
        """Get the device instances used within this specific device server instance.

        :return: the device instances used within this specific device server instance.
        """
        # pylint: disable=no-value-for-parameter
        return dict(
            self.device_classes.values()
            | select(lambda item: cast(DeviceClassInServer, item).devices_in_server.values())
            | traverse
            | select(lambda item: (cast(Device, item).name, item))
        )


class Devices(UserDict):
    """Groupings of tango devices as dictionaries capable of commanding the group as a whole."""

    def __init__(self, dict: Union[None, Dict[str, Device]] = None, /, **kwargs):
        super().__init__(dict, **kwargs)
        self.data: Dict[str, Device] = cast(Dict[str, Device], self.data)

    @property
    def names(self) -> List[str]:
        """The device names as strings."""
        return list(self.data.keys())

    @property
    def content(self) -> List[Device]:
        """The device content as device objects."""
        return list(self.data.values())

    def read_attributes(self, attr_name: str) -> Dict[str, Any]:
        """Read a given attribute of all the devices.

        :return: the corresponding attributes values for each device
        """
        device_query = get_devices_query()
        device_names = list(self.data.keys())
        try:
            return device_query.query(device_names, attr_name)
        except Exception as exception:
            logger.warning(exception.args)
            return {}

    @staticmethod
    def _ping_device(name: str) -> float:
        device = get_device_proxy(name)
        return device.ping()

    @staticmethod
    def _command_device(cmd_name: str, cmd_param: Any) -> Callable[[str], Any]:
        def wrapper(cmd_name: str, cmd_param: Any, device_name: str) -> Any:
            device = get_device_proxy(device_name)
            return device.command_inout(cmd_name, cmd_param)

        return partial(wrapper, cmd_name, cmd_param)

    @staticmethod
    def _write_attributes_device(attr_name: str, value: Any) -> Callable[[str], Any]:
        def wrapper(attr_name: str, value: Any, device_name: str) -> Any:
            device = get_device_proxy(device_name)
            return device.write_attribute(attr_name, value)

        return partial(wrapper, attr_name, value)

    def ping(self, in_series=False) -> Dict[str, float]:
        """Ping the set of devices.

        :param in_series: wethere the commands should be done consecutively in series
            , defaults to False
        :returns: The results of each command per device name
        """
        if in_series:
            return dict(
                zip(
                    self.names,
                    self.names | piping.serialize(self._ping_device),
                )
            )
        return dict(
            zip(
                self.names,
                self.names | piping.parallelize(self._ping_device),
            )
        )

    def command_inout(self, cmd_name: str, cmd_param=None, in_series=False) -> Dict[str, Any]:
        """Command the set of devices.

        :param in_series: wethere the commands should be done consecutively in series
            , defaults to False
        :returns: The results of each command per device name
        """
        if in_series:
            return dict(
                zip(
                    self.names,
                    self.names | piping.serialize(self._command_device(cmd_name, cmd_param)),
                )
            )
        return dict(
            zip(
                self.names,
                self.names | piping.parallelize(self._command_device(cmd_name, cmd_param)),
            )
        )

    @property
    def states(self) -> Dict[str, str]:
        """Get the the device States all the devices.

        :return: the corresponding tango device State values
        """
        device_query = get_devices_query()
        device_names = list(self.data.keys())
        try:
            result = device_query.query(device_names, "state")
            return result
        except Exception as exception:
            logger.warning(exception.args)
            return {}

    def write_attributes(self, attr_name: str, value: Any, in_series=False) -> Dict[str, Any]:
        """Write a given value to the named attribute for all the devices.

        :param in_series: wethere the write commands should be done consecutively in series
            , defaults to False
        :returns: The results of each write per device name
        """
        if in_series:
            return dict(
                zip(
                    self.names,
                    self.names | piping.serialize(self._write_attributes_device(attr_name, value)),
                )
            )
        return dict(
            zip(
                self.names,
                self.names | piping.parallelize(self._write_attributes_device(attr_name, value)),
            )
        )

    @property
    def not_ready(self) -> "Devices":
        """Get devices within this chart that are not ready."""
        return Devices(
            {
                states[0]: device
                for states, device in zip(self.states.items(), self.data.values())
                if states[1] in ["INIT", "UNKNOWN", "DISABLE"]
            }
        )

    @property
    def in_error(self) -> "Devices":
        """Get devices within this chart that in error."""
        return Devices(
            {
                states[0]: device
                for states, device in zip(self.states.items(), self.data.values())
                if states[1] == "FAULT"
            }
        )

    @property
    def ready(self) -> "Devices":
        """Get devices within this chart that are ready."""
        return Devices(
            {
                states[0]: device
                for states, device in zip(self.states.items(), self.data.values())
                if states[1] not in ["INIT", "FAULT"]
            }
        )


class _MapDisabled(NamedTuple):
    device: Device
    not_disabled: bool


class _DeviceProfileMapping(NamedTuple):
    device: Device
    profile: DeviceProfile | None


class DevicesChart(helm.Chart, DeviceMapper):
    """Represents a Helm chart capable of providing tango information and k8 resources info."""

    def __new__(cls: type["DevicesChart"], name: str, *_, **__) -> "DevicesChart":
        if derived_chart_cls := _derived_charts.get(name):
            return object.__new__(derived_chart_cls)
        return object.__new__(cls)

    def __init__(
        self, name: str, version: str, subsystem: str, cluster: "ClusterWithDevices"
    ) -> None:
        helm.Chart.__init__(self, name, version, subsystem, cluster)
        self._disabled_devices: List[str] = []
        self._devices = Devices()
        self._device_servers = {}
        self._device_allocation: Dict[str, base.DeviceAllocation] = {}
        self._lock_accessing_devices = Lock()

    def get_cpu_stats(self, pod: base.V1Pod) -> None | dict[str, CPUStats]:
        """"""
        return self._cluster.get_cpu_stats(pod)

    def get_mem_stats(self, pod: base.V1Pod) -> None | dict[str, MemStats]:
        """"""
        return self._cluster.get_mem_stats(pod)

    @staticmethod
    def _configmap_is_a_configurator(configmap: base.V1ConfigMap):
        """'configurator'"""
        if metadata := configmap.metadata:
            if hasattr(metadata, "labels"):
                if component := metadata.labels.get("domain"):
                    if component == "self-configuration":
                        return True
        return False

    @staticmethod
    def chart_has_devices(chart: helm.Chart) -> bool:
        return len(chart.deviceservers_crds) > 0 or any(
            chart.configmaps.values() | where(DevicesChart._configmap_is_a_configurator)
        )

    @staticmethod
    def _device_not_disabled(device: Device) -> bool:
        if allocation := DevicesChart._get_device_allocation(device.name):
            if allocation.pod_set:
                return True
        return False

    @staticmethod
    def _map_disabled(device: Device) -> _MapDisabled:
        return _MapDisabled(device, DevicesChart._device_not_disabled(device))

    @staticmethod
    def generate_devices(device_servers: list[DeviceServer]):
        prelim_devices = list(
            device_servers
            | select(lambda item: cast(dict, cast(DeviceServer, item).devices).values())
            | traverse
        )
        return Devices(
            dict(
                prelim_devices
                | piping.parallelize(DevicesChart._map_disabled)
                | where(lambda item: cast(_MapDisabled, item).not_disabled)
                | select(lambda item: cast(_MapDisabled, item).device)
                | select(lambda item: (cast(Device, item).name, item))
            )
        )

    @property
    def devices(self) -> Devices:
        # pylint: disable=no-value-for-parameter
        if self._devices:
            return self._devices
        if self.deviceservers_crds:
            self._devices = self._load_devices_from_deviceservers_crds()
        elif self.device_servers:
            self._devices = self._load_devices_from_deviceservers()
        else:
            self._devices = Devices({})
        return self._devices

    def _load_devices_from_deviceservers(self) -> Devices:
        prelim_devices: List[Device] = list(
            self.device_servers.values()
            | select(lambda item: cast(dict, cast(DeviceServer, item).devices).values())
            | traverse
        )
        if prelim_devices:
            return Devices(
                dict(
                    prelim_devices
                    # throttling added; otherwise the method spawns too many threads too quickly
                    | piping.parallelize(DevicesChart._map_disabled, 0.01)
                    | where(lambda item: cast(_MapDisabled, item).not_disabled)
                    | select(lambda item: cast(_MapDisabled, item).device)
                    | select(lambda item: (cast(Device, item).name, item))
                )
            )
        return Devices({})

    def _load_devices_from_deviceservers_crds(self) -> Devices:
        device_dict: Dict[str, Device] = {
            d.name: d
            for v in self.deviceservers_crds.values()
            for d in parse_dsconfig(json.loads(v.spec.config)["servers"], self)
        }
        prelim_devices: List[Device] = list(device_dict.values())
        if len(prelim_devices) > 0:
            devices = {}
            for d in prelim_devices:
                disabled = DevicesChart._map_disabled(d)
                if disabled.not_disabled:
                    devices[d.name] = d
            return Devices(devices)
        return Devices({})

    @property
    def device_servers(self) -> Dict[str, DeviceServer]:
        """Get all known device server instances used within this chart.

        :return: all known device server instances used within this chart.
        """
        # pylint: disable=no-value-for-parameter
        if self._device_servers or self.configmaps is None:
            return self._device_servers
        self._device_servers = dict(
            self.configmaps.values()
            | where(self._configmap_is_a_configurator)
            | select(lambda x: cast(Dict[str, Dict], cast(base.V1ConfigMap, x).data).items())
            | chain
            | where(piping.regex_true(re_patterns.select_json_file))
            | select(lambda x: json.loads(x[1]))
            | where(lambda x: x.get("servers"))
            | select(lambda x: x["servers"].items())
            | chain
            | select(piping.append_to_tuple(self))
            | select(piping.instantiate(DeviceServerClass))
            | select(lambda item: (cast(DeviceServerClass, item).instances.values()))
            | chain
            | select(lambda item: (cast(DeviceServer, item).qualified_name, item))
        )
        return self._device_servers

    @property
    def device_states(self) -> Dict[str, str]:
        """Get an aggregate of the devices and their corresponding state.

        :return: an aggregate of the devices and their corresponding state.
        """
        # pylint: disable=no-value-for-parameter
        return self.devices.states

    def get_device_deployment_status(
        self, device: str
    ) -> Literal["Running", "Pending", "Succeeded", "Failed", "Unknown"]:
        """Get the deployment status of a particular tango device for the pod containing it.

        :return: the status in terms of the deployment phase of the pod containing it.
        """
        device_allocation = self.device_allocation.copy()
        if device_allocation := device_allocation.get(device):
            pod_sets = {**self.deployments.copy(), **self.statefulsets.copy()}
            if device_pod_set := pod_sets.get(device_allocation.pod_set):
                return device_pod_set.phase
        return "Unknown"

    @staticmethod
    def _get_device_allocation(
        device_name: str,
    ) -> Union[None, base.DeviceAllocation]:
        device_db = get_device_proxy("sys/database/2", fast_load=True)
        info = device_db.command_inout("DbGetDeviceInfo", device_name)
        if not info:
            return None
        allocation_str: str = info[1][4]
        if allocation_str.startswith("ds"):
            parts: List[str] = allocation_str.split("-")
            if len(parts) < 3:
                return None
            if parts[-2].isdigit():
                pod_set = "-".join(parts[:-2])
                pod_instance = parts[-2]
            else:
                pod_set = "-".join(parts[:-1])
                pod_instance = parts[-1]
        else:
            pod_set = allocation_str | piping.regex_find_first_match(re.compile(r"(?<=\.)([\w-]+)"))
            pod_instance = allocation_str | piping.regex_find_first_match(
                re.compile(r"([\w-]+)(?=\.)")
            )
        return base.DeviceAllocation(device_name, pod_instance, pod_set)

    @staticmethod
    def _generate_device_allocation(devices: list[str]):
        return dict(
            list(devices)
            | piping.parallelize(DevicesChart._get_device_allocation)
            | where(lambda x: x)
            | select(lambda item: (cast(base.DeviceAllocation, item).device, item))
        )

    @property
    def device_allocation(self) -> Dict[str, base.DeviceAllocation]:
        """Get the devices and their corresponding allocation as deployments within this chart.

        :return: the devices and their corresponding allocation as deployments within this chart.
        """
        # pylint: disable=no-value-for-parameter

        if self.devices:
            with self._lock_accessing_devices:
                if self._device_allocation:
                    return self._device_allocation.copy()
                devices = self.devices.keys()
                self._device_allocation = self._generate_device_allocation(devices)
                return self._device_allocation.copy()
        return {}

    @property
    def devices_health(self) -> Literal["NOT_READY", "READY", "ERROR"]:
        """Return an aggregate deployment health of tango devices being deployed within this chart.

        :return: the aggregate deployment health of tango devices being deployed within this chart.
        """
        # pylint: disable=no-value-for-parameter
        device_query = get_devices_query()
        try:
            devices = list(self.devices.keys())
            device_states = device_query.query(devices, "state")
            if any(device_states.values() | select(lambda state: state == "INIT")):
                return "NOT_READY"
            if any(device_states.values() | select(lambda state: state == "FAULT")):
                return "ERROR"
            return "READY"
        except Exception:
            return "ERROR"

    def _get_device_profile(self, device: Device) -> _DeviceProfileMapping:
        return _DeviceProfileMapping(device, device.get_profile())

    def get_device_profile(self, device_name: str) -> DeviceProfile | None:
        if (device := self.devices.get(device_name)) is not None:
            device = cast(Device, device)
            return self._get_device_profile(device).profile

    def get_devices_profile(self) -> Dict[str, DeviceProfile | None]:
        return dict(
            self.devices.values()
            | piping.parallelize(self._get_device_profile)
            | select(
                lambda item: (
                    cast(_DeviceProfileMapping, item).device.name,
                    cast(_DeviceProfileMapping, item).profile,
                )
            )
        )

    def get_devices_not_ready(self) -> List[str]:
        """Get devices within this chart that are not ready."""
        return list(self.devices.not_ready.names)

    def get_devices_in_error(self) -> List[str]:
        """Get devices within this chart that in error."""
        return list(self.devices.in_error.names)

    def get_devices_ready(self) -> List[str]:
        """Get devices within this chart that are ready."""
        return list(self.devices.ready.names)


class CBFMidChart(DevicesChart):
    """A specific devices derived chart representing the ska-mid-cbf chart."""

    def __init__(
        self, name: str, version: str, subsystem: str, cluster: "ClusterWithDevices"
    ) -> None:
        super().__init__(name, version, subsystem, cluster)
        self._disabled_devices.append("sys/access_control/1")


_derived_charts = {"ska-mid-cbf": CBFMidChart}


class BaseTangoClusterWithCharts(helm.BaseClusterWithCharts):
    """Abstract class resembling an cluster object that can only retrieve charts and sub charts.

    This is to prevent circular dependencies/self referencing deadlocks between an object
    (e.g. Release).
    needing to use the cluster to get info about charts but for which the chart itself which
    may reference the same object.
    """

    @property
    @abstractmethod
    def devices(self) -> Devices:
        """"""


class ClusterWithDevices(BaseTangoClusterWithCharts, helm.ClusterWithCharts):
    """Represents a kubernetes helm deployed cluster containing tango devices."""

    chart_cls = DevicesChart

    def __init__(self, chart_subsystem_mapping: Dict[str, str] = None) -> None:
        super().__init__(chart_subsystem_mapping)
        self._devices: Devices | None = None
        self._devices_thread = Lock()

    def get_new_chart(self, name: str, version: str) -> Union[helm.Chart, DevicesChart]:
        subsystem = self.subsystem_for_chart(name)
        base_chart_cls = helm.ClusterWithCharts.chart_cls
        base_chart = base_chart_cls(name, version, subsystem, self)
        if DevicesChart.chart_has_devices(base_chart):
            return self.chart_cls(name, version, subsystem, self)
        return base_chart

    @property
    def devices(self) -> Devices:
        """Get all tango devices contained within this cluster.

        :return: all tango devices contained within this cluster.
        """
        with self._devices_thread:
            if self._devices is None:
                devices = Devices()
                for chart in self.sub_charts.values():
                    if isinstance(chart, DevicesChart):
                        devices = Devices({**devices, **chart.devices})
                self._devices = devices
            return self._devices

    @property
    def release(self) -> Union[None, "TangoBasedRelease"]:
        if base_release := super().release:
            return TangoBasedRelease(base_release.name, self)
        return None

    @property
    def chart(self) -> Union[None, DevicesChart]:
        chart = super().chart
        return cast(DevicesChart, chart)


class TangoBasedRelease(helm.Release, base.DeviceProfiler):
    """A helm release within current cluster capable of containing tango device info."""

    def __init__(self, name: str, cluster: BaseTangoClusterWithCharts) -> None:
        super().__init__(name, cluster)
        self._cluster = cluster

    @property
    def devices(self) -> Devices:
        """Get all tango devices contained within this cluster.

        :return: all tango devices contained within this cluster.
        """
        return self._cluster.devices

    @property
    def device_states(self) -> Dict[str, str]:
        """Get an aggregate of the devices and their corresponding state within this release.

        :return: an aggregate of the devices and their corresponding state.
        """
        device_status = {}
        for chart in self.sub_charts.values():
            if isinstance(chart, DevicesChart):
                device_status = {**device_status, **chart.device_states}
        return device_status

    @property
    def devices_health(self) -> Literal["NOT_READY", "READY", "ERROR"]:
        """Return aggregate deployment health of tango devices being deployed within this release.

        :return: aggregate deployment health of tango devices being deployed within this release.
        """
        devices_health = set()
        for chart in self.sub_charts.values():
            if isinstance(chart, DevicesChart):
                devices_health.add(chart.devices_health)
        if "ERROR" in devices_health:
            return "ERROR"
        if "NOT_READY" in devices_health:
            return "NOT_READY"
        return "READY"

    def get_devices_not_ready(self) -> Dict[str, DevicesChart]:
        """Get devices within this release that are not ready."""
        devices_not_ready = {}
        for chart in self.sub_charts.values():
            if isinstance(chart, DevicesChart):
                devices_not_ready = {
                    **devices_not_ready,
                    **{device: chart for device in chart.get_devices_not_ready()},
                }
        return devices_not_ready

    def get_devices_in_error(self) -> Dict[str, DevicesChart]:
        """Get devices within this release that in error."""
        devices_in_error = {}
        for chart in self.sub_charts.values():
            if isinstance(chart, DevicesChart):
                devices_in_error = {
                    **devices_in_error,
                    **{device: chart for device in chart.get_devices_in_error()},
                }
        return devices_in_error

    def get_devices_profile(self) -> "DevicesProfile":
        devices_profile: Dict[str, DeviceProfile] = {}
        sub_charts = list(
            chart for chart in self.sub_charts.values() if isinstance(chart, DevicesChart)
        )
        for chart in sub_charts:
            chart_profile = {
                device: item
                for device, item in chart.get_devices_profile().items()
                if item is not None
            }
            devices_profile = {**devices_profile, **chart_profile}
        return DevicesProfile(devices_profile)

    def get_devices_ready(self) -> Dict[str, DevicesChart]:
        """Get devices within this release that are ready."""
        devices_ready = {}
        for chart in self.sub_charts.values():
            if isinstance(chart, DevicesChart):
                devices_ready = {
                    **devices_ready,
                    **{device: chart for device in chart.get_devices_ready()},
                }
        return devices_ready

    def get_devices_in_charts(self) -> Dict[str, Devices]:
        devices_in_charts = {}
        for chart in self.sub_charts.values():
            if isinstance(chart, DevicesChart):
                devices_in_charts = {
                    **devices_in_charts,
                    **{chart.name: chart.devices},
                }
        return devices_in_charts

    def get_device_profile(self, device_name: str):
        if device := self.devices.get(device_name):
            chart_name = cast(Device, device).chart
            if chart := self.sub_charts.get(chart_name):
                return cast(DevicesChart, chart).get_device_profile(device_name)


def parse_dsconfig(servers: Dict[str, Dict], device_mapper: DeviceMapper) -> List[Device]:
    return [
        d
        for server_name, server in servers.items()
        for d in parse_devices(server_name, server, device_mapper)
    ]


def parse_devices(server_name: str, server: dict, device_mapper: DeviceMapper) -> List[Device]:
    server_name_split = server_name.split("/")
    if len(server_name_split) == 2:
        server_name = server_name_split[0]
        instance_name = server_name_split[1]
        return _parse_devices(server_name, instance_name, server, device_mapper)
    return [
        d
        for instance_name, instance in server.items()
        for d in _parse_devices(server_name, instance_name, instance, device_mapper)
    ]


def _parse_devices(
    server_name, instance_name, instance: Dict[str, Dict], device_mapper: DeviceMapper
) -> List[Device]:
    return [
        Device(device_name, data, class_name, server_name, instance_name, device_mapper)
        for class_name, clss in instance.items()
        for device_name, data in clss.items()
    ]
