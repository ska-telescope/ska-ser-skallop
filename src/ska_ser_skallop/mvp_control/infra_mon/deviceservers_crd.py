from typing import Dict, List

from pydantic import BaseModel


class Fields(BaseModel):
    apiVersion: str
    fieldsType: str
    fieldsV1: dict
    manager: str
    operation: str
    subresource: str = ""
    time: str


class Metadata(BaseModel):
    annotations: Dict[str, str] = {}
    creationTimestamp: str = ""
    generation: int = 1
    labels: Dict[str, str] = {}
    managedFields: List[Fields] = []
    name: str = ""
    namespace: str = ""
    resourceVersion: str = ""
    uid: str = ""


class ResourceLimits(BaseModel):
    cpu: str
    memory: str


class Resources(BaseModel):
    limits: ResourceLimits
    requests: ResourceLimits


class Spec(BaseModel):
    args: str = ""
    clusterDomain: str = ""
    command: str = ""
    config: str = ""
    databaseds: str = ""
    dependsOn: List[str] = []
    dsname: str = ""
    enableLoadBalancer: bool = False
    eventport: int = 0
    heartbeatport: int = 0
    image: str = ""
    imagePullPolicy: str = "Always"
    legacycompatibility: bool = False
    orbport: int = 0
    resources: Resources = Resources(
        limits=ResourceLimits(cpu="", memory=""), requests=ResourceLimits(cpu="", memory="")
    )


class Status(BaseModel):
    replicas: int = 1
    resources: str = ""
    state: str = ""
    succeeded: int = 1


class DeviceServerCRD(BaseModel):
    apiVersion: str = ""
    kind: str = ""
    metadata: Metadata
    spec: Spec
    status: Status


class DeviceServersCRD(BaseModel):
    apiVersion: str
    items: List[DeviceServerCRD]
    kind: str
