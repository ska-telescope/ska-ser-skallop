"""Module dealing with getting basic level information about a kubernetes cluster."""

import logging
import os
import re
from contextlib import contextmanager
from typing import Dict, Tuple, TypeVar, Union, cast

from kubernetes import client, config
from kubernetes.client.exceptions import ApiException
from kubernetes.stream import stream
from pipe import select, where

from . import base
from .deviceservers_crd import DeviceServerCRD, DeviceServersCRD
from .resource_stats import CPUStats, MemStats

T = TypeVar("T")

Y = TypeVar("Y")

logger = logging.getLogger(__name__)


class UnauthorizedKubernetesConnection(Exception):
    pass


class ShellExecutionInPodException(Exception):
    pass


class WSExecutionResult:
    def __init__(
        self,
        ws_client: base.WSClient,
        stdout: str | None = None,
        stderr: str | None = None,
    ) -> None:
        self._stdout = stdout
        self._stderr = stderr
        self._client = ws_client

    def __bool__(self) -> bool:
        return self._client.returncode == 0

    def command_result_err(self) -> bool:
        return self._stderr is not None

    @property
    def return_code(self) -> int:
        assert isinstance(self._client.returncode, int)
        return self._client.returncode

    @property
    def command_result(self) -> str:
        if self.command_result_err():
            assert isinstance(self._stderr, str)
            return self._stderr
        assert isinstance(self._stdout, str)
        return self._stdout


def get_output_from_ws_client(client: base.WSClient, suppress_error: bool = True):
    stdout: str = ""
    stderr: str | None = None
    while client.is_open():
        try:
            client.update(timeout=1)
            if client.peek_stdout():
                stdout = cast(str, client.read_stdout())
            if client.peek_stderr():
                stderr = cast(str, client.read_stderr())
        except Exception as exception:
            if suppress_error:
                pass
            else:
                raise exception
    client.close()
    result = WSExecutionResult(client, stdout, stderr)
    return result


class Cluster:
    """Represents a kubernetes cluster."""

    def __init__(self) -> None:
        """Initialise the object."""
        if os.getenv("USE_POD_KUBECONFIG"):
            self._kube_config_file = "internal to pod"
            logger.info("loading kubeconfig from pod service account")
            assert (
                namespace := os.getenv("KUBE_NAMESPACE")
            ), "you need to set an namespace value when running in a pod"
            self._namespace = namespace
            config.load_incluster_config()
        else:
            if not (namespace := os.getenv("KUBE_NAMESPACE")):
                namespace = "default"
                if kube_branch := os.getenv("KUBE_BRANCH"):
                    if tel := os.getenv("TEL"):
                        namespace = f"ci-ska-skampi-{kube_branch}-{tel}"
            self._namespace = namespace
            if not (kube_config_file := os.getenv("KUBECONFIG")):
                logger.info("loading kubeconfig from home dir")
            config.load_kube_config(kube_config_file)
            self._kube_config_file = kube_config_file
        self._api_v1 = client.CoreV1Api()
        self._custom_api_v1 = client.CustomObjectsApi()
        self._api_appsv1 = client.AppsV1Api()
        self._ping()
        self._config_maps: None | base.V1ConfigMapList = None
        self._statefulsets: None | base.V1StatefulSetList = None
        self._services: None | base.V1ServiceList = None
        self._deployments: None | base.V1DeploymentList = None
        self._pods: None | base.V1PodList = None
        self._deviceservers_crds: None | DeviceServersCRD = None

    @property
    def config_maps(self) -> base.V1ConfigMapList:
        if self._config_maps is not None:
            return self._config_maps
        self._config_maps = self._get_all_config_maps()
        return self._config_maps

    @property
    def statefulsets(self) -> base.V1StatefulSetList:
        if self._statefulsets is not None:
            return self._statefulsets
        self._statefulsets = self._get_all_statefulsets()
        return self._statefulsets

    @property
    def services(self) -> base.V1ServiceList:
        if self._services is not None:
            return self._services
        self._services = self._get_all_services()
        return self._services

    @property
    def deployments(self) -> base.V1DeploymentList:
        if self._deployments is not None:
            return self._deployments
        self._deployments = self._get_all_deployments()
        return self._deployments

    @property
    def pods(self) -> base.V1PodList:
        if self._pods is not None:
            return self._pods
        self._pods = self._get_all_pods()
        return self._pods

    @property
    def deviceservers_crds(self) -> DeviceServersCRD:
        if self._deviceservers_crds is not None:
            return self._deviceservers_crds
        self._deviceservers_crds = self._get_all_deviceservers_crds()
        return self._deviceservers_crds

    def _ping(self):
        try:
            self._api_v1.get_api_resources()
        except ApiException as exception:
            if exception.reason == "Unauthorized":
                raise UnauthorizedKubernetesConnection(
                    "You do not seem to have authorization "
                    "to connect to this cluster, "
                    f"did you specify the correct kubeconfig file? ({self._kube_config_file})"
                ) from exception
            raise exception

    def _execute_shell_within_pod(
        self, pod: base.V1Pod, sh_command: str, suppress_error: bool = True
    ) -> dict[str, WSExecutionResult]:
        exec_command = ["/bin/sh", "-c", sh_command]
        metadata = cast(base.V1ObjectMeta, pod.metadata)
        results: dict[str, WSExecutionResult] = {}
        for container in cast(list[base.V1Container], cast(base.V1PodSpec, pod.spec).containers):
            try:
                client = cast(
                    base.WSClient,
                    stream(
                        self._api_v1.connect_get_namespaced_pod_exec,
                        metadata.name,
                        metadata.namespace,
                        command=exec_command,
                        container=container.name,
                        stderr=True,
                        stdin=False,
                        stdout=True,
                        tty=False,
                        _preload_content=False,
                    ),
                )
            except Exception as exception:
                if suppress_error:
                    return results
                else:
                    raise exception
            result = get_output_from_ws_client(client, suppress_error)
            if not result:
                raise ShellExecutionInPodException(
                    f"Unable to execute shell command {sh_command}"
                    f"in container {container}. Client return code: {result.return_code}"
                )
            results[cast(str, container.name)] = result
        return results

    def get_cpu_stats(self, pod: base.V1Pod) -> None | dict[str, CPUStats]:
        results = self._execute_shell_within_pod(pod, "cat /sys/fs/cgroup/cpu.stat")
        cpu_stats: dict[str, CPUStats] = {}
        for container, result in results.items():
            if result.command_result_err():
                pass
            else:
                cpu_stats[container] = CPUStats(result.command_result)
        return cpu_stats

    def get_mem_stats(self, pod: base.V1Pod) -> None | dict[str, MemStats]:
        results = self._execute_shell_within_pod(pod, "cat /sys/fs/cgroup/memory.stat")
        mem_stats: dict[str, MemStats] = {}
        for container, result in results.items():
            if result.command_result_err():
                pass
            else:
                mem_stats[container] = MemStats(result.command_result)
        return mem_stats

    def get_pods(self, **label_selectors: str) -> Dict[str, base.V1Pod]:
        """List the set of pod names defined within the given namespace.
        :return: kubernetes V1Pod
        """
        pod_list = self.pods
        return self._map_to_item_names(pod_list.items, **label_selectors)

    def get_deployments(self, **label_selectors: str) -> Dict[str, base.V1Deployment]:
        """List the set deployment names defined within the given namespace."""
        deployment_list = self.deployments
        return self._map_to_item_names(deployment_list.items, **label_selectors)

    def get_services(self, **label_selectors: str) -> Dict[str, base.V1Service]:
        """List the set of service names defined within the given namespace."""
        servies_list = self.services
        return self._map_to_item_names(servies_list.items, **label_selectors)

    def get_configmaps(self, **label_selectors: str) -> Dict[str, base.V1ConfigMap]:
        """List the set of configmap names defined within the given namespace."""
        configmap_list = self.config_maps
        return self._map_to_item_names(configmap_list.items, **label_selectors)

    def get_statefulsets(self, **label_selectors: str) -> Dict[str, base.V1StatefulSet]:
        """List the set of configmap names defined within the given namespace."""
        statefulset_list = self.statefulsets
        return self._map_to_item_names(statefulset_list.items, **label_selectors)

    def get_deviceservers_crds(self, **label_selectors: str) -> Dict[str, DeviceServerCRD]:
        """List the set of deviceserver crds defined within the given namespace."""
        deviceservers_crds_list = self.deviceservers_crds
        filtered = self._map_to_item_names(deviceservers_crds_list.items, **label_selectors)
        return filtered

    @staticmethod
    def _value_as_regex(value: str) -> str:
        result = re.findall(r"re/(.*)/", value)
        if len(result) > 0:
            return result[0]
        return ""

    def _values_match(self, desired: str, actual: str) -> bool:
        if desired_as_regex := self._value_as_regex(desired):
            if re.findall(desired_as_regex, actual):
                return True
            return False
        return desired == actual

    def _values_does_not_match(self, desired: str, actual: str) -> bool:
        if desired_as_regex := self._value_as_regex(desired):
            if re.findall(desired_as_regex, actual):
                return False
            return True
        return desired != actual

    def _map_to_item_names(self, items: Union[None, T], **label_selectors: str) -> Dict[str, T]:
        # pylint: disable=no-value-for-parameter

        def any_label_found(label_selectors: Dict[str, str], labels: Dict[str, str]):
            if label_selectors:
                for label_name, label_value in label_selectors.items():
                    if label_name in labels.keys():
                        if self._values_match(label_value, labels[label_name]):
                            return True
            return False

        def all_labels_found(all_off: Dict[str, str], labels: Dict[str, str]):
            if all_off:
                if all(label in labels.keys() for label in all_off):
                    for label_name, label_value in all_off.items():
                        if self._values_does_not_match(label_value, labels[label_name]):
                            return False
                    return True
                return False
            return False

        def label_equals_selector(item: base.AbstractResource) -> bool:
            if label_selectors:
                label_selectors_copy = label_selectors.copy()
                if "name" in label_selectors_copy:
                    label_value = label_selectors_copy.pop("name")
                    if self._values_match(label_value, item.metadata.name):
                        return True
                if label_selectors_copy:
                    if hasattr(item.metadata, "labels"):
                        if labels := cast(base.AbstractMetadataWithLabels, item.metadata).labels:
                            all_off = {}
                            if "all_of" in label_selectors_copy:
                                all_off = cast(
                                    Dict[str, str],
                                    label_selectors_copy.pop("all_of"),
                                )
                            if any_label_found(label_selectors_copy, labels):
                                return True
                            if all_labels_found(all_off, labels):
                                return True
                        return False
                return False
            return True

        def set_as_tuple(
            item: base.AbstractResource,
        ) -> Tuple[str, base.AbstractResource]:
            return (item.metadata.name, item)

        if items:
            return dict(items | where(label_equals_selector) | select(set_as_tuple))
        return {}

    @contextmanager
    def _api_error_handling(self):
        try:
            yield
        except ApiException as exception:
            if exception.reason == "Unauthorized":
                raise UnauthorizedKubernetesConnection(
                    "You do not seem to have authorization "
                    "to connect to this cluster, "
                    f"did you specify the correct kubeconfig file? ({self._kube_config_file});"
                    f"also check that your namespace is correct: {self._namespace}"
                ) from exception
            raise exception

    def _get_all_pods(self) -> base.V1PodList:
        with self._api_error_handling():
            return self._api_v1.list_namespaced_pod(self._namespace)

    def _get_all_config_maps(self) -> base.V1ConfigMapList:
        with self._api_error_handling():
            return self._api_v1.list_namespaced_config_map(self._namespace)

    def _get_all_services(self) -> base.V1ServiceList:
        with self._api_error_handling():
            return self._api_v1.list_namespaced_service(self._namespace)

    def _get_all_deployments(self) -> base.V1DeploymentList:
        with self._api_error_handling():
            return self._api_appsv1.list_namespaced_deployment(self._namespace)

    def _get_all_statefulsets(self) -> base.V1StatefulSetList:
        with self._api_error_handling():
            return self._api_appsv1.list_namespaced_stateful_set(self._namespace)

    def _get_all_deviceservers_crds(self) -> base.V1ConfigMapList:
        with self._api_error_handling():
            result = self._custom_api_v1.list_namespaced_custom_object(
                group="tango.tango-controls.org",
                version="v1",
                namespace=self._namespace,
                plural="deviceservers",
            )
            ds = DeviceServersCRD(
                apiVersion=result["apiVersion"], items=result["items"], kind=result["kind"]
            )
            return ds
