import abc
from typing import Dict, List, Literal, NamedTuple, Union

from kubernetes import client, config
from kubernetes.client.api.apps_v1_api import AppsV1Api
from kubernetes.client.api.core_v1_api import CoreV1Api
from kubernetes.client.models.v1_config_map import V1ConfigMap
from kubernetes.client.models.v1_config_map_list import V1ConfigMapList
from kubernetes.client.models.v1_container import V1Container
from kubernetes.client.models.v1_deployment import V1Deployment
from kubernetes.client.models.v1_deployment_list import V1DeploymentList
from kubernetes.client.models.v1_deployment_spec import V1DeploymentSpec
from kubernetes.client.models.v1_deployment_status import V1DeploymentStatus
from kubernetes.client.models.v1_label_selector import V1LabelSelector
from kubernetes.client.models.v1_object_meta import V1ObjectMeta
from kubernetes.client.models.v1_pod import V1Pod
from kubernetes.client.models.v1_pod_list import V1PodList
from kubernetes.client.models.v1_pod_spec import V1PodSpec
from kubernetes.client.models.v1_pod_status import V1PodStatus
from kubernetes.client.models.v1_service import V1Service
from kubernetes.client.models.v1_service_list import V1ServiceList
from kubernetes.client.models.v1_stateful_set import V1StatefulSet
from kubernetes.client.models.v1_stateful_set_list import V1StatefulSetList
from kubernetes.client.models.v1_stateful_set_spec import V1StatefulSetSpec
from kubernetes.client.models.v1_stateful_set_status import V1StatefulSetStatus
from kubernetes.stream.ws_client import WSClient

from ...connectors.base import AbstractFactory
from .resource_stats import DeviceProfile

__all__ = (
    "client",
    "config",
    "V1PodList",
    "V1Pod",
    "V1PodStatus",
    "V1DeploymentList",
    "V1Deployment",
    "V1DeploymentSpec",
    "V1StatefulSetList",
    "V1StatefulSet",
    "V1StatefulSetSpec",
    "V1ConfigMapList",
    "V1ConfigMap",
    "V1ServiceList",
    "V1Service",
    "V1LabelSelector",
    "V1DeploymentStatus",
    "V1StatefulSetStatus",
    "CoreV1Api",
    "AppsV1Api",
    "V1ObjectMeta",
    "V1PodSpec",
    "V1Container",
    "WSClient",
    "DeviceProfile",
    "AbstractFactory",
)

ResourceHealth = Literal["READY", "PENDING", "ERROR", "UNKNOWN"]


class DeviceAllocation(NamedTuple):
    device: str
    pod: str
    pod_set: str


class AbstractMetadata:
    name: str


class AbstractMetadataWithLabels(AbstractMetadata):
    labels: Dict[str, str]


class AbstractResource:
    metadata: AbstractMetadata


Items = Union[None, List[AbstractResource]]


class DeviceProfiler:
    @abc.abstractmethod
    def get_device_profile(self, device_name: str) -> DeviceProfile | None:
        """"""
