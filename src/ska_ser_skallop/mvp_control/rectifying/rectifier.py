import logging
from contextlib import contextmanager
from queue import Empty, Queue
from typing import Any, Callable, List, Union, cast
from unittest import mock

from ska_ser_skallop.connectors import configuration
from ska_ser_skallop.connectors.base import AbstractDeviceProxy
from ska_ser_skallop.mvp_control.describing.mvp_names import DeviceName, Masters, Mid
from ska_ser_skallop.subscribing.base import CHANGE_EVENT
from ska_ser_skallop.subscribing.helpers import get_attr_value_as_str
from ska_ser_skallop.utils.generic_classes import Symbol
from ska_ser_skallop.utils.singleton import Singleton

logger = logging.getLogger(__name__)


@contextmanager
def atomic(
    proxy: AbstractDeviceProxy,
    attr: str,
    end_state: str,
    io_call: str = "io call",
):
    queue = Queue()

    def callback(event: Any):
        queue.put_nowait(event)

    if not proxy.is_attribute_polled(attr):
        proxy.poll_attribute(attr, 1000)
    sub_id = proxy.subscribe_event(attr, CHANGE_EVENT, callback)
    yield
    while True:
        try:
            event = cast(Any, queue.get(timeout=5))
        except Empty as exception:
            raise Exception(
                f"timed out waiting for"
                f" {proxy.name()} for an event on {attr}"
                f" after 5 seconds (should become {end_state})"
                f" whilst performing {io_call}"
            ) from exception
        value = get_attr_value_as_str(event.attr_value)
        if value == end_state:
            proxy.unsubscribe_event(sub_id)
            return


class Rectifier:
    @staticmethod
    def _proxy(name: Union[str, Symbol]) -> AbstractDeviceProxy:
        return configuration.get_device_proxy(name)

    def switch_on(self, off_devices: list[str]) -> None:
        for device in off_devices:
            try:
                for master_device in Masters().subtract("tm"):
                    if device == master_device:
                        if "sdp" in device:
                            self._proxy(device).command_inout("On")
                        else:
                            self._proxy(device).command_inout("On", "")
            # TODO determine more exact exception
            except Exception as exception:
                logger.exception(exception)
        logger.warning(f"{off_devices} have been switched ON")

    def switch_off(self, off_devices: list[DeviceName]) -> None:
        if off_devices:
            successfully_commanded_devices = []
            for device in off_devices:
                try:
                    for master_device in Masters().subtract("tm"):
                        if device == master_device:
                            self._proxy(device).command_inout("Off")
                            successfully_commanded_devices.append(device)
                except Exception as exception:
                    logger.exception(exception)
            logger.warning(f"{successfully_commanded_devices} have been switched OFF")

    def standby_masters(self, devices: list[DeviceName]):
        if Mid.csp.controller in devices:
            self._proxy(Mid.csp.controller).command_inout("Standby", "")
            logger.warning(f"{Mid.csp.controller} have been switched to Standby")
        elif Mid.csp.cbf.controller in devices:
            self._proxy(Mid.csp.cbf.controller).command_inout("Standby")
        logger.warning(f"{devices} have been switched to Standby")

    def restart_faulty_subarrays(self, devices: List[str]) -> None:
        for device in devices:
            dev_p = self._proxy(device)
            with atomic(dev_p, "obsState", "EMPTY", "restart command"):
                dev_p.command_inout("Restart")
            logger.warning(f"{device} have been restarted")

    def reset_aborted_subarrays(self, devices: List[str]) -> None:
        for device in devices:
            dev_p = self._proxy(device)
            with atomic(dev_p, "obsState", "IDLE", "reset command"):
                dev_p.command_inout("ObsReset")
            logger.warning(f"{device} have been resetted")

    def reset_faulty_subarrays(self, devices: List[str]) -> None:
        for device in devices:
            dev_p = self._proxy(device)
            with atomic(dev_p, "obsState", "IDLE", "reset command from fault state"):
                dev_p.command_inout("ObsReset")
            logger.warning(f"{device} have been resetted")

    def abort_and_restart_subarrays(self, devices: List[str]) -> None:
        for device in devices:
            dev_p = self._proxy(device)
            with atomic(dev_p, "obsstate", "ABORTED", "abort command"):
                dev_p.command_inout("Abort")
            logger.warning(f"{device} have been aborted")
            with atomic(dev_p, "obsstate", "EMPTY", "restart command"):
                dev_p.command_inout("Restart")
            logger.warning(f"{device} have been restarted")

    def abort_and_reset_subarrays(self, devices: List[str]) -> None:
        for device in devices:
            dev_p = self._proxy(device)
            with atomic(dev_p, "obsstate", "ABORTED", "abort command"):
                dev_p.command_inout("Abort")
            logger.warning(f"{device} have been aborted")
            with atomic(dev_p, "obsstate", "IDLE", "reset command from aborted"):
                dev_p.command_inout("ObsReset")
            logger.warning(f"{device} have been reset")

    # TODO: Confirm if skalow devices accept the command in reset
    def rectify_faulty_devices(self, devices: List[str], end_state: str) -> None:
        for device in devices:
            self._proxy(device).command_inout("Reset")
            if end_state == "ON":
                logger.warning(f"{device} have been reset, will switch it ON")
                self._proxy(device).command_inout("On")
            else:
                logger.warning(f"{device} have been reset")

    def switch_off_subarrays(self, devices: List[str]) -> None:
        for device in devices:
            self._proxy(device).command_inout("Off")
        logger.warning(f"{devices} have been switched OFF")

    def switch_off_resources(self, devices: List[str]) -> None:
        for device in devices:
            self._proxy(device).command_inout("Off")
        logger.warning(f"{devices} have been switched OFF")

    def switch_on_resources(self, devices: List[str]) -> None:
        for device in devices:
            self._proxy(device).command_inout("On")
        logger.warning(f"{devices} have been switched on")

    def switch_off_dish_masters(self, dishes: List[str]) -> None:
        for dish in dishes:
            dev_p = self._proxy(dish)
            # in case there is an inconsistency with states (e.g. dev state is ON but
            # dishMode is STANDBY_LP)
            # set the dishmode first to FP before setting it LP as a means to get dev
            # state to STANDBY
            if dev_p.read_attribute("dishmode").name == "STANDBY_LP":
                with atomic(dev_p, "dishmode", "STANDBY_FP", "SetStandbyFPMode command"):
                    dev_p.command_inout("SetStandbyFPMode")
            with atomic(dev_p, "state", "STANDBY", "SetStandbyLPMode command"):
                dev_p.command_inout("SetStandbyLPMode")
        logger.warning(f"{dishes} have been switched OFF")

    def switch_on_dish_masters(self, dishes: List[str]) -> None:
        for dish in dishes:
            dev_p = self._proxy(dish)
            with atomic(dev_p, "dishmode", "STANDBY_FP", "SetStandbyFPMode command"):
                dev_p.command_inout("SetStandbyFPMode")
            with atomic(dev_p, "state", "ON", "SetOperateMode command"):
                dev_p.command_inout("SetOperateMode")
        logger.warning(f"{dishes} have been switched ON")


@contextmanager
def patch_rectifier(wrapping: bool = False):
    if wrapping:
        mock_rectifier = mock.Mock(Rectifier, wraps=Rectifier())
    else:
        mock_rectifier = mock.Mock(Rectifier)
    inject_rectifier_cls(mock_rectifier)
    yield mock_rectifier.return_value
    reset_configurations()


def inject_rectifier_cls(rectifier_cls: Callable[[], Rectifier]):
    factory = Factory()  # type:ignore
    factory.inject_rectifier_cls(rectifier_cls)
    rectifier_cls = factory.get_rectifier_cls()
    return rectifier_cls()


def reset_configurations():
    factory = Factory()  # type:ignore
    factory.reset()


def create_rectifier():
    factory = Factory()  # type:ignore
    rectifier_cls = factory.get_rectifier_cls()
    return rectifier_cls()


class Factory(metaclass=Singleton):
    def __init__(self) -> None:
        self._rectifier_cls = None

    def reset(self):
        self.__init__()

    def inject_rectifier_cls(self, rectifier_cls: Callable[[], Rectifier]):
        self._rectifier_cls = rectifier_cls

    def get_rectifier_cls(self):
        if self._rectifier_cls:
            return self._rectifier_cls
        return Rectifier
