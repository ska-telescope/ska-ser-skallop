import functools
from abc import abstractmethod
from functools import reduce
from typing import Any, Callable, Dict, List, ParamSpec, Type, TypedDict, TypeVar

from ska_ser_skallop.subscribing.base import AttributeInt, Producer

P = ParamSpec("P")
T = TypeVar("T")


def stringify(the_list: List[str]) -> str:
    if the_list:
        return reduce(lambda x, y: f"{x}\n{y}", the_list)
    return "none"


class SideCarException(Exception):
    """Used to signal an general exception occurred during a particular
    routine, the routine of which is specified by having a more specific exception class
    derived from this one. The handler of the exception can then retrieve the original
    general exception that was tagged along with the additional knowledge of when it
    occurred
    """

    def __init__(self, base_exception: Exception) -> None:
        self.base_exception = base_exception


def except_it(E: Type[SideCarException]):
    def decorator(func: Callable[P, T]):  # type: ignore
        @functools.wraps(func)
        def wrapper(*args: P.args, **kwargs: P.kwargs) -> T:
            try:
                result = func(*args, **kwargs)
            except Exception as e:
                raise E(e) from e
            return result

        return wrapper

    return decorator


class Spec(TypedDict):
    name: str
    attr: str


class AbstractDevicesQuery:
    @abstractmethod
    def query(self, device_list: List[str], attr: str, timeout: float = 3) -> Dict[str, Any]:
        pass

    @abstractmethod
    def complex_query(self, device_specs: List[Spec], timeout: float = 3) -> Dict[str, Any]:
        pass


class AbstractDeviceProxy(Producer):
    @abstractmethod
    def read_attribute(self, attr_name: str, *args: Any, **kwargs: Any) -> AttributeInt:
        pass

    @abstractmethod
    def ping(self) -> float:
        pass

    @abstractmethod
    def command_inout(self, cmd_name: str, cmd_param: Any = None, *args: Any, **kwargs: Any) -> Any:
        pass

    @abstractmethod
    def State(self) -> str:
        pass

    @abstractmethod
    def write_attribute(self, attr_name: str, value: Any, *args: Any, **kwargs: Any) -> Any:
        """Write a given value to the named attribute."""

    def set_timeout_millis(self, timeout_in_ms: int) -> None:
        pass
