import logging
from contextlib import contextmanager
from typing import List, Union

from ska_ser_skallop.mvp_control import base
from ska_ser_skallop.mvp_control.configuration.types import Composition
from ska_ser_skallop.mvp_control.describing import mvp_states as states
from ska_ser_skallop.mvp_control.describing.mvp_conditions import NotReady, Readiness
from ska_ser_skallop.mvp_control.dishes.failure_handling import dish_labels
from ska_ser_skallop.mvp_control.entry_points import configuration
from ska_ser_skallop.mvp_control.event_waiting import wait
from ska_ser_skallop.mvp_control.subarray.base import SBConfig
from ska_ser_skallop.mvp_control.subarray.failure_handling import sub_labels
from ska_ser_skallop.mvp_control.telescope.failure_handling import tel_labels
from ska_ser_skallop.mvp_fixtures.base import ExecSettings

from .job_scheduling import Job

logger = logging.getLogger(__name__)


class NotReadyCompose(NotReady):
    pass


class NotReadyTearDown(NotReady):
    @property
    def already_torn_down(self) -> bool:
        label = sub_labels.subarrays_obs_state.label
        conditional = self.result.conditions[label]
        condition = conditional.cond
        if condition.value == "EMPTY":
            return True
        return False


class EWhileComposing(base.SideCarException):
    pass


class EWhileTearingDown(base.SideCarException):
    pass


class EWhileRestarting(base.SideCarException):
    pass


def assert_I_can_compose_a_subarray(subarray_id: int, receptors: List[int]):
    subarray_ready = Readiness(tel_labels, sub_labels, dish_labels)

    # first check the general health state of the system
    subarray_ready.expect(*tel_labels.masters_health).state(states.get_master_states).to_be("ON")
    subarray_ready.expect(*tel_labels.resources_health).state(
        states.get_sensor_states, receptors
    ).to_be("ON")
    subarray_ready.expect(*sub_labels.subarrays_health).state(
        states.get_subarray_states, subarray_id
    ).to_be("ON")
    subarray_ready.expect(*dish_labels.dish_masters_operational).state(
        states.get_dishes_ops_state, receptors
    ).to_be("OPERATE")
    subarray_ready.expect(*dish_labels.dish_masters_health).state(
        states.get_dishes_health, receptors
    ).to_be("OK", "DEGRADED")
    subarray_ready.expect(*dish_labels.dish_masters_dev_state).state(
        states.get_dishes_state, receptors
    ).to_be("ON")

    # Check if it is working properly
    subarray_ready.expect(*tel_labels.cn_telescope_state).state(
        states.get_centralnode_telescopestate
    ).to_be("ON")

    # then look at the current controllable state of devices related to that subarray
    subarray_ready.expect(*sub_labels.subarrays_obs_state).state(
        states.get_subarray_obs_states, subarray_id
    ).to_be("EMPTY")

    if not subarray_ready:
        raise NotReadyCompose(subarray_ready.describe_why_not_ready(), subarray_ready)


def assert_I_can_tear_down_a_subarray(subarray_id: int, receptors: List[int]):
    subarray_ready = Readiness(tel_labels, sub_labels, dish_labels)

    # first check the general health state of the system
    subarray_ready.expect(*tel_labels.masters_health).state(states.get_master_states).to_be("ON")
    subarray_ready.expect(*tel_labels.resources_health).state(
        states.get_sensor_states, receptors
    ).to_be("ON")
    subarray_ready.expect(*sub_labels.subarrays_health).state(
        states.get_subarray_states, subarray_id
    ).to_be("ON")
    subarray_ready.expect(*dish_labels.dish_masters_operational).state(
        states.get_dishes_ops_state, receptors
    ).to_be("OPERATE")
    subarray_ready.expect(*dish_labels.dish_masters_health).state(
        states.get_dishes_health, receptors
    ).to_be("OK", "DEGRADED")
    subarray_ready.expect(*dish_labels.dish_masters_dev_state).state(
        states.get_dishes_state, receptors
    ).to_be("ON")

    # Check if it is working properly
    subarray_ready.expect(*tel_labels.cn_telescope_state).state(
        states.get_centralnode_telescopestate
    ).to_be("ON")

    # then look at the current controllable state of devices related to that subarray
    subarray_ready.expect(*sub_labels.subarrays_obs_state).state(
        states.get_subarray_obs_states, subarray_id
    ).to_be("IDLE")

    if not subarray_ready:
        raise NotReadyTearDown(subarray_ready.describe_why_not_ready(), subarray_ready)


def I_can_compose_a_subarray(subarray_id: int, receptors: List[int]) -> bool:
    try:
        assert_I_can_compose_a_subarray(subarray_id, receptors)
    except NotReadyCompose:
        return False
    return True


def I_can_teardown_a_subarray(subarray_id: int, receptors: List[int]) -> bool:
    try:
        assert_I_can_tear_down_a_subarray(subarray_id, receptors)
    except NotReadyTearDown:
        return False
    return True


def compose_subarray(
    subarray_id: int,
    receptors: List[int],
    composition: Composition,
    sb_config: SBConfig,
    settings: ExecSettings,
    entry_point: configuration.EntryPoint | None = None,
):
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    builder = entry_point.set_waiting_for_assign_resources(subarray_id)

    @base.except_it(EWhileComposing)
    @wait.wait_for_it(
        builder,
        settings.time_out,
        settings.log_enabled,
        settings.play_logbook,
        attr_synching=settings.attr_synching,
    )
    def execute() -> None:
        settings.touched = True
        entry_point.compose_subarray(subarray_id, receptors, composition, sb_config.sbid)

    execute()


def start_subarray_composing(
    subarray_id: int,
    receptors: List[int],
    composition: Composition,
    sb_config: SBConfig,
    settings: ExecSettings,
    entry_point: configuration.EntryPoint | None = None,
) -> Job[None]:
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    builder = entry_point.set_waiting_until_resourcing(subarray_id)
    # note attr synching is always False since the transition is ephemeral

    @base.except_it(EWhileComposing)
    @wait.wait_for_it(
        builder,
        settings.time_out,
        settings.log_enabled,
        settings.play_logbook,
        attr_synching=False,
    )
    def execute() -> Job[None]:
        job = Job(
            entry_point.compose_subarray,
            subarray_id,
            receptors,
            composition,
            sb_config.sbid,
        )
        job.schedule_for_execution()
        settings.touched = True
        return job

    return execute()


def tear_down_subarray(
    subarray_id: int,
    settings: ExecSettings,
    entry_point: configuration.EntryPoint | None = None,
):
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    builder = entry_point.set_waiting_for_release_resources(subarray_id)

    @base.except_it(EWhileTearingDown)
    @wait.wait_for_it(
        builder,
        settings.time_out,
        settings.log_enabled,
        settings.play_logbook,
        attr_synching=settings.attr_synching,
    )
    def execute() -> None:
        settings.touched = True
        entry_point.tear_down_subarray(subarray_id)

    execute()


@contextmanager
def wait_for_subarray_allocated(
    subarray_id: int,
    settings: ExecSettings,
    entry_point: configuration.EntryPoint | None = None,
):
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    builder = entry_point.set_waiting_for_assign_resources(subarray_id)
    with wait.wait_for(
        builder,
        settings.time_out,
        settings.log_enabled,
        attr_synching=settings.attr_synching,
        recorder=settings.recorder,
    ) as board:
        yield
    if settings.play_logbook:
        wait.print_logbook(board, settings.play_logbook)


@contextmanager
def wait_for_subarray_released(
    subarray_id: int,
    settings: ExecSettings,
    entry_point: configuration.EntryPoint | None = None,
):
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    builder = entry_point.set_waiting_for_release_resources(subarray_id)
    with wait.wait_for(
        builder,
        settings.time_out,
        settings.log_enabled,
        attr_synching=settings.attr_synching,
        recorder=settings.recorder,
    ) as board:
        yield
    if settings.play_logbook:
        wait.print_logbook(board, settings.play_logbook)


def restart_aborted(
    subarray_id: int,
    settings: ExecSettings,
    entry_point: Union[configuration.EntryPoint, None] = None,
):
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    builder = entry_point.set_waiting_for_restart(subarray_id)

    @base.except_it(EWhileRestarting)
    @wait.wait_for_it(
        builder,
        settings.time_out,
        settings.log_enabled,
        settings.play_logbook,
        attr_synching=settings.attr_synching,
    )
    def execute() -> None:
        settings.touched = True
        entry_point.restart_subarray(subarray_id)

    execute()
