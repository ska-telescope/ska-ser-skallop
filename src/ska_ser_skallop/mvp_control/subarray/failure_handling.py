import logging
from time import sleep

from ska_ser_skallop.mvp_control.describing.mvp_conditions import (
    ConditionLabel,
    ConditionLabels,
    Readiness,
)
from ska_ser_skallop.mvp_control.entry_points import configuration as entry_conf
from ska_ser_skallop.mvp_control.rectifying.rectifier import create_rectifier

logger = logging.getLogger(__name__)


class SubarrayConditionLabels(ConditionLabels):
    @property
    def subarrays_health(self) -> ConditionLabel:
        """The general health of all the subarrays, they should especially not be in
        fault but otherwise 'OFF',"STANDBY" when starting up and ON when shutting down.

        :return: the condition label
        """
        return ConditionLabel("subbarray devices dev state", "subarrays_health")

    @property
    def tmc_subarrays_health(self) -> ConditionLabel:
        """The general health of all the tmc subarrays, they should especially not be in
        fault but otherwise 'ON' irrespective of starting up or shutting down
        """
        return ConditionLabel("tmc subbarray devices dev state", "tmc_subarrays_health")

    @property
    def csp_subarray_health(self) -> ConditionLabel:
        """The general health of all the subarrays, they should especially not be in
        fault but otherwise 'OFF',"STANDBY" when starting up and ON when shutting down.

        :return: a condition label
        """
        return ConditionLabel("csp subarray device dev state", "csp_subarray_health")

    @property
    def sdp_subarray_health(self) -> ConditionLabel:
        """The general health of all the subarrays, they should especially not be in
        fault but otherwise 'OFF',"STANDBY" when starting up and ON when shutting down.

        :return: a condition label
        """
        return ConditionLabel("sdp subarray device dev state", "sdp_subarray_health")

    @property
    def subarrays_obs_state(self) -> ConditionLabel:
        """The current observational state of tango devices related to the subarrays
        must be in state EMPTY when starting up and shutting down
        """
        return ConditionLabel("subbarray devices ObsState", "subarrays_obs_state", "obsState")


sub_labels = SubarrayConditionLabels()


class DirtySubarraysHandler:
    def __init__(self, result: Readiness) -> None:
        not_ok_conditions = result.get_not_ok_conditions()
        self.subarrays_health = not_ok_conditions.get(sub_labels.subarrays_health)
        self.subarrays_obs_state = not_ok_conditions.get(sub_labels.subarrays_obs_state)
        self.entry_point = entry_conf.get_entry_point()
        self.rectifier = create_rectifier()

    def switch_off_any_on_subarrays(self):
        if self.subarray_health_is_not_ok():
            assert self.subarrays_health
            on_subarrays = self.subarrays_health.in_state("ON")
            self.rectifier.switch_off_subarrays(on_subarrays)

    def subarray_health_is_not_ok(self) -> bool:
        return self.subarrays_health is not None

    def subarray_obsstate_is_not_ok(self) -> bool:
        return self.subarrays_obs_state is not None

    def clear_any_aborted_subarrays(self):
        if self.subarray_obsstate_is_not_ok():
            assert self.subarrays_obs_state
            for index, sub_states in self.subarrays_obs_state.items():
                if all(sub_states.are_in_state("ABORTED")):
                    # then we can conduct a gracefull obs reset
                    self.entry_point.reset_subarray(int(index))
                    logger.warning("aborted subarray {index}")
                elif any(sub_states.are_in_state("ABORTED")):
                    self.rectifier.restart_faulty_subarrays(sub_states.devices)

    def clear_any_devices_in_obsstate_fault(self):
        if self.subarray_obsstate_is_not_ok():
            assert self.subarrays_obs_state
            if any(self.subarrays_obs_state.are_in_state("FAULT")):
                subs_in_fault = self.subarrays_obs_state.in_state("FAULT")
                logger.warning(
                    f"Attempt at restarting subarray devices,{subs_in_fault} , in " "FAULT state..."
                )
                self.rectifier.restart_faulty_subarrays(subs_in_fault)

    def abort_and_restart_stuck_subarray_devices(self):
        if self.subarray_obsstate_is_not_ok():
            stuck_states = ["CONFIGURING", "SCANNING", "RESETTING"]
            assert self.subarrays_obs_state
            if any(self.subarrays_obs_state.are_in_state(*stuck_states)):
                subs_stuck = self.subarrays_obs_state.in_state(*stuck_states)
                logger.warning(
                    f"Attempt at aborting and restarting subarray devices,{subs_stuck} "
                    "being stuck..."
                )
                self.rectifier.abort_and_restart_subarrays(subs_stuck)

    def reset_any_devices_in_obsstate_fault(self):
        if self.subarray_obsstate_is_not_ok():
            assert self.subarrays_obs_state
            if any(self.subarrays_obs_state.are_in_state("FAULT")):
                subs_in_fault = self.subarrays_obs_state.in_state("FAULT")
                logger.warning(
                    f"Attempt at resetting subarray devices,{subs_in_fault} , in FAULT " "state..."
                )
                self.rectifier.reset_faulty_subarrays(subs_in_fault)

    def abort_and_reset_stuck_subarray_devices(self):
        if self.subarray_obsstate_is_not_ok():
            stuck_states = ["CONFIGURING", "SCANNING", "RESETTING"]
            assert self.subarrays_obs_state
            if any(self.subarrays_obs_state.are_in_state(*stuck_states)):
                subs_stuck = self.subarrays_obs_state.in_state(*stuck_states)
                logger.warning(
                    "Attempt at aborting and restarting subarray devices,{subs_stuck} "
                    "being stuck..."
                )
                self.rectifier.abort_and_reset_subarrays(subs_stuck)

    def take_any_idle_subarrays_to_empty(self):
        if self.subarray_obsstate_is_not_ok():
            assert self.subarrays_obs_state
            if "IDLE" in self.subarrays_obs_state.value:
                for index, sub_states in self.subarrays_obs_state.items():
                    # go through all the collections of subarray devices for each
                    # subarrays
                    # and try to tear down any ones in IDLE
                    if all(sub_states.are_in_state("IDLE")):
                        # release the subarray in a coordinated way
                        logger.warning(
                            f"The devices for subarray {index} still seem to be in "
                            "IDLE, will attempt to release them..."
                        )
                        self.entry_point.tear_down_subarray(int(index))
                    else:
                        # else we attempt to clear the subarrays devices individualy
                        self.rectifier.abort_and_restart_subarrays(sub_states.in_state("IDLE"))

    def abort_and_restart_subarrays(self):
        if self.subarray_obsstate_is_not_ok():
            assert self.subarrays_obs_state
            self.rectifier.abort_and_restart_subarrays(
                self.subarrays_obs_state.in_state("IDLE", "READY")
            )

    def abort_and_reset_subarrays(self):
        if self.subarray_obsstate_is_not_ok():
            assert self.subarrays_obs_state
            self.rectifier.abort_and_reset_subarrays(
                self.subarrays_obs_state.in_state("IDLE", "READY")
            )

    def obstates_of_subarrays_are_not_ok(self):
        return self.subarray_obsstate_is_not_ok()

    def take_any_ready_subarrays_to_empty(self):
        if self.subarray_obsstate_is_not_ok():
            assert self.subarrays_obs_state
            if "READY" in self.subarrays_obs_state.value:
                for index, sub_states in self.subarrays_obs_state.items():
                    # go through all the collections of subarray devices for each
                    # subarray and try to tear down any ones in READY
                    if all(sub_states.are_in_state("READY")):
                        # clear the subarray configuration (end SB) in a coordinated
                        # manner
                        logger.warning(
                            "The devices for subarray {index} stil seem to be in "
                            "READY, will attempt at clearing the configuration..."
                        )
                        self.entry_point.clear_configuration(int(index))
                        # in case something is broken and we are not yet ready for the
                        # next action
                        # we wait for a short period of time
                        sleep(1)
                        # release the subarray in a coordinated way
                        logger.warning(
                            "The devices for subarray {index} have been put to IDLE "
                            "state, will now attempt at putting them into EMPTY..."
                        )
                        # how tear_down_subarray will work, is it a part of future
                        # activity?
                        self.entry_point.tear_down_subarray(int(index))
                    else:
                        # else we attempt to clear the subarrays devices individualy
                        self.rectifier.abort_and_restart_subarrays(sub_states.in_state("READY"))

    def take_any_ready_subarrays_to_idle(self):
        if self.subarray_obsstate_is_not_ok():
            assert self.subarrays_obs_state
            if "READY" in self.subarrays_obs_state.value:
                for index, sub_states in self.subarrays_obs_state.items():
                    # go through all the collections of subarray devices for each
                    # subarray and try to tear down any ones in READY
                    if all(sub_states.are_in_state("READY")):
                        # clear the subarray configuration (end SB) in a coordinated
                        # manner
                        logger.warning(
                            "The devices for subarray {index} stil seem to be in "
                            "READY, will attempt at clearing the configuration..."
                        )
                        self.entry_point.clear_configuration(int(index))
                    else:
                        # else we attempt to clear the subarrays devices individualy
                        self.rectifier.abort_and_reset_subarrays(sub_states.in_state("READY"))

    def any_subarrays_are_in_ready(self) -> bool:
        if self.subarray_obsstate_is_not_ok():
            assert self.subarrays_obs_state
            return "READY" in self.subarrays_obs_state.value
        return False
