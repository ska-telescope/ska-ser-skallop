import logging
from contextlib import contextmanager
from typing import List

from ska_ser_skallop.event_handling import logging as log
from ska_ser_skallop.mvp_control import base
from ska_ser_skallop.mvp_control.describing import mvp_states as states
from ska_ser_skallop.mvp_control.describing.mvp_conditions import NotReady, Readiness
from ska_ser_skallop.mvp_control.dishes.failure_handling import dish_labels
from ska_ser_skallop.mvp_control.entry_points import configuration
from ska_ser_skallop.mvp_control.event_waiting import wait
from ska_ser_skallop.mvp_control.subarray.failure_handling import sub_labels
from ska_ser_skallop.mvp_control.telescope.failure_handling import tel_labels
from ska_ser_skallop.mvp_fixtures.base import ExecSettings

from .job_scheduling import Job

logger = logging.getLogger(__name__)


class NotReadyScan(NotReady):
    pass


class EWhileScanning(base.SideCarException):
    pass


class ScanFailed(NotReady):
    pass


class ScanUnsuccessful(NotReady):
    pass


def assert_I_can_run_a_scan(subarray_id: int, receptors: List[int]):
    subarray_ready = Readiness(tel_labels, sub_labels, dish_labels)

    # first check the general health state of the system
    subarray_ready.expect(*tel_labels.masters_health).state(states.get_master_states).to_be("ON")
    subarray_ready.expect(*tel_labels.resources_health).state(
        states.get_sensor_states, receptors
    ).to_be("ON")
    subarray_ready.expect(*sub_labels.subarrays_health).state(
        states.get_subarray_states, subarray_id
    ).to_be("ON")
    subarray_ready.expect(*dish_labels.dish_masters_operational).state(
        states.get_dishes_ops_state, receptors
    ).to_be("OPERATE")
    subarray_ready.expect(*dish_labels.dish_masters_health).state(
        states.get_dishes_health, receptors
    ).to_be("OK", "DEGRADED")
    subarray_ready.expect(*dish_labels.dish_masters_dev_state).state(
        states.get_dishes_state, receptors
    ).to_be("ON")

    subarray_ready.expect(*tel_labels.cn_telescope_state).state(
        states.get_centralnode_telescopestate
    ).to_be("ON")

    # then look at the current controllable state of devices related to that subarray
    subarray_ready.expect(*sub_labels.subarrays_obs_state).state(
        states.get_subarray_obs_states, subarray_id
    ).to_be("READY")

    if not subarray_ready:
        raise NotReadyScan(subarray_ready.describe_why_not_ready(), subarray_ready)


def assert_scan_successfull(subarray_id: int, receptors: List[int]):
    try:
        assert_I_can_run_a_scan(subarray_id, receptors)
    # implication of a successful scan is that it must be able to run again afterwards
    except NotReadyScan as exception:
        raise ScanUnsuccessful(*exception.args, result=exception.result)


def I_can_run_a_scan(subarray_id: int, receptors: List[int]) -> bool:
    try:
        assert_I_can_run_a_scan(subarray_id, receptors)
    except NotReadyScan:
        return False
    return True


def scan(
    subarray_id: int,
    settings: ExecSettings,
    entry_point: configuration.EntryPoint | None = None,
):
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    builder = entry_point.set_waiting_for_configure(subarray_id)

    @base.except_it(EWhileScanning)
    @log.log_devices(builder, settings.get_log_specs())
    @wait.wait_for_it(
        builder,
        settings.time_out,
        settings.log_enabled,
        settings.play_logbook,
        attr_synching=settings.attr_synching,
    )
    def execute() -> None:
        settings.touched = True
        entry_point.scan(subarray_id)

    execute()


def start_running_scan(
    subarray_id: int,
    settings: ExecSettings,
    entry_point: configuration.EntryPoint | None = None,
) -> Job[None]:
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    builder = entry_point.set_waiting_until_scanning(subarray_id)
    # attr synching is always disabled as transition is ephemeral

    @base.except_it(EWhileScanning)
    @log.log_devices(builder, settings.get_log_specs())
    @wait.wait_for_it(
        builder,
        settings.time_out,
        settings.log_enabled,
        settings.play_logbook,
        attr_synching=False,
    )
    def execute() -> Job[None]:
        job = Job(entry_point.scan, subarray_id)
        job.schedule_for_execution()
        settings.touched = True
        return job

    return execute()


@contextmanager
def check_configuration_when_finished(
    subarray_id: int, receptors: List[int], settings: ExecSettings
):
    yield
    assert_scan_successfull(subarray_id, receptors)
