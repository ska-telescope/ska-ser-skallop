from ska_ser_skallop.utils.nrgen import get_id


class SBConfig:
    def __init__(self, sbid: str = "") -> None:
        if not sbid:
            sbid = get_id("eb-mvp01-********-*****")
        self.sbid = sbid
