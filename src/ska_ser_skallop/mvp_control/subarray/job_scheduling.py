from concurrent.futures import Future, ThreadPoolExecutor
from typing import Callable, Generic, ParamSpec, TypeVar

P = ParamSpec("P")
T = TypeVar("T")


class Job(Generic[T]):
    def __init__(self, func: Callable[P, T], *args: P.args, **kwars: P.kwargs) -> None:
        self._func = func
        self._task: Future[T] | None = None
        self._args = args
        self._kwargs = kwars
        self._pool = ThreadPoolExecutor(max_workers=1, thread_name_prefix="job")

    def schedule_for_execution(self) -> None:
        self._task = self._pool.submit(self._func, *self._args, **self._kwargs)

    def wait_until_job_is_finished(self, timeout: float = 5) -> T | None:
        result = None
        if self._task:
            result = self._task.result(timeout)
        return result
