"""Configures implementations and factories for implementing connector objects."""

import inspect
import logging
from contextlib import contextmanager
from threading import Lock
from typing import Generator, NamedTuple, Union

from ska_ser_skallop.connectors.mockingfactory import MockingFactory
from ska_ser_skallop.mvp_control.base import AbstractDeviceProxy
from ska_ser_skallop.mvp_control.infra_mon.configuration import set_factory_provider
from ska_ser_skallop.utils.env import build_in_testing
from ska_ser_skallop.utils.generic_classes import Symbol
from ska_ser_skallop.utils.singleton import Singleton

from .base import AbstractFactory
from .remotefactory import get_remote_factory
from .tangofactory import get_tango_factory

logger = logging.getLogger(__name__)


def determine_factory() -> AbstractFactory:
    """Determine from environment what factory to use for getting connectors.

    :return: the factory to use for getting connectors
    """
    if build_in_testing():
        logger.info("Selecting components for doing build in testing system")
        factory = get_tango_factory()
        set_factory_provider(factory)
        return factory
    logger.info("Selecting components for doing build out testing system")
    factory = get_remote_factory()
    set_factory_provider(factory)
    return factory


def get_factory() -> AbstractFactory:
    """Retreive the current factory instance to use.

    :return: the current factory instance to use
    """
    container = FactoryContainer()
    return container.factory


def get_device_proxy(name: Union[str, Symbol], fast_load: bool = True) -> AbstractDeviceProxy:
    """Retrieve a particular device proxy instance and implementation based on current factory.

    :param name: The device name (FQN) to use for getting a device.
        Note the device can either be defined directly as a string
        or indirectly as a Symbol object that will show the
        device name when called via the str magic method (e.g. str(device))

    :type name: Union[str, Symbol]
    :param fast_load: whether device (only in case of remote impl)
        must be loaded without pre remote calls, defaults to False
    :return: the constructed and configured device proxy implementation
    """
    factory = get_factory()
    logger.debug(f"Using factory {factory} for getting device proxy")
    if isinstance(name, Symbol):
        name = name.__str__()
    return factory.get_device_proxy(name, fast_load)


def get_devices_query():
    """Construct a device query implementation instance.

    :return: [description]
    """
    factory = get_factory()
    logger.debug(f"Using factory {factory} for getting devices_query")
    return factory.get_devices_query()


def get_producer(name: str, fast_load: bool = False):
    """Construct a producer implementation instance.

    :param name: [description]
    :type name: str
    :param fast_load: [description], defaults to False
    :return: [description]
    """
    factory = get_factory()
    return factory.get_producer(name, fast_load)


def reset_configurations():
    """Re initialise configurations and factories."""
    container = FactoryContainer()
    container.reset()


def set_factory(factory: AbstractFactory, provided_by: str):
    """Set a particular factory implementation to use for getting connectors.

    :param factory: [description]
    :param provided_by: [description]
    :type provided_by: str
    """
    container = FactoryContainer()
    container.inject(factory, provided_by)


@contextmanager
def patch_factory_for_testing() -> Generator[MockingFactory, None, None]:
    """Temporarily replace the factory to use by a testing factory for testing purposes.

    Use this as a context manager.

    :yield: The updated factory that will be used for the duration of the context.
    """
    factory = MockingFactory()
    with patch_factory(factory):
        yield factory


@contextmanager
def patch_factory(factory: AbstractFactory, provided_by: str = "") -> Generator[None, None, None]:
    """Temporarily replace the factory to use with an injected factory.

    This can be used in testing environments whereby a test requires a specific implementation
    environment.

    :param factory: the factory to temporarily inject.
    :param provided_by: optional description of injector that provided
        the factory, defaults to ""
    :type provided_by: str
    :yield: None
    """
    if not provided_by:
        parent = inspect.getouterframes(inspect.currentframe())[1][3]
        provided_by = f"configured from {parent}"
    container = FactoryContainer()
    original_factory_provided = container.get_current_factory_provided()
    container.reset()
    container.inject(factory, provided_by)
    yield
    container.reset()
    if original_factory_provided:
        container.inject(
            original_factory_provided.provider,
            original_factory_provided.provided_by,
        )


class Provider(NamedTuple):
    """Bundles provided description and provider implementation into single object."""

    provider: AbstractFactory
    provided_by: str


class FactoryContainer(metaclass=Singleton):
    """
    Container that is used to hold the factory for use within the package.

    A factory can only be injected once during the running lifetime of
    the package.

    USAGE:
    >>> factory = FactorySelector().factory
    """

    def __init__(self):
        """Initialise object."""
        self._factory = None
        self.lock = Lock()

    def reset(self):
        """Re-initialise object."""
        self.__init__()

    def get_current_factory_provided(self) -> Union[Provider, None]:
        """Get the current provided factory.

        If no factory is provided it will return None.

        :returns: the named Tuple containing the current provided factory.
        """
        return self._factory

    @property
    def factory(self) -> AbstractFactory:
        """
        Retrieve the configured factory for use in the app.

        If this is the first time call has been made, the object shall
        first determine which factory to use based on an env variable.

        :returns: Either a newly created factory or the existing one.
        """
        with self.lock:
            if self._factory:
                return self._factory.provider
            factory = determine_factory()
            self.inject(factory, "environment during dependency call")
        return factory

    def inject(self, factory: AbstractFactory, provided_by: str):
        """Inject a new factory to use when asked for.

        :param factory:The factory to use
        :param provided_by: A string describing the context and called that set this factory.
        :type provided_by: str
        """
        assert not self._factory, (
            f"A factory ({type(self._factory.provider)}) has already been defined to "
            f"be used by entrypoint {self._factory.provided_by}"
        )
        self._factory = Provider(factory, provided_by)
