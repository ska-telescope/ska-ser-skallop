"""Implements the connectivity needs for skallop testing packages with external services.

In order to realise control and monitoring of the SUT, a set of proxies,/wrappers
are made available to interact with. The main technology used in ska for this is Tango;
therefore a large part of connectors has to do with implimenting this interface. The interfaces
are abstracted and allows (by means of abstract factories) for configuring a particular realization
of these connectors depending on the environment.

"""
