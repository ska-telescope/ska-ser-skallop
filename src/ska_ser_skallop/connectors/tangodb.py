"""Module containing general read functionality on tango db."""

from typing import List

from .configuration import get_device_proxy, get_devices_query


class TangoDB:
    """Class wrapping a tango interface to the tango database."""

    def __init__(self) -> None:
        """Initialise the object.

        Note this will try to establish a connection with the tango db device server.
        """
        self._db_device = get_device_proxy("sys/database/2")
        self._query = get_devices_query()

    @property
    def devices(self) -> List[str]:
        """Rteurn a list of all currently 'exported' tango devices.

        :return: a list of all currently 'exported' tango devices
        """
        return self._db_device.command_inout("DbGetDeviceExportedList", "*")

    def get_db_state(self):
        """Get the dev state of all the current devices withing the tango db.

        :return: Dictionary with the corresponding dev state of all devices.
        """
        return self._query.query(self.devices, "state")
