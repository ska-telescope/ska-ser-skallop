"""Base objects and abstractions for use within connectors package."""

from abc import abstractmethod
from typing import Any

from ska_ser_skallop.mvp_control.base import AbstractDeviceProxy, AbstractDevicesQuery
from ska_ser_skallop.subscribing.base import Producer


class AbstractFactory:
    """Factory for constructing connector like objects."""

    @abstractmethod
    def get_mvp_release(self) -> Any:
        """Get an instance of mvp released."""

    @abstractmethod
    def get_device_proxy(self, name: str, fast_load: bool = False) -> AbstractDeviceProxy:
        """Construct and configure a tango device proxy implementation.

        :param name: The FQDN name for the tango device
        :type name: str
        :param fast_load: whether device (in case of remote implementation) should be loaded
            fast, defaults to False
        :return: the constructed object
        """
        return AbstractDeviceProxy(name)

    @abstractmethod
    def get_devices_query(self) -> AbstractDevicesQuery:
        """Construct and configure a devices query implementation.

        :return: the constructed object
        """
        return AbstractDevicesQuery()

    @abstractmethod
    def get_producer(self, name: str, fast_load: bool = False) -> Producer:
        """Construct and configure a producer implementation.

        :param name: The name of the producer
        :type name: str
        :param fast_load: whether the producer must be generated without
            any IO calls first, defaults to False
        :return: [description]
        """
        return AbstractDeviceProxy(name)
