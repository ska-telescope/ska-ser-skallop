"""Constructs objects for implementing connectors via a remote tango bridge option."""

import os
from typing import Dict

from ska_ser_skallop.connectors.remoting.remote_devices import RemoteDeviceProxy, RemoteDevicesQuery
from ska_ser_skallop.connectors.remoting.tangobridge.factories import TBridgeFactory
from ska_ser_skallop.connectors.remoting.tangobridge.tangobridge import get_tango_bridge
from ska_ser_skallop.mvp_control.base import AbstractDeviceProxy, AbstractDevicesQuery
from ska_ser_skallop.mvp_control.infra_mon.configuration import (
    get_mvp_release,
    set_factory_provider,
)
from ska_ser_skallop.subscribing.base import Producer
from ska_ser_skallop.utils import env

from . import poller
from .base import AbstractFactory


def get_remote_factory(
    bridge_factory: TBridgeFactory | None = None,
) -> AbstractFactory:
    """Return a factory for implementing connectors via a remote tango bridge option.

    :param bridge_factory: the tango bridge implementation to use, defaults to None
    :type bridge_factory: TBridgeFactory
    :return: the remote factory
    """
    assert not env.build_in_testing()
    if not bridge_factory:
        bridge_factory = TBridgeFactory()
    return RemoteFactory(bridge_factory)


class RemoteFactory(AbstractFactory):
    """Factory for implementing connectors via a remote tango bridge option."""

    def __init__(self, bridge_factory: TBridgeFactory) -> None:
        """[Initialise object.

        :param bridge_factory: the tango bridge implementation to use
        :type bridge_factory: TBridgeFactory
        """
        super().__init__()
        self._bridge_factory = bridge_factory
        self._tango_bridge = get_tango_bridge(bridge_factory)
        self._device_pool: Dict[str, AbstractDeviceProxy] = {}
        # initialise factories for poller
        poller.set_device_factory(self.get_device_proxy)
        poller.set_release_factory(get_mvp_release)
        # initialise the factory for infra mon
        set_factory_provider(self)

    def get_mvp_release(self):
        """Get an instance of mvp release.

        :return: an instance of mvp release
        """
        return get_mvp_release()

    def get_device_proxy(self, name: str, fast_load: bool = True) -> AbstractDeviceProxy:
        """Return a device proxy implementation using remote tango bridge.

        :param name: The FQDN name for the tango device
        :param fast_load: whether device should be loaded fast, defaults to False
        :return: device proxy implementation
        """
        if fast_load:
            return RemoteDeviceProxy(name, self._tango_bridge, fastload=fast_load)
        if self._device_pool.get(name):
            return self._device_pool[name]
        proxy = RemoteDeviceProxy(name, self._tango_bridge, fastload=fast_load)
        self._device_pool[name] = proxy
        return proxy

    def get_devices_query(self) -> AbstractDevicesQuery:
        """Generate a devices query implementation using tango bridge.

        :return: the devices query implementation
        """
        return RemoteDevicesQuery(self._tango_bridge)

    def get_producer(self, name: str, fast_load: bool = False) -> Producer:
        """Generate a producer implementation using tango bridge.

        Note this will be the same as for get devices as tango device is
        considered a sub class of producer (ducked typed)

        :param name: The producer name
        :type name: str
        :param fast_load: [description], defaults to False
        :return: the producer implementation
        """
        if os.getenv("USE_ONLY_POLLING"):
            return poller.get_polling_producer(name)
        return self.get_device_proxy(name, fast_load)
