"""Factory for constructing connectors to use for testing purposes."""

from typing import Callable, Dict, Optional, Union

from mock import Mock

from ska_ser_skallop.connectors.base import AbstractFactory
from ska_ser_skallop.mvp_control.base import AbstractDeviceProxy, AbstractDevicesQuery
from ska_ser_skallop.mvp_control.infra_mon.configuration import set_factory_provider
from ska_ser_skallop.subscribing.base import AttributeInt, Producer
from ska_ser_skallop.utils.generic_classes import Symbol


class FabricationError(Exception):
    """Exceptions related to inability of getting a requested object."""

    pass


class MockingFactory(AbstractFactory):
    """Factory for constructing connectors to use for testing purposes.

    The testing factory should be configured in a pytest fixture with appropriate,
    mocks.

    The testing factory can typically be configured in three ways:


    (1) Using a default spy:

        .. code-block:: python

            factory = TestingFactory(device_proxy_spy = my_mock)
            assert_that(factory.get_device_proxy("any name will do")).is_equal_to(my_mock)

    (2) Using a default constructor:

        .. code-block:: python

            def make_mock_proxy(name: Union[str, Symbol]):
                return my_mock

            factory.inject_device_proxy_constructor(make_mock_proxy)
            assert_that(factory.get_device_proxy("any name will do")).is_equal_to(my_mock)

    (3) Using a dictionary:

        .. code-block:: python

            mock_devices = {"device1": mock1, "device2" : mock2}
            factory = TestingFactory(mock_device_proxies = mock_devices)
            assert_that(factory.get_device_proxy("device1")).is_equal_to(mock1)
            assert_that(factory.get_device_proxy("device2")).is_equal_to(mock2)

    (4) Setting the factory after initialization:

        .. code-block:: python

            factory = TestingFactory()
            factory.inject_device_proxy_mock("device1",mock1)
            factory.inject_device_proxy_mock("device2",mock2)
            assert_that(factory.get_device_proxy("device1")).is_equal_to(mock1)
            assert_that(factory.get_device_proxy("device2")).is_equal_to(mock2)

    The same pattern can be used for producers, and device query objects.

    """

    def __init__(
        self,
        mock_device_proxies: Dict[str, AbstractDeviceProxy] = None,
        mock_producers: Dict[str, Producer] = None,
        device_proxy_spy: AbstractDeviceProxy = None,
        producer_spy: Producer = None,
        mock_devices_query: AbstractDevicesQuery = None,
    ) -> None:
        """Initialise factory with a give set of mocks to use.

        Note if no arguments are given it will just return mock spy for
        all the get methods.

        :param mock_device_proxies: a list of mock device proxies to use
            (keyed by device name), defaults to None
        :param mock_producers: a list of mock producers to use
            (keyed by device name), defaults to None
        :param device_proxy_spy: a singleton mock to always return
            irrespective of device name, defaults to None
        :param producer_spy:  a singleton mock to always return
            irrespective of device name, defaults to None
        :param mock_devices_query: A mock query device to return, defaults to None
        """
        if not mock_device_proxies:
            mock_device_proxies = {}
        if not mock_producers:
            mock_producers = {}
        self._mock_device_proxies: Dict[str, AbstractDeviceProxy] = mock_device_proxies
        self._mock_producers: Dict[str, Producer] = mock_producers
        self._mock_devices_query = mock_devices_query
        self._device_proxy_spy = device_proxy_spy
        self._producer_spy = producer_spy
        self._device_proxy_constructor = None
        self._producer_constructor = None
        self._devices_query_constructor = None
        # initialise the factory for infra mon
        set_factory_provider(self)

    def inject_device_proxy_mock(
        self, name: Union[str, Symbol], proxy: AbstractDeviceProxy
    ) -> None:
        """Set a device proxy to be returned for a given name.

        :param name: a unique name identifying the device
        :param proxy: either a mock or a device proxy that will be returned
        """
        self._mock_device_proxies[str(name)] = proxy

    def inject_device_proxy_constructor(self, constructor: Callable[[str], AbstractDeviceProxy]):
        """Set a device proxy constructor.

        :param constructor: the constructor to use.
        """
        self._device_proxy_constructor = constructor

    def inject_producer_constructor(self, constructor: Callable[[str], Producer]):
        """Set a producer constructor.

        :param constructor: the producer to use.
        """
        self._producer_constructor = constructor

    def inject_devices_query_constructor(self, constructor: Callable[[], AbstractDevicesQuery]):
        """Set a devices query constructor to use.

        :param constructor: the devices query constructor to use.
        """
        self._devices_query_constructor = constructor

    def set_device_spy(
        self, device_proxy_spy: Optional[AbstractDeviceProxy] = None
    ) -> AbstractDeviceProxy:
        """
        Set the device proxy spy.

        This will "interject" the get mechanism by producing a
        singleton device proxy irrespective of the name used.

        :param device_proxy_spy: the device or mock of the device.
            Defaults to None in which case the factory will set a mock
            itself named "spy".

        :return: either the same input argument or a newly created mock
            in case of no arguments
        """
        if not device_proxy_spy:
            device_proxy_spy = Mock(AbstractDeviceProxy("spy"))
            mock_attribute_value = Mock(AttributeInt("spy", "spy"))
            mock_attribute_value.value = "value from mock"
            device_proxy_spy.read_attribute.return_value = mock_attribute_value
        self._device_proxy_spy = device_proxy_spy
        return device_proxy_spy

    def set_producer_spy(self, producer_spy: Optional[Producer] = None) -> Producer:
        """Set the device producer spy.

        This will  "interject" the get mechanism by producing a singleton
        producer irrespective of the name used.

        :param producer_spy: the producer or mock of the producer.
            Defaults to None in which case the factory will set a mock
            itself named 'spy".

        :return: either the same input argument or a newly created mock
            in case of no arguments
        """
        if not producer_spy:
            producer_spy = Mock(Producer)
        self._producer_spy = producer_spy
        return producer_spy

    def inject_producer(self, name: Union[str, Symbol], producer: Producer):
        """Set a specific producer to be returned for a given name.

        :param name: the name of the producer
        :param producer: the mock to be returned for that producer.
        """
        self._mock_producers[str(name)] = producer

    def get_device_proxy(self, name: Union[str, Symbol], fast_load=False) -> AbstractDeviceProxy:
        """Return a mock device proxy implementation based on the factory configuration.

        :param name: the name of the device to return
        :param fast_load: ignored if given, defaults to False
        :return: the mock device proxy
        """
        if self._device_proxy_spy:
            return self._device_proxy_spy
        if self._device_proxy_constructor:
            return self._device_proxy_constructor(str(name))
        return self._mock_device_proxies[str(name)]

    def inject_devices_query(self, query: AbstractDevicesQuery):
        """Enable the devices query to be interjected by the given mock.

        :param query: the mock to interject with.
        :type query: AbstractDevicesQuery
        """
        self._mock_devices_query = query

    def set_devices_query(self) -> AbstractDevicesQuery:
        """Set a specific devices query mock to use when queried.

        :return: the mock to interject with.
        """
        devices_query = Mock(AbstractDevicesQuery())
        devices_query.query.return_value = {}
        self._mock_devices_query = devices_query
        return self._mock_devices_query

    def get_devices_query(self) -> AbstractDevicesQuery:
        """Return a mock implementation of devices query.

        :return: the mock implementation of devices query
        """
        if self._devices_query_constructor:
            return self._devices_query_constructor()
        assert self._mock_devices_query
        return self._mock_devices_query

    def get_producer(self, name: Union[str, Symbol], fast_load=False) -> Producer:
        """Return a mock implementation of producer.

        :param name: the name of the producer
        :param fast_load: ignored, defaults to False
        :return: the mock implementation of producer
        :raises FabricationError: when name not found
        """
        if self._producer_spy:
            return self._producer_spy
        if self._producer_constructor:
            return self._producer_constructor(str(name))
        if producer := self._mock_producers.get(str(name)):
            return producer
        raise FabricationError(f"producer named {name} not found")

    @property
    def devices_query_set(self) -> bool:
        """Check if a devices query has already been set.

        :return: True if either device query constructor or instance already exists.
        """
        if self._devices_query_constructor:
            return True
        if self._mock_devices_query:
            return True
        return False

    @property
    def existing_producers(self) -> Dict[str, Producer]:
        """Get the existing producers set for the factory.

        :return: the producers keyed by their names, in case of a
            spy the name will be "spy"
        """
        if self._producer_spy:
            return {"spy": self._producer_spy}
        return self._mock_producers

    @property
    def existing_devices(self) -> Dict[str, AbstractDeviceProxy]:
        """Get the existing devices set for the factory.

        :return: the devices keyed by their names, in case of a
            spy the name will be "spy"
        """
        if self._device_proxy_spy:
            return {"spy": self._device_proxy_spy}
        return self._mock_device_proxies

    def clear(self):
        """Clear current mocks configured on the factory."""
        self.__init__()
