"""Module containing base classes."""

import time
from abc import abstractmethod
from collections import UserDict
from typing import Any, Union


class MessagePusher:
    """Abstract class representing an object capable of pushing messages."""

    @abstractmethod
    def push_message(self, item: Any):
        """Push a new message to be send by the websocket being controlled.

        :param item: The message to be send
        :type item: Any
        """

    @abstractmethod
    async def push_message_routine(self, item: Any):
        """Send an asynchronous message on the websocket (must be from asyncio thread).

        :param item: The item to send
        """


class Subscriber:
    """Abstract class representing a subscriber in a pub sub connection."""

    @abstractmethod
    def push_event(self, event: Any):
        """Receives a event from a producer to which it is being subscribed to.

        :param event: A event that the producer is required to push to subscribers
        :type event: Any
        """
        pass


class TimedEvent(UserDict):
    """Represents a an event at a specific time stamp."""

    def __init__(self):
        """Initialise the object."""
        self.data = {}
        self.timestamp = time.time()


class Disconnected(TimedEvent):
    """Represents a websocket disconnect event."""

    pass


class Reconnected(TimedEvent):
    """Represents a websocket re-connection event."""

    pass


class WSHealthSubscriber:
    """Abstract class representing a ws health subscriber in a pub sub connection."""

    @abstractmethod
    async def push_health_event(self, event: Union["Disconnected", "Reconnected"]):
        """Receives a event from a producer to which it is being subscribed to.

        :param event: A event that the producer is required to push to subscribers
        :type event: Any
        """
        pass
