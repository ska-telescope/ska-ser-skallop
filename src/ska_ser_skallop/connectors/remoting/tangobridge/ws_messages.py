"""Module containing basic ws messages for tangogql."""

import json

from . import queries


def ws_message_init() -> str:
    """Return a tangogql ws init message.

    :return: The ws init message
    """
    return """
        {"type": "connection_init", "payload": {}}
        """


def ws_message_subscribe(device: str, attribute: str, subscription_id: int) -> str:
    """Return a tangogql ws subscribe message based on given args.

    :param device: The device to which must be subscribed to
    :type device: str
    :param attribute: The attribute to which must be subscribed to.
    :type attribute: str
    :param subscription_id: The subscription id that will be used to identify incoming messages.
    :type subscription_id: int
    :return: The subscription message
    """
    query = queries.subscribe_device(device, attribute)
    message = {
        "type": "start",
        "id": f"{subscription_id}",
        "payload": {"query": query},
    }
    return json.dumps(message)


def ws_message_unsubscribe(subscription_id: int = 1) -> str:
    """Return a tango gql message to unsubscribe to a websocket.

    :param subscription_id: The id used for the subscription, defaults to 1
    :type subscription_id: int
    :return: the tangogql message
    """
    message = {"type": "stop", "id": f"{subscription_id}", "payload": {}}
    return json.dumps(message)
