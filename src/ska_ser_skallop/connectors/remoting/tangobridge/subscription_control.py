"""Module dealing with websocket level aggregation of publish subscribe type of connections."""

import logging
from collections import defaultdict
from typing import Dict, NamedTuple, Union

from ska_ser_skallop.utils.singleton import Singleton

from .base import Disconnected, MessagePusher, Reconnected, WSHealthSubscriber
from .ws_messages import ws_message_subscribe, ws_message_unsubscribe

logger = logging.getLogger(__name__)


class DeviceAttribute(NamedTuple):
    """Bundle of device name and attribute as a single object."""

    device: str
    attr: str


class SubscriptionCounter(metaclass=Singleton):
    """Generate ordered subscription id's.

    This class keeps track of existing id's generated and ensures a new id follows sequentially
    from previous ones by incrementing the number each time. Note this class is a singleton.

    """

    def __init__(self) -> None:
        """Initialise the counter."""
        self.index = 0

    def get_id(self) -> int:
        """Get a new id for subscription identification that is unique and sequentially ordered.

        :return: The new subscription id
        """
        self.index += 1
        return self.index


class SubscriptionReference:
    """Normalize multiple subscriptions of the same type into a single subscription.

    This ensures the websocket need not have to maintain multiple subscriptions if the subscription
    requires the same event from the same producer by keeping track of references to the base
    subscription and only closing down the base, once it has zero references.

    """

    def __init__(self) -> None:
        """Initialise the subscription references."""
        self.id = SubscriptionCounter().get_id()  # type: ignore
        self.count = 0

    def add_reference(self):
        """Record a new reference for this subscription have been generated."""
        self.count += 1

    def remove_reference(self):
        """Remove a reference to this subscription."""
        self.count -= 1

    def is_new(self) -> bool:
        """Whether this subscription has been referenced before.

        This amounts to the same thing is there being no more references to it.
        (see :py:meth:`is_not_referenced_anymore`)

        :return: returns True if no references to it exists.
        """
        return self.count == 0

    def is_not_referenced_anymore(self) -> bool:
        """Whether this subscription has no more references to it.

        This amounts to the same thing is if it is a new subscription.
        (see :py:meth:`is_new`)

        :return: returns True if no references to it exists.
        """
        return self.count == 0


class SubscriptionController(WSHealthSubscriber):
    """Object responsible for setting up subscriptions on a ws."""

    def __init__(self, message_pusher: MessagePusher) -> None:
        """Initialise  the object.

        :param message_pusher: the object responsible for sending messages
        """
        self._message_pusher = message_pusher
        self.subscription_refs: Dict[DeviceAttribute, SubscriptionReference] = defaultdict(
            SubscriptionReference
        )

    def add_subscription(self, device: str, attribute: str) -> int:
        """Add a new subscription to the websocket based on events from a device attribute.

        Note the websocket will only produce a new subscription if there does not already exist
        a subscription for the same device and attribute, otherwise it will just "piggyback" on
        an existing subscription.

        :param device: The device (tango device producer) which must be subscribed to
        :type device: str
        :param attribute: The attribute from the device which will generate events.
        :type attribute: str
        :return: The subscription id to use for when a subscription needs to be removed
            (:py:meth:`remove_subscription`).
        """
        subscripion = self.subscription_refs[DeviceAttribute(device, attribute)]
        if subscripion.is_new():
            message = ws_message_subscribe(device, attribute, subscripion.id)
            self._message_pusher.push_message(message)
        self.subscription_refs[DeviceAttribute(device, attribute)].add_reference()
        return subscripion.id

    def remove_subscription(self, device: str, attribute: str) -> Union[None, int]:
        """Remove a subscription as identified by the given id.

        Note a subscription will only be removed virtually if other subscriptions still exist to
        the same device and attribute. If no subscriptions to the same device and attribute remains,
        then the actual subscription will be removed.

        :param device: The device for which a subscription have been made.
        :type device: str
        :param attribute: The attribute for which a subscription has been made.
        :type attribute: str
        :return: Returns empty if subscriptions still remain to the given device and attribute,
            otherwise will return the "base" subscription id upon which the subscriptions have been
            "piggy backed" on.
        """
        subscription = self.subscription_refs[DeviceAttribute(device, attribute)]
        subscription.remove_reference()
        if subscription.is_not_referenced_anymore():
            message = ws_message_unsubscribe(subscription.id)
            self._message_pusher.push_message(message)
            return subscription.id
        return None

    async def push_health_event(self, event: Union["Disconnected", "Reconnected"]):
        """Receive new ws health events.

        :param event: The health event data.
        """
        if isinstance(event, Disconnected):
            self._disconnect_flag = True
            logger.warning("subscriptions disconnected")
            return
        if isinstance(event, Reconnected):
            self._disconnect_flag = False
            logger.info("subscriptions reconnected, attempt to re-establish subscriptions.")
            await self._re_subscribe()
            logger.info("subscriptions re-established")

    async def _re_subscribe(self):
        for device_attribute, subscription in self.subscription_refs.items():
            device, attribute = device_attribute
            message = ws_message_subscribe(device, attribute, subscription.id)
            await self._message_pusher.push_message_routine(message)
