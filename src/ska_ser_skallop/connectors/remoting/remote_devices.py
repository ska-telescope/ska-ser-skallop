"""Implements AbstractDeviceProxy and AbstractDevicesQuery as remote connections."""

import logging
from datetime import datetime
from functools import partial
from typing import Any, Callable, Dict, List, NamedTuple, Union

from ska_ser_skallop.datatypes.attributes import get_enum_type, to_def_state_from_str
from ska_ser_skallop.mvp_control.base import AbstractDeviceProxy, AbstractDevicesQuery, Spec
from ska_ser_skallop.subscribing import base
from ska_ser_skallop.subscribing.base import EventDataInt, Subscriber
from ska_ser_skallop.subscribing.helpers import get_attr_value_as_str
from ska_ser_skallop.utils.coll import flatten
from ska_ser_skallop.utils.singleton import Singleton
from ska_ser_skallop.utils.string_manipulation import trunc

from .tangobridge import queries
from .tangobridge.parsing import parse
from .tangobridge.tangobridge import TangoBridge, get_tango_bridge

logger = logging.getLogger(__name__)


class AttributeReadError(Exception):
    """Raised when a tango attribute was unsuccessfully red."""

    pass


class MultipleAttributeReadErrorr(Exception):
    """Raised when multiple attributes was unsuccessfully red."""

    pass


class Attribute(NamedTuple):
    """Bundles attribute values into a single value."""

    name: str
    datatype: str
    value: str


class Device(NamedTuple):
    """Represents a Device as a collection of attributes."""

    attributes: List[Attribute]


def parse_attributes(data: Dict) -> List[Attribute]:
    """Parse a given dictionary resulting from a query for multiple attributes.

    :param data: The input data received from the query
    :type data: Dict
    :return: A list of attributes parsed from the query
    """
    attributes = parse(data, "attributes").value
    attributes_list = []
    for attr in attributes:
        assert isinstance(attr, dict)
        attribute_name = parse(attr, "name", "not loaded").value
        attribute_type = parse(attr, "datatype", "str").value
        attribute_value = parse(attr, "value", "not loaded").value
        attributes_list.append(Attribute(attribute_name, attribute_type, attribute_value))
    return attributes_list


def parse_devices(data: Dict) -> List[Device]:
    """Parse a given dictionary resulting from a query for multiple devices.

    :param data: The input data received from the query
    :type data: Dict
    :return: A list of Device objects (each item represented as collections of attributes)
    """
    devices = parse(data, "data").parse("devices").value
    device_list = []
    for device in devices:
        assert isinstance(device, dict)
        attributes = parse_attributes(device)
        device_list.append(Device(attributes))
    return device_list


class CommandResponse(NamedTuple):
    """Bundles a set of values from a command response into a single object."""

    message: str
    result_ok: bool
    command_output: Any


class WriteAttributeResponse(NamedTuple):
    """Bundles a set of values from a write attribute response into a single object."""

    message: str
    result_ok: bool
    value_before: Any


class DeviceAttribute(NamedTuple):
    """Bundles a set of values from a device  attribute into a single object."""

    name: str
    label: str
    dataformat: str
    datatype: str


def parse_attribute_values(data: Dict) -> Dict[str, base.AttributeInt]:
    """Parse a given dictionary resulting from a query for attributes of devices.

    :param data: The input data received from the query
    :type data: Dict
    :raises AttributeReadError: when attribute was parsed unsuccessful
    :return: A dictionary listing corresponding attribute per device name
    """
    check_error_in_response(data)
    attr_list = parse(data, "data").parse("attributes").value
    if attr_list is None:
        raise AttributeReadError("no data returned from attribute read call")
    assert isinstance(attr_list, list), f"Expected {attr_list} to be an list"
    result = {}
    for attr in attr_list:
        device_name = parse(attr, "device").value
        attr_name = parse(attr, "name").value
        attr_value = parse(attr, "value").value
        timestamp = parse(attr, "timestamp").value
        result[f"{device_name}/{attr_name}"] = base.AttributeInt(attr_name, attr_value, timestamp)
    return result


def parse_attribute_value(data: Dict) -> base.AttributeInt:
    """Parse a given dictionary resulting from a query for a device attribute value.

    :param data: The input data received from the query
    :type data: Dict
    :raises AttributeReadError: when attribute was parsed unsuccessful
    :return: the attribute value as an AttributeInt object
    """
    check_error_in_response(data)
    attr = parse(data, "data").parse("attributes").value_as_singleton
    if attr is None:
        raise AttributeReadError("no data returned from attribute read call")
    assert isinstance(attr, dict), f"Expected {attr} to be an object"
    attr_name = parse(attr, "name").value
    val = parse(attr, "value").value
    timestamp = parse(attr, "timestamp").value
    return base.AttributeInt(attr_name, val, timestamp)


def parse_attributes_list(data: Dict) -> List[DeviceAttribute]:
    """Parse a given dictionary resulting from a query for a multiple device attributes.

    :param data: The input data received from the query
    :type data: Dict
    :return: A list of attribute values for that device
    """
    check_error_in_response(data)
    commands = parse(data, "data").parse("device").parse("attributes").value
    device_attributes = []
    for command in commands:
        name = parse(command, "name").value
        label = parse(command, "label").value
        dataformat = parse(command, "dataformat").value
        datatype = parse(command, "datatype").value
        device_attributes.append(DeviceAttribute(name, label, dataformat, datatype))
    return device_attributes


class DeviceCommand(NamedTuple):
    """Bundles device command values into a single object."""

    name: str
    intype: str


def parse_command_list(data: Dict) -> List[DeviceCommand]:
    """Parse a given dictionary resulting from a query for commands on a device.

    :param data: The input data received from the query
    :type data: Dict
    :return: A list of device commands
    """
    check_error_in_response(data)
    commands = parse(data, "data").parse("device").parse("commands").value
    device_commands = []
    for command in commands:
        name = parse(command, "name").value
        intype = parse(command, "intype").value
        device_commands.append(DeviceCommand(name, intype))
    return device_commands


def check_error_in_response(data: Dict):
    """Check if response from query indicates an error.

    :param data: The input data received from the query
    :type data: Dict
    :raises CommandError: when response from query indicates an error.
    """
    errors = parse(data, "errors", False).value
    if errors:
        error_messages = []
        for error in errors:
            error_messages.append(parse(error, "reason").value)
        if len(error_messages) == 1:
            error_messages = error_messages[0]
        raise CommandError(error_messages)


def parse_command(data: Dict) -> CommandResponse:
    """Parse the results returned from a command send to a device.

    :param data: The input data received from the command response
    :type data: Dict
    :return: A command response object
    """
    check_error_in_response(data)
    command_response = parse(data, "data").parse("executeCommand")
    message = command_response.parse("message").value
    result_ok = command_response.parse("ok").value
    command_output = command_response.parse("output").value
    return CommandResponse(message, result_ok, command_output)


def parse_write_attr(data: Dict) -> WriteAttributeResponse:
    """Parse the results returned from a command send to a device.

    :param data: The input data received from the command response
    :type data: Dict
    :return: A command response object
    """
    check_error_in_response(data)
    write_response = parse(data, "data").parse("setAttributeValue")
    message = write_response.parse("message").value
    result_ok = write_response.parse("ok").value
    value_before = write_response.parse("valueBefore").value
    return WriteAttributeResponse(message, result_ok, value_before)


class CommandError(Exception):
    """Raised when a command response contained an error."""

    pass


class WriteAttributeError(Exception):
    """Raised when a command response contained an error."""

    pass


class DeviceInstantiationError(Exception):
    """Raised when a device could not be instantiated."""

    pass


class AttributeSubScription(NamedTuple):
    """Raised when a subscription to a device was unsuccessful."""

    id: int
    attr: str


class RemoteDeviceProxy(AbstractDeviceProxy):
    """Implements AbstractDeviceProxy as a remote object connected via a tangobridge connection."""

    def __init__(
        self,
        device_name: str,
        tango_bridge: TangoBridge = None,
        fastload=True,
    ) -> None:
        """Initialise object.

        :param device_name: The tango device name (FQDN)
        :type device_name: str
        :param tango_bridge: An injected tangobridge to use,
            defaults to None in which case the normal Tangobridge object shall be used.
        :type tango_bridge: TangoBridge
        :param fastload: Whether the object should be initialised without any
            calls made to tango gql services to get meta data about the device.
            A fastload can save significant time if multiple objects need
            to be instantiated, defaults to False
        :type fastload: bool, optional
        """
        super().__init__(device_name)
        if tango_bridge:
            self.tango_bridge = tango_bridge
        else:
            self.tango_bridge = get_tango_bridge()
        self.device_name = device_name
        self.subscription_refs: Dict[int, AttributeSubScription] = {}
        self._subscription_index = 0
        self._fast_load = fastload
        if not fastload:
            self._attribute_list = self._load_attributes()
            self._command_list = self._load_commands()
        else:
            self._attribute_list, self._command_list = [], []

    def _load_attributes(self):
        query = queries.fetch_attributes(self.device_name)
        results = self.tango_bridge.call_graphql(*query)  # type: ignore
        device_exists = False
        if "data" in results:
            if "device" in results["data"]:
                if results["data"]["device"]:
                    device_exists = True
        if not device_exists:
            raise DeviceInstantiationError(
                f"Unable to get data from {self.device_name} on "
                f"{self.tango_bridge.url}, are you sure the device exists?"
            )
        attrs = parse_attributes_list(results)
        return attrs

    def __getattr__(self, attr: str) -> Any:
        """Get an object attribute including the tango device attributes.

        The device attributes will be obtained from the device metadata when
        the device was not instantiated via the fast load option.

        :param attr: the object attribute member
        :type attr: str
        :raises AttributeError: when attribute does not exist (statically or dynamically)
        :return: the attribute value
        """
        if hasattr(super(), attr):
            return super().__getattribute__(attr)
        if attr in flatten([(attr.name, attr.label) for attr in self._attribute_list]):
            attr_type = [
                attr_type for attr_type in self._attribute_list if attr_type.label == attr
            ][0]
            data_type = get_enum_type(attr_type.label)
            result = self.get_attr_value(attr_type.name)
            if data_type:
                return data_type(result)
            return result
        try:
            return super().__getattribute__(attr)
        except AttributeError as exception:
            raise AttributeError(
                f"RemoteDeviceProxy object has no attribute '{attr}',\
             avaliable attributes are {self._attribute_list}."
            ) from exception

    def get_attr_value(self, attr_name: str) -> Any:
        """Get tango device attribute from the provided attribute name.

        :param attr_name: The tango attribute name (small caps)
        :type attr_name: str
        :return: The value from the Attribute
        """
        return self.read_attribute(attr_name).value

    def _load_commands(self) -> List[DeviceCommand]:
        query = queries.fetch_commands(self.device_name)
        result = self.tango_bridge.call_graphql(*query)  # type: ignore
        commands = parse_command_list(result)
        for command in commands:
            self.__dict__[command.name] = partial(self.command_inout, command.name)
        return commands

    def _validate_attribute(self, attr_name: str):
        if self._attribute_list:
            available_attributes = [attr.name for attr in self._attribute_list]
            assert attr_name in available_attributes, (
                f"{attr_name} not availale on {self.device_name}, available attributes "
                f"are: {available_attributes}"
            )

    def read_attribute(self, attr_name: str) -> Any:
        """Get a tango device attribute value red from the device.

        :param attr_name: the tango device attribute name
        :type attr_name: str
        :raises AttributeReadError: Raised when unable to get device attribute.
        :return: the value red from the device.
        """
        if not self.tango_bridge.tango_gql_healthy:
            timeout = 5
            logger.warning(
                f"Tango gql rest interface not healthy, will wait for {timeout} in "
                "case it comes online.."
            )
            self.tango_bridge.wait_for_tango_gql_healthy(timeout)
        attr_name = attr_name.lower()
        self._validate_attribute(attr_name)
        query = queries.read_attribute(self.device_name, attr_name)
        result = self.tango_bridge.call_graphql(*query)  # type: ignore
        try:
            attribute_value = parse_attribute_value(result)
        except AttributeReadError as exception:
            raise AttributeReadError(
                f"error in response for getting {attr_name} value "
            ) from exception
        return attribute_value

    def write_attribute(self, attr_name: str, value: Any) -> Any:
        """Write a given value to the named attribute.

        :param attr_name: The attribute of the device
        :param value: The value to set the attribute to
        :raises WriteAttributeError: When command was unsuccessful
        :return: the result of the write command by returning the value of the attribute before
            it was set
        """
        if not self.tango_bridge.tango_gql_healthy:
            timeout = 5
            logger.warning(
                f"Tango gql rest interface not healthy, will wait for {timeout} in "
                "case it comes online.."
            )
            self.tango_bridge.wait_for_tango_gql_healthy(timeout)
        attr_name = attr_name.lower()
        self._validate_attribute(attr_name)
        query = queries.write_attribute(self.device_name, attr_name, value)
        result = self.tango_bridge.call_graphql(*query)  # type: ignore
        response = parse_write_attr(result)
        if response.result_ok:
            return response.value_before
        raise WriteAttributeError(
            f"Unable to write attribute{attr_name} on device {self.device_name} "
            f"with given value {value}: "
            f"{response.message}"
        )

    def ping(self) -> float:
        """Send a ping command and calculate the latency.

        :return: the ping command latency in microseconds.
        """
        start_time = datetime.now()
        self.read_attribute("state")
        delta_time = datetime.now() - start_time
        return delta_time.microseconds

    def get_events(self, subscription_id: int) -> List[EventDataInt]:
        """Return events from a buffered subscription.

        This is for when a type of subscription was requested that
        populates a buffer with events if and when they occur.

        :param subscription_id: the subscription id
        :type subscription_id: int
        :return: a list of events collected since subscription started.
        """
        device_subscription_id, attr = self.subscription_refs[subscription_id]
        return self.tango_bridge.get_events(self.device_name, attr, device_subscription_id)

    def unsubscribe_event(self, subscription_id: int) -> None:
        """Remove a subscription identified by an id.

        :param subscription_id: the subscription id
        :type subscription_id: int
        """
        device_subscription_id, attr = self.subscription_refs[subscription_id]
        self.tango_bridge.remove_subscription(self.device_name, attr, device_subscription_id)

    def subscribe_event(
        self,
        attr: str,
        event_type: Any,
        subscriber: Union[Subscriber, int, Callable[[EventDataInt], None]],
    ) -> int:
        """Subscribe to a tango event on a device attribute.

        The device will register the given subscriber to be notified (via the
        push_event method) when a particular event type occurs on the specified
        attribute. If an integer is given in stead of a Subscriber the subscription
        will result in a buffer being populated as events occur.

        Note the current implementation allows for only change events to be
        subscribed to.

        :param attr: The tango device attribute to be subscribed to
        :type attr: str
        :param event_type: (ignored as only change events have currently been implemented)
        :type event_type: Any
        :param subscriber: The subscribing method as implied by the type of paramater given.
            A Subscriber object implies an object will be called on it's `push_event` method.
            An integer implies incoming events must be buffered up to given integer value that can
            be retrieved at a later stage. A callable implies a given calback function
            (with the EventData as as argument) will be called.
        :type subscriber: Union[Subscriber, int, Callable[[EventDataInt], None]]
        :return: the subscription id used to identify and cancel the given subscription with
        """
        if not self.tango_bridge.tango_subscriptions_healthy:
            timeout = 5
            logger.warning(
                f"Tango subscriptions not healthy, will wait for {timeout} in case it "
                "comes online.."
            )
            self.tango_bridge.wait_for_tango_subscriptions_healthy(timeout)
            logger.warning(f"Tango subscriptions health restored {timeout}")
        subscription_id = self.tango_bridge.add_subscription(self.device_name, attr, subscriber)
        self._subscription_index += 1
        self.subscription_refs[self._subscription_index] = AttributeSubScription(
            subscription_id, attr
        )
        return self._subscription_index

    def reload_connections(self):
        """Reload tango bridge connections."""
        self.tango_bridge.tear_down_rest_connection()

    def reload_rest_connection(self):
        """Reload tango bridge rest connections."""
        self.tango_bridge.reload_rest_connection()

    def reload_ws_connection(self):
        """Reload tango bridge websocket connections."""
        self.tango_bridge.reload_ws_connection()

    def tear_down_ws_connection(self):
        """Tear down websocket connection on tango bridge."""
        self.tango_bridge.tear_down_ws_connection()

    def tear_down_rest_connection(self):
        """Tear down rest connection of tango bridge."""
        self.tango_bridge.tear_down_rest_connection()

    def tear_down_connections(self):
        """Tear tango bridge connections."""
        self.tango_bridge.tear_down_connections()

    # rest comands and queries

    def _validate_command(self, cmd_name, cmd_param=None):
        if self._command_list:
            available_commands = [command.name for command in self._command_list]
            assert cmd_name in available_commands, (
                f"{cmd_name} not availale on {self.device_name}, available attributes "
                f"are: {available_commands}"
            )
            # TODO validate cmd_param against argin

    def command_inout(self, cmd_name, cmd_param: Union[str, None] = None) -> Any:
        """Call a tango device command.

        :param cmd_name: The name of the command
        :type cmd_name: The device command name (Capitalized)
        :param cmd_param: the argument to use with the command, defaults to None
        :type cmd_param: Union[str, None], optional
        :raises CommandError: raised when command unsuccessful
        :return: The result of the command
        """
        self._validate_command(cmd_name, cmd_param)
        command = queries.command(self.device_name, cmd_name, cmd_param)
        response = parse_command(self.tango_bridge.call_graphql(*command))  # type: ignore
        if response.result_ok:
            if cmd_name == "State":
                result = to_def_state_from_str(response.command_output)
                return result
            return response.command_output
        raise CommandError(
            f"Unable to perform {cmd_name} on device {self.device_name}: " f"{response.message}"
        )

    def State(self) -> str:
        """Get the the tango device State.

        :return: the tango device State value.
        """
        query = queries.get_device_state(self.device_name)
        result = self.tango_bridge.call_graphql(query)
        parse_result = parse_devices(result)
        first_device = parse_result[0]
        first_device_state = first_device.attributes[0]
        first_device_state_value = first_device_state.value
        return first_device_state_value

    def get_attribute_list(self) -> List[str]:
        """Get a list of attributes that can be obtained for the given device instance.

        :return: list of attributes for device
        """
        return [attr.label for attr in self._attribute_list]


class RemoteDevicesQuery(AbstractDevicesQuery, metaclass=Singleton):
    """Implement a AbstractDevicesQuery by means of a tango bridge connection."""

    def __init__(
        self,
        tango_bridge: TangoBridge = None,
    ) -> None:
        """Initialise object.

        :param tango_bridge: An injected tangobridge to use,
            defaults to None in which case the normal Tangobridge object shall be used.
        :type tango_bridge: TangoBridge
        """
        if tango_bridge:
            self.tango_bridge = tango_bridge
        else:
            self.tango_bridge = get_tango_bridge()

    @staticmethod
    def _missing_data_in_query(
        attribute_values: Dict[str, Any],
        device_list: List[str],
        attr: Union[str, List[str]],
    ):
        if isinstance(attr, str):
            attr_list = [attr for _ in device_list]
        else:
            attr_list = attr
        fqdn_device_list = [f"{device}/{attr}" for device, attr in zip(device_list, attr_list)]
        return [device for device in fqdn_device_list if device not in attribute_values.keys()]

    def _query(
        self,
        device_list: List[str],
        attr: Union[str, List[str]],
        timeout: float = 3,
    ) -> Dict:
        if not self.tango_bridge.tango_gql_healthy:
            logger.warning(
                f"Tango gql rest interface not ready, will wait maximum {timeout} " "seconds.."
            )
            self.tango_bridge.wait_for_tango_gql_healthy(timeout)
        query = queries.read_attributes_from_multiple_devices(device_list, attr)
        result = self.tango_bridge.call_graphql(*query)  # type: ignore
        try:
            attribute_values = parse_attribute_values(result)
        except AttributeReadError as exception:
            raise AttributeReadError("error in response for getting attributes") from exception
        if missing_data := self._missing_data_in_query(attribute_values, device_list, attr):
            raise MultipleAttributeReadErrorr(f"unable to get {attr} data from {missing_data}")
        return {key: get_attr_value_as_str(attr) for key, attr in attribute_values.items()}

    def query(self, device_list: List[str], attr: str, timeout: float = 3) -> Dict:
        """Get an attribute vale for multiple devices.

        For each device in the list the given attribute value will be obtained.
        It is therefore assumed that each device has a the given attribute as it's
        member.

        :param device_list: The list of devices to query.
        :type device_list: List[str]
        :param attr: the attribute to read from each device.
        :type attr: str
        :param timeout: The amount of time to wait for a result from the query
            , defaults to 3
        :type timeout: float
        :return: the attribute values for each device as a dictionary keyed by device name.
        """
        result = self._query(device_list, attr, timeout)
        # we truncate the attribute part as normal query returns non qualified names.
        temp = {trunc(key, "/", from_last=True): value for key, value in result.items()}
        return temp

    def complex_query(self, device_specs: List[Spec], timeout: float = 3) -> Dict:
        """Get a specific attribute value for the corresponding device.

        For each item in the list specifying a device name and attribute,
        the correspoding attribute value is obtained.

        :param device_specs: A list of device query specifications (name and attribute)
        :type device_specs: List[Spec]
        :param timeout: The amount of time to wait for a result from the query
            , defaults to 3
        :type timeout: float
        :return: the attribute values for each device as a dictionary keyed by device name.
        """
        device_list_tuple = [(spec["name"], spec["attr"]) for spec in device_specs]
        device_list, attr_list = zip(*device_list_tuple)
        return self._query(list(device_list), list(attr_list), timeout)
