"""Module that implements a purely polling based mechanism for subscribing to tango attributes."""

import abc
import asyncio
import datetime
from threading import Lock, Thread
from typing import Any, Callable, Union

from ska_ser_skallop.mvp_control.infra_mon.resource_stats import CPUStats

from ..mvp_control.infra_mon.base import DeviceProfiler
from ..subscribing import base
from .base import AbstractDeviceProxy

_poller: Union["_Poller", None] = None
_device_factory: Union[Callable[[str], AbstractDeviceProxy], None] = None
_release_factory: Union[Callable[[], DeviceProfiler], None] = None

_throttling_key = "throttling"


def set_device_factory(factory: Callable[[str], AbstractDeviceProxy]):
    """Set a device factory to be used by this module.

    :param factory: Teh callable device factory to use
    """
    global _device_factory
    _device_factory = factory


def set_release_factory(factory: Callable[[], DeviceProfiler]):
    """Set a release factory to be used by this module.

    :param factory: Teh callable device factory to use
    """
    global _release_factory
    _release_factory = factory


def _get_device(dev_name: str, *_: Any):
    assert _device_factory, "Device factory not set, ensure set_device_factory is called"
    return _device_factory(dev_name)


def _get_release():
    assert _release_factory, "Device factory not set, ensure set_device_factory is called"
    return _release_factory()


def _get_device_attr_value(dev_name: str, attr: str):
    device = _get_device(dev_name)
    return device.read_attribute(attr)


def _get_profile(dev_name: str) -> Any:
    release = _get_release()
    profile = release.get_device_profile(dev_name)
    if profile is not None:
        if cpu_stats := profile.cpu_stats:
            stats: CPUStats = list(cpu_stats.values())[0]
            return stats.throttling_severity
    return None


def _create_event(dev_name: str, attr: str, value: Any):
    return base.EventDataInt(dev_name, attr, value, datetime.datetime.now().timestamp())


class AbstractPoll:
    """Generic representation of a poll object."""

    @abc.abstractmethod
    def add_subscriber(self, subscriber: base.EventsPusherBase) -> int:
        """Add a subscriber.

        :param subscriber: _description_
        """

    @abc.abstractmethod
    async def poll(self):
        """Poll the monitoring point."""

    @abc.abstractmethod
    def unsubscribe(self, sub_id: int):
        """Unsubscribe the polling.

        :param sub_id: _description_
        """

    @abc.abstractmethod
    def is_empty(self) -> bool:
        """Determine wether any subscriptions are still active."""


class _DeviceAttributePoll(AbstractPoll):
    def __init__(
        self,
        device_name: str,
        attr: str,
    ) -> None:
        self._current_value: None | Any = None
        self._subscriptions: dict[int, base.EventsPusherBase] = {}
        self._sub_index = 0
        self._device_name = device_name
        self._attr = attr

    def add_subscriber(self, subscriber: base.EventsPusherBase) -> int:
        self._sub_index += 1
        self._subscriptions[self._sub_index] = subscriber
        return self._sub_index

    async def poll(self):
        if self._subscriptions:
            value = await asyncio.to_thread(_get_device_attr_value, self._device_name, self._attr)
            if value.value != self._current_value:
                self._current_value = value.value
                for subscriber in self._subscriptions.values():
                    event = _create_event(self._device_name, self._attr, value)
                    subscriber.push_event(event)

    def unsubscribe(self, sub_id: int):
        self._subscriptions.pop(sub_id, None)

    def is_empty(self) -> bool:
        return not bool(self._subscriptions)


def _get_poller() -> "_Poller":
    global _poller
    if not _poller:
        _poller = _Poller()
    return _poller


class _DeviceResourcePoll(AbstractPoll):
    def __init__(self, device_name: str) -> None:
        self._current_value: None | Any = None
        self._subscriptions: dict[int, base.EventsPusherBase] = {}
        self._sub_index = 0
        self._device_name = device_name

    def add_subscriber(self, subscriber: base.EventsPusherBase) -> int:
        self._sub_index += 1
        self._subscriptions[self._sub_index] = subscriber
        return self._sub_index

    def unsubscribe(self, sub_id: int):
        self._subscriptions.pop(sub_id, None)

    async def poll(self):
        if self._subscriptions:
            if throttling := await asyncio.to_thread(_get_profile, self._device_name):
                push_event = False
                if self._current_value is not None:
                    if abs(self._current_value - throttling) > 0.1:
                        self._current_value = throttling
                        push_event = True
                else:
                    self._current_value = throttling
                    push_event = True
                if push_event:
                    for subscriber in self._subscriptions.values():
                        event = _create_event(
                            self._device_name,
                            _throttling_key,
                            self._current_value,
                        )
                        subscriber.push_event(event)

    def is_empty(self) -> bool:
        return not bool(self._subscriptions)


class _Poller:
    def __init__(self, poll_period: float = 0.05) -> None:
        self._thread = Thread(target=self._poll_thread, daemon=True)
        self._polls: dict[str, AbstractPoll] = {}
        self._poll_period = poll_period
        self._lock = Lock()
        self._thread_started = False

    def _poll_thread(self):
        asyncio.run(self._async_poll_thread())

    async def _async_poll_thread(self):
        while True:
            with self._lock:
                items = list(self._polls.values())
            polls = [poll.poll() for poll in items]
            await asyncio.gather(*polls)
            await asyncio.sleep(self._poll_period)

    def subscribe_to_device_attr(
        self, device_name: str, attr: str, subscriber: base.EventsPusherBase
    ):
        key = f"{device_name}/{attr}"
        with self._lock:
            if not (poll := self._polls.get(key)):
                poll = _DeviceAttributePoll(device_name, attr)
                self._polls[key] = poll
        sub_id = poll.add_subscriber(subscriber)
        if not self._thread_started:
            self._thread.start()
            self._thread_started = True
        return sub_id

    def subscribe_to_device_resource_stats(
        self, device_name: str, subscriber: base.EventsPusherBase
    ):
        attr = _throttling_key
        key = f"{device_name}/{attr}"
        with self._lock:
            if not (poll := self._polls.get(key)):
                poll = _DeviceResourcePoll(device_name)
                self._polls[key] = poll
        sub_id = poll.add_subscriber(subscriber)
        if not self._thread_started:
            self._thread.start()
            self._thread_started = True
        return sub_id

    def unsubscribe(self, device_name: str, attr: str, sub_id: int):
        key = f"{device_name}/{attr}"
        with self._lock:
            if poll := self._polls.get(key):
                poll.unsubscribe(sub_id)
                if poll.is_empty():
                    self._polls.pop(key)


class PollingProducer(base.Producer):
    """Implements a producer of attr events using polling only."""

    def __init__(self, name: str) -> None:
        """Instantiate the object.

        :param name: the device name that will produce events.
        """
        super().__init__(name)
        self._poller = _get_poller()
        self._current_subscriptions: dict[int, str] = {}

    def subscribe_event(  # type:ignore
        self, attr: str, event_type: Any, subscriber: base.EventsPusherBase
    ) -> int:
        """Set up an subscription to the producer.

        :param attr: The device attributes to listen to
        :param event_type: ignored (will always be changed based)
        :param subscriber:  the EventsPusherBase object to use for handling events
        :return: An unique id to use for unsubscribing.
        """
        init_value = _get_device_attr_value(self._name, attr)
        event = _create_event(self._name, attr, init_value)
        subscriber.push_event(event)
        sub_id = self._poller.subscribe_to_device_attr(self._name, attr, subscriber)
        self._current_subscriptions[sub_id] = attr
        return sub_id

    def unsubscribe_event(self, subscription_id: int) -> None:
        """Unsubscribe listening to changes on a particular event.

        :param subscription_id: The id that identifies a subscription.
        """
        if attr := self._current_subscriptions.get(subscription_id):
            self._poller.unsubscribe(self._name, attr, subscription_id)


class ResourceStateProducer(base.Producer):
    """Implements a producer of attr events using polling only."""

    def __init__(self, name: str) -> None:
        """Instantiate the object.

        :param name: the device name that will produce events.
        """
        super().__init__(name)
        self._poller = _get_poller()
        self._current_subscriptions: dict[int, str] = {}

    def subscribe_event(  # type:ignore
        self, attr: str, event_type: Any, subscriber: base.EventsPusherBase
    ) -> int:
        """Set up an subscription to the producer.

        :param attr: The device attributes to listen to
        :param event_type: ignored (will always be changed based)
        :param subscriber:  the EventsPusherBase object to use for handling events
        :return: An unique id to use for unsubscribing.
        """
        throttling = _get_profile(self._name)
        attr = _throttling_key
        event = _create_event(self._name, attr, throttling)
        subscriber.push_event(event)
        sub_id = self._poller.subscribe_to_device_resource_stats(self._name, subscriber)
        self._current_subscriptions[sub_id] = attr
        return sub_id

    def unsubscribe_event(self, subscription_id: int) -> None:
        """Unsubscribe listening to changes on a particular event.

        :param subscription_id: The id that identifies a subscription.
        """
        if attr := self._current_subscriptions.get(subscription_id):
            self._poller.unsubscribe(self._name, attr, subscription_id)


def get_polling_producer(name: str):
    """Generate a PollingProducer for subscribing to attributes that are polling based.

    :param name: the device name that will produce events.
    :return: The PollingProducer instance
    """
    assert _device_factory, "You need tio set a device factory first by calling set_device_factory"
    return PollingProducer(name)


def get_resource_state_producer(name: str):
    """Generate a PollingProducer for subscribing to attributes that are polling based.

    :param name: the device name that will produce events.
    :return: The PollingProducer instance
    """
    assert _device_factory, "You need tio set a device factory first by calling set_device_factory"
    return ResourceStateProducer(name)
