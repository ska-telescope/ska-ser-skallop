"""Implements AbstractDevicesQuery using tango package."""

import logging
from concurrent.futures import Future, ThreadPoolExecutor
from queue import Empty, Queue
from threading import Lock
from typing import Any, Dict, List, NamedTuple, Union

from ska_ser_skallop.connectors.base import AbstractFactory
from ska_ser_skallop.mvp_control.base import AbstractDeviceProxy, AbstractDevicesQuery, Spec
from ska_ser_skallop.subscribing.base import AttributeInt
from ska_ser_skallop.subscribing.helpers import get_attr_value_as_str
from ska_ser_skallop.utils.singleton import Singleton
from ska_ser_skallop.utils.string_manipulation import trunc

logger = logging.getLogger(__name__)


class Task(NamedTuple):
    """Represents a concurrent tango query with a pending result and outcome."""

    future: Future[AttributeInt]
    result: Union[AttributeInt, None] = None
    exception: Union[Exception, None] = None

    def wait_for_task_to_finish(self, timeout=3) -> None:
        """Wait for task to finish and store result.

        :param timeout: the maximum time to wait for task to finish, defaults to 3
        :type timeout: int, optional
        """
        try:
            self.result = self.future.result(timeout=timeout)
        except Exception as exception:
            # TODO create a more specific exception handling
            self.exception = exception


class QueryException(Exception):
    """Raised when an exception was raised on the query."""

    pass


class CallResult(NamedTuple):
    """Represents the result of a call to a device as a single object."""

    device: str
    attr: str
    result: Union[AttributeInt, None] = None
    exception: Union[Exception, None] = None

    @property
    def qualified_name(self) -> str:
        """Return a qualified name based on the device name and attribute.

        :return: the fqd in the pattern of <device>/<attr>
        """
        return f"{self.device}/{self.attr}"


def get_new_tango_device_query(factory: AbstractFactory) -> "TangoDeviceQuery":
    """Configure and construct a device query.

    :param factory: The factory to use for constructing the object
    :type factory: AbstractFactory
    :return: the tangodevice query object
    """
    query = TangoDeviceQuery(factory)
    logger.info("resetting factory")
    query.reset(factory)
    return query


class TangoDeviceQuery(AbstractDevicesQuery, metaclass=Singleton):
    """Implementation of AbstractDevicesQuery using tango device.

    This class read attributes from multiple devices using a tango device object
    as proxy for reading the needed values in a concurrent manner.

    In other words if a query consists of n devices to be red,
    the call will concurrently query n devices at the same time and retrieve the
    results if and when they arrive back before returning the collected final
    results. This ensures the latency for a query is O(k) instead of O(n).

    Note this device is a singleton that uses the same injected factory during initialization
    for constructing tango device proxies. If a new factory should be used the reset command
    must be called on the object,

    """

    def __init__(self, factory: AbstractFactory) -> None:
        """Initialise the object.

        :param factory: factory to use for constructing device proxies
        :type factory: AbstractFactory
        """
        super().__init__()
        self.factory = factory
        self.lock = Lock()

    def reset(self, factory: AbstractFactory):
        """Re-initialise the object to use a different factory for generating device proxies.

        :param factory: The new factory to use.
        :type factory: AbstractFactory
        """
        self.__init__(factory)

    @staticmethod
    def _fetch_state(
        device_name: str,
        device_proxy: AbstractDeviceProxy,
        attr: str,
        queue: "Queue[CallResult]",
    ):
        result = None
        try:
            result = device_proxy.read_attribute(attr)
        except Exception as exc:  # TODO expand exception type
            call_result = CallResult(device_name, exception=exc, attr=attr)
            queue.put(call_result)
            return
        call_result = CallResult(device_name, attr, result)
        queue.put(call_result)

    def _query(self, device_list: List[str], attr: Union[str, List[str]], timeout=100) -> Dict:
        exceptions = []
        results: Dict[str, Any] = {}
        with ThreadPoolExecutor(
            max_workers=len(device_list) + 1, thread_name_prefix="dev_query"
        ) as pool:
            queue: "Queue[CallResult]" = Queue()
            if isinstance(attr, str):
                use_fully_qualified_names = False
                waiting_devices = set(device_list.copy())
                for device_name in device_list:
                    device_proxy = self.factory.get_device_proxy(device_name, fast_load=True)
                    pool.submit(
                        self._fetch_state,
                        device_name,
                        device_proxy,
                        attr,
                        queue,
                    )
            else:
                use_fully_qualified_names = True
                combined = {
                    f"{device_name}/{device_attr}": (device_name, device_attr)
                    for device_name, device_attr in zip(device_list, attr)
                }

                # we use qualified names in case of complex queries
                waiting_devices = set(combined.keys())
                for device_name, device_attr in combined.values():
                    device_proxy = self.factory.get_device_proxy(device_name, fast_load=True)
                    pool.submit(
                        self._fetch_state,
                        device_name,
                        device_proxy,
                        device_attr,
                        queue,
                    )
            while waiting_devices:
                try:
                    result = queue.get(timeout=timeout)
                except Empty as exception:
                    raise QueryException(exceptions) from exception
                device_name = (
                    f"{result.device}/{result.attr}" if use_fully_qualified_names else result.device
                )
                waiting_devices.remove(device_name)
                if result.exception:
                    exceptions.append(result.exception)
                elif result.result:
                    result_value = get_attr_value_as_str(result.result)
                    results[result.qualified_name] = result_value
                else:
                    exceptions.append(Exception(f"unknown result:{result}"))
        if exceptions:
            raise QueryException(exceptions)
        return results

    def query(self, device_list: List[str], attr: str, timeout=10) -> Dict:
        """Get an attribute vale for multiple devices.

        For each device in the list the given attribute value will be obtained.
        It is therefore assumed that each device has a the given attribute as it's
        member.

        :param device_list: The list of devices to query.
        :type device_list: List[str]
        :param attr: the attribute to read from each device.
        :type attr: str
        :param timeout: The amount of time to wait for a result from the query
            , defaults to 3
        :type timeout: float
        :return: the attribute values for each device as a dictionary keyed by device name.
        """
        result = self._query(device_list, attr, timeout)
        # we truncate the attribute part as normal query returns non qualified names.
        return {trunc(key, "/", from_last=True): value for key, value in result.items()}

    def complex_query(self, device_specs: List[Spec], timeout: float = 3) -> Dict[str, Any]:
        """Get a specific attribute value for the corresponding device.

        For each item in the list specifying a device name and attribute,
        the correspoding attribute value is obtained.

        :param device_specs: A list of device query specifications (name and attribute)
        :type device_specs: List[Spec]
        :param timeout: The amount of time to wait for a result from the query
            , defaults to 3
        :type timeout: float
        :return: the attribute values for each device as a dictionary keyed by device name.
        """
        device_list_tuple = [(spec["name"], spec["attr"]) for spec in device_specs]
        device_list, attr_list = zip(*device_list_tuple)
        return self._query(list(device_list), list(attr_list), timeout)
