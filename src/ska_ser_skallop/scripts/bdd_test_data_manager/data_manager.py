import argparse

from ska_ser_skallop.bdd_test_data_manager.data_manager import (
    download_test_data,
    search_for_file,
    upload_test_data,
)

parser = argparse.ArgumentParser(
    "\tA script to simplify and organize handling of data to be used by BDD and system "
    "tests.\nThe following environment variables must be set:\n\t$CAR_RAW_USERNAME\n\t"
    "$CAR_RAW_PASSWORD\n\t$CAR_RAW_REPOSITORY_URL\n"
)
subparsers = parser.add_subparsers()

# upload sub-parser
upload_parser = subparsers.add_parser("upload", help="Upload the test data file to the repository")
upload_parser.add_argument(
    "-f",
    "--file-path",
    type=str,
    help="The absolute path of the file to be uploaded",
)
upload_parser.add_argument("-a", "--artefact-directory", type=str, help="e.g. path/to/files/")
upload_parser.set_defaults(function=upload_test_data)

# download sub-parser
download_parser = subparsers.add_parser(
    "download", help="Download the test data file from the repository"
)
download_parser.add_argument(
    "-f",
    "--file-name",
    type=str,
    help="The name of the file on the repository",
)
download_parser.add_argument("-a", "--artefact-directory", type=str, help="e.g. path/to/files/")
download_parser.set_defaults(function=download_test_data)

# search sub-parser
search_parser = subparsers.add_parser("search", help="Display a list of test data files")
search_parser.add_argument(
    "-f",
    "--filter",
    type=str,
    help=(
        "The name of the file on the repository. It can also be a wildcard which will "
        "have all files listed in that directory"
    ),
)
search_parser.add_argument(
    "-a",
    "--artefact-directory",
    type=str,
    help=(
        "e.g. /path/to/files. It can also include a wildcard to list files in "
        "subdirectories e.g. /path/to/files/*"
    ),
)
search_parser.set_defaults(function=search_for_file)


def main():
    args = parser.parse_args()

    if args.function.__name__ == "upload_test_data":
        args.function(args.file_path, args.artefact_directory)
        file_name = args.file_path.split("/")[-1]
        print(f"{file_name} uploaded at {args.artefact_directory}")
    elif args.function.__name__ == "download_test_data":
        download_path = args.function(args.file_name, args.artefact_directory)
        print(f"{args.file_name} downloaded to {download_path}")
    elif args.function.__name__ == "search_for_file":
        print(args.function(args.filter, args.artefact_directory))


if __name__ == "__main__":
    main()
