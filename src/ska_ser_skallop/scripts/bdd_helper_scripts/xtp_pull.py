import argparse
import getpass
import os
import sys

from ska_ser_skallop.scripts.bdd_helper_scripts.xtp_utils import (
    XrayException,
    export_xtp_feature,
    get_xtp_issue_type,
)

parser = argparse.ArgumentParser(
    description=(
        "Retrieve a feature file from an XTP ticket. "
        "Your JIRA username and password will be required to perform this operation"
    )
)


parser.add_argument(
    "-u",
    "--username",
    type=str,
    help="JIRA account username",
)

parser.add_argument(
    "-p",
    "--password",
    type=str,
    help="password for specified user",
)

parser.add_argument(
    "-t",
    "--xtp-ticket",
    type=str,
    help="JIRA XTP ticket number",
)

parser.add_argument(
    "-o",
    "--output-path",
    type=str,
    help="preferred path to store feature file. Default is current path",
)
parser.add_argument(
    "-v",
    "--verbose",
    required=False,
    action="store_true",
    help="Verbose output",
)
parser.add_argument(
    "--dry-run",
    required=False,
    action="store_true",
    help="Run the script without downloading the XTP-*.feature file.",
)


def create_feature_file(args):
    feature_file = f"{args.xtp_ticket}.feature"
    if args.output_path:
        feature_file = f"{args.output_path}/{feature_file}"

    if not args.dry_run:
        try:
            json_response = export_xtp_feature(args, args.xtp_ticket)
        except XrayException as err:
            raise SystemExit(err) from err
        if args.verbose:
            try:
                issue_type = get_xtp_issue_type(args, args.xtp_ticket)
            except XrayException as err:
                raise SystemExit(err) from err
            print(
                f"Downloaded {args.xtp_ticket} [Type: {issue_type}]",
                file=sys.stderr,
            )
            print(
                f"Saving file to: {args.output_path or os.getcwd()}",
                file=sys.stderr,
            )
        with open(feature_file, "wb") as open_ff:
            open_ff.write(json_response.content)
    else:
        try:
            issue_type = get_xtp_issue_type(args, args.xtp_ticket)
        except XrayException as err:
            raise SystemExit(err) from err
        print(
            f"[DRY RUN] Downloaded {args.xtp_ticket} [Type: {issue_type}]",
            file=sys.stderr,
        )
        print(
            f"[DRY RUN] Saving file to: {args.output_path or os.getcwd()}",
            file=sys.stderr,
        )
        print("[DRY RUN] Writing file to disk.")


def main():
    args = parser.parse_args()
    setattr(args, "jira_auth_token", os.environ.get("JIRA_AUTH", None))

    if not args.jira_auth_token:
        if not args.username:
            print("A username is required when the environment variable JIRA_AUTH is not " "set")
            sys.exit(0)

        if not args.password:
            stdin_password = getpass.getpass()
            setattr(args, "password", stdin_password)

    create_feature_file(args)


if __name__ == "__main__":
    main()
