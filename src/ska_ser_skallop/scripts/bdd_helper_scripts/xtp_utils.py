import argparse
import json

import requests
from requests.auth import HTTPBasicAuth

JIRA_ISSUE_URL = "https://jira.skatelescope.org/rest/api/2/issue/{}"
XTP_TICKETS_URL = "https://jira.skatelescope.org/rest/raven/2.0/export/test?keys={}"


class XrayException(Exception):
    pass


def get_xtp_issue_type(args: argparse.Namespace, xtp_ticket: str) -> str:
    """Get the issue type metadata

    :param args: The parsed parameters
    :param xtp_ticket: The XTP ticket. E.g XTP-1234

    :return: A string representing the type of issue, Test or Test Set.
    """
    url = JIRA_ISSUE_URL.format(xtp_ticket)
    if args.jira_auth_token:
        response = requests.get(
            url,
            headers={
                "Authorization": f"Basic {args.jira_auth_token}",
                "Accept": "application/json",
            },
            timeout=30,
        )
    else:
        response = requests.get(
            url,
            headers={"Accept": "application/json"},
            auth=HTTPBasicAuth(args.username, args.password),
            timeout=30,
        )

    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError as err:
        if response.status_code == 401:
            raise XrayException(
                "Authentication failure, either the credentials are incorrect or the "
                "Xray license has expired."
            ) from err
        if response.status_code == 404:
            raise XrayException(f"Issue {xtp_ticket} is not found.") from err
        raise XrayException(err) from err

    data = json.loads(response.text)
    return data["fields"]["issuetype"]["name"]


def export_xtp_feature(args: argparse.Namespace, xtp_ticket: str) -> str:
    """
    Export the cucumber feature file from Jira

    :param args: the parsed parameters
    :param xtp_ticket: the XTP ticket. E.g XTP-1234

    :return: the JSON content of the XTP ticket
    """
    if args.verbose:
        print(f"Downloading {xtp_ticket}")
    url = XTP_TICKETS_URL.format(xtp_ticket)
    if args.jira_auth_token:
        response = requests.get(
            url,
            headers={"Authorization": f"Basic {args.jira_auth_token}"},
            timeout=30,
        )
    else:
        response = requests.get(
            url,
            auth=HTTPBasicAuth(args.username, args.password),
            timeout=30,
        )

    try:
        response.raise_for_status()
    except requests.exceptions.HTTPError as err:
        if response.status_code == 400:
            raise XrayException(
                f"No feature files where generated. {xtp_ticket} may not exist. Please "
                "check Jira log."
            ) from err
        if response.status_code == 401:
            raise XrayException(
                "Authentication failure, either the credentials are incorrect or the "
                "Xray license has expired."
            ) from err
        if response.status_code == 500:
            raise XrayException(
                "An internal error occurred when generating the feature file(s)."
            ) from err
        raise XrayException(err) from err
    return response
