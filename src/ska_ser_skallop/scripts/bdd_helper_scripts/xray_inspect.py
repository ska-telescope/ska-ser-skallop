import argparse
import base64
import os

from ska_ser_skallop.xray import multipart_connection

parser = argparse.ArgumentParser(
    description=(
        "get particular information on jira ticket types and jira ticket fields for a given"
        "issue key and project"
    )
)
parser.add_argument(
    "proj",
    type=str,
    help="The proj key to use",
)
parser.add_argument(
    "-k",
    "--key",
    type=str,
    required=False,
    help="The issue type key to use, if empty it will return a list of know issue types"
    "for a project",
)
parser.add_argument("-u", "--username", type=str, default="", help="JIRA account username")
parser.add_argument(
    "-p",
    "--password",
    type=str,
    default="",
    help=(
        "Password for the JIRA user. If not specified and the environment variable "
        "`JIRA_AUTH` is not set then you will be prompted for one."
    ),
)


def main():
    args = parser.parse_args()
    host = "https://jira.skatelescope.org"
    if not (token := os.getenv("JIRA_AUTH")):
        assert (
            args.username and args.password
        ), "expected a username and password since no JIRA_AUTH has been set"
        token = base64.b64encode(f"{args.username}:{args.password}".encode("ascii")).decode("ascii")

    result = multipart_connection.inspect_jira(host, token, args.proj, args.key)
    print(result)
