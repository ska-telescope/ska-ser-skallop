from ska_ser_skallop.mvp_control.infra_mon.configuration import get_mvp_release


def main():
    release = get_mvp_release()
    result = release.get_devices_profile()
    print(result.get_summary())
