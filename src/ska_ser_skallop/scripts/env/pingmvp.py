from ska_ser_skallop.connectors.configuration import get_device_proxy


def ping_db():
    device_name = "sys/database/2"
    device_proxy = get_device_proxy(device_name)
    print(f"got response from {device_name}: {device_proxy.ping()/1000} ms")


def main():
    ping_db()


if __name__ == "__main__":
    main()
