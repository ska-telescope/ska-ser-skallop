"""Module dealing with overall managing of tasks ensuring a subarray is properly composed."""

import logging
from contextlib import contextmanager
from typing import List, Literal, Union

from ska_ser_skallop.event_handling import builders
from ska_ser_skallop.mvp_control.entry_points import configuration
from ska_ser_skallop.mvp_control.subarray import compose
from ska_ser_skallop.mvp_control.subarray.failure_handling import DirtySubarraysHandler
from ska_ser_skallop.mvp_control.subarray.job_scheduling import Job
from ska_ser_skallop.mvp_control.telescope.failure_handling import DirtyTelescopeHandler
from ska_ser_skallop.mvp_management.base import DisablePost
from ska_ser_skallop.utils.env import get_temp_dir

from .types import Composition, CompositionByFile, CompositionType, ExecSettings, SBConfig

logger = logging.getLogger(__name__)

ConfigurationType = Literal["standard"]


def generate_allocation_configuration(
    configuration_type: ConfigurationType, file_based: bool = False
) -> Composition:
    """Generate an allocation configuration object.

    :param configuration_type: The type of configuration to select.
    :param file_based: Whether configuration must be passed as a file, defaults to True
    :raises NotImplementedError: If given type not yet implemented
    :return: The specified configuration as a configuration object
    """
    if configuration_type == "standard":
        composition_type = CompositionType.STANDARD
        if file_based:
            file_location = get_temp_dir()
            return CompositionByFile(file_location, composition_type)
        else:
            return Composition(composition_type)
    raise NotImplementedError("Only handles standard configuration type")


def _handle_dirty_subarrays(
    subarray_handler: DirtySubarraysHandler, handler: DirtyTelescopeHandler
):
    if subarray_handler.obstates_of_subarrays_are_not_ok():
        subarray_handler.clear_any_devices_in_obsstate_fault()
        subarray_handler.abort_and_restart_stuck_subarray_devices()
        if handler.telescope_on:
            # this means we can attempt coordinated tear downs
            # first tear down any subarrays in IDLE
            subarray_handler.take_any_idle_subarrays_to_empty()
            # then tear down any subarrays in READY
            subarray_handler.take_any_ready_subarrays_to_empty()
        else:
            logger.warning("will attempt to abort subarrays and restart them...")
            subarray_handler.abort_and_restart_subarrays()


def _handle_not_ready_compose(exception: compose.NotReadyCompose):
    logger.warning("subarray not ready to be composed")
    handler = DirtyTelescopeHandler(exception.result)
    subarray_handler = DirtySubarraysHandler(exception.result)
    # all devices in dev state FAULT should be rectified in a similar manner
    handler.clear_all_faulty_to_be("ON")
    # now we look at subarrays
    _handle_dirty_subarrays(subarray_handler, handler)


def _handle_not_ready_teardown(exception: compose.NotReadyTearDown):
    logger.warning(
        f"subarray not ready to be torn down: Condition:\n"
        f"{exception.result.describe_why_not_ready()}"
    )
    handler = DirtyTelescopeHandler(exception.result)
    handler = DirtyTelescopeHandler(exception.result)
    subarray_handler = DirtySubarraysHandler(exception.result)
    # all devices in dev state FAULT should be rectified in a similar manner
    handler.clear_all_faulty_to_be("ON")
    # now we look at subarrays
    _handle_dirty_subarrays(subarray_handler, handler)


def _handle_error_whilst_composing(
    exception: Exception,
    subarray_id: int,
    receptors: List[int],
    settings: ExecSettings,
):
    # TODO use exception in handling
    _ = exception
    try:
        logger.warning(
            f"exception whilst attempting to compose subarray {subarray_id}, will "
            "check if it can be torn down.."
        )
        compose.assert_I_can_tear_down_a_subarray(subarray_id, receptors)
        logger.warning(f"attempt to tear down subarray {subarray_id}...")
        compose.tear_down_subarray(subarray_id, settings)
    except compose.NotReadyTearDown as exception_shut_down:
        _handle_not_ready_teardown(exception_shut_down)


def _handle_error_whilst_tearding_down(exception: Exception, settings: ExecSettings):
    # TODO use exception  and settings in handling
    _, _ = exception, settings
    logger.warning(
        "exception whilst attempting to tear down subarray, will not attempt to tear " "it down"
    )


def _handle_allocated_subarray_general_error(
    exception: Exception,
    subarray_id: int,
    receptors: List[int],
    settings: ExecSettings,
):
    # TODO use exception in handling
    _ = exception
    try:
        logger.warning(
            f"general exception whilst operating on subarray {subarray_id}, will check "
            "if it can be torn down"
        )
        compose.assert_I_can_tear_down_a_subarray(subarray_id, receptors)
        logger.warning(f"attempt to tear down subarray {subarray_id}...")
        compose.tear_down_subarray(subarray_id, settings)
    except compose.NotReadyTearDown as exc:
        _handle_not_ready_teardown(exc)


def compose_subarray(
    receptors: List[int],
    subarray_id: int,
    composition: Composition,
    sb_config: SBConfig,
    settings: ExecSettings,
    entry_point: Union[configuration.EntryPoint, None] = None,
):
    """Attempt to correctly place a subarray in the IDLE state and handle any exceptions.

    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param subarray_id: Integer used to identify the particular subarray
    :param composition: configuration info about how subarray must be allocated to resources.
    :param sb_config: object holding context information about the Scheduling Block.
    :param settings: Executions settings that must be used during the task.
    :param entry_point: The optional injected entry_point to use, default to None
    :raises not_ready_to_compose: when subarray not ready to be set to IDLE
    :raises exception_whilst_composing: when exception occurred whilst busy setting it to IDLE
    :raises Exception: when general error occurred that was not about not being ready or not
        occurred during allocation
    """
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    try:
        compose.assert_I_can_compose_a_subarray(subarray_id, receptors)
        compose.compose_subarray(
            subarray_id,
            receptors,
            composition,
            sb_config,
            settings,
            entry_point,
        )
    except compose.NotReadyCompose as not_ready_to_compose:
        _handle_not_ready_compose(not_ready_to_compose)
        raise not_ready_to_compose
    except compose.EWhileComposing as exception:
        exception_whilst_composing = exception.base_exception
        if settings.touched:
            _handle_error_whilst_composing(
                exception_whilst_composing, subarray_id, receptors, settings
            )
        raise exception_whilst_composing
    except Exception as exception:
        if settings.touched:
            _handle_allocated_subarray_general_error(exception, subarray_id, receptors, settings)
        raise exception


def teardown_subarray(
    receptors: List[int],
    subarray_id: int,
    settings: ExecSettings,
    entry_point: Union[configuration.EntryPoint, None] = None,
):
    """Attempt to correctly place a subarray in the EMPTY state and handle any exceptions.

    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param subarray_id: Integer used to identify the particular subarray
    :param settings: Executions settings that must be used during the task.
    :param entry_point: The optional injected entry_point to use, default to None
    :raises Exception: any unhandled exceptions raised during shutdown. Note exceptions as
        a consequence of not being ready or that occurred whilst tearing down is not thrown
        but just logged to the logger as exception messages.
    """
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    try:
        compose.assert_I_can_tear_down_a_subarray(subarray_id, receptors)
        compose.tear_down_subarray(subarray_id, settings, entry_point)
    except compose.NotReadyTearDown as exception:
        _handle_not_ready_teardown(exception)
        logger.exception(exception)
    except compose.EWhileTearingDown as exception:
        _handle_error_whilst_tearding_down(exception.base_exception, settings)
        logger.exception(exception.base_exception)
    except Exception as exception:
        if settings.touched:
            _handle_allocated_subarray_general_error(exception, subarray_id, receptors, settings)
        raise exception


@contextmanager
def allocated_subarray(
    receptors: List[int],
    subarray_id: int,
    composition: Composition,
    sb_config: SBConfig,
    settings: ExecSettings,
    entry_point: Union[configuration.EntryPoint, None] = None,
    disable_post: Union[DisablePost, bool] = False,
):
    """Create a context in which a subarray is placed in the IDLE state.

    After the context is finished the manager attempt to tear down and release
    allocated resources to the subarray.

    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param subarray_id: Integer used to identify the particular subarray
    :param composition: configuration info about how subarray must be allocated to resources.
    :param entry_point: The optional injected entry_point to use, default to None
    :param sb_config: object holding context information about the Scheduling Block.
    :param disable_post: flag indicating if post steps should be disabled, default = False
    :param settings: Executions settings that must be used during the task.

    :yields: None
    """
    entry_point = configuration.get_entry_point()
    compose_subarray(receptors, subarray_id, composition, sb_config, settings, entry_point)
    yield
    if not disable_post:
        builders.clear_supscription_specs()
        teardown_subarray(receptors, subarray_id, settings)


class ComposeSubarrayJob:
    """Wrapper for concurrent job to allocate (compose) resources to a subarray."""

    def __init__(
        self,
        job: Job,
        settings: ExecSettings,
        subarray_id: int,
        receptors: List[int],
        entry_point: Union[configuration.EntryPoint, None] = None,
    ) -> None:
        """Initialise the object.

        :param job: The job representing the task to allocate resources to a subarray.
        :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
            subarray.
        :param entry_point: Optional injected entry_point to use, defaults to None
        :param subarray_id: Integer used to identify the particular subarray
        :param settings: Executions settings that must be used during the task.
        """
        self._job = job
        self._settings = settings
        self._subarray_id = subarray_id
        self._receptors = receptors
        self._entry_point = entry_point if entry_point else configuration.get_entry_point()

    def wait_and_teardown_allocation(self, clear_afterwards: bool = True):
        """Wait for job composing a subarray to finish and teardown resources afterwards.

        :param clear_afterwards: Whether to skip the tearing down afterwards by settting to False,
            defaults to True meaning tear down will occur.
        :raises Exception: any unhandled exceptions raised during tear down. Note exceptions as
            a consequence of not being ready or that occurred whilst tearing down is not thrown
            but just logged to the logger as exception messages.
        """
        try:
            self._job.wait_until_job_is_finished(self._settings.time_out)
            if clear_afterwards:
                if self._settings.touched:
                    compose.assert_I_can_tear_down_a_subarray(self._subarray_id, self._receptors)
                    builders.clear_supscription_specs()
                    compose.tear_down_subarray(self._subarray_id, self._settings, self._entry_point)
        except compose.EWhileComposing as exception:
            if self._settings.touched:
                _handle_error_whilst_composing(
                    exception.base_exception,
                    self._subarray_id,
                    self._receptors,
                    self._settings,
                )
            logger.exception(exception.base_exception)
        except compose.NotReadyTearDown as exception:
            _handle_not_ready_teardown(exception)
            logger.exception(exception)
        except compose.EWhileTearingDown as exception:
            _handle_error_whilst_tearding_down(exception.base_exception, self._settings)
            logger.exception(exception.base_exception)
        except Exception as exception:
            if self._settings.touched:
                _handle_allocated_subarray_general_error(
                    exception,
                    self._subarray_id,
                    self._receptors,
                    self._settings,
                )
            raise exception


def dispatch_composed_subarray_job(
    receptors: List[int],
    subarray_id: int,
    composition: Composition,
    sb_config: SBConfig,
    settings: ExecSettings,
) -> ComposeSubarrayJob:
    """Create a concurrent task (Job) to attempt to allocate resources to a subarray.

    Note this method will return the task wrapped in a :py:class:`ComposeSubarrayJob`
    object representing the concurrent task of allocating resources.

    The user can use :py:meth:`ComposeSubarrayJob.wait_and_teardown_allocation`
    as a robust way for waiting for job to complete and tear down subarray afterwards.

    e.g.:

    .. code-block:: python

        job = dispatch_composed_subarray_job(receptors, sub_id, comp, sb_cf, settings)
        check_states_whilst_composing()
        job.wait_and_teardown_allocation()


    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param subarray_id: Integer used to identify the particular subarray
    :param composition: configuration info about how subarray must be allocated to resources.
    :param sb_config: object holding context information about the Scheduling Block.
    :param settings: Executions settings that must be used during the task.
    :returns: The concurrent task wrapped as a :py:class:`ComposeSubarrayJob` object.
    :raises not_ready_to_compose: when subarray not ready to be set to IDLE
    :raises Exception: when general error occurred that was not about not being ready or not
        occurred during allocation
    """
    entry_point = configuration.get_entry_point()
    try:
        compose.assert_I_can_compose_a_subarray(subarray_id, receptors)
        job = compose.start_subarray_composing(
            subarray_id,
            receptors,
            composition,
            sb_config,
            settings,
            entry_point,
        )
        return ComposeSubarrayJob(job, settings, subarray_id, receptors, entry_point)
    except compose.NotReadyCompose as not_ready_to_compose:
        _handle_not_ready_compose(not_ready_to_compose)
        raise not_ready_to_compose
    except Exception as exception:
        if settings.touched:
            _handle_allocated_subarray_general_error(exception, subarray_id, receptors, settings)
        raise exception


@contextmanager
def allocating_subarray(
    receptors: List[int],
    subarray_id: int,
    composition: Composition,
    sb_config: SBConfig,
    settings: ExecSettings,
    disable_post: Union[DisablePost, bool] = False,
):
    """Create a context in which a subarray is concurrently placed in the IDLE state.

    During the context the subarray will be in a state of allocating resources and
    may change to it's end state within the context. At the end of the context the
    manager will wait for the subarray to get to it's end state and then tear down
    the resources.

    After the context is finished the manager will attempt to tear down and release
    allocated resources to the subarray.

    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param subarray_id: Integer used to identify the particular subarray
    :param composition: configuration info about how subarray must be allocated to resources.
    :param sb_config: object holding context information about the Scheduling Block.
    :param settings: Executions settings that must be used during the task.
    :param disable_post: flag indicating if post steps should be disabled, default = False
    :yields: The allocation job which user can wait upon himself if need be.
    """
    job = dispatch_composed_subarray_job(receptors, subarray_id, composition, sb_config, settings)
    yield job
    if disable_post:
        clear_afterwards = False
    else:
        clear_afterwards = True
    job.wait_and_teardown_allocation(clear_afterwards)


@contextmanager
def tear_down_when_finished(subarray_id: int, receptors: List[int], settings: ExecSettings):
    """Create a context in which the manager expects a subarray to be placed in the IDLE state.

    After the context the manager will check the subarray is ready for tearing down and perform
    the tearing down steps.

    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param subarray_id: Integer used to identify the particular subarray
    :param settings: Executions settings that must be used during the task.
    :yields: None
    """
    entry_point = configuration.get_entry_point()
    try:
        yield
    finally:
        if settings.touched:
            try:
                compose.assert_I_can_tear_down_a_subarray(subarray_id, receptors)
                builders.clear_supscription_specs()
                compose.tear_down_subarray(subarray_id, settings, entry_point)
            except compose.NotReadyTearDown as exception:
                if exception.already_torn_down:
                    logger.warning(
                        "Subarray is already torn down; will not attempt to tear it down"
                    )
                else:
                    logger.exception(exception)


@contextmanager
def wait_for_subarray_allocated(subarray_id: int, settings: ExecSettings):
    """Create a context in which the manager in which composing a subarray will be properly waited.

    :param subarray_id: Integer used to identify the particular subarray
    :param settings: Executions settings that must be used during the task.
    :yields: None
    """
    entry_point = configuration.get_entry_point()
    with compose.wait_for_subarray_allocated(subarray_id, settings, entry_point):
        yield


@contextmanager
def wait_for_subarray_released(subarray_id: int, settings: ExecSettings):
    """Create a context in which the manager in which composing a subarray will be properly waited.

    :param subarray_id: Integer used to identify the particular subarray
    :param settings: Executions settings that must be used during the task.
    :yields: None
    """
    entry_point = configuration.get_entry_point()
    with compose.wait_for_subarray_released(subarray_id, settings, entry_point):
        yield


@contextmanager
def restart_after(
    subarray_id: int,
    settings: ExecSettings,
    entry_point: Union[configuration.EntryPoint, None] = None,
):
    """Provide a context at the end of which an aborted subarray will be cleared and set to IDLE.

    Note the manager expects the user to have placed the subarray in the Aborted state.

    :param entry_point: Optional injected entry_point to use, defaults to None
    :param subarray_id: Integer used to identify the particular subarray
    :param settings: Executions settings that must be used during the task.
    :yields: None
    """
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    yield
    builders.clear_supscription_specs()
    compose.restart_aborted(subarray_id, settings, entry_point)
