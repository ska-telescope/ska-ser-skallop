"""Module dealing with managing the overall tasks necessary to configure a subarray for scanning."""

import logging
from contextlib import contextmanager
from typing import List, Literal, Union

from ska_ser_skallop.event_handling import builders
from ska_ser_skallop.mvp_control.entry_points import configuration
from ska_ser_skallop.mvp_control.subarray import configure as sub_conf
from ska_ser_skallop.mvp_control.subarray import scan
from ska_ser_skallop.mvp_control.subarray.failure_handling import DirtySubarraysHandler
from ska_ser_skallop.mvp_control.subarray.job_scheduling import Job
from ska_ser_skallop.mvp_control.telescope.failure_handling import DirtyTelescopeHandler
from ska_ser_skallop.utils.env import get_temp_dir

from .base import DisablePost
from .types import (
    ExecSettings,
    SBConfig,
    ScanConfiguration,
    ScanConfigurationByFile,
    ScanConfigurationType,
)

logger = logging.getLogger(__name__)

ConfigurationType = Literal["standard"]


def generate_scan_configuration(
    configuration_type: ConfigurationType, file_based: bool = False
) -> ScanConfiguration:
    """Generate an allocation configuration object.

    :param configuration_type: The type of configuration to select.
    :param file_based: Whether configuration must be passed as a file, defaults to True
    :raises NotImplementedError: If given type not yet implemented
    :return: The specified configuration as a configuration object
    """
    if configuration_type == "standard":
        scan_configuration_type = ScanConfigurationType.STANDARD
        if file_based:
            file_location = get_temp_dir()
            return ScanConfigurationByFile(file_location, scan_configuration_type)
        else:
            return ScanConfiguration(scan_configuration_type)
    raise NotImplementedError("Only handles standard configuration type")


def _handle_dirty_subarrays(
    subarray_handler: DirtySubarraysHandler, handler: DirtyTelescopeHandler
):
    if subarray_handler.obstates_of_subarrays_are_not_ok():
        subarray_handler.clear_any_devices_in_obsstate_fault()
        subarray_handler.abort_and_restart_stuck_subarray_devices()
        if handler.telescope_on:
            # this means we can attempt coordinated tear downs
            # then tear down any subarrays in READY
            subarray_handler.take_any_ready_subarrays_to_idle()
        else:
            subarray_handler.abort_and_restart_subarrays()


def _handle_not_ready_configure(exception: sub_conf.NotReadyConfigure):
    logger.warning("subarray not ready to be configured")
    handler = DirtyTelescopeHandler(exception.result)
    subarray_handler = DirtySubarraysHandler(exception.result)
    # TODO add dish handling
    # dish_handler = DirtyDishHandler(exception.result)
    # all devices in dev state FAULT should be rectified in a similar manner
    handler.clear_all_faulty_to_be("ON")
    # now we look at subarrays
    _handle_dirty_subarrays(subarray_handler, handler)


def _telescope_healthy(exception: sub_conf.NotReadyClear):
    subarrays_health = exception.result[sub_conf.sub_labels.subarrays_health]
    masters_health = exception.result[sub_conf.tel_labels.masters_health]
    resources_health = exception.result[sub_conf.tel_labels.resources_health]
    return all(
        [
            subarrays_health.is_ok(),
            masters_health.is_ok(),
            resources_health.is_ok(),
        ]
    )


def _subarray_in_lower_state(exception: sub_conf.NotReadyClear):
    subarrays_obs_state = exception.result[sub_conf.sub_labels.subarrays_obs_state]
    return subarrays_obs_state.are_in_state("IDLE", "EMPTY")


def _handle_not_ready_clear(exception: sub_conf.NotReadyClear):
    logger.warning("subarray not ready to be cleared")
    handler = DirtyTelescopeHandler(exception.result)
    subarray_handler = DirtySubarraysHandler(exception.result)
    # TODO add dish handling
    # dish_handler = DirtyDishHandler(exception.result)
    # all devices in dev state FAULT should be rectified in a similar manner
    handler.clear_all_faulty_to_be("ON")
    # now we look at subarrays
    _handle_dirty_subarrays(subarray_handler, handler)


def _handle_error_whilst_configuring(
    exception: Exception,
    subarray_id: int,
    receptors: List[int],
    settings: ExecSettings,
):
    _ = exception
    try:
        logger.warning(
            f"exception whilst attempting to configure subarray {subarray_id}, will "
            "check if configuration can be cleared.."
        )
        sub_conf.assert_I_can_clear_subarray_configuration(subarray_id, receptors)
        logger.warning(f"attempt to clear subarray {subarray_id} configuration ...")
        sub_conf.clear_subarray_configuration(subarray_id, settings)
    except sub_conf.NotReadyClear as e_shut_down:
        _handle_not_ready_clear(e_shut_down)


def _handle_error_whilst_clearing(exception: Exception, settings: ExecSettings):
    _ = exception
    _ = settings
    logger.warning(
        "exception whilst attempting to clear subarray configuration, will not attempt "
        "to clear it"
    )


def _handle_configured_subarray_general_error(
    exception: Exception,
    subarray_id: int,
    receptors: List[int],
    settings: ExecSettings,
):
    _ = exception
    try:
        logger.warning(
            f"general exception whilst operating on subarray {subarray_id}, will "
            "check if the configuration can be cleared"
        )
        sub_conf.assert_I_can_clear_subarray_configuration(subarray_id, receptors)
        logger.warning(f"attempt to clear subarray {subarray_id} configuration...")
        sub_conf.clear_subarray_configuration(subarray_id, settings)
    except sub_conf.NotReadyClear as e:
        _handle_not_ready_clear(e)


def _handle_scan_failed(
    exception: scan.ScanUnsuccessful,
    subarray_id: int,
    receptors: List[int],
    settings: ExecSettings,
):
    logger.warning(
        f"Exception in executing scan on subarray {subarray_id}, will check if the "
        "configuration can be cleared"
    )
    handler = DirtyTelescopeHandler(exception.result)
    subarray_handler = DirtySubarraysHandler(exception.result)
    handler.clear_all_faulty_to_be("ON")
    subarray_handler.clear_any_aborted_subarrays()
    _handle_dirty_subarrays(subarray_handler, handler)


def configure_subarray(
    subarray_id: int,
    receptors: List[int],
    scan_config: ScanConfiguration,
    sb_config: SBConfig,
    scan_duration: float,
    settings: ExecSettings,
    entry_point: Union[configuration.EntryPoint, None] = None,
):
    """Attempt to correctly place a subarray in the READY state and handle any exceptions.

    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param subarray_id: Integer used to identify the particular subarray
    :param scan_config: configuration info about how subarray must be configured for the scan.
    :param sb_config: object holding context information about the Scheduling Block.
    :param scan_duration: The expected duration which the scan should take.
    :param settings: Executions settings that must be used during the task.
    :param entry_point: Optional injected entry_point to use, defaults to None
    :raises not_ready_to_configure: when subarray not ready to be set to READY
    :raises exception_whilst_configuring: when exception occurred whilst busy setting it to READY
    :raises Exception: when general error occurred that was not about not being ready or not
        occurred during configuration
    """
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    try:
        sub_conf.assert_I_can_configure_a_subarray(subarray_id, receptors)
        sub_conf.configure_subarray(
            subarray_id,
            scan_config,
            sb_config,
            scan_duration,
            settings,
            entry_point,
        )
    except sub_conf.NotReadyConfigure as not_ready_to_configure:
        _handle_not_ready_configure(not_ready_to_configure)
        raise not_ready_to_configure
    except sub_conf.EWhileConfiguring as exception:
        exception_whilst_configuring = exception.base_exception
        if settings.touched:
            _handle_error_whilst_configuring(
                exception_whilst_configuring, subarray_id, receptors, settings
            )
        raise exception_whilst_configuring
    except Exception as exception:
        if settings.touched:
            _handle_configured_subarray_general_error(exception, subarray_id, receptors, settings)
        raise exception


def clear_subarray(
    subarray_id: int,
    receptors: List[int],
    settings: ExecSettings,
    entry_point: Union[configuration.EntryPoint, None] = None,
):
    """Attempt to correctly place an configured subarray in the IDLE state and handle exceptions.

    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param entry_point: Optional injected entry_point to use, defaults to None
    :param subarray_id: Integer used to identify the particular subarray
    :param settings: Executions settings that must be used during the task.
    :raises Exception: any unhandled exceptions raised during shutdown. Note exceptions as
        a consequence of not being ready or that occurred whilst tearing down is not thrown
        but just logged to the logger as exception messages.
    """
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    try:
        if settings.touched:
            sub_conf.assert_I_can_clear_subarray_configuration(subarray_id, receptors)
            sub_conf.clear_subarray_configuration(subarray_id, settings, entry_point)
    except sub_conf.NotReadyClear as e:
        if _subarray_in_lower_state(e):
            if _telescope_healthy(e):
                logger.warning(
                    "it seems the subarray have already been put in a lower state, "
                    f"will not perform clear configuration. Subarray condition:\n"
                    f"{e.result.describe_why_not_ready()}"
                )
                return
        _handle_not_ready_clear(e)
        logger.exception(e)
    except sub_conf.EWhileClearing as e:
        _handle_error_whilst_clearing(e.base_exception, settings)
        logger.exception(e.base_exception)
    except Exception as e:
        if settings.touched:
            _handle_configured_subarray_general_error(e, subarray_id, receptors, settings)
        raise e


@contextmanager
def configured_subarray(
    subarray_id: int,
    receptors: List[int],
    scan_config: ScanConfiguration,
    sb_config: SBConfig,
    scan_duration: float,
    settings: ExecSettings,
    entry_point: Union[configuration.EntryPoint, None] = None,
    disable_post: Union[DisablePost, bool] = False,
):
    """Create a context in which a subarray is placed in the READY state.

    After the context is finished the manager will attempt to clear the configuration
    and place it back in the IDLE state.

    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param subarray_id: Integer used to identify the particular subarray
    :param entry_point: Optional injected entry_point to use, defaults to None
    :param scan_config: configuration info about how subarray must be configured for the scan.
    :param sb_config: object holding context information about the Scheduling Block.
    :param scan_duration: The expected duration which the scan should take.
    :param disable_post: flag indicating if post steps should be disabled, default = False
    :param settings: Executions settings that must be used during the task.
    :yields: None
    """
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    configure_subarray(
        subarray_id,
        receptors,
        scan_config,
        sb_config,
        scan_duration,
        settings,
        entry_point,
    )
    yield
    if not disable_post:
        builders.clear_supscription_specs()
        clear_subarray(subarray_id, receptors, settings, entry_point)


class ConfigureSubarrayJob:
    """Wrapper for concurrent job to configure a scan on a subarray."""

    def __init__(
        self,
        job: Job,
        settings: ExecSettings,
        subarray_id: int,
        receptors: List[int],
        entry_point: Union[configuration.EntryPoint, None] = None,
    ) -> None:
        """Initialise the object.

        :param job: The job representing the task to configure a scan on subarray.
        :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
            subarray.
        :param entry_point: Optional injected entry_point to use, defaults to None
        :param subarray_id: Integer used to identify the particular subarray
        :param settings: Executions settings that must be used during the task.
        """
        self._job = job
        self._settings = settings
        self._subarray_id = subarray_id
        self._receptors = receptors
        self._entry_point = entry_point if entry_point else configuration.get_entry_point()

    def wait_and_clear_configuration(self, clear_afterwards: bool = True):
        """Wait for job configuring a subarray to finish and clear configuration afterwards.

        :param clear_afterwards: Whether to skip clearing the configuration afterwards
            by setting to False, defaults to True meaning tear down will occur.
        :raises Exception: any unhandled exceptions raised during tear down. Note exceptions as
            a consequence of not being ready or that occurred whilst tearing down is not thrown
            but just logged to the logger as exception messages.
        """
        try:
            self._job.wait_until_job_is_finished(self._settings.time_out)
            if clear_afterwards:
                if self._settings.touched:
                    sub_conf.assert_I_can_clear_subarray_configuration(
                        self._subarray_id, self._receptors
                    )
                    builders.clear_supscription_specs()
                    sub_conf.clear_subarray_configuration(
                        self._subarray_id,
                        self._settings,
                        self._entry_point,
                    )
        except sub_conf.EWhileConfiguring as e:
            if self._settings.touched:
                _handle_error_whilst_configuring(
                    e.base_exception,
                    self._subarray_id,
                    self._receptors,
                    self._settings,
                )
            logger.exception(e.base_exception)
        except sub_conf.NotReadyClear as e:
            _handle_not_ready_clear(e)
            logger.exception(e)
        except sub_conf.EWhileClearing as e:
            _handle_error_whilst_clearing(e.base_exception, self._settings)
            logger.exception(e.base_exception)
        except Exception as e:
            if self._settings.touched:
                _handle_configured_subarray_general_error(
                    e, self._subarray_id, self._receptors, self._settings
                )
            raise e


def dispatch_configure_subarray_job(
    subarray_id: int,
    receptors: List[int],
    scan_config: ScanConfiguration,
    sb_config: SBConfig,
    scan_duration: float,
    settings: ExecSettings,
) -> ConfigureSubarrayJob:
    """Create a concurrent task (Job) to attempt to configure a subarray.

    Note this method will return the task wrapped in a :py:class:`ConfigureSubarrayJob`
    object representing the concurrent task of configuring a scan.

    The user can use :py:meth:`ConfigureSubarrayJob.wait_and_clear_configuration`
    as a robust way for waiting for job to complete and clear the configuration afterwards.

    e.g.:

    .. code-block:: python

        job = dispatch_configure_subarray_job(sub_id, receptors, conf, sb_cf, 2, settings)
        check_states_whilst_configuring()
        job.wait_and_clear_configuration()


    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param subarray_id: Integer used to identify the particular subarray
    :param scan_config: configuration info about how subarray must be configured for the scan.
    :param sb_config: object holding context information about the Scheduling Block.
    :param scan_duration: The expected duration which the scan should take.
    :param settings: Executions settings that must be used during the task.
    :returns: The concurrent task wrapped as a :py:class:`ConfigureSubarrayJob` object.
    :raises not_ready_to_configure: when subarray not ready to be set to READY
    :raises Exception: when general error occurred that was not about not being ready or not
        occurred during configuration
    """
    entry_point = configuration.get_entry_point()
    try:
        sub_conf.assert_I_can_configure_a_subarray(subarray_id, receptors)
        job = sub_conf.start_configuring_a_subarray(
            subarray_id,
            scan_config,
            sb_config,
            scan_duration,
            settings,
            entry_point,
        )
        return ConfigureSubarrayJob(job, settings, subarray_id, receptors, entry_point)
    except sub_conf.NotReadyConfigure as not_ready_to_configure:
        _handle_not_ready_configure(not_ready_to_configure)
        raise not_ready_to_configure
    except Exception as exception:
        if settings.touched:
            _handle_configured_subarray_general_error(exception, subarray_id, receptors, settings)
        raise exception


@contextmanager
def configuring_subarray(
    subarray_id: int,
    receptors: List[int],
    scan_config: ScanConfiguration,
    sb_config: SBConfig,
    scan_duration: float,
    settings: ExecSettings,
    clear_afterwards: bool = True,
):
    """Create a context in which a subarray is concurrently placed in the READY state.

    During the context the subarray will be in a state of configuring resources and
    may change to it's end state within the context. At the end of the context the
    manager will wait for the subarray to get to it's end state and then clear the configuration.

    After the context is finished the manager will attempt to clear the applied configuration.

    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param subarray_id: Integer used to identify the particular subarray
    :param scan_config: configuration info about how subarray must be configured for the scan.
    :param sb_config: object holding context information about the Scheduling Block.
    :param scan_duration: The expected duration which the scan should take.
    :param settings: Executions settings that must be used during the task.
    :param clear_afterwards: Whether to skip the tearing down afterwards by settting to False,
        defaults to True meaning tear down will occur.
    :yields: The configuration job which user can wait upon himself if need be.
    """
    job = dispatch_configure_subarray_job(
        subarray_id, receptors, scan_config, sb_config, scan_duration, settings
    )
    yield job
    job.wait_and_clear_configuration(clear_afterwards)


@contextmanager
def check_configuration_when_finished(
    subarray_id: int, receptors: List[int], settings: ExecSettings
):
    """Provide a context in which a subarray's READY state gets validated at the end.

    At the end of the context a `scan.ScanUnsuccessful` exception will be raised if the
    subarray is not in the READY state anymore.

    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param subarray_id: Integer used to identify the particular subarray
    :param settings: Executions settings that must be used during the task.
    :yields: None
    """
    try:
        with scan.check_configuration_when_finished(subarray_id, receptors, settings):
            yield
    except scan.ScanUnsuccessful as exception:
        _handle_scan_failed(exception, subarray_id, receptors, settings)


@contextmanager
def reset_after(
    subarray_id: int,
    settings: ExecSettings,
    entry_point: Union[configuration.EntryPoint, None] = None,
):
    """Provide a context at the end of which an aborted subarray will be cleared and set to IDLE.

    Note the manager expects the user to have placed the subarray in the Aborted state.

    :param entry_point: Optional injected entry_point to use, defaults to None
    :param subarray_id: Integer used to identify the particular subarray
    :param settings: Executions settings that must be used during the task.
    :yields: None
    """
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    yield
    builders.clear_supscription_specs()
    sub_conf.reset_aborted(subarray_id, settings, entry_point)


@contextmanager
def clear_config_when_finished(
    subarray_id: int,
    receptors: List[int],
    settings: ExecSettings,
    entry_point: Union[configuration.EntryPoint, None] = None,
):
    """Provide a context at the end of which an configured subarray will be cleared and set to IDLE.

    Note the manager expects the user to have placed the subarray in the READY state.

    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param entry_point: Optional injected entry_point to use, defaults to None
    :param subarray_id: Integer used to identify the particular subarray
    :param settings: Executions settings that must be used during the task.
    :yields: None
    """
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    try:
        yield
    finally:
        if settings.touched:
            try:
                sub_conf.assert_I_can_clear_subarray_configuration(subarray_id, receptors)
                builders.clear_supscription_specs()
                sub_conf.clear_subarray_configuration(subarray_id, settings, entry_point)
            except sub_conf.NotReadyClear as exception:
                if exception.already_clear:
                    logger.warning(
                        "Subarray is already cleared from configuration;"
                        " will not attempt to clear it."
                    )
                    builders.clear_supscription_specs()
                else:
                    logger.exception(exception)


@contextmanager
def wait_for_subarray_configured(subarray_id: int, receptors: List[int], settings: ExecSettings):
    """Create a context in which the manager in which composing a subarray will be properly waited.

    :param subarray_id: Integer used to identify the particular subarray
    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param settings: Executions settings that must be used during the task.
    :yields: None
    """
    entry_point = configuration.get_entry_point()
    with sub_conf.wait_for_subarray_configured(subarray_id, receptors, settings, entry_point):
        yield
