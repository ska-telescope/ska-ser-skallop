"""Module dealing with overall managing of tasks around setting a subarray to scanning."""

import logging
from contextlib import contextmanager
from typing import List, Union

from ska_ser_skallop.mvp_control.entry_points import configuration
from ska_ser_skallop.mvp_control.subarray import scan
from ska_ser_skallop.mvp_control.subarray.failure_handling import DirtySubarraysHandler
from ska_ser_skallop.mvp_control.subarray.job_scheduling import Job
from ska_ser_skallop.mvp_control.telescope.failure_handling import DirtyTelescopeHandler

from .types import ExecSettings

logger = logging.getLogger(__name__)


def _handle_dirty_subarrays(subarray_handler: DirtySubarraysHandler):
    if subarray_handler.obstates_of_subarrays_are_not_ok():
        subarray_handler.reset_any_devices_in_obsstate_fault()
        subarray_handler.abort_and_reset_stuck_subarray_devices()
        # note the effect from this is a subarray in "IDLE" state


def _handle_except_while_scanning(
    exception: scan.EWhileScanning,
    subarray_id: int,
    receptors: List[int],
    settings: ExecSettings,
):
    logger.warning(f"Exception occurred whilst running a scan {exception.args}")
    try:
        clean_up_after_scan(subarray_id, receptors, settings)
    except scan.ScanUnsuccessful as unsuccessful_exception:
        _handle_except_after_scanning(unsuccessful_exception)
        # note will not raise this exception as it is a result of teardow


def _handle_except_after_scanning(exception: scan.ScanUnsuccessful):
    logger.warning(f"Scan not successfull {exception.args}")
    handler = DirtyTelescopeHandler(exception.result)
    subarray_handler = DirtySubarraysHandler(exception.result)
    handler.clear_all_faulty_to_be("ON")
    _handle_dirty_subarrays(subarray_handler)


def _handle_not_ready_scan(exception: scan.NotReadyScan):
    logger.warning(f"Not ready to perform a scan {exception.args}")
    handler = DirtyTelescopeHandler(exception.result)
    subarray_handler = DirtySubarraysHandler(exception.result)
    handler.clear_all_faulty_to_be("ON")
    _handle_dirty_subarrays(subarray_handler)


def run_scan(subarray_id: int, receptors: List[int], settings: ExecSettings):
    """Attempt to correctly place a subarray in the SCANNING state and block until it is finished.

    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param subarray_id: Integer used to identify the particular subarray
    :param settings: Executions settings that must be used during the task.
    :raises exception_whilst_scanning: when exception occurred whilst busy setting it to SCANNING
    :raises not_ready_for_scan: when subarray is not ready to set to scanning.
    """
    try:
        scan.assert_I_can_run_a_scan(subarray_id, receptors)
        scan.scan(subarray_id, settings)
    except scan.NotReadyScan as not_ready_for_scan:
        _handle_not_ready_scan(not_ready_for_scan)
        raise not_ready_for_scan
    except scan.EWhileScanning as exception_whilst_scanning:
        _handle_except_while_scanning(exception_whilst_scanning, subarray_id, receptors, settings)
        raise exception_whilst_scanning


def clean_up_after_scan(subarray_id: int, receptors: List[int], settings: ExecSettings):
    """Verify scan concluded back to READY without any failings and handle any actual occurrences.

    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param subarray_id: Integer used to identify the particular subarray
    :param settings: Executions settings that must be used during the task.
    :raises scan_unsuccessful: when the scan ended unsuccessfully
    """
    if settings.touched:
        try:
            scan.assert_scan_successfull(subarray_id, receptors)
        except scan.ScanUnsuccessful as scan_unsuccessful:
            _handle_except_after_scanning(scan_unsuccessful)
            raise scan_unsuccessful


class ScanningJob:
    """Wrapper for concurrent job to run a scan on a subarray."""

    def __init__(
        self,
        job: Job,
        settings: ExecSettings,
        subarray_id: int,
        receptors: List[int],
    ) -> None:
        """Initialise the object.

        :param job: The job representing the task to allocate resources to a subarray.
        :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
            subarray.
        :param subarray_id: Integer used to identify the particular subarray
        :param settings: Executions settings that must be used during the task.
        """
        self._job = job
        self._settings = settings
        self._subarray_id = subarray_id
        self._receptors = receptors

    def wait_for_scanning_to_complete(self):
        """Wait for job running a scan to complete and handle possible errors.

        :raises exception_whilst_scanning: if an exception occurred whilst waiting for
            scanning to complete.
        """
        try:
            self._job.wait_until_job_is_finished(timeout=self._settings.time_out)
        except scan.EWhileScanning as exception_whilst_scanning:
            _handle_except_while_scanning(
                exception_whilst_scanning,
                self._subarray_id,
                self._receptors,
                self._settings,
            )
            raise exception_whilst_scanning


def dispatch_scanning(
    subarray_id: int,
    receptors: List[int],
    settings: ExecSettings,
    entry_point: Union[configuration.EntryPoint, None] = None,
) -> ScanningJob:
    """Create a concurrent task (Job) to attempt to run a scan on a subarray.

    Note this method will return the task wrapped in a :py:class:`ScanningJob`
    object representing the concurrent task of running a scan.

    The user can use :py:meth:`ScanningJob.wait_for_scanning_to_complete`
    as a robust way for waiting for job to complete.

    e.g.:

    .. code-block:: python

        job = dispatch_scanning(receptors, sub_id, settings)
        check_states_whilst_composing()
        job.wait_and_teardown_allocation()

    :param subarray_id: Integer used to identify the particular subarray
    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param settings: Executions settings that must be used during the task.
    :param entry_point: Optional injected entry_point to use, defaults to None
    :returns: The concurrent task wrapped as a :py:class:`ScanningJob` object.
    :raises not_ready_to_scan: when subarray not ready to run a scan.
    """
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    try:
        scan.assert_I_can_run_a_scan(subarray_id, receptors)
        job = scan.start_running_scan(subarray_id, settings, entry_point)
        return ScanningJob(job, settings, subarray_id, receptors)
    except scan.NotReadyScan as not_ready_to_scan:
        _handle_not_ready_scan(not_ready_to_scan)
        raise not_ready_to_scan


@contextmanager
def scanning_subarray(
    subarray_id: int,
    receptors: List[int],
    settings: ExecSettings,
    clean_up_after_scanning: bool = True,
):
    """Create a context in which a subarray is concurrently placed in the SCANNING state.

    During the context the subarray will be busy scanning and
    may change to it's end state (READY) within the context. At the end of the context the
    manager will wait for the subarray to get to it's end state and then verify no failures
    occurred during the task.

    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param subarray_id: Integer used to identify the particular subarray
    :param settings: Executions settings that must be used during the task.
    :param clean_up_after_scanning: Whether to clean up afterwards. E.g. whether to verify
        no failures occurred during the task, defaults to True
    :yields: The scanning job which user can wait upon himself if need be.
    """
    entry_point = configuration.get_entry_point()
    job = dispatch_scanning(subarray_id, receptors, settings, entry_point)
    yield job
    job.wait_for_scanning_to_complete()
    if clean_up_after_scanning:
        clean_up_after_scan(subarray_id, receptors, settings)


@contextmanager
def scan_subarray(
    subarray_id: int,
    receptors: List[int],
    settings: ExecSettings,
    clean_up_after_scanning: bool = True,
):
    """Create a context in which a subarray finishes a scan before entering it.

    When the context finishes the subarray will be verified that no failures
    occurred during the scan.

    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param subarray_id: Integer used to identify the particular subarray
    :param settings: Executions settings that must be used during the task.
    :param clean_up_after_scanning: wether to clean up after scanning
    :yields: None
    """
    run_scan(subarray_id, receptors, settings)
    yield
    if clean_up_after_scanning:
        clean_up_after_scan(subarray_id, receptors, settings)


@contextmanager
def check_configuration_when_finished(
    subarray_id: int, receptors: List[int], settings: ExecSettings
):
    """Create a context in which a manager will afterwards verify a scan was successfull.

    :param receptors: a list of dish receptors (in the case of Mid telescope) to allocate to the
        subarray.
    :param subarray_id: Integer used to identify the particular subarray
    :param settings: Executions settings that must be used during the task.
    :yields: None
    """
    try:
        yield
    finally:
        if settings.touched:
            scan.assert_scan_successfull(subarray_id, receptors)
