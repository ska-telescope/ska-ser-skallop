"""Classes dealing with high level management of MVP (SUT) in the context of an SKA Telescope."""

import logging
from contextlib import contextmanager
from typing import Literal, Union

from ska_ser_skallop.event_handling import builders
from ska_ser_skallop.mvp_control.dishes.failure_handling import DirtyDishHandler
from ska_ser_skallop.mvp_control.entry_points import configuration
from ska_ser_skallop.mvp_control.subarray.failure_handling import DirtySubarraysHandler
from ska_ser_skallop.mvp_control.telescope import start_up as tel
from ska_ser_skallop.mvp_control.telescope.failure_handling import DirtyTelescopeHandler
from ska_ser_skallop.utils import env

from .base import DisablePost
from .types import ExecSettings

logger = logging.getLogger(__name__)


class TelState:
    """Abstraction of Telescope as a simple object with a state."""

    state: Literal["ON", "OFF"] = "OFF"


def _handle_dirty_subarrays(
    subarray_handler: DirtySubarraysHandler, handler: DirtyTelescopeHandler
):
    if subarray_handler.obstates_of_subarrays_are_not_ok():
        subarray_handler.clear_any_devices_in_obsstate_fault()
        subarray_handler.abort_and_restart_stuck_subarray_devices()
        if handler.telescope_on:
            # this means we can attempt coordinated tear downs
            # first tear down any subarrays in IDLE
            subarray_handler.take_any_idle_subarrays_to_empty()
            # then tear down any subarrays in READY
            subarray_handler.take_any_ready_subarrays_to_empty()
        else:
            subarray_handler.abort_and_restart_subarrays()


def _handle_not_ready_shutdown(exception: tel.NotReadyShutdown, settings: ExecSettings):
    logger.warning(
        f"telescope not ready to be shutdown: {exception.result.describe_why_not_ready()}"
    )
    handler = DirtyTelescopeHandler(exception.result)
    subarray_handler = DirtySubarraysHandler(exception.result)
    if env.telescope_type_is_mid():
        dish_handler = DirtyDishHandler(exception.result)
        # switch on any dish masters
        dish_handler.switch_on_any_off_dishes()
    # first check general state
    if handler.is_telescope_in_wrong_state(["OFF", "STANDBY"]):
        logger.warning("It seems the entire telescope is already off will not attempt to shutdown")
        return
    # all devices in dev state FAULT should be rectified in a similar manner
    handler.clear_all_faulty_to_be("ON")
    # now we look at resources in general
    handler.switch_on_any_off_resources()
    # now we look at subarrays
    _handle_dirty_subarrays(subarray_handler, handler)
    # now we look at masters
    handler.switch_on_any_off_masters()
    # now we attempt at putting the telescope in standby
    logger.warning(
        "Attempting to put the telescope in standby after rectifying failed " "conditions..."
    )
    tel.set_telescope_to_standby(settings)
    logger.warning("telescope was put back to standby")


def _handle_not_ready_startup(exception: tel.NotReadyStartUp, settings: ExecSettings):
    logger.warning("telescope not ready to set to running")
    handler = DirtyTelescopeHandler(exception.result)
    subarray_handler = DirtySubarraysHandler(exception.result)
    dish_handler = DirtyDishHandler(exception.result)
    if handler.is_telescope_in_wrong_state(["ON"]):
        _handle_dirty_subarrays(subarray_handler, handler)
        logger.warning(
            "It seems the entire telescope is already ON will attempt to shut it " "down..."
        )
        try:
            tel.assert_I_can_set_telescope_to_standby(
                settings.nr_of_subarrays, settings.nr_of_receptors
            )
            tel.set_telescope_to_standby(settings)
        except tel.NotReadyShutdown as shutdown_exception:
            _handle_not_ready_shutdown(shutdown_exception, settings)
        return
    if handler.telescope_on:
        # if all the masters are ON we can attempt to switch down the telescope
        logger.warning("it appears the telescope is already on, will switch it off...")
        try:
            tel.assert_I_can_set_telescope_to_standby(
                settings.nr_of_subarrays, settings.nr_of_receptors
            )
            tel.set_telescope_to_standby(settings)
        except tel.NotReadyShutdown as shutdown_exception:
            _handle_not_ready_shutdown(shutdown_exception, settings)
        return
    # all devices in dev state FAULT should be rectified in a similar manner
    handler.clear_all_faulty_to_be("OFF")
    # switch off any ON dishes
    dish_handler.switch_off_any_on_dishes()
    # now we look at resources in general
    handler.switch_off_any_on_resources()
    # next is subarrays
    _handle_dirty_subarrays(subarray_handler, handler)
    subarray_handler.switch_off_any_on_subarrays()
    handler.switch_off_any_on_masters()


def _handle_error_whilst_starting_up(exception: Exception, settings: ExecSettings):
    _ = exception
    try:
        logger.warning(
            "exception whilst attempting to start up telescope, will check if it can "
            "be shutdown..."
        )
        tel.assert_I_can_set_telescope_to_standby(
            settings.nr_of_subarrays, settings.nr_of_receptors
        )
        tel.set_telescope_to_standby(settings)
        logger.warning("telescope has been put into standby")
    except tel.NotReadyShutdown as e_shut_down:
        _handle_not_ready_shutdown(e_shut_down, settings)


def _handle_error_whilst_shutting_down(exception: Exception, settings: ExecSettings):
    _ = exception
    _ = settings
    logger.warning(
        "exception whilst attempting to shut down telescope, will not attempt to shut " "down"
    )


def _handle_running_telescope_general_error(exception: Exception, settings: ExecSettings):
    _ = exception
    try:
        logger.warning(
            "general exception whilst running telescope, will check if it can be " "shutdown..."
        )
        tel.assert_I_can_set_telescope_to_standby(
            settings.nr_of_subarrays, settings.nr_of_receptors
        )
        tel.set_telescope_to_standby(settings)
        logger.warning("telescope has been put into standby")
    except tel.NotReadyShutdown as e_shut_down:
        _handle_not_ready_shutdown(e_shut_down, settings)


def run_a_telescope(
    running_telescope_settings: ExecSettings,
    entry_point: Union[configuration.EntryPoint, None] = None,
):
    """Attempt to correctly place a telescope in the running state and handle any exceptions.

    :param running_telescope_settings: Executions settings that must be used during the task.
    :param entry_point: Optional injected entry_point, defaults to None
    :raises exception_whilst_starting_up: when an error occurred whilst attempting to set Telescope
        to ON.
    :raises not_ready_startup: when telescope was not ready to be started up
    :raises Exception: general exception which was not one of the previous
    """
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    settings = running_telescope_settings
    try:
        tel.assert_I_can_set_telescope_to_running(
            nr_of_subarrays=settings.nr_of_subarrays,
            nr_of_receptors=settings.nr_of_receptors,
        )
        tel.set_telescope_to_running(settings, entry_point)
    except tel.NotReadyStartUp as not_ready_startup:
        try:
            _handle_not_ready_startup(not_ready_startup, settings)
        except Exception as exception_whilst_getting_ready:
            raise exception_whilst_getting_ready from not_ready_startup
        raise not_ready_startup
    except tel.EWhileStartingUp as error_whilst_starting_up:
        exception_whilst_starting_up = error_whilst_starting_up.base_exception
        _handle_error_whilst_starting_up(exception_whilst_starting_up, settings)
        raise exception_whilst_starting_up
    except Exception as general_exception:
        _handle_running_telescope_general_error(general_exception, settings)
        raise general_exception


def tear_down_a_telescope(
    running_telescope_settings: ExecSettings,
    entry_point: Union[configuration.EntryPoint, None] = None,
):
    """Attempt to correctly place a telescope in the standby or OFF state and handle any exceptions.

    :param running_telescope_settings: Executions settings that must be used during the task.
    :param entry_point: optional injected entrypoint to use for commanding SUT, defaults to None
    :raises Exception: any unhandled exceptions raised during shutdown. Note exceptions as
        a consequence of not being ready or that occurred whilst shutting down is not thrown
        but just logged to the logger as exception messages.
    """
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    settings = running_telescope_settings
    try:
        tel.assert_I_can_set_telescope_to_standby(
            settings.nr_of_subarrays, settings.nr_of_receptors
        )
        tel.set_telescope_to_standby(settings, entry_point)
    except tel.NotReadyShutdown as not_ready_shutdown:
        _handle_not_ready_shutdown(not_ready_shutdown, settings)
        logger.exception(not_ready_shutdown)
    except tel.EWhileShuttingDown as whilst_shutting_down:
        exception_whilst_shutting_down = whilst_shutting_down.base_exception
        _handle_error_whilst_shutting_down(exception_whilst_shutting_down, settings)
        logger.exception(exception_whilst_shutting_down)
    except Exception as general_exception:
        _handle_running_telescope_general_error(general_exception, settings)
        raise general_exception


def set_offline_components_to_online(settings: ExecSettings):
    """Set any tango device an offline mode to online so as to enable its functionality.

    :param settings: Executions settings that must be used during the task.
    """
    tel.set_telescope_to_online(settings)


def wait_sut_ready_for_session(settings: ExecSettings):
    """Set any tango device an offline mode to online so as to enable its functionality.

    :param settings: Executions settings that must be used during the task.
    """
    tel.wait_sut_ready_for_session(settings)


def I_can_set_telescope_to_standby(settings: ExecSettings) -> bool:
    """Evaluate if a telescope is ready to be shutdown.

    :param settings: Executions settings that must be used during the task.
    :return: True if the telescope is ready.
    """
    try:
        tel.assert_I_can_set_telescope_to_standby(
            settings.nr_of_subarrays, settings.nr_of_receptors
        )
    except tel.NotReadyShutdown:
        return False
    return True


def _handle_not_ready_running(exception: tel.NotReadyRunning, settings: ExecSettings):
    logger.warning("telescope not ready to be running")
    handler = DirtyTelescopeHandler(exception.result)
    subarray_handler = DirtySubarraysHandler(exception.result)
    if env.telescope_type_is_mid():
        dish_handler = DirtyDishHandler(exception.result)
        # switch on any dish masters
        dish_handler.switch_on_any_off_dishes()
    # first check general state
    if handler.is_telescope_in_wrong_state(["OFF", "STANDBY"]):
        logger.warning("It seems the entire telescope is off will not attempt to shutdown")
        run_a_telescope(settings)
        return
    # all devices in dev state FAULT should be rectified in a similar manner
    handler.clear_all_faulty_to_be("ON")
    # now we look at resources in general
    handler.switch_on_any_off_resources()
    # now we look at subarrays
    _handle_dirty_subarrays(subarray_handler, handler)
    # now we look at masters
    handler.switch_on_any_off_masters()


@contextmanager
def standby_telescope(settings: ExecSettings, tel_state: TelState):
    """Create a context in which a telescope is placed in OFF state.

    After the context is finished the manager will check that the user
    has placed the telescope in an ON state and raise an error if not so.

    In other words the context creates an environment in which a user can place
    the telescope to ON and afterwards verified by the manager that it was
    done correctly.

    :param settings: Executions settings that must be used during IO tasks.
    :param tel_state: an object for which the state will be changed to OFF
        for the duration of the context and to ON if successfully done so by user.
    :yields: None
    :raises exception: when the telescope was not correctly set to ON
    """
    entry_point = configuration.get_entry_point()
    tear_down_a_telescope(settings, entry_point)
    tel_state.state = "OFF"
    yield
    try:
        tel.assert_telescope_running(settings.nr_of_subarrays, settings.nr_of_receptors)
        tel_state.state = "ON"
    except tel.NotReadyRunning as exception:
        _handle_not_ready_running(exception, settings)
        raise exception


@contextmanager
def running_telescope(
    settings: ExecSettings,
    tel_state: TelState,
    disable_post: Union[DisablePost, bool] = False,
):
    """Create a context in which a telescope is placed in ON and taken back to OFF afterwards.

    :param settings: Executions settings that must be used during IO tasks.
    :param disable_post: flag indicating if post steps should be disabled, default = False
    :yields: None
    :param tel_state: an object for which the state will be changed to ON
        for the duration of the context and to OFF afterwards.
    """
    entry_point = configuration.get_entry_point()
    run_a_telescope(settings, entry_point)
    tel_state.state = "ON"
    yield
    if not disable_post:
        builders.clear_supscription_specs()
        tel_state.state = "OFF"
        tear_down_a_telescope(settings, entry_point)


@contextmanager
def maintain_telescope_to_running(settings: ExecSettings, tel_state: TelState):
    """Create a context in which a telescope is expected to be correctly placed in ON.

    After the context has finished the manager will check that the telescope is correctly
    in the ON state.

    :param settings: Executions settings that must be used during IO tasks.
    :yields: None
    :param tel_state: an object for which the state will be changed to ON
        for the duration of the context.
    :raises exception: If the telescope was afterwards not correctly maintained ON.
    """
    yield
    try:
        tel.assert_telescope_running(settings.nr_of_subarrays, settings.nr_of_receptors)
        tel_state.state = "ON"
    except tel.NotReadyRunning as exception:
        _handle_not_ready_running(exception, settings)
        raise exception


@contextmanager
def tear_down_when_finished(
    settings: ExecSettings,
    tel_state: TelState,
    entry_point: Union[configuration.EntryPoint, None] = None,
):
    """Create a context in which a user can set a telescope to ON.

    At the end of the context the telescope will automatically be switched to OFF.

    :param settings: Executions settings that must be used during IO tasks.
    :yields: None
    :param tel_state: an object for which the state will be changed to ON
        for the duration of the context.
    :param entry_point: Optional injected entry_point to use, defaults to None
    :raises not_ready_for_start_up: if the telescope was not ready for switching on.
    :raises Exception: if unable to handle the telescope not being ready for switching on.
    """
    entry_point = entry_point if entry_point else configuration.get_entry_point()
    try:
        tel.assert_I_can_set_telescope_to_running(
            nr_of_subarrays=settings.nr_of_subarrays,
            nr_of_receptors=settings.nr_of_receptors,
        )
        tel_state.state = "OFF"
        yield
    except tel.NotReadyStartUp as not_ready_for_start_up:
        try:
            _handle_not_ready_startup(not_ready_for_start_up, settings)
        except Exception as unable_to_handle_not_ready_for_startup:
            raise unable_to_handle_not_ready_for_startup from not_ready_for_start_up
        raise not_ready_for_start_up
    finally:
        if tel_state.state == "ON":
            tear_down_a_telescope(settings, entry_point)
            tel_state.state = "OFF"


@contextmanager
def wait_for_starting_up(settings: ExecSettings):
    """Create a context in which switching a telescope on will be properly waited for.

    :param settings: Executions settings that must be used during IO tasks.
    :yields: None
    """
    entry_point = configuration.get_entry_point()
    with tel.wait_for_starting_up(settings, entry_point):
        yield


@contextmanager
def wait_for_shutting_down(settings: ExecSettings):
    """Create a context in which switching a telescope off will be properly waited for.

    :param settings: Executions settings that must be used during IO tasks.
    :yields: None
    """
    entry_point = configuration.get_entry_point()
    with tel.wait_for_shutting_down(settings, entry_point):
        yield
