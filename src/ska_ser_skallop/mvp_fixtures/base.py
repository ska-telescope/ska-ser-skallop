"""Contains base dependencies used by fixture modules."""

import copy
from typing import Any

from ska_ser_skallop.event_handling.handlers import Recorder
from ska_ser_skallop.event_handling.logging import DeviceLogLevel, LogSpec
from ska_ser_skallop.mvp_control.event_waiting import base as waiting_base


class ExecSettings:
    """Contain execution related settings for a particular pytest test call.

    This object get's used in almost all the calls entailing IO execution with the SUT.
    It has a set of default arguments for the most common case in which the tester require
    no specific debug related settings. If a tester requires to investigate a particular call
    (or set of calls) this object can be configured in a fixture method and injected where needed.

    """

    def __init__(self, request_node: Any = None):
        """Initialise the object.

        If initialised with a pytest request node, the object
        will use it to record whether a test has failed or not.

        :param request_node: The pytest request node indicating
            the test context (function), defaults to None
        """
        self.request_node = request_node
        self.touched = False
        self.log_enabled = False
        self.play_logbook: waiting_base.PlaySpec = waiting_base.PlaySpec(False)
        self.wait_when_faulty: bool = False
        self.time_out: float = 100
        self._log_spec: LogSpec = LogSpec()
        self.attr_synching = False
        self.nr_of_subarrays = 2
        self.nr_of_receptors = 4
        self.recorder = Recorder()

    def replica(
        self,
        log_enabled: bool | None = None,
        wait_when_faulty: bool | None = None,
        time_out: float | None = None,
    ) -> "ExecSettings":
        """Create a replica of the setting to pass as a differentiated argument for a single case.

        This allows a user to set a specific setting for a single call without affecting the
        global settings.

        e.g.:

        .. code-block:: python

            call_with(settings.replica(time_out=10))

            # or alternatively

            call_with(settings.replica().run_with_live_logging())

        :param log_enabled: Whether live log events from remote devices should be generated to
            stdout
        :param wait_when_faulty: Whether to continue waiting for events if a fault occurred
        :param time_out: The maximum amount of time to wait for waiting for events.
        :return: A clone or deep copy of the current object with attributes changed accordingly
        """
        # stash request node before copying
        stashed_request_node = self.request_node
        self.request_node = None
        clone = copy.deepcopy(self)
        self.request_node = stashed_request_node
        clone.request_node = stashed_request_node
        self.recorder = Recorder()
        if log_enabled:
            clone.log_enabled = log_enabled
        if wait_when_faulty:
            clone.wait_when_faulty = wait_when_faulty
        if time_out:
            clone.time_out = time_out
        return clone

    def touch(self):
        """Set a touched flag indicating an SUT has been affected.

        Use this to determine possible tear down strategies if a failure caused
        test code to exit before test finished.

        """
        self.touched = True

    def exec_finished(self) -> bool:
        """Indicate if the current test call phase has finished.

        Note: if the object was initialised without a pytest node
        as argument, it will always return negative. In other words
        a test is assumed to have not yet finished the call phase.

        :return: True if the test call phase finished
        """
        if self.request_node:
            if hasattr(self.request_node, "rep_call"):
                return True
        return False

    def exec_failed(self) -> bool:
        """Indicate if the current test has failed.

        Note: if the object was initialised without a pytest node
        as argument, it will always return negative. In other words
        a test is assumed to have passed if it is unable to determine
        whether it has passed or failed.

        :return: True if the test has failed
        """
        if self.request_node:
            if hasattr(self.request_node, "rep_call"):
                return self.request_node.rep_call.failed
        return False

    def clear_touch(self):
        """Reset the flag indicating an SUT has been touched."""
        self.touched = False

    def get_log_specs(self) -> LogSpec:
        """Get the specifications for capturing logs from remote devices.

        :returns: the specifications for capturing logs from remote devices
        """
        return self._log_spec

    def continue_waiting_for_events_after_fault(self):
        """Set a flag indicating test code should wait for expected events after fault occurred."""
        self.wait_when_faulty = True

    def run_with_live_logging(self):
        """Set flag to display on stdout log messages as they are captured."""
        self.log_enabled = True

    def replay_events_afterwards(self, filter_logs: bool = True, log_filter_pattern: str = ""):
        """Specify how events should be replayed after an event capturing context finished.

        :param filter_logs: Weather logs from remote tango devices should
            be filtered out, defaults to True
        :param log_filter_pattern: specify the regex pattern to
            filter logs from remote devices if they are required
            to be replayed, defaults to ""
        """
        self.play_logbook = self.play_logbook.replicate(
            enabled=True,
            filter_logs=filter_logs,
            log_filter_pattern=log_filter_pattern,
        )

    def capture_logs_from(
        self,
        device_name: str,
        log_level: DeviceLogLevel = DeviceLogLevel.LOG_INFO,
    ):
        """Add an entry to the internal remote device log capturing spec for a device.

        :param device_name: The device for which logs must be captured with.
        :type device_name: str
        :param log_level: The level at which logs
            must be captured, defaults to DeviceLogLevel.LOG_INFO
        """
        self._log_spec.add_log(device_name, log_level)

    def capture_logs_from_devices(self, *device_names: str):
        """Add a set of log capturing entries for a list of device names.

        Note the log level for each device will be fixed to `DeviceLogLevel.LOG_INFO`
        as a set of positional arguments.
        """  # noqa: DAR101
        self._log_spec.add_logs(*device_names)
