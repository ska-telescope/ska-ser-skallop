"""Module delivering a reflector object for writing data to a test device."""

import time
from concurrent.futures import Future, ThreadPoolExecutor
from contextlib import contextmanager
from threading import Event
from typing import Any, List, Union

from ska_ser_skallop.connectors.configuration import get_device_proxy

from .base import ExecSettings
from .context_management import TelescopeContext


class Reflector:
    """Object that can be used to write data to a tango device attribute in different manners."""

    def __init__(
        self,
        telescope_context: TelescopeContext,
        settings: ExecSettings,
        device_name: str = "sys/tg_test/1",
        device_attr: str = "string_scalar",
    ) -> None:
        """Init object.

        :param telescope_context: object that can be used to load context managers
            into a pytest context.
        :param settings: object referencing execution settings
        :param device_name: tango device name which will be written to, defaults to "sys/tg_test/1"
        :param device_attr: tango device attr which will be written to, defaults to "string_scalar"
        """
        self._device_name = device_name
        self._attribute = device_attr
        self._telescope_context = telescope_context
        self._settings = settings
        self._thread: Union[None, Future[None]] = None

    @property
    def name(self) -> str:
        """Return the tango device name which will be written to.

        :return: tango device name which will be written to
        """
        return self._device_name

    @property
    def attribute(self) -> str:
        """Return the tango device attr which will be written to.

        :return: the tango device attr which will be written to
        """
        return self._attribute

    def reflect(
        self,
        value: Union[str, List[str]],
        concurrent: bool = False,
        period: float = 0,
    ):
        """Write a value or list of values to test device.

        The attribute of the test device can then be subscribed to by your SUT.
        The writing routine can either happen concurrently as a thread or inline
        with the testing script (i.e. blocking for each write).

        Note that if writing concurrently the thread task will attempt to finish even if the test
        fails.

        :param value: _description_
        :param concurrent: _description_, defaults to False
        :param period: _description_, defaults to 0
        """
        self._telescope_context.push_context_onto_test(self._reflect(value, concurrent, period))

    def block_until_finished(self, timeout: float = 5):
        """Block until a concurrent reflection is completed.

        Note if now thread has been created as a result of a concurrent flag, it will raise an
        exception.

        :raises AssertionError: when non thread was created.
        :param timeout: how long to wait before an timed out event should occur
        """
        thread_complete = Event()

        def done_callback(_: Any):
            thread_complete.set()

        assert (
            self._thread
        ), "No concurrent thread exists, did you set one with reflect flag concurrent=True?"
        self._thread.add_done_callback(done_callback)
        thread_complete.wait(timeout)

    @contextmanager
    def _reflect(
        self,
        value: Union[str, List[str]],
        concurrent: bool = False,
        period: float = 0,
    ):
        def _write(values: List[str], period: float):
            proxy = get_device_proxy(self._device_name)
            for val in values:
                if not self._settings.exec_finished():
                    proxy.write_attribute(self._attribute, val)
                    if not self._settings.exec_finished():
                        # prevent going into sleep if test call already finished
                        # in case of concurrent runs
                        time.sleep(period)

        values = value if isinstance(value, List) else [value]
        if not concurrent:
            _write(values, period)
            yield
        else:
            with ThreadPoolExecutor(max_workers=1) as pool:
                self._thread = pool.submit(_write, values, period)
                yield
