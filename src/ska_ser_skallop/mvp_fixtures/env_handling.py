"""Manage externally set environmental settings in the context of mvp fixtures."""

import copy
import logging
from contextlib import ExitStack
from typing import Any, Callable, ContextManager, List, Literal, Union

from ska_ser_skallop.mvp_control.entry_points import configuration as entry_conf
from ska_ser_skallop.mvp_control.entry_points.base import EntryPoint

from .base import ExecSettings

logger = logging.getLogger("__name__")


class ExecEnv:
    """Contains a set of user defined env settings in teh context of a pytest execution.

    This in analogous to a set of env settings that a user can define by injecting
    specific settings into the object.

    """

    def __init__(
        self,
        entrypoint: Union[
            Literal["tmc", "sdp", "csp", "oet", "mock"],
            Callable[[], EntryPoint],
            EntryPoint,
            None,
        ] = None,
        telescope_type: Union[Literal["skalow", "skamid"], None] = None,
        maintain_on: Union[bool, None] = None,
        scope: Union[None, List[str]] = None,
        session_scope: Union[None, List[str]] = None,
    ) -> None:
        """Initialise the object.

        :param entrypoint: A given entrypoint to use, defaults to None
        :param telescope_type: The telescope Type to use, defaults to None
        :param session_scope: The list of tags that defines to filter devices to be monitored
            over the scope of the session.
        :param scope: The list of tags that defines to filter devices to be monitored
            over the scope of the test.
        :param maintain_on: Whether the SUT must be maintained on for entire session
            , defaults to True
        """
        self.entrypoint = entrypoint
        self.session_entry_point = entrypoint
        self.telescope_type: Union[Literal["skalow", "skamid"], None] = telescope_type
        self.maintain_on = maintain_on
        self.scope = scope
        self.session_scope = session_scope


class ExecutionEnvironment:
    """Contain the execution environment state and settings for a given test."""

    def __init__(
        self,
        exitstack: ExitStack,
        settings: ExecSettings = ExecSettings(),
        request_node: Any = None,
        entrypoint: EntryPoint | None = None,
    ) -> None:
        """Initialise the object.

        :param exitstack: The test exit stack to use, scoped either by session or test function.
        :param settings: Test general test executions to use for
            the duration of the test. Use this to set the settings for
            all the calls you intend to make during the test, defaults to ExecSettings()
        :param entrypoint: An explicit entrypoint object to use.
        :param request_node: The pytest fixture request node to use for
            determining if a test failed or passed, defaults to None in which case no
            test verification will take place and is assumed to have passed.
        """
        self._entrypoint = entrypoint
        self._exec_settings = settings
        self._touched = False
        self._request_node = request_node
        self._exit_stack = exitstack

    def touch(self):
        """Set a touched flag indicating an SUT has been affected.

        Use this to determine possible tear down strategies if a failure caused
        test code to exit before test finished.

        """
        self._touched = True

    def add_to_teardown_stack(self, c_m: ContextManager[Any]):
        """Set a particular context manager's exit to occur at tear down of test.

        :param c_m: The context manager.
        :return: The result after the entry part of context manager has been called.
        """
        return self._exit_stack.enter_context(c_m)

    @property
    def base_settings(self) -> ExecSettings:
        """Return a clone of the current exec settings.

        Use it to change specific fields for a specific call without
        affecting the general execution settings.

        :return: The cloned execution settings object.
        """
        settings = copy.copy(self.base_settings)
        return settings

    @property
    def touched(self) -> bool:
        """Indicate if the SUT has been affected by an IO call.

        :return: True if the SUT has been affected by an IO call.
        """
        return self._touched

    def execution_failed(self):
        """Indicate if the current test has failed.

        Note: if the object was initialised without a pytest node
        as argument, it will always return negative. In other words
        a test is assumed to have passed if it is unable to determine
        whether it has passed or failed.

        :return: True if the test has failed
        """
        if self._request_node:
            if hasattr(self._request_node, "rep_call"):
                return self._request_node.rep_call.failed
        return False

    def clear_touch(self):
        """Reset the flag indicating an SUT has been touched."""
        self._touched = False

    def set_entry_point(
        self,
        entry_point: Union[str, Callable[[], EntryPoint]],
        reason: str = "",
    ):
        """Configure the environment to explicitly use a given entry point object.

        Note this will inject an entry point into singleton factories and affect all
        configuration calls in internal objects requiring a factory to provide it with
        an entry point.

        :param entry_point: The entry point class to use.
        :param reason: A human description of the client that has set the entry point
            (usefull for debugging), defaults to ""
        """
        entry_conf.reset_configurations()
        if isinstance(entry_point, str):
            entry_point_cls = entry_conf.determine_entry_point(entry_point)
        else:
            entry_point_cls = entry_point
        entry_conf.set_entry_point(entry_point_cls, reason)
        self._entrypoint = entry_point_cls()

    @property
    def entry_point(self):
        """Return the current entrypoint that has been configured.

        :return: the current entrypoint that has been configured.
        """
        if not self._entrypoint:
            self.set_entry_point("tmc")
        return self._entrypoint
