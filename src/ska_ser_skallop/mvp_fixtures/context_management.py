"""Module dealing with managing a test context (tear down and setup)."""

import logging
from contextlib import ExitStack, contextmanager
from typing import ContextManager, List, TypeVar, Union

from ska_ser_skallop.mvp_fixtures.base import ExecSettings
from ska_ser_skallop.mvp_management import telescope_management as tel
from ska_ser_skallop.mvp_management.base import DisablePost
from ska_ser_skallop.mvp_management.subarray_composition import (
    allocated_subarray,
    allocating_subarray,
    restart_after,
    tear_down_when_finished,
    wait_for_subarray_allocated,
    wait_for_subarray_released,
)
from ska_ser_skallop.mvp_management.subarray_configuration import (
    clear_config_when_finished,
    configured_subarray,
    configuring_subarray,
    reset_after,
    wait_for_subarray_configured,
)
from ska_ser_skallop.mvp_management.subarray_scanning import (
    check_configuration_when_finished,
    scan_subarray,
    scanning_subarray,
)
from ska_ser_skallop.mvp_management.types import SBConfig, conf_types
from ska_ser_skallop.utils.coll import IndexedDictionary

logger = logging.getLogger(__name__)

T = TypeVar("T")


class StackableContext:
    """Enable contexts to be loaded as an execution stack for a particular test.

    This allows a tester to specify at particular points in the code execution to
    load a setup and 'tear down' as a stack.

    E.g.:

    .. code-block:: python

        @pytest.fixture()
        def stack():
            with ExitStack() as stack:
                yield stack

        @contextmanager
        def my_context():
            set_up()
            yield
            tear_down()

        def test_it(stack):
            context = StackableContext(stack)
            # failures in code before this point will not cause tear_down() to be called
            # this will result in set_up() being called
            context.push_context_onto_test(my_context())
            # test will fail but then pytest will call tear_down() after logging FAIL
            assert False


    """

    def __init__(
        self,
        session_stack: ExitStack,
        test_stack: Union[None, ExitStack] = None,
    ) -> None:
        """Initialise object.

        The objects gets ExitStack objects as input arguments.
        The underlying implication is that the client has setup these
        exit stack objects within a pytest fixture with scope corresponding
        to the type of stack (e.g. session will be scope = 'session'):

        .. code-block:: python

            @pytest.fixture(scope='session')
            def session_stack():
                with ExitStack() as stack:
                    yield stack

            @pytest.fixture
            def test_stack():
                with ExitStack() as stack:
                    yield stack

            @pytest.fixture
            def context_with_session_and_function_scope(session_stack, test_stack):
                return StackableContext(session_stack, test_stack)

            @pytest.fixture
            def context_with_only_session_scope(session_stack, test_stack):
                # cm's pushed on test stack will only exit at the end of session
                return StackableContext(session_stack)

            @pytest.fixture
            def context_with_only_test_scope(test_stack):
                # cm's pushed on session stack will exit at the end of test
                # use this if you don't want to discremenate between session and test scope
                return StackableContext(test_stack)

        Note that if only a test_session is given the session stack will be used
        for all contextmanagers (session and normal calls). This allows for optionally
        changing teardowns that normally occur at the end of a test to only occur at the
        end of a session.

        Note a test session can specifically be set after initialisation also.

        :param session_stack: The ExitStack object to use for sessions,
            if no test_stack is given then the session stack will be used
            as a test_session also.
        :param test_stack: The ExitStack object to use for each test call,
            defaults to None
        """
        self._session_stack = session_stack
        # if only one stack is given then session = test
        if not test_stack:
            test_stack = session_stack  # we treat session stack as a test stack
        self._test_stack = test_stack

    def push_context_onto_test(self, c_m: ContextManager[T]) -> T:
        """Set a contextmanager to call its exit parts at the end of a test.

        The entry part will be called immediately.
        If the object was initialised without any explicit test_stack given,
        it will uses the test_session. This means the teardown will only occur
        at the end of a session even though the client set it to happen at the end
        of a test.

        :param c_m: the contextmanager object.
        :return: The result of the first part executing on the context manager.
            Note the exit part will execute after the test function has exited.
        """
        return self._test_stack.enter_context(c_m)

    def push_context_onto_session(self, c_m: ContextManager[T]) -> T:
        """Place a context manager on the execution stack in a session wide scope.

        :param c_m: The context manager object.
        :return: The result of the first part executing on the context manager.
            Note if no test stack was given the session will still exit after test function ended.
        """
        return self._session_stack.enter_context(c_m)

    def set_test_stack(self, test_stack: ExitStack):
        """Set a function scoped test stack after initialisation.

        :param test_stack: A exit stack scoped test function wide.
        """
        self._test_stack = test_stack


class TelescopeContext(StackableContext, tel.TelState):
    """StackableContext for a telescope wide context.

    The class also inherits from TelState making it a type of object
    holding a Telescope State attribute that can be changed by the controller
    object.
    """

    def __init__(self, exitstack: ExitStack, settings: Union[ExecSettings, None] = None) -> None:
        """Initialise the object.

        After initialisation the telescope state will be set to `OFF`

        :param exitstack: The exit stack to use.
            If a function wide scoped is given, the telescope will
            perform tear down after each test, otherwise it will
            only perform tear down at the end of a test session.
        :param settings: execution settings that will be used "globally" in
            the context of this object, defaults to None (i.e default exec settings)
        """
        super().__init__(exitstack)
        self.existing_subarrays = IndexedDictionary()
        self.state = "OFF"
        self._global_settings = settings if settings else ExecSettings()
        self._disable_telescope_teardown: DisablePost = DisablePost()

    def clear_flags(self):
        """Clear any flags (e.g. disable teardown) that could have been set by a previous test."""
        self._disable_telescope_teardown.enable()
        self.state = "OFF"

    def set_exec_settings(self, settings: ExecSettings):
        """Set the telescope context with a given settings.

        :param settings: execution settings that will be used "globally" in
            the context of this object, defaults to None (i.e default exec settings)
        """
        self._global_settings = settings

    def set_down_a_telescope(self, settings: Union[ExecSettings, None] = None):
        """Switches a telescope OFF for the duration of the test, and ON afterwards.

        Note the telescope will return to the ON state after the context has finished.
        This can be either at the end of the test or the end of test session (if a session wide
        context manager was given).

        A successfull execution will result in the object's state turning to OFF

        :param settings: The executions settings to use during a call, defaults to a new
            ExecSettings object if none was given.
        """
        settings = settings if settings else self._global_settings
        self.push_context_onto_test(tel.standby_telescope(settings, self))

    def disable_automatic_setdown(self) -> None:
        """Disable the automatic set down of a telescope at the end of the test."""
        self._disable_telescope_teardown.disable()

    def set_up_a_telescope_per_session(self, settings: Union[ExecSettings, None] = None):
        """Switches a telescope ON for the duration of a test session, and OFF afterwards.

        Note the telescope will return to the OFF state after the context has finished.

        A successfull execution will result in the object's state turning to OFF.

        :param settings: The executions settings to use during a call, defaults to a new
            ExecSettings object if none was given.
        """
        settings = settings if settings else self._global_settings
        self.push_context_onto_session(
            tel.running_telescope(settings, self, self._disable_telescope_teardown)
        )

    def set_up_a_telescope(self, settings: Union[ExecSettings, None] = None):
        """Switches a telescope ON for the duration of the test, and OFF afterwards.

        A successfull execution will result in the object's state turning to OFF.

        :param settings: The executions settings to use during a call, defaults to a new
            ExecSettings object if none was given.
        """
        settings = settings if settings else self._global_settings
        self.push_context_onto_test(
            tel.running_telescope(settings, self, self._disable_telescope_teardown)
        )

    def switch_off_after_test(self, settings: Union[ExecSettings, None] = None):
        """Set the context so that the telescope will be switched off at the end of a test.

        :param settings: The executions settings to use during a call, defaults to a new
            ExecSettings object if none was given.
        """
        settings = settings if settings else self._global_settings
        self.push_context_onto_test(tel.tear_down_when_finished(settings, self))

    @contextmanager
    def wait_for_starting_up(self, settings: Union[ExecSettings, None] = None):
        """Return a context manager within which a telescope start up sequence can be performed.

        At the end of the context the object will block until it has verified the telescope is ON.
        The state attribute will also switch to ON at the end of context

        E.g:

        .. code-block:: python

            with telescope.wait_for_starting_up(settings):
                start_up()
            assert telescope.state is 'ON'

        :param settings: The executions settings to use during a call, defaults to a new
            ExecSettings object if none was given.
        :yields: None
        """
        settings = settings if settings else self._global_settings
        with tel.wait_for_starting_up(settings):
            yield
            self.state = "ON"

    @contextmanager
    def wait_for_shutting_down(self, settings: Union[ExecSettings, None] = None):
        """Return a context manager within which a telescope shut down sequence can be performed.

        At the end of the context the object will block until it has verified the telescope is OFF.
        The state attribute will also switch to OFF at the end of context

        E.g:

        .. code-block:: python

            with telescope.wait_for_starting_up(settings):
                start_up()
            assert telescope.state is 'OFF'

        :param settings: The executions settings to use during a call, defaults to a new
            ExecSettings object if none was given.
        :yields: None
        """
        settings = settings if settings else self._global_settings
        with tel.wait_for_shutting_down(settings):
            yield
            self.state = "OFF"

    @contextmanager
    def maintain_telescope_to_running(self, settings: Union[ExecSettings, None] = None):
        """Return a context manager within which a telescope is expected to maintained in `ON`.

        In other words at the end of the context the object will check and attempt to have the
        telescope in the ON state.

        E.g:

        .. code-block:: python

            with telescope.maintain_telescope_to_running(settings):
                switch_telescope_off()
            # will raise an exception telescope not NotReadyRunning as it wasn't `ON` afterwards

        :param settings: The executions settings to use during a call, defaults to a new
            ExecSettings object if none was given.
        :yields: None

        """
        settings = settings if settings else self._global_settings
        with tel.maintain_telescope_to_running(settings, self):
            yield

    @contextmanager
    def wait_for_allocating_a_subarray(
        self,
        subarray_id: int,
        receptors: Union[List[int], None] = None,
        settings: Union[ExecSettings, None] = None,
        release_when_finished: bool = True,
    ):
        """Return a context manager within which a new subarray can be composed (allocated).

        At the end of the context the object will block until it has verified the subarray
        is in the IDLE state.

        E.g:

        .. code-block:: python

            with telescope.wait_for_allocating_a_subarray(settings):
                entry_point.compose_subarray(
                    subarray_id, receptors, composition, sb_config.sbid
                )
            assert telescope.state is 'ON'

        :param subarray_id: the identification nr for the subarray
        :param receptors: the receptors that will be used for the subarray.
            (needed for releasing a subarray)
        :param release_when_finished: Wether  pytest should automatically release the
            subarray at the end of the test, defaults to True
        :param settings: The executions settings to use during a call, defaults to a new
            ExecSettings object if none was given.
        :yields: None
        """
        settings = settings if settings else self._global_settings
        teardown_settings = self._global_settings
        if release_when_finished:
            assert receptors, (
                "receptors can not be None when release_when_finished == True so as to be able to"
                "release automatically"
            )
            self.release_subarray_when_finished(subarray_id, receptors, teardown_settings)
        with wait_for_subarray_allocated(subarray_id, settings):
            yield
            teardown_settings.touch()

    def set_to_resourcing(
        self,
        subarray_id: int,
        receptors: Union[List[int], None] = None,
        sb_config: SBConfig = SBConfig(),
        settings: Union[ExecSettings, None] = None,
        composition: conf_types.Composition = conf_types.Composition(
            conf_types.CompositionType.STANDARD
        ),
    ) -> "SubarrayContext":
        """Create a subarray but block only until it is in RESOURCING.

        :param subarray_id: the identification nr for the subarray
        :param receptors: the receptors that will be used for the subarray.
            If none is given it will use a default set of two receptors 1 and 2.
        :param sb_config: The SB configuration to use as context, defaults to SBConfig()
        :param settings: the execution settings to use during the IO calls., defaults to
            ExecSettings()
        :param composition: The type of composition configuration to use.
            , defaults to conf_types.Composition( conf_types.CompositionType.STANDARD )
        :type composition: conf_types.Composition, optional
        :return: A subarray context manager to ue for subsequent commands.
        """
        settings = settings if settings else self._global_settings
        if receptors is None:
            receptors = [1, 2]

        subarray_context = SubarrayContext(
            self._test_stack,
            subarray_id,
            receptors,
            composition,
            sb_config,
            settings,
        )
        self.push_context_onto_test(
            allocating_subarray(
                receptors,
                subarray_id,
                composition,
                sb_config,
                self._global_settings,
                disable_post=subarray_context.disable_automatic_teardown_flag,
            )
        )
        self.existing_subarrays[subarray_id] = subarray_context
        return subarray_context

    def release_subarray_when_finished(
        self,
        subarray_id: int,
        receptors: List[int],
        settings: Union[ExecSettings, None] = None,
    ):
        """Set context so that subarray will be torn down at the end of the test.

        :param subarray_id: the identification nr for the subarray
        :param receptors: the receptors that will be used for the subarray.
        :param settings: the execution settings to use during the IO calls.
        """
        settings = settings if settings else self._global_settings
        self.push_context_onto_test(tear_down_when_finished(subarray_id, receptors, settings))

    def allocate_a_subarray(
        self,
        subarray_id: int,
        receptors: List[int] | None = None,
        sb_config: SBConfig = SBConfig(),
        settings: Union[ExecSettings, None] = None,
        composition: conf_types.Composition = conf_types.Composition(
            conf_types.CompositionType.STANDARD
        ),
    ) -> "SubarrayContext":
        """Allocate resources to a given subarray.

        The subarray will be torn down automatically at the end of the test.

        :param subarray_id: the identification nr for the subarray
        :param receptors: the receptors that will be used for the subarray.
            If none is given it will use a default set of two receptors 1 and 2.
        :param sb_config: The SB configuration to use as context, defaults to SBConfig()
        :param settings: the execution settings to use during the IO calls., defaults to
            ExecSettings()
        :param composition: The type of composition configuration to use.
            , defaults to conf_types.Composition( conf_types.CompositionType.STANDARD )
        :type composition: conf_types.Composition, optional
        :return: A subarray context manager to ue for subsequent commands.
        """
        settings = settings if settings else self._global_settings
        if receptors is None:
            receptors = [1, 2]

        subarray_context = SubarrayContext(
            self._test_stack,
            subarray_id,
            receptors,
            composition,
            sb_config,
            settings,
        )

        self.push_context_onto_test(
            allocated_subarray(
                receptors,
                subarray_id,
                composition,
                sb_config,
                self._global_settings,
                disable_post=subarray_context.disable_automatic_teardown_flag,
            )
        )

        self.existing_subarrays[subarray_id] = subarray_context
        return subarray_context


class SubarrayContext(StackableContext):
    """Manages the context for controlling a subarray."""

    def __init__(
        self,
        exitstack: ExitStack,
        subarray_id: int,
        receptors: List[int],
        composition: conf_types.Composition,
        sb_config: SBConfig,
        settings: Union[ExecSettings, None] = None,
    ) -> None:
        """Initialise the object.

        :param exitstack: The exit stack to use
            (should be inherited from the telescope context manager)
        :param subarray_id: the identification nr for the subarray
        :param receptors: the receptors that will be used for the subarray.
        :param composition: The type of composition configuration to use.
        :param sb_config: The SB configuration to use as context
        :param settings: execution settings that will be used "globally" in
            the context of this object, defaults to None (i.e default exec settings)
        """
        super().__init__(exitstack)

        self.id = subarray_id
        self.receptors = receptors
        self.composition = composition
        self.sb_config = sb_config
        self._exitstack = exitstack
        self._global_settings = settings if settings else ExecSettings()
        self.state = "IDLE"
        self.disable_automatic_teardown_flag: DisablePost = DisablePost()
        self.disable_automatic_clear_flag: DisablePost = DisablePost()
        self.nr_of_configures = 0
        self.nr_of_scans = 0

    def disable_automatic_teardown(self) -> None:
        """Prevents automic subarray teardown from occurring at the end of the test."""
        self.disable_automatic_teardown_flag.disable()

    def disable_automatic_clear(self) -> None:
        """Prevents automic subarray configuration clear from occurring at the end of the test."""
        self.disable_automatic_clear_flag.disable()

    def clear_configuration_when_finished(self, settings: Union[ExecSettings, None] = None):
        """Set the subarray to automatically clear scan configuration at the end of a test.

        :param settings: the execution settings to use during the IO calls.

        """
        settings = settings if settings else self._global_settings
        self.push_context_onto_test(clear_config_when_finished(self.id, self.receptors, settings))

    def configure(
        self,
        configuration: conf_types.ScanConfiguration = conf_types.ScanConfiguration(
            conf_types.ScanConfigurationType.STANDARD
        ),
        duration: float = 2.0,
        settings: Union[ExecSettings, None] = None,
    ) -> "SubarrayContext":
        """Configure the subarray for a given scan.

        The call will block until the subarray is configured and automically clear the
        configuration at the end.

        :param configuration: The scan configuration to use.
            , defaults to conf_types.ScanConfiguration( conf_types.ScanConfigurationType.STANDARD )
        :param duration: the scan duration, defaults to 2.0
        :param settings: the execution settings to use during the IO calls,
            defaults to ExecSettings()
        :returns: itself to allow for client code return the newly affected item in one statement.
        """
        if self.nr_of_configures > 0:
            disable_post = True
        else:
            disable_post = self.disable_automatic_clear_flag
        self.nr_of_configures += 1
        settings = settings if settings else self._global_settings
        self.push_context_onto_test(
            configured_subarray(
                self.id,
                self.receptors,
                configuration,
                self.sb_config,
                duration,
                settings,
                disable_post=disable_post,
            )
        )
        return self

    def set_to_configuring(
        self,
        configuration: conf_types.ScanConfiguration = conf_types.ScanConfiguration(
            conf_types.ScanConfigurationType.STANDARD
        ),
        duration: float = 2.0,
        settings: Union[ExecSettings, None] = None,
        clear_afterwards: bool = True,
    ):
        """Configure a subarray but return immediately after configure task has been dispatched.

        The test will automically wait for configuration process to complete at the end of the test.
        It will also clear configuration at the end of the test unless clear_afterwards have been
        explicitly set to False.

        E.g.:

        .. code-block:: python

            subarray.set_to_configuring()
            assert subarray.obsState is 'CONFIGURING'

        :param configuration: The scan configuration to use.
            , defaults to conf_types.ScanConfiguration( conf_types.ScanConfigurationType.STANDARD )
        :param duration: the scan duration, defaults to 2.0
        :param settings: the execution settings to use during the IO calls,
            defaults to ExecSettings()
        :param clear_afterwards: Whether to automatically clear the scan
            configuration afterwards, Note this will only be cleared once,
            defaults to True
        """
        if self.nr_of_configures > 0:
            # we only clear a configuration once
            clear_afterwards = False
        self.nr_of_configures += 1
        settings = settings if settings else self._global_settings
        self.push_context_onto_test(
            configuring_subarray(
                self.id,
                self.receptors,
                configuration,
                self.sb_config,
                duration,
                settings,
                clear_afterwards,
            )
        )

    def set_to_scanning(
        self,
        settings: Union[ExecSettings, None] = None,
        clean_up_after_scanning: bool = False,
    ):
        """Start a subarray scan job but return immediately after the subarray is in scanning.

        The subarray will automatically wait for scanning process to have been completed at the end
        of the test.
        If the clean_up_after_scanning flag have been set it will also check afterwards if the
        subarray returned to the correct state (and handle if not so).

        :param settings: the execution settings to use during the IO calls,
            defaults to ExecSettings()
        :param clean_up_after_scanning: Whether to check afterwards if the subarray is in the
            READY state and handle if it isn't the case, note this happens only once,
            defaults to False
        """
        if self.nr_of_scans > 0:
            # we only check after scans once
            clean_up_after_scanning = False
        self.nr_of_scans += 1
        settings = settings if settings else self._global_settings
        self.push_context_onto_test(
            scanning_subarray(self.id, self.receptors, settings, clean_up_after_scanning)
        )

    def scan(
        self,
        settings: Union[ExecSettings, None] = None,
        clean_up_after_scanning: bool = True,
    ):
        """Perform a predetermined configured scan on the subarray.

        The call will block until the scan has completed.

        :param settings: the execution settings to use during the IO calls,
            defaults to ExecSettings()
        :param clean_up_after_scanning: whether a cleanup after scanning should take
            place, defaults to True, note this only happens once
        """
        if self.nr_of_scans > 0:
            # we only check after scans once
            clean_up_after_scanning = False
        self.nr_of_scans += 1
        settings = settings if settings else self._global_settings
        self.push_context_onto_test(
            scan_subarray(self.id, self.receptors, settings, clean_up_after_scanning)
        )

    def reset_after_test(self, settings: Union[ExecSettings, None] = None):
        """Perform a subarray observation reset at the end of the test.

        This setting makes the implicit assumption that the subarray will be placed
        in a condition requiring its observation state to be cleared at the end of the test.
        Use this setting if you therefore aim to test paths that will result in the SUT in a
        abnormal condition.

        :param settings: the execution settings to use during the IO calls,
            defaults to ExecSettings()
        """
        settings = settings if settings else self._global_settings
        self.push_context_onto_test(reset_after(self.id, settings))

    def restart_after_test(self, settings: Union[ExecSettings, None] = None):
        """Perform a subarray reset at the end of the test.

        This setting makes the implicit assumption that the subarray will be placed
        in a condition requiring its observation state to be restarted at the end of the test.
        Use this setting if you therefore aim to test paths that will result in the SUT in a
        abnormal condition.

        :param settings: the execution settings to use during the IO calls,
            defaults to ExecSettings()
        """
        settings = settings if settings else self._global_settings
        self.push_context_onto_test(restart_after(self.id, settings))

    def check_configuration_when_finished(self, settings: Union[ExecSettings, None] = None):
        """Set the test to verify during teardown that the subarray configuration is intact.

        Note the the test will not perform automatic waiting for the scanning to complete
        and raise an error if the test was still busy scanning when the test finished.

        :param settings: the execution settings to use during the IO calls
        """
        settings = settings if settings else self._global_settings
        self.push_context_onto_test(
            check_configuration_when_finished(self.id, self.receptors, settings)
        )

    @contextmanager
    def wait_for_configuring_a_subarray(
        self,
        settings: Union[ExecSettings, None] = None,
        clear_when_finished: bool = True,
    ):
        """Return a context manager within which a new subarray can be composed (allocated).

        At the end of the context the object will block until it has verified the subarray
        is in the IDLE state.

        :param clear_when_finished: Wether  pytest should automatically clear the
            subarray configuration at the end of the test, defaults to True
        :param settings: The executions settings to use during a call, defaults to a new
            ExecSettings object if none was given.
        :yields: None
        """
        settings = settings if settings else self._global_settings
        release_settings = self._global_settings
        if self.nr_of_configures == 0:
            if clear_when_finished:
                self.clear_configuration_when_finished(release_settings)
        with wait_for_subarray_configured(self.id, self.receptors, settings):
            yield
            release_settings.touch()
            self.state = "READY"

    @contextmanager
    def wait_for_releasing_a_subarray(
        self,
        settings: Union[ExecSettings, None] = None,
    ):
        """Return a context manager within which the current subarray can be released.

        At the end of the context the object will block until it has verified the subarray
        is in the EMPTY state.

        :param settings: The executions settings to use during a call, defaults to a new
            ExecSettings object if none was given.
        :yields: None
        """
        settings = settings if settings else self._global_settings
        release_settings = self._global_settings
        with wait_for_subarray_released(self.id, settings):
            yield
            release_settings.touch()
            self.state = "EMPTY"
            self.disable_automatic_teardown()
