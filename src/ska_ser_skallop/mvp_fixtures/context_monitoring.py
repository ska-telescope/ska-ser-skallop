"""Module containing context monitoring using subscriptions on a messageboard."""

import logging
from contextlib import contextmanager
from threading import Event, Thread
from typing import List, Union

from ska_ser_skallop.event_handling.builders import (
    ForAttr,
    SetWaitingOn,
    get_new_message_board_builder,
    interject_builder,
)
from ska_ser_skallop.event_handling.logging import LogChecking
from ska_ser_skallop.mvp_control.event_waiting.wait import (
    EWhilstWaiting,
    concurrent_wait,
    wait,
    wait_for,
)
from ska_ser_skallop.mvp_fixtures.base import ExecSettings
from ska_ser_skallop.utils.generic_classes import Symbol

logger = logging.getLogger(__name__)


class ConcurrentMonitoring:
    """Object containing info about a concurrent task monitoring."""

    def __init__(self, thread: Thread, stop_signal: Event, completed_signal: Event) -> None:
        """Initialise object.

        :param thread: input thread representing monitoring task
        :param stop_signal: stop signal to use for stopping thread
        :param completed_signal: signal indicating there are no more subscription

        """
        self._thread = thread
        self._stop_signal = stop_signal
        self._completed_signal = completed_signal

    def stop(self):
        """Signal to the thread to stop."""
        self._stop_signal.set()

    def wait_until_complete(self, timeout: float = 10):
        """Wait until the thread task has completed.

        :param timeout: the maximum time in seconds to wait, defaults to 10
        """
        self._completed_signal.wait(timeout)
        self._thread.join(timeout)


class ContextMonitor:
    """class for setting up a context for subscriptions on a messageboard."""

    def __init__(self) -> None:
        """Initialise object."""
        self.builder = get_new_message_board_builder()
        self.logging_context: Union[None, LogChecking] = None
        self._set_waiting_on: Union[None, SetWaitingOn] = None
        self._for_attr: Union[None, ForAttr] = None
        self._wait_after_setting_builder = False

    def add_logging_context(self, logging_context: LogChecking):
        """Add a logging context so as to capture logs during the context.

        :param logging_context: The initialised Log checking object to use.
        """
        self.logging_context = logging_context

    def re_init_builder(self):
        """Get a new messageboard from which to build a specification from."""
        self.builder = get_new_message_board_builder()

    @contextmanager
    def observe_while_running(
        self,
        settings: Union[None, ExecSettings] = None,
    ):
        """Handle messages from incoming events in a background thread whilst running main thread.

        This mechanism does not block until all event handlers have been concluded but
        rather immediately stop subscriptions when the main thread has finished within
        the context.

        :yields: None
        :param settings: settings related to the waiting whilst running the main thread
            , defaults to None
        """
        poll_period = 0.5
        settings = settings if settings else ExecSettings()
        with self.context_monitoring():
            board = self.builder.setup_board()
            stop_signal = Event()
            empty_subscriptions_signal = Event()
            thread = Thread(
                target=concurrent_wait,
                args=[
                    board,
                    stop_signal,
                    poll_period,
                    empty_subscriptions_signal,
                    settings.log_enabled,
                    settings.recorder,
                ],
                daemon=True,
            )
            thread.start()
            try:
                yield ConcurrentMonitoring(thread, stop_signal, empty_subscriptions_signal)
            finally:
                stop_signal.set()

    @contextmanager
    def context_monitoring(self):
        """Set up context monitoring by interjecting builder with its own.

        :raises EWhilstWaiting: when a wait within context manager times out.
        :yields: None
        """
        # reset board so as not be left with archived subscriptions from previous waits
        self.builder.board.__init__()
        if self.logging_context:
            with self.logging_context.device_logging_context(self.builder):
                with interject_builder(self.builder):
                    try:
                        yield
                    except EWhilstWaiting as exception:
                        board = self.builder.board
                        logs = board.play_log_book()
                        logger.info(f"Log messages during waiting:\n{logs}")
                        raise exception
        else:
            with interject_builder(self.builder):
                try:
                    yield
                except EWhilstWaiting as exception:
                    board = self.builder.board
                    logs = board.play_log_book()
                    logger.info(f"Log messages during waiting:\n{logs}")
                    raise exception

    def set_waiting_on(self, device_name: Union[str, Symbol]) -> "ContextMonitor":
        """Generate a helper object upon which subsequent types of event waiting can be specified.

        The resulting object will contain a handle to the builder in order tor write back subsequent
        specifications.

        :param device_name: the qualified device name as a string

        :return: the object used to hold the device information and generate subsequent event
            waiting specifications on.
        """
        # use a new builder
        # self.builder = get_new_message_board_builder()
        self._set_waiting_on = self.builder.set_waiting_on(device_name)
        return self

    def wait_for(self, device_name: Union[str, Symbol]) -> "ContextMonitor":
        """Initiate a wait for a device attribute change event.

        The resulting object will contain a handle to the builder in order tor write back subsequent
        specifications.
        The waiting will start to block when the chain is ended off with a `to_become_equal_to`,
        `to_change`, `to_change_in_order` or `and_observe` command which must in turn be preceded by
        `for_attribute` command.

        :param device_name: the qualified device name as a string

        :return: the object used to hold the device information and generate subsequent event
            waiting specifications on.
        """
        # use a new builder
        # self.builder = get_new_message_board_builder()
        self._wait_after_setting_builder = True
        self._set_waiting_on = self.builder.set_waiting_on(device_name)
        return self

    def for_attribute(self, attr: str, polling: bool = False) -> "ContextMonitor":
        """
        Return an intermediate object containing device data and attributes.

        This will in turn then be used to generate a specification for
        subscriptions on a messageboard.

        :param attr: the attribute of the device to be waited on polling
            (bool, optional): whether the subscription on that device
            should be polled base or not. Defaults to False.
        :param polling: Wether to use polling for the subscription

        :return: itself so as to chain commands.
        """
        # ensure all attributes are send as lower case
        assert self._set_waiting_on, "You must first call set_waiting_on"
        self._for_attr = self._set_waiting_on.for_attribute(attr, polling)
        return self

    def _block_if_waited(self, settings: Union[None, ExecSettings] = None):
        if self._wait_after_setting_builder:
            self._wait_after_setting_builder = False
            settings = settings if settings else ExecSettings()
            board = self.builder.setup_board()
            wait(
                board,
                settings.time_out,
                settings.log_enabled,
                settings.attr_synching,
            )

    def to_become_equal_to(
        self,
        value: Union[str, List[str]],
        master: bool = False,
        ignore_first: bool = False,
        settings: Union[None, ExecSettings] = None,
    ) -> "ContextMonitor":
        """Add a specification for an event subscription on a messageboard.

        This will be based on the given device and attribute in which each event will be handled by
        a type of handler that waits for an event (i.e the value of the event) to be
        equal to the expected value.

        Note if this function was used in a chain in which the first method was 'wait_for'
        the method is interpreted as the end of a wait command and not just a set to wait command.
        In other words the call will result in a block until the necessary events has occurred.

        :param value: the value that is expected
        :param settings: Executions settings that must be used during the task, default to None
        :param master: whether the handling of these events should should
            affect all other subscriptions on the messageboard by unsubscribing all
            other auxillary subscriptions once the expected conditions have been met.
            Defaults to False.
        :param ignore_first: flag indicating whether first events should be
            disregarded, Defaults to False i.e. it will not ignore first events
        :return: itself so as to chian commands.
        """
        assert self._for_attr, "you must first call for_attribute"
        self._for_attr.to_become_equal_to(value, master, ignore_first)
        self._block_if_waited(settings)
        return self

    def to_change(
        self, master: bool = False, settings: Union[None, ExecSettings] = None
    ) -> "ContextMonitor":
        """Add a specification for an event subscription on a messageboard.

        This will be based on the given device and attribute in which each event will be handled by
        a type of handler that waits for events (i.e. the value of the event) to have changed from
        it's original state to a new one.

        :param master: whether the handling of these events should should
            affect all other subscriptions on the messageboard by unsubscribing all
            other auxillary subscriptions once the expected conditions have been met.
            Defaults to False.
        :param settings: Executions settings that must be used during the task, default to None
        :return: itself so as to chian commands.
        """
        assert self._for_attr, "you must first call for_attribute"
        self._for_attr.to_change(master)
        self._block_if_waited(settings)
        return self

    def to_change_in_order(
        self,
        order: list[str],
        master: bool = False,
        settings: Union[None, ExecSettings] = None,
    ) -> "ContextMonitor":
        """Add a specification for an event subscription on a messageboard.

        This will be based on the given device and attribute in which each event will be handled by
        a type of handler that waits for events (i.e. the value of the event) to occur
        in a specific order after which it will unsubscribe and end the waiting on the
        message board.

        :param order: the list of expected values (strings) in the provided order
        :param settings: Executions settings that must be used during the task, default to None
        :param master: whether the handling of these events should should
            affect all other subscriptions on the messageboard by unsubscribing all
            other auxillary subscriptions once the expected conditions have been met.
            Defaults to False.
        :return: itself so as to chian commands.
        """
        assert self._for_attr, "you must first call for_attribute"
        self._for_attr.to_change_in_order(order, master)
        self._block_if_waited(settings)
        return self

    def and_observe(
        self,
        settings: Union[None, ExecSettings] = None,
    ) -> "ContextMonitor":
        """Add a specification for an event subscription on a messageboard.

        This will be based on the given device and attribute in which each event will be handled by
        a type of handler that only records the values of the generated events but do
        not wait for a specific event to occur. It therefore rests on the assumption
        that some other event handler will be responsible for removing the subscription
        and therefore end the waiting for events.
        :param settings: Executions settings that must be used during the task, default to None
        :return: itself so as to chian commands.
        """
        assert self._for_attr, "you must first call for_attribute"
        self._for_attr.and_observe()
        self._block_if_waited(settings)
        return self

    @contextmanager
    def wait_before_complete(self, settings: ExecSettings):
        """Wait for a predefined set of events to occur before continuing.

        :param settings: Executions settings that must be used during the task.
        :yields: None

        """
        with wait_for(
            self.builder,
            settings.time_out,
            settings.log_enabled,
            attr_synching=settings.attr_synching,
            recorder=settings.recorder,
        ):
            yield
